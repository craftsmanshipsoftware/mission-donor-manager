include(common-widgets/common-widgets.pri)

INCLUDEPATH+= $$PWD

HEADERS+= $$PWD/login.h
SOURCES+= $$PWD/login.cpp

HEADERS+= $$PWD/about-screen.h
SOURCES+= $$PWD/about-screen.cpp

SOURCES+= $$PWD/tab-screen.cpp
HEADERS+= $$PWD/tab-screen.h

# main screen
HEADERS+= $$PWD/main-screen.h
SOURCES+= $$PWD/main-screen.cpp

# user input
HEADERS+= $$PWD/user-input.h
SOURCES+= $$PWD/user-input.cpp

# users
INCLUDEPATH+= $$PWD/users
HEADERS+= $$PWD/users/users-screen.h
SOURCES+= $$PWD/users/users-screen.cpp

HEADERS+= $$PWD/users/user-info-widget.h
SOURCES+= $$PWD/users/user-info-widget.cpp

HEADERS+= $$PWD/users/user-permissions-widget.h
SOURCES+= $$PWD/users/user-permissions-widget.cpp

HEADERS+= $$PWD/users/user-missions-widget.h
SOURCES+= $$PWD/users/user-missions-widget.cpp

# donors
INCLUDEPATH+= $$PWD/donors
HEADERS+= $$PWD/donors/donors-screen.h
SOURCES+= $$PWD/donors/donors-screen.cpp

HEADERS+= $$PWD/donors/donor-info-widget.h
SOURCES+= $$PWD/donors/donor-info-widget.cpp

HEADERS+= $$PWD/donors/donor-mail-widget.h
SOURCES+= $$PWD/donors/donor-mail-widget.cpp

HEADERS+= $$PWD/donors/donor-summary-widget.h
SOURCES+= $$PWD/donors/donor-summary-widget.cpp

HEADERS+= $$PWD/donors/donor-account-dm.h
SOURCES+= $$PWD/donors/donor-account-dm.cpp
HEADERS+= $$PWD/donors/donor-account-widget.h
SOURCES+= $$PWD/donors/donor-account-widget.cpp

HEADERS+= $$PWD/donors/donor-receipts-widget.h
SOURCES+= $$PWD/donors/donor-receipts-widget.cpp
HEADERS+= $$PWD/donors/donor-receipts-dm.h
SOURCES+= $$PWD/donors/donor-receipts-dm.cpp

# manual donations
INCLUDEPATH+= $$PWD/donations
HEADERS+= $$PWD/donations/donations-screen.h
SOURCES+= $$PWD/donations/donations-screen.cpp

HEADERS+= $$PWD/donations/limited-plain-text-edit.h
SOURCES+= $$PWD/donations/limited-plain-text-edit.cpp

# reocurring donations
INCLUDEPATH+= $$PWD/recurring
HEADERS+= $$PWD/recurring/recurring-screen.h
SOURCES+= $$PWD/recurring/recurring-screen.cpp

HEADERS+= $$PWD/recurring/recurring-selector-widget.h
SOURCES+= $$PWD/recurring/recurring-selector-widget.cpp

HEADERS+= $$PWD/recurring/recurring-donation-widget.h
SOURCES+= $$PWD/recurring/recurring-donation-widget.cpp

HEADERS+= $$PWD/recurring/recurring-donation-list-dm.h
SOURCES+= $$PWD/recurring/recurring-donation-list-dm.cpp

# iif export
INCLUDEPATH+= $$PWD/iif-export
HEADERS+= $$PWD/iif-export/iif-export-screen.h
SOURCES+= $$PWD/iif-export/iif-export-screen.cpp

HEADERS+= $$PWD/iif-export/iif-list-dm.h
SOURCES+= $$PWD/iif-export/iif-list-dm.cpp

HEADERS+= $$PWD/iif-export/iif-audit-list-dm.h
SOURCES+= $$PWD/iif-export/iif-audit-list-dm.cpp

HEADERS+= $$PWD/iif-export/iif-audit-widget.h
SOURCES+= $$PWD/iif-export/iif-audit-widget.cpp

HEADERS+= $$PWD/iif-export/iif-unmarked-dm.h
SOURCES+= $$PWD/iif-export/iif-unmarked-dm.cpp

# donor export
INCLUDEPATH+= $$PWD/donor-export
HEADERS+= $$PWD/donor-export/donor-export-screen.h
SOURCES+= $$PWD/donor-export/donor-export-screen.cpp

HEADERS+= $$PWD/donor-export/donor-export-general.h
SOURCES+= $$PWD/donor-export/donor-export-general.cpp

HEADERS+= $$PWD/donor-export/donor-export-new.h
SOURCES+= $$PWD/donor-export/donor-export-new.cpp

HEADERS+= $$PWD/donor-export/donor-export-new-dm.h
SOURCES+= $$PWD/donor-export/donor-export-new-dm.cpp

# setup
INCLUDEPATH+= $$PWD/setup
HEADERS+= $$PWD/setup/setup-screen.h
SOURCES+= $$PWD/setup/setup-screen.cpp

HEADERS+= $$PWD/setup/setup-general-widget.h
SOURCES+= $$PWD/setup/setup-general-widget.cpp

HEADERS+= $$PWD/setup/setup-income-accounts-widget.h
SOURCES+= $$PWD/setup/setup-income-accounts-widget.cpp

HEADERS+= $$PWD/setup/setup-income-accounts-dm.h
SOURCES+= $$PWD/setup/setup-income-accounts-dm.cpp

HEADERS+= $$PWD/setup/setup-campaigns-widget.h
SOURCES+= $$PWD/setup/setup-campaigns-widget.cpp

HEADERS+= $$PWD/setup/setup-advantage-accounts-widget.h
SOURCES+= $$PWD/setup/setup-advantage-accounts-widget.cpp

HEADERS+= $$PWD/setup/missions-setup-screen.h
SOURCES+= $$PWD/setup/missions-setup-screen.cpp

# mission view
INCLUDEPATH+= $$PWD/mission-view
HEADERS+= $$PWD/mission-view/mission-view-screen.h
SOURCES+= $$PWD/mission-view/mission-view-screen.cpp

HEADERS+= $$PWD/mission-view/mission-financials-dm.h
SOURCES+= $$PWD/mission-view/mission-financials-dm.cpp
HEADERS+= $$PWD/mission-view/mission-financials-widget.h
SOURCES+= $$PWD/mission-view/mission-financials-widget.cpp

HEADERS+= $$PWD/mission-view/mission-abstract-donors-dm.h
HEADERS+= $$PWD/mission-view/mission-donors-sum-list-dm.h
SOURCES+= $$PWD/mission-view/mission-donors-sum-list-dm.cpp
HEADERS+= $$PWD/mission-view/mission-donors-last-list-dm.h
SOURCES+= $$PWD/mission-view/mission-donors-last-list-dm.cpp
HEADERS+= $$PWD/mission-view/mission-donations-list-dm.h
SOURCES+= $$PWD/mission-view/mission-donations-list-dm.cpp
HEADERS+= $$PWD/mission-view/mission-donors-widget.h
SOURCES+= $$PWD/mission-view/mission-donors-widget.cpp
HEADERS+= $$PWD/mission-view/mission-donation-sort-proxy-model.h
android {
HEADERS+= $$PWD/mission-view/mission-donor-info-widget-mobile.h
SOURCES+= $$PWD/mission-view/mission-donor-info-widget-mobile.cpp
}

# tax receipts
INCLUDEPATH+= $$PWD/tax
HEADERS+= $$PWD/tax/tax-screen.h
SOURCES+= $$PWD/tax/tax-screen.cpp

HEADERS+= $$PWD/tax/tax-summary-widget.h
SOURCES+= $$PWD/tax/tax-summary-widget.cpp

HEADERS+= $$PWD/tax/tax-summary-totals-widget.h
SOURCES+= $$PWD/tax/tax-summary-totals-widget.cpp

HEADERS+= $$PWD/tax/tax-agency-list-dm.h
SOURCES+= $$PWD/tax/tax-agency-list-dm.cpp

HEADERS+= $$PWD/tax/tax-signator-widget.h
SOURCES+= $$PWD/tax/tax-signator-widget.cpp

HEADERS+= $$PWD/tax/tax-receipt-send-widget.h
SOURCES+= $$PWD/tax/tax-receipt-send-widget.cpp

HEADERS+= $$PWD/tax/tax-generate-receipts-widget.h
SOURCES+= $$PWD/tax/tax-generate-receipts-widget.cpp

HEADERS+= $$PWD/tax/tax-inkind-widget.h
SOURCES+= $$PWD/tax/tax-inkind-widget.cpp

HEADERS+= $$PWD/tax/tax-inkind-dm.h
SOURCES+= $$PWD/tax/tax-inkind-dm.cpp

# thank you
INCLUDEPATH+= $$PWD/thankyou
HEADERS+= $$PWD/thankyou/thankyou-screen.h
SOURCES+= $$PWD/thankyou/thankyou-screen.cpp

HEADERS+= $$PWD/thankyou/thankyou-common-widget.h
SOURCES+= $$PWD/thankyou/thankyou-common-widget.cpp

HEADERS+= $$PWD/thankyou/thankyou-missions-widget.h
SOURCES+= $$PWD/thankyou/thankyou-missions-widget.cpp

HEADERS+= $$PWD/thankyou/thankyou-template-edit-widget.h
SOURCES+= $$PWD/thankyou/thankyou-template-edit-widget.cpp

HEADERS+= $$PWD/thankyou/thankyou-letter-renderer.h
SOURCES+= $$PWD/thankyou/thankyou-letter-renderer.cpp

#reports
INCLUDEPATH+= $$PWD/reports
HEADERS+= $$PWD/reports/reports-screen.h
SOURCES+= $$PWD/reports/reports-screen.cpp

HEADERS+= $$PWD/reports/reports-donations-widget.h
SOURCES+= $$PWD/reports/reports-donations-widget.cpp

HEADERS+= $$PWD/reports/reports-stats-widget.h
SOURCES+= $$PWD/reports/reports-stats-widget.cpp

HEADERS+= $$PWD/reports/reports-monthly-dm.h
SOURCES+= $$PWD/reports/reports-monthly-dm.cpp

HEADERS+= $$PWD/reports/reports-monthly-widget.h
SOURCES+= $$PWD/reports/reports-monthly-widget.cpp
