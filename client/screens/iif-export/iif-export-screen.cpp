// clang-format off
/**************************************************************************************************
	Copyright (C) 2023-2024  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QScreen>
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QRadioButton>
#include <QGroupBox>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QTableView>
#include <QHeaderView>
#include <QCalendarWidget>
#include <QFileDialog>
#include <QFile>
#include <QKeyEvent>
#include "qslidingstackedwidget.h"
#include "main-window.h"
#include "connection-manager.h"
#include "user-input.h"
#include "common.h"
#include "styler.h"
#include "theme.h"
#include "source-account-selector-widget.h"
#include "copy-and-paste.h"
#include "iif-audit-widget.h"
#include "iif-export-screen.h"

IifExportScreen::IifExportScreen(MainWindow &mainWindow) : mainWindow(mainWindow), cm(mainWindow.connectionManager)
{
	// the only gui objects that are owned bt IifExportScreen is the QSlidingStackedWidget.  (these six lines)
	// The other gui objects created are owned by mainWidget (which is owned by the QSlidingStackedWidget)
	QVBoxLayout *layoutThatHoldsStackedWidget = new QVBoxLayout;
	setLayout(layoutThatHoldsStackedWidget);
	int m = Theme::OuterMargin * qMin(screen()->geometry().width(), screen()->geometry().height());
	layoutThatHoldsStackedWidget->setContentsMargins(m, m, m, m);
	content = new QSlidingStackedWidget();
	content->setDuration(500);
	layoutThatHoldsStackedWidget->addWidget(content);

	QWidget *mainWidget = new QWidget;
	QVBoxLayout *layout = new QVBoxLayout;
	mainWidget->setLayout(layout);
	content->addWidget(mainWidget);

	auditview = new IifAuditWidget;
	content->addWidget(auditview);

	QHBoxLayout *topLayout = new QHBoxLayout;
	layout->addLayout(topLayout);
	QHBoxLayout *bottomLayout = new QHBoxLayout;
	layout->addLayout(bottomLayout);

	topLayout->addStretch(1);
	QLabel *title = new QLabel(tr("IIF Export"));
	Styler::styleLabel(title, Theme::H1Size);
	topLayout->addWidget(title);
	topLayout->addStretch(1);

	// bottom layout
	bottomLayout->addStretch(1);

	QVBoxLayout *ungroupedLayout = new QVBoxLayout;
	bottomLayout->addLayout(ungroupedLayout);

	title = new QLabel(tr("Ungrouped Transactions"));
	Styler::styleLabel(title, Theme::H1Size);
	ungroupedLayout->addWidget(title);
	ungroupedLayout->setAlignment(title, Qt::AlignHCenter);

	title = new QLabel(tr("Ungrouped Donations in"));
	Styler::styleLabel(title);
	ungroupedLayout->addWidget(title);
	sourceAccountSelector2 = new SourceAccountSelectorWidget;
	Styler::styleComboBox(sourceAccountSelector2);
	ungroupedLayout->addWidget(sourceAccountSelector2);

	ungroupedView = new QTableView;
	Styler::styleTableView(ungroupedView);
	ungroupedView->verticalHeader()->hide();
	ungroupedView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	ungroupedView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
	ungroupedView->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
	ungroupedView->setAlternatingRowColors(true);
	ungroupedLayout->addWidget(ungroupedView);
	unmarkedSortFilter.setSourceModel(&iifUnmarkedDataModel);
	unmarkedSortFilter.sort(0, Qt::AscendingOrder);
	ungroupedView->setModel(&unmarkedSortFilter);
	ungroupedView->hide();
	noTransactionsLabel = new QLabel(tr("No Transactions"));
	Styler::styleLabel(noTransactionsLabel);
	ungroupedLayout->addWidget(noTransactionsLabel);

	ungroupedLayout->addStretch(1);

	QHBoxLayout *totalsLayout = new QHBoxLayout;
	ungroupedLayout->addLayout(totalsLayout);
	title = new QLabel(tr("Gross Total:"));
	Styler::styleLabel(title);
	totalsLayout->addWidget(title);
	gross = new QLineEdit;
	Styler::styleLineEdit(gross);
	totalsLayout->addWidget(gross);
	totalsLayout->addStretch(1);
	title = new QLabel(tr("Net Total:"));
	Styler::styleLabel(title);
	totalsLayout->addWidget(title);
	net = new QLineEdit;
	Styler::styleLineEdit(net);
	totalsLayout->addWidget(net);

	bottomLayout->addStretch(1);
	QVBoxLayout *setupLayout = new QVBoxLayout;
	setupLayout->addStretch(3);
	bottomLayout->addLayout(setupLayout);
	//bottomLayout->setStretchFactor(setupLayout, 2);

	title = new QLabel(tr("Group Transactions"));
	Styler::styleLabel(title, Theme::H1Size);
	setupLayout->addWidget(title);
	setupLayout->setAlignment(title, Qt::AlignHCenter);

	setupLayout->addStretch(1);

	QGroupBox *saGroup = new QGroupBox(tr("Choose Income Account for Grouping"));
	Styler::styleGroupBox(saGroup);
	setupLayout->addWidget(saGroup);
	QVBoxLayout *saLayout = new QVBoxLayout;
	saGroup->setLayout(saLayout);
	saAll = new QRadioButton(tr("All"));
	Styler::styleRadioButton(saAll);
	saLayout->addWidget(saAll);
	saLimit = new QRadioButton(tr("Limit to"));
	Styler::styleRadioButton(saLimit);
	saLayout->addWidget(saLimit);
	sourceAccountSelector = new SourceAccountSelectorWidget;
	Styler::styleComboBox(sourceAccountSelector);
	saLayout->addWidget(sourceAccountSelector);

	setupLayout->addStretch(1);

	fromCalendar = new QCalendarWidget;
	setupLayout->addWidget(fromCalendar);

	setupLayout->addStretch(1);

	calendar = new QCalendarWidget;
	setupLayout->addWidget(calendar);

	setupLayout->addStretch(3);

	QHBoxLayout *editButtonLayout = new QHBoxLayout;
	setupLayout->addLayout(editButtonLayout);
	editButton = new QPushButton(tr("Edit"));
	Styler::styleButton(editButton);
	editButton->setEnabled(true);
	editButtonLayout->addWidget(editButton);
	cancelButton = new QPushButton(tr("Cancel"));
	Styler::styleButton(cancelButton);
	cancelButton->setEnabled(false);
	editButtonLayout->addWidget(cancelButton);
	groupButton = new QPushButton(tr("Group"));
	Styler::styleButton(groupButton);
	groupButton->setEnabled(false);
	editButtonLayout->addWidget(groupButton);

	ungroupButton = new QPushButton(tr("Ungroup Last"));
	Styler::styleButton(ungroupButton);
	ungroupButton->setEnabled(false);
	editButtonLayout->addWidget(ungroupButton);

	bottomLayout->addStretch(1);

	QVBoxLayout *selectorLayout = new QVBoxLayout;
	bottomLayout->addLayout(selectorLayout);

	exportView = new QTableView;
	Styler::styleTableView(exportView);
	exportView->verticalHeader()->hide();
	exportView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	exportView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
	exportView->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
	exportView->setSelectionBehavior(QAbstractItemView::SelectRows);
	exportView->setSelectionMode(QAbstractItemView::SingleSelection);
	exportView->setAlternatingRowColors(true);
	selectorLayout->addWidget(exportView);
	exportSortFilter.setSourceModel(&iifListDataModel);
	exportSortFilter.sort(0, Qt::DescendingOrder);
	exportView->setModel(&exportSortFilter);

	downloadButton = new QPushButton(tr("Download"));
	Styler::styleButton(downloadButton);
	selectorLayout->addWidget(downloadButton);
	selectorLayout->setAlignment(downloadButton, Qt::AlignHCenter);

	auditButton = new QPushButton(tr(" Audit "));
	Styler::styleButton(auditButton);
	selectorLayout->addWidget(auditButton);
	selectorLayout->setAlignment(auditButton, Qt::AlignHCenter);

	proxyButton = new QPushButton(tr("Proxies"));
	Styler::styleButton(proxyButton);
	selectorLayout->addWidget(proxyButton);
	selectorLayout->setAlignment(proxyButton, Qt::AlignHCenter);

	bottomLayout->addStretch(1);

	enableForms(false);
	connect(downloadButton, &QPushButton::clicked, this, &IifExportScreen::downloadClicked);
	connect(auditButton, &QPushButton::clicked, this, &IifExportScreen::auditClicked);
	connect(proxyButton, &QPushButton::clicked, this, &IifExportScreen::proxyClicked);
	connect(editButton, &QPushButton::clicked, this, &IifExportScreen::editClicked);
	connect(cancelButton, &QPushButton::clicked, this, &IifExportScreen::doneEdits);
	connect(groupButton, &QPushButton::clicked, this, &IifExportScreen::groupClicked);
	connect(ungroupButton, &QPushButton::clicked, this, &IifExportScreen::ungroupClicked);
	connect(saAll, &QRadioButton::clicked, this, &IifExportScreen::saAllClicked);
	connect(saLimit, &QRadioButton::clicked, this, &IifExportScreen::saLimitClicked);
	connect(auditview, &IifAuditWidget::backClicked, this, &IifExportScreen::backClicked);
	connect(sourceAccountSelector2, qOverload<int>(&SourceAccountSelectorWidget::currentIndexChanged), this, &IifExportScreen::ungroupedIncomeAccountChanged);

	topicsToReceive << "iif" << "income-accounts.list-enabled" << "donations.md.income-accounts-updated";
	cm.registerApiForReceiver(this);
	cm.foxmq.subscription("donations.md.income-accounts-updated", true);
}

void IifExportScreen::subscribe()
{
	cm.foxmq.subscription("iif.exports", true);
	cm.foxmq.subscription("income-accounts.list-enabled", true);
	if (updateUngroupedListWhenTabSelected) {  // refresh ungrouped list when switching back to iif screen after parent tab screen's recentlySelected has timed out.
		updateUngroupedListWhenTabSelected = false;
		int incomeAccountId = sourceAccountSelector2->accountIdAtCurrentIndex();
		cm.foxmq.post("iif.unmarked", FoxHead::ContentType::IntAsString, QByteArray::number(incomeAccountId));
	}
}

void IifExportScreen::unsubscribe()
{
	cm.foxmq.subscription("iif.exports", false);
	cm.foxmq.subscription("income-accounts.list-enabled", false);
}

void IifExportScreen::processPublish(const QByteArray &topic, ssize_t, FoxID, FoxHead::ContentType, const QByteArray &content)
{
	QJsonArray jArray = QJsonDocument::fromJson(content).array();
	if (topic == "iif.exports") {
		iifListDataModel.layoutAboutToBeChanged();
		iifListDataModel.clear();
		for (auto jDonor : jArray)
			iifListDataModel.append(jDonor.toObject());
		iifListDataModel.layoutChanged();
		exportView->resizeColumnsToContents();
	} else if (topic == "income-accounts.list-enabled") {
		sourceAccountSelector->update(jArray);
		sourceAccountSelector2->update(jArray);
	} else if (topic == "donations.md.income-accounts-updated") {
		int activeIncomeAccount = sourceAccountSelector2->accountIdAtCurrentIndex();
		for (int i = 0; i < jArray.size(); i++) {
			if (jArray.at(i).toInt() == activeIncomeAccount) {
				if (recentlySelected)
					cm.foxmq.post("iif.unmarked", FoxHead::ContentType::IntAsString, QByteArray::number(activeIncomeAccount));
				else
					updateUngroupedListWhenTabSelected = true;
				break;
			}
		}
	}
}

void IifExportScreen::processPostResponce(const QByteArray &topic, FoxID, FoxHead::ContentType, const QByteArray &content)
{
	if (topic == "iif.download") {
		iifToSave = content;
		saveFileDialog = QSharedPointer<QFileDialog>(new QFileDialog(nullptr, "Save File", exportFileName, "iif (*.iif)"), &QObject::deleteLater);
		saveFileDialog->setAcceptMode(QFileDialog::AcceptSave);
		connect(saveFileDialog.data(), &QFileDialog::fileSelected, this, &IifExportScreen::saveFileSelected);
		connect(saveFileDialog.data(), &QFileDialog::finished, this, &IifExportScreen::saveFileSelectorFinished);
		saveFileDialog->show();
		mainWindow.setEnabled(false);
	} else if (topic == "iif.mark") {
		connect(mainWindow.userInput, &UserInput::button1Clicked, this, &IifExportScreen::markConfirmCleanUp);
		mainWindow.userInput->askForInput(QString(tr("%1 Transactions added")).arg(content.toUInt()), tr("OK"));
		mainWindow.showUserInput();
	} else if (topic == "iif.audit" || topic == "iif.proxy") {
		QJsonDocument jDoc = QJsonDocument::fromJson(content);
		QJsonObject root = jDoc.object();
		auditview->setTotals(root.value("total-gross").toInt(), root.value("total-fees").toInt());
		QJsonArray jArray = root.value("transactions").toArray();
		auditview->populateValues(jArray, topic == "iif.proxy");
	} else if (topic == "iif.unmarked") {
		QJsonObject root = QJsonDocument::fromJson(content).object();
		iifUnmarkedDataModel.layoutAboutToBeChanged();
		QJsonArray jDonations = root.value("donations").toArray();
		iifUnmarkedDataModel.update(jDonations);
		iifUnmarkedDataModel.layoutChanged();
		ungroupedView->resizeColumnsToContents();
		if (jDonations.size()) {
			gross->setText(convertCentsToString(root.value("gross-total").toInt(), true));
			net->setText(convertCentsToString(root.value("net-total").toInt(), true));
			ungroupedView->show();
			noTransactionsLabel->hide();
		} else {
			gross->setText(convertCentsToString(0, true));
			net->setText(convertCentsToString(0, true));
			ungroupedView->hide();
			noTransactionsLabel->show();
		}
	}
}

void IifExportScreen::markConfirmCleanUp()
{
	disconnect(mainWindow.userInput, &UserInput::button1Clicked, this, &IifExportScreen::markConfirmCleanUp);
	mainWindow.showMainScreen();
}

void IifExportScreen::processAck(const QByteArray &topic)
{
	if (topic == "iif.lock") {
		enableForms(true);
		editButton->setEnabled(false);
		cancelButton->setEnabled(true);
		groupButton->setEnabled(true);
		ungroupButton->setEnabled(true);
		saAll->setChecked(true);
	}
}

void IifExportScreen::processNack(const QByteArray &topic)
{
	if (topic == "iif.lock") {
		connect(mainWindow.userInput, &UserInput::button1Clicked, this, &IifExportScreen::okFromLockNack);
		mainWindow.userInput->askForInput(tr("Another user is using iif export, try again later"));
		mainWindow.showUserInput();
	}
}

void IifExportScreen::editClicked()
{
	cm.foxmq.post("iif.lock", FoxHead::ContentType::Empty, "");
}

void IifExportScreen::groupClicked()
{
	QJsonDocument jDoc;
	QJsonObject json;
	json.insert("start-day", fromCalendar->selectedDate().toJulianDay());
	json.insert("end-day", calendar->selectedDate().toJulianDay());
	int sa;
	if (saAll->isChecked())
		sa = -1;
	else
		sa = sourceAccountSelector->accountIdAtCurrentIndex();
	json.insert("source-account", sa);
	jDoc.setObject(json);
	cm.foxmq.post("iif.mark", FoxHead::ContentType::Json, jDoc.toJson(QJsonDocument::Compact));
	doneEdits();
}

void IifExportScreen::downloadClicked()
{
	QModelIndexList selection = exportView->selectionModel()->selectedRows();
	if (selection.count() == 1) {
		QModelIndex index = selection.at(0);
		int row = exportSortFilter.mapToSource(index).row();
		int id = iifListDataModel.getID(row);
		exportFileName = iifListDataModel.getFileName(row);
		cm.foxmq.post("iif.download", FoxHead::ContentType::IntAsString, QByteArray::number(id));
	}
}

void IifExportScreen::auditClicked()
{
	sendAuditRequest("iif.audit");
}

void IifExportScreen::proxyClicked()
{
	sendAuditRequest("iif.proxy");
}

void IifExportScreen::sendAuditRequest(const char *topic)
{
	QModelIndexList selection = exportView->selectionModel()->selectedRows();
	if (selection.count() == 1) {
		QModelIndex index = selection.at(0);
		int row = exportSortFilter.mapToSource(index).row();
		int id = iifListDataModel.getID(row);
		cm.foxmq.post(topic, FoxHead::ContentType::IntAsString, QByteArray::number(id));
		content->setCurrentIndex(1);
	}
}

void IifExportScreen::doneEdits()
{
	groupButton->setEnabled(false);
	ungroupButton->setEnabled(false);
	enableForms(false);
	cancelButton->setEnabled(false);
	editButton->setEnabled(true);
	cm.foxmq.post("iif.unlock", FoxHead::ContentType::Empty, "");
}

void IifExportScreen::okFromLockNack()
{
	disconnect(mainWindow.userInput, &UserInput::button1Clicked, this, &IifExportScreen::okFromLockNack);
	mainWindow.showMainScreen();
}

void IifExportScreen::enableForms(bool enabled)
{
	if (!enabled)
		sourceAccountSelector->setEnabled(false);
	fromCalendar->setEnabled(enabled);
	calendar->setEnabled(enabled);
	saAll->setEnabled(enabled);
	saLimit->setEnabled(enabled);
}

void IifExportScreen::saAllClicked()
{
	sourceAccountSelector->setEnabled(false);
}

void IifExportScreen::saLimitClicked()
{
	sourceAccountSelector->setEnabled(true);
}

void IifExportScreen::backClicked()
{
	content->setCurrentIndex(0);
}

void IifExportScreen::saveFileSelected(QString fn)
{
	QFile file(fn);
	if (file.open(QIODevice::WriteOnly)) {
		file.write(iifToSave);
		file.close();
	}
	iifToSave.clear();
}

void IifExportScreen::saveFileSelectorFinished()
{
	mainWindow.setEnabled(true);
	saveFileDialog.clear();
}

void IifExportScreen::ungroupedIncomeAccountChanged(int)
{
	int incomeAccountId = sourceAccountSelector2->accountIdAtCurrentIndex();
	cm.foxmq.post("iif.unmarked", FoxHead::ContentType::IntAsString, QByteArray::number(incomeAccountId));
}

void IifExportScreen::keyPressEvent(QKeyEvent *e)
{
	if (e->type() == QKeyEvent::KeyPress && e->matches(QKeySequence::Copy))
		copyFromTable(ungroupedView->selectionModel()->selectedIndexes());
}

void IifExportScreen::ungroupClicked()
{
	QString msg;
	msg = QString(tr("Are you sure you want to Ungroup the last Grouping?"));
	connect(mainWindow.userInput, &UserInput::button1Clicked, this, &IifExportScreen::ungroupConfirmed);
	connect(mainWindow.userInput, &UserInput::button2Clicked, this, &IifExportScreen::ungroupCancelled);
	mainWindow.userInput->askForInput(msg, tr("Yes"), tr("No"));
	mainWindow.showUserInput();
}

void IifExportScreen::ungroupConfirmed()
{
	cm.foxmq.post("iif.ungroup-last", FoxHead::ContentType::Empty, "");
	ungroupCancelled();
}

void IifExportScreen::ungroupCancelled()
{
	disconnect(mainWindow.userInput, &UserInput::button1Clicked, this, &IifExportScreen::ungroupConfirmed);
	disconnect(mainWindow.userInput, &UserInput::button2Clicked, this, &IifExportScreen::ungroupCancelled);
	mainWindow.showMainScreen();
	doneEdits();
}
