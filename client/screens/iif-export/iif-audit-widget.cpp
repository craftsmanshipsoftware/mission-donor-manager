// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QApplication>
#include <QClipboard>
#include <QVBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QTableView>
#include <QHeaderView>
#include <QJsonArray>
#include <QJsonObject>
#include "styler.h"
#include "theme.h"
#include "qcolorablesvg.h"
#include "../common/common.h"
#include "iif-audit-widget.h"

IifAuditWidget::IifAuditWidget()
{
	QVBoxLayout *layout = new QVBoxLayout;
	setLayout(layout);

	QHBoxLayout *topButtonLayout = new QHBoxLayout;
	layout->addLayout(topButtonLayout);

#ifdef Q_OS_WIN
	QIcon backIcon(QColorableSvg::color(QString(":/back.svg"), 32, 32, Qt::black));
#else
	QIcon backIcon(":/back.svg");
#endif
	QPushButton *button = new QPushButton(backIcon, "");
	button->setIconSize(QSize(32, 32));
	button->setFocusPolicy(Qt::NoFocus);
	button->setStyleSheet("QPushButton { background-color: rgba(255, 255, 255, 0); border: 0px;}");
	topButtonLayout->addWidget(button);
	connect(button, &QPushButton::clicked, this, &IifAuditWidget::backClicked);

	topButtonLayout->addStretch(1);
	QLabel *title = new QLabel(tr("Iif Export Audit"));
	Styler::styleLabel(title, Theme::H1Size);
	topButtonLayout->addWidget(title);
	topButtonLayout->addStretch(1);

	button = new QPushButton(tr("Copy to Clipboard"));
	Styler::styleButton(button);
	topButtonLayout->addWidget(button);
	connect(button, &QPushButton::clicked, this, &IifAuditWidget::copyToClipboardClicked);

	QHBoxLayout *viewLayout = new QHBoxLayout;
	layout->addLayout(viewLayout);
	viewLayout->addStretch(1);

	view = new QTableView;
	Styler::styleTableView(view);
	view->verticalHeader()->hide();
	view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
	view->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
	view->setAlternatingRowColors(true);
	view->setSelectionMode(QAbstractItemView::NoSelection);
	viewLayout->addWidget(view);
	view->setModel(&dm);
	view->setColumnHidden(17, true);
	view->setColumnHidden(18, true);
	view->setColumnHidden(19, true);
	view->setColumnHidden(20, true);
	view->setColumnHidden(21, true);
	view->setColumnHidden(22, true);
	viewLayout->addStretch(1);

	QHBoxLayout *feeLayout = new QHBoxLayout;
	layout->addLayout(feeLayout);
	feeLayout->addStretch(1);

	QLabel *totalGrossLable = new QLabel(tr("Gross Total:"));
	Styler::styleLabel(totalGrossLable);
	feeLayout->addWidget(totalGrossLable);
	totalGrossEdit = new QLineEdit;
	totalGrossEdit->setReadOnly(true);
	Styler::styleLineEdit(totalGrossEdit);
	feeLayout->addWidget(totalGrossEdit);

	QLabel *totalFeeLable = new QLabel(tr("Total Fees:"));
	Styler::styleLabel(totalFeeLable);
	feeLayout->addWidget(totalFeeLable);
	totalFeesEdit = new QLineEdit;
	totalFeesEdit->setReadOnly(true);
	Styler::styleLineEdit(totalFeesEdit);
	feeLayout->addWidget(totalFeesEdit);

	layout->addStretch(1);
}

void IifAuditWidget::populateValues(QJsonArray jArray, bool proxyView)
{
	dm.isProxies = proxyView;
	dm.layoutAboutToBeChanged();
	dm.clear();
	for (auto jIt : jArray)
		dm.append(jIt.toObject());
	dm.layoutChanged();
	view->resizeColumnsToContents();
}

void IifAuditWidget::copyToClipboardClicked()
{
	QString clipboardText;

	// first add header
	for (int column=0; column< dm.columnCount(); column++) {
		if (column)
			clipboardText+= '\t';  // don't add tab before first column
		clipboardText+= dm.headerData(column, Qt::Horizontal, Qt::DisplayRole).toString();
	}
	clipboardText+= '\n';

	// then add data
	for (int row=0; row < dm.rowCount(); row++) {
		for (int column=0; column< dm.columnCount(); column++) {
			if (column)
				clipboardText+= '\t';  // don't add tab before first column
			clipboardText+= dm.index(row, column).data().toString();
		}
		clipboardText+= '\n';
	}
	QApplication::clipboard()->setText(clipboardText);
}

void IifAuditWidget::setTotals(uint totalGross, uint totalFees)
{
	totalGrossEdit->setText(convertCentsToString(totalGross, true));
	totalFeesEdit->setText(convertCentsToString(totalFees, true));
}
