// clang-format off
/**************************************************************************************************
	Copyright (C) 2023-2024  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef MDM_IIF_EXPORT_SCREEN_H_
#define MDM_IIF_EXPORT_SCREEN_H_

#include <QVBoxLayout>
#include <QSortFilterProxyModel>
#include "tab-screen.h"
#include "receiver.h"
#include "iif-list-dm.h"
#include "iif-unmarked-dm.h"

class MainWindow;
class ConnectionManager;
class QSlidingStackedWidget;
class IifAuditWidget;
class QPushButton;
class QLabel;
class QLineEdit;
class QRadioButton;
class QCalendarWidget;
class QTableView;
class QFileDialog;
class SourceAccountSelectorWidget;

class IifExportScreen : public TabScreen, public Receiver
{
	Q_OBJECT

public:
	IifExportScreen(MainWindow &mainWindow);
	void subscribe() override;
	void unsubscribe() override;

private:
	void processPublish(const QByteArray &topic, ssize_t count, FoxID id, FoxHead::ContentType contentType, const QByteArray &content) override;
	void processPostResponce(const QByteArray &topic, FoxID id, FoxHead::ContentType contentType, const QByteArray &content) override;
	void markConfirmCleanUp();
	void processAck(const QByteArray &topic) override;
	void processNack(const QByteArray &topic) override;
	void groupClicked();
	void ungroupClicked();
	void ungroupConfirmed();
	void ungroupCancelled();
	void downloadClicked();
	void auditClicked();
	void proxyClicked();
	void sendAuditRequest(const char *topic);
	void editClicked();
	void doneEdits();
	void okFromLockNack();
	void enableForms(bool enabled);
	void saAllClicked();
	void saLimitClicked();
	void backClicked();
	void copyToClipboardClicked();
	void saveFileSelected(QString fn);
	void saveFileSelectorFinished();
	void ungroupedIncomeAccountChanged(int index);
	void keyPressEvent(QKeyEvent *e) override;
	MainWindow &mainWindow;
	ConnectionManager &cm;
	QSlidingStackedWidget *content;
	IifAuditWidget *auditview;
	QTableView *exportView;
	QTableView *ungroupedView;
	QPushButton *downloadButton;
	QPushButton *auditButton;
	QPushButton *proxyButton;
	QPushButton *editButton;
	QPushButton *cancelButton;
	QPushButton *groupButton;
	QPushButton *ungroupButton;
	QCalendarWidget *fromCalendar;
	QCalendarWidget *calendar;
	QSortFilterProxyModel exportSortFilter;
	QSortFilterProxyModel unmarkedSortFilter;
	IifListDataModel iifListDataModel;
	IifUnmarkedDataModel iifUnmarkedDataModel;
	QString exportFileName;
	QRadioButton *saAll;
	QRadioButton *saLimit;
	SourceAccountSelectorWidget *sourceAccountSelector;
	SourceAccountSelectorWidget *sourceAccountSelector2;
	QSharedPointer<QFileDialog> saveFileDialog;
	QByteArray iifToSave;
	QLabel *noTransactionsLabel;
	QLineEdit *gross;
	QLineEdit *net;
	bool updateUngroupedListWhenTabSelected = false;
};

#endif
