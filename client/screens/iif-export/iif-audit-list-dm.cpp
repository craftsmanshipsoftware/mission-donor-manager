// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QJsonObject>
#include "../common/common.h"
#include "iif-audit-list-dm.h"

int IifAuditListDataModel::rowCount(const QModelIndex &) const
{
	return iifTransactionList.size();
}

int IifAuditListDataModel::columnCount(const QModelIndex &) const
{
	return 23;
}

QVariant IifAuditListDataModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	QVariant text;

	if (role != Qt::DisplayRole)
		return QVariant();

	if (orientation == Qt::Horizontal) {
		switch(section) {  // clang format off
			case 0: text = QString(tr("Date")); break;
			case 1: text = QString(tr("Income Account")); break;
			case 2: text = QString(tr("Income Account #")); break;
			case 3: text = QString(tr("Mission")); break;
			case 4:
				if (isProxies)
					text = QString(tr("Account #"));
				else
					text = QString(tr("QB Account #"));
				break;
			case 5: text = QString(tr("Donor Name")); break;
			case 6: text = QString(tr("Donor Surname")); break;
			case 7: text = QString(tr("Donor Id #")); break;
			case 8: text = QString(tr("DID")); break;
			case 9: text = QString(tr("Source")); break;
			case 10: text = QString(tr("Type")); break;
			case 11: text = QString(tr("Gross")); break;
			case 12: text = QString(tr("Net")); break;
			case 13: text = QString(tr("Operations")); break;
			case 14: text = QString(tr("%")); break;
			case 15: text = QString(tr("Fee")); break;
			case 16: text = QString(tr("Anon.")); break;
			case 17: text = QString(tr("NEFTD")); break;  // this is hidden, only used for copy and paste
			case 18: text = QString(tr("Campaign")); break;  // this is hidden, only used for copy and paste
			case 19: text = QString(tr("Advantage")); break;  // this is hidden, only used for copy and paste
			case 20: text = QString(tr("Advantage Account")); break;  // this is hidden, only used for copy and paste
			case 21: text = QString(tr("Advantage Account #")); break;  // this is hidden, only used for copy and paste
			case 22: text = QString(tr("Note")); break;  // this is hidden, only used for copy and paste
		}  // clang format on
	}

	return text;
}

QVariant IifAuditListDataModel::data(const QModelIndex &index, int role) const
{
	QVariant item;
	QString typeString;
	if (role == Qt::DisplayRole) {
		switch(index.column()) {  // clang format off
			case 0: item = QDate::fromJulianDay(iifTransactionList.at(index.row()).date).toString("yyyy/MM/dd"); break;
			case 1: item = iifTransactionList.at(index.row()).sourceAccount; break;
			case 2: item = iifTransactionList.at(index.row()).sourceAccountNumber; break;
			case 3: item = iifTransactionList.at(index.row()).missionName; break;
			case 4: item = iifTransactionList.at(index.row()).missionAccount; break;
			case 5: item = iifTransactionList.at(index.row()).donorFirstName; break;
			case 6: item = iifTransactionList.at(index.row()).donorLastName; break;
			case 7: item = iifTransactionList.at(index.row()).donorId; break;
			case 8: item = iifTransactionList.at(index.row()).donorAcountIndex; break;
			case 9:
				switch (iifTransactionList.at(index.row()).source) {
					case DonationVia::Manual:    item = tr("Manual");    break;
					case DonationVia::Paypal:    item = tr("Paypal");    break;
					case DonationVia::Recurring: item = tr("Recurring"); break;
				}
				break;
			case 10:
				switch (iifTransactionList.at(index.row()).type) {
					case DonationType::Normal:            item = tr("Normal");     break;
					case DonationType::ThirdParty:        item = tr("3rd Party");  break;
					case DonationType::Inkind:            item = tr("In Kind");    break;
					case DonationType::ForeignThirdParty: item = tr("F3rd Party"); break;
				}
				break;
			case 11: item = convertCentsToString(iifTransactionList.at(index.row()).amountGross, true); break;
			case 12: item = convertCentsToString(iifTransactionList.at(index.row()).amountNet, true); break;
			case 13: item = convertCentsToString(iifTransactionList.at(index.row()).amountOperations, true); break;
			case 14: item = QString("%1%").arg(iifTransactionList.at(index.row()).percent, 0, 'f', 1); break;
			case 15: item = convertCentsToString(iifTransactionList.at(index.row()).fee, true); break;
			case 16:
				if (iifTransactionList.at(index.row()).anonymous)
					item = "Anon.";
				else
					item = "No";
				break;
			case 17: item = iifTransactionList.at(index.row()).neftd; break;
			case 18: item = iifTransactionList.at(index.row()).campaign; break;
			case 19: item = convertCentsToString(iifTransactionList.at(index.row()).amountAdvantage, true); break;
			case 20: item = iifTransactionList.at(index.row()).advantageAccount; break;
			case 21: item = iifTransactionList.at(index.row()).advantageAccountNumber; break;
			case 22: item = iifTransactionList.at(index.row()).note; break;
		}  // clang format on
	}
	return item;
}

void IifAuditListDataModel::clear()
{
	iifTransactionList.clear();
}

void IifAuditListDataModel::append(QJsonObject json)
{
	struct IifTransaction it;
	it.missionName = json.value("mission-name").toString();
	it.missionAccount = json.value("mission-account").toString();
	it.sourceAccount = json.value("source-account-name").toString();
	it.sourceAccountNumber = json.value("source-account-number").toString();
	it.advantageAccount = json.value("advantage-account-name").toString();
	it.advantageAccountNumber = json.value("advantage-account-number").toString();
	it.donorFirstName = json.value("first-name").toString();
	it.donorLastName = json.value("last-name").toString();
	it.date = json.value("date").toInt();
	it.source = json.value("source").toInt();
	it.type = json.value("type").toInt();
	it.amountGross = json.value("amount-gross").toInt();
	it.amountNet = json.value("amount-net").toInt();
	it.amountOperations = json.value("amount-operations").toInt();
	it.amountAdvantage = json.value("advantage").toInt();
	it.fee = json.value("fee").toInt();
	it.percent = json.value("percent").toDouble() * 100;
	it.donorId = json.value("donor-id").toInt();
	it.donorAcountIndex = json.value("donor-acnt-idx").toInt();
	it.anonymous = json.value("anonymous").toBool();
	QString note = json.value("note").toString();
	note.remove('\t');
	note.remove('\n');
	it.note = note;
	it.campaign = json.value("campaign").toString();
	it.neftd = json.value("neftd").toBool();
	iifTransactionList.append(it);
}
