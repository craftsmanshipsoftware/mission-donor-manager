// clang-format off
/**************************************************************************************************
	Copyright (C) 2024  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QJsonObject>
#include <QJsonArray>
#include "common.h"
#include "iif-unmarked-dm.h"

int IifUnmarkedDataModel::rowCount(const QModelIndex &) const
{
	return iifUnmarked.size();
}

int IifUnmarkedDataModel::columnCount(const QModelIndex &) const
{
	return 3;
}

QVariant IifUnmarkedDataModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	QVariant text;

	if (role != Qt::DisplayRole)
		return QVariant();

	if (orientation == Qt::Horizontal) {
		switch(section) {  // clang format off
			case 0: text = QString(tr("Date"));           break;
			case 1: text = QString(tr("Description"));    break;
			case 2: text = QString(tr("Gross"));          break;
		}  // clang format on
	}

	return text;
}

QVariant IifUnmarkedDataModel::data(const QModelIndex &index, int role) const
{
	QVariant item;
	int row = index.row();
	if (role == Qt::DisplayRole) {
		switch(index.column()) {  // clang format off
			case 0: item = iifUnmarked.at(row).date;                              break;
			case 1: item = iifUnmarked.at(row).description.left(48);              break;
			case 2: item = convertCentsToString(iifUnmarked.at(row).gross, true); break;
		}  // clang format on
	}
	return item;
}

void IifUnmarkedDataModel::update(QJsonArray jArray)
{
	iifUnmarked.clear();
	for (int i = 0; i < jArray.size(); i++) {
		struct IifUnmarkedItem unmarkedItem;
		QJsonObject jItem = jArray.at(i).toObject();
		unmarkedItem.date = QDate::fromJulianDay(jItem.value("date").toInt());
		unmarkedItem.gross = jItem.value("gross").toInt();
		unmarkedItem.description = jItem.value("description").toString();
		iifUnmarked.append(unmarkedItem);
	}
}
