// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef MDM_IIF_AUDIT_LIST_DM_H_
#define MDM_IIF_AUDIT_LIST_DM_H_

#include <QAbstractListModel>

class IifAuditListDataModel : public QAbstractTableModel
{
	Q_OBJECT

public:
	int rowCount(const QModelIndex &parent = QModelIndex()) const override ;
	int columnCount(const QModelIndex &parent = QModelIndex()) const override;
	QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
	void clear();
	void append(QJsonObject json);
	using QAbstractItemModel::endInsertRows;
	bool isProxies;

private:
	struct IifTransaction {
		QString missionName;
		QString missionAccount;
		QString sourceAccount;
		QString sourceAccountNumber;
		QString advantageAccount;
		QString advantageAccountNumber;
		QString donorFirstName;
		QString donorLastName;
		QString note;
		QString campaign;
		float percent;
		int date;
		int source;
		int type;
		int amountNet;
		int amountGross;
		int amountOperations;
		int amountAdvantage;
		int fee;
		int donorId;
		int donorAcountIndex;
		bool anonymous;
		bool neftd;
	};
	QList<IifTransaction> iifTransactionList;
};

#endif
