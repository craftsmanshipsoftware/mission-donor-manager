// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QJsonObject>
#include "iif-list-dm.h"

int IifListDataModel::rowCount(const QModelIndex &) const
{
	return iifList.size();
}

int IifListDataModel::columnCount(const QModelIndex &) const
{
	return 3;
}

QVariant IifListDataModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	QVariant text;

	if (role != Qt::DisplayRole)
		return QVariant();

	if (orientation == Qt::Horizontal) {
		switch(section) {  // clang format off
			case 0: text = QString(tr("Iif Export #")); break;
			case 1: text = QString(tr("End Date")); break;
			case 2: text = QString(tr("Income Account")); break;
		}  // clang format on
	}

	return text;
}

QVariant IifListDataModel::data(const QModelIndex &index, int role) const
{
	QVariant item;
	if (role == Qt::DisplayRole) {
		switch(index.column()) {  // clang format off
			case 0: item = iifList.at(index.row()).id;                                               break;
			case 1: item = QDate::fromJulianDay(iifList.at(index.row()).end).toString("yyyy-MM-dd"); break;
			case 2: item = iifList.at(index.row()).sa;                                               break;
		}  // clang format on
	}
	return item;
}

void IifListDataModel::clear()
{
	iifList.clear();
}

void IifListDataModel::append(QJsonObject jExport)
{
	struct IifListItem dli;
	dli.id = jExport.value("id").toInt();
	dli.end = jExport.value("end").toInt();
	dli.sa = jExport.value("sa").toString();
	iifList.append(dli);
}

QString IifListDataModel::getFileName(uint row)
{
	QString fileName;
	fileName+= QString("%1_").arg(iifList.at(row).id);
	fileName+= QDate::fromJulianDay(iifList.at(row).end).toString("yyyy-MM-dd");
	fileName+= ".iif";
	return fileName;
}
