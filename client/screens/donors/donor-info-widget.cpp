// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QGroupBox>
#include <QRadioButton>
#include <QCheckBox>
#include <QJsonObject>
#include "../common/common.h"
#include "styler.h"
#include "theme.h"
#include "donors-screen.h"
#include "donor-info-widget.h"

DonorInfo::DonorInfo()
{
	auto addLabel = [&](const QString &title, uint row, QGridLayout *gl) {
		QLabel *label = new QLabel(title);
		QLineEdit *lineEdit = new QLineEdit;
		Styler::styleLabel(label);
		Styler::styleLineEdit(lineEdit);
		gl->addWidget(label, row, 0);
		gl->addWidget(lineEdit, row, 1);
		return lineEdit;
	};

	QHBoxLayout *layout = new QHBoxLayout;
	setLayout(layout);
	QGridLayout *leftLayout = new QGridLayout;
	layout->addLayout(leftLayout);
	layout->setStretchFactor(leftLayout, 15);
	layout->addStretch(1);
	QVBoxLayout *rightLayout = new QVBoxLayout;
	layout->addLayout(rightLayout);
	layout->setStretchFactor(rightLayout, 15);
	layout->addStretch(1);

	QGridLayout *paypalLayout = new QGridLayout;
	rightLayout->addLayout(paypalLayout);
	rightLayout->addStretch(1);

	uint row = 0;
	first = addLabel(tr("First:"), row++, leftLayout);
	last = addLabel(tr("Last:"), row++, leftLayout);
	salutation = addLabel(tr("Salutation:"), row++, leftLayout);
	addressSalutation = addLabel(tr("Address Salutation:"), row++, leftLayout);
	otherName = addLabel(tr("Other Name:"), row++, leftLayout);
	address = addLabel(tr("Address:"), row++, leftLayout);
	apt = addLabel(tr("Apt:"), row++, leftLayout);
	city = addLabel(tr("City:"), row++, leftLayout);
	province = addLabel(tr("Province:"), row++, leftLayout);
	province->setMaxLength(2);
	country = addLabel(tr("Country Code:"), row++, leftLayout);
	country->setMaxLength(2);
	postalCode = addLabel(tr("Postal Code:"), row++, leftLayout);
	email = addLabel(tr("Email:"), row++, leftLayout);
	cellPhone = addLabel(tr("Cell Phone:"), row++, leftLayout);
	homePhone = addLabel(tr("Home Phone:"), row++, leftLayout);
	workPhone = addLabel(tr("Work Phone:"), row++, leftLayout);
	importedId = addLabel(tr("Imported ID:"), row++, leftLayout);
	importedId->setReadOnly(true);
	dateAdded = addLabel(tr("Date Added:"), row++, leftLayout);
	dateAdded->setReadOnly(true);
	contact = addLabel(tr("Contact:"), row++, leftLayout);
	deceased = new QCheckBox(tr("Deceased"));
	Styler::styleCheckBox(deceased);
	deceased->setEnabled(false);
	leftLayout->addWidget(deceased, row++, 1);
	row = 0;
	ppa0 = addLabel(tr("Paypal Account 1:"), row++, paypalLayout);
	ppa0Last = addLabel(tr("Last Used:"), row++, paypalLayout);
	ppa0Last->setReadOnly(true);
	ppa1 = addLabel(tr("Paypal Account 2:"), row++, paypalLayout);
	ppa1Last = addLabel(tr("Last Used:"), row++, paypalLayout);
	ppa1Last->setReadOnly(true);
	ppa2 = addLabel(tr("Paypal Account 3:"), row++, paypalLayout);
	ppa2Last = addLabel(tr("Last Used:"), row++, paypalLayout);
	ppa2Last->setReadOnly(true);
	ppa3 = addLabel(tr("Paypal Account 4:"), row++, paypalLayout);
	ppa3Last = addLabel(tr("Last Used:"), row++, paypalLayout);
	ppa3Last->setReadOnly(true);
	ppa4 = addLabel(tr("Paypal Account 5:"), row++, paypalLayout);
	ppa4Last = addLabel(tr("Last Used:"), row++, paypalLayout);
	ppa4Last->setReadOnly(true);

	is3rdParty = new QCheckBox(tr("Is a 3rd Party"));
	Styler::styleCheckBox(is3rdParty);
	is3rdParty->setEnabled(false);
	paypalLayout->addWidget(is3rdParty, row++, 1);

//------ Donor Type selection
	QGroupBox *typeGroup = new QGroupBox(tr("Type"));
	Styler::styleGroupBox(typeGroup);
	QGridLayout *typeGroupLayout = new QGridLayout;
	typeGroup->setLayout(typeGroupLayout);
	auto addType = [&](const QString &name, int pos) {
		QRadioButton *typeRB = new QRadioButton(name);
		Styler::styleRadioButton(typeRB);
		typeGroupLayout->addWidget(typeRB, pos%4, pos/4);
		return typeRB;
	};
	int pos = 0;
	typePerson = addType(tr("Individual"), pos++);
	typeCorporation = addType(tr("Business"), pos++);
	typeCharity = addType(tr("Canadian Charity"), pos++);
	typeFoundation = addType(tr("Canadian Foundation"), pos++);
	typeForeignCharity = addType(tr("Foreign Charity"), pos++);
	typeNonTaxPayer = addType(tr("Non Tax payer"), pos++);
	typeGovernmentAgency = addType(tr("Government Agency"), pos++);
	rightLayout->addStretch(1);
	rightLayout->addWidget(typeGroup);

	connect(typePerson, &QRadioButton::clicked, [&](){
		type = DonorType::Individual;
		is3rdParty->setEnabled(false);
		is3rdParty->setChecked(false);
		flagChanged();
	});
	connect(typeCorporation, &QRadioButton::clicked, [&](){
		type = DonorType::Business;
		is3rdParty->setEnabled(false);
		is3rdParty->setChecked(false);
		flagChanged();
	});
	connect(typeGovernmentAgency, &QRadioButton::clicked, [&](){
		type = DonorType::GovernmentAgency;
		is3rdParty->setEnabled(false);
		is3rdParty->setChecked(false);
		flagChanged();
	});
	connect(typeCharity, &QRadioButton::clicked, [&](){
		type = DonorType::CanadianCharity;
		is3rdParty->setEnabled(true);
		flagChanged();
	});
	connect(typeForeignCharity, &QRadioButton::clicked, [&](){
		type = DonorType::ForeignCharity;
		is3rdParty->setEnabled(true);
		flagChanged();
	});
	connect(typeNonTaxPayer, &QRadioButton::clicked, [&](){
		type = DonorType::NonTaxPayer;
		is3rdParty->setEnabled(false);
		is3rdParty->setChecked(false);
		flagChanged();
	});
	connect(typeFoundation, &QRadioButton::clicked, [&](){
		type = DonorType::CanadianFoundation;
		is3rdParty->setEnabled(false);
		is3rdParty->setChecked(false);
		flagChanged();
	});


	connect(first, &QLineEdit::textEdited, this, &DonorInfo::flagChanged);
	connect(last, &QLineEdit::textEdited, this, &DonorInfo::flagChanged);
	connect(salutation, &QLineEdit::textEdited, this, &DonorInfo::flagChanged);
	connect(addressSalutation, &QLineEdit::textEdited, this, &DonorInfo::flagChanged);
	connect(otherName, &QLineEdit::textEdited, this, &DonorInfo::flagChanged);
	connect(address, &QLineEdit::textEdited, this, &DonorInfo::flagChanged);
	connect(apt, &QLineEdit::textEdited, this, &DonorInfo::flagChanged);
	connect(city, &QLineEdit::textEdited, this, &DonorInfo::flagChanged);
	connect(province, &QLineEdit::textEdited, this, &DonorInfo::flagChanged);
	connect(country, &QLineEdit::textEdited, this, &DonorInfo::flagChanged);
	connect(postalCode, &QLineEdit::textEdited, this, &DonorInfo::flagChanged);
	connect(email, &QLineEdit::textEdited, this, &DonorInfo::flagChanged);
	connect(cellPhone, &QLineEdit::textEdited, this, &DonorInfo::flagChanged);
	connect(homePhone, &QLineEdit::textEdited, this, &DonorInfo::flagChanged);
	connect(workPhone, &QLineEdit::textEdited, this, &DonorInfo::flagChanged);
	connect(contact, &QLineEdit::textEdited, this, &DonorInfo::flagChanged);
	connect(ppa0, &QLineEdit::textEdited, this, &DonorInfo::flagChanged);
	connect(ppa1, &QLineEdit::textEdited, this, &DonorInfo::flagChanged);
	connect(ppa2, &QLineEdit::textEdited, this, &DonorInfo::flagChanged);
	connect(ppa3, &QLineEdit::textEdited, this, &DonorInfo::flagChanged);
	connect(ppa4, &QLineEdit::textEdited, this, &DonorInfo::flagChanged);
	connect(is3rdParty, &QCheckBox::clicked, this, &DonorInfo::flagChanged);
	connect(deceased, &QCheckBox::clicked, this, &DonorInfo::flagChanged);
}

void DonorInfo::flagChanged()
{
	if (!isModified) {
		isModified = true;
		emit changed();
	}
}

void DonorInfo::clear()
{
	first->clear();
	last->clear();
	salutation->clear();
	addressSalutation->clear();
	otherName->clear();
	address->clear();
	apt->clear();
	city->clear();
	province->clear();
	country->clear();
	postalCode->clear();
	email->clear();
	cellPhone->clear();
	homePhone->clear();
	workPhone->clear();
	importedId->clear();
	dateAdded->clear();
	contact->clear();
	ppa0->clear();
	ppa1->clear();
	ppa2->clear();
	ppa3->clear();
	ppa4->clear();
	ppa0Last->clear();
	ppa1Last->clear();
	ppa2Last->clear();
	ppa3Last->clear();
	ppa4Last->clear();
	typePerson->setChecked(true);
	is3rdParty->setChecked(false);
	deceased->setChecked(false);
}

void DonorInfo::setReadOnly(bool ro)
{
	readOnly = ro;
	first->setReadOnly(ro);
	last->setReadOnly(ro);
	salutation->setReadOnly(ro);
	addressSalutation->setReadOnly(ro);
	otherName->setReadOnly(ro);
	address->setReadOnly(ro);
	apt->setReadOnly(ro);
	city->setReadOnly(ro);
	province->setReadOnly(ro);
	country->setReadOnly(ro);
	postalCode->setReadOnly(ro);
	email->setReadOnly(ro);
	cellPhone->setReadOnly(ro);
	homePhone->setReadOnly(ro);
	workPhone->setReadOnly(ro);
	contact->setReadOnly(ro);
	ppa0->setReadOnly(ro);
	ppa1->setReadOnly(ro);
	ppa2->setReadOnly(ro);
	ppa3->setReadOnly(ro);
	ppa4->setReadOnly(ro);
	typePerson->setEnabled(!ro);
	typeCorporation->setEnabled(!ro);
	typeGovernmentAgency->setEnabled(!ro);
	typeCharity->setEnabled(!ro);
	typeForeignCharity->setEnabled(!ro);
	typeNonTaxPayer->setEnabled(!ro);
	typeFoundation->setEnabled(!ro);
	if (!ro && (typeCharity->isChecked() || typeForeignCharity->isChecked()))
		is3rdParty->setEnabled(true);
	else
		is3rdParty->setEnabled(false);
	deceased->setEnabled(!ro);

}

void DonorInfo::populateValues(QJsonObject json)
{
	isModified = false;

	first->setText(json.value("forename").toString());
	last->setText(json.value("surname").toString());
	salutation->setText(json.value("salutation").toString());
	addressSalutation->setText(json.value("address-salutation").toString());
	otherName->setText(json.value("other-name").toString());
	address->setText(json.value("address").toString());
	apt->setText(json.value("apt").toString());
	city->setText(json.value("city").toString());
	province->setText(json.value("province").toString());
	country->setText(json.value("country").toString());
	postalCode->setText(json.value("postalcode").toString());
	email->setText(json.value("email").toString());
	cellPhone->setText(json.value("cellphone").toString());
	homePhone->setText(json.value("homephone").toString());
	workPhone->setText(json.value("workphone").toString());
	importedId->setText(json.value("imported-id").toString());
	contact->setText(json.value("contact").toString());
	ppa0->setText(json.value("ppa0").toString());
	ppa1->setText(json.value("ppa1").toString());
	ppa2->setText(json.value("ppa2").toString());
	ppa3->setText(json.value("ppa3").toString());
	ppa4->setText(json.value("ppa4").toString());
	is3rdParty->setChecked(json.value("is-3rdparty").toBool());
	deceased->setChecked(json.value("deceased").toBool());
	auto setPpaLast = [&](const char *element, QLineEdit *lineEdit) {
		int julianDay = json.value(element).toInt();
		if (julianDay)
			lineEdit->setText(QDate::fromJulianDay(julianDay).toString());
		else
			lineEdit->clear();
	};
	setPpaLast("ppa0-last", ppa0Last);
	setPpaLast("ppa1-last", ppa1Last);
	setPpaLast("ppa2-last", ppa2Last);
	setPpaLast("ppa3-last", ppa3Last);
	setPpaLast("ppa4-last", ppa4Last);
	setPpaLast("added", dateAdded);
	type = json.value("type").toInt();
	switch (type) {  // clang format off
		case DonorType::Individual: typePerson->setChecked(true); break;
		case DonorType::Business: typeCorporation->setChecked(true); break;
		case DonorType::GovernmentAgency: typeCorporation->setChecked(true); break;
		case DonorType::CanadianCharity: typeCharity->setChecked(true); break;
		case DonorType::ForeignCharity: typeForeignCharity->setChecked(true); break;
		case DonorType::NonTaxPayer: typeNonTaxPayer->setChecked(true); break;
		case DonorType::CanadianFoundation: typeFoundation->setChecked(true); break;
	}  // clang format on
}

void DonorInfo::buildJson(QJsonObject &json)
{
	json.insert("type", type);
	json.insert("forename", first->text());
	json.insert("surname", last->text());
	json.insert("salutation", salutation->text());
	json.insert("address-salutation", addressSalutation->text());
	json.insert("other-name", otherName->text());
	json.insert("address", address->text());
	json.insert("apt", apt->text());
	json.insert("city", city->text());
	json.insert("province", province->text());
	json.insert("country", country->text());
	json.insert("postalcode", postalCode->text());
	json.insert("email", email->text());
	json.insert("cellphone", cellPhone->text());
	json.insert("homephone", homePhone->text());
	json.insert("workphone", workPhone->text());
	json.insert("contact", contact->text());
	json.insert("ppa0", ppa0->text());
	json.insert("ppa1", ppa1->text());
	json.insert("ppa2", ppa2->text());
	json.insert("ppa3", ppa3->text());
	json.insert("ppa4", ppa4->text());
	json.insert("is-3rdparty", is3rdParty->isChecked());
	json.insert("deceased", deceased->isChecked());
}
