// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include "../common/common.h"
#include "donor-account-dm.h"

void DonorAccountDataModel::setDonations(const QByteArray &donations)
{
	layoutAboutToBeChanged();
	account.clear();
	QJsonArray jDonations = QJsonDocument::fromJson(donations).array();
	for (auto jDonation : jDonations) {
		QJsonObject json = jDonation.toObject();
		DonorAccountItem item;
		item.id = json.value("id").toInt();
		item.ammount = json.value("amount").toInt();
		item.advantage = json.value("advantage").toInt();
		item.fee = json.value("fee").toInt();
		item.percentage = json.value("percentage").toDouble() * 100;
		item.balance = json.value("balance").toInt();
		item.receiptable = json.value("receiptable").toInt();
		item.anonymous = json.value("anonymous").toBool();
		item.date = QDate::fromJulianDay(json.value("date").toInt());
		item.mission = json.value("benefactor").toString();
		item.voided = json.value("invalid").toBool();
		item.type = json.value("type").toInt();
		item.source = json.value("source").toInt();
		item.exportId = json.value("export-id").toInt();
		item.note = json.value("note").toString();
		account.append(item);
	}
	layoutChanged();
}

int DonorAccountDataModel::rowCount(const QModelIndex &) const
{
	return account.size();
}

int DonorAccountDataModel::columnCount(const QModelIndex &) const
{
	return 13;
}

QVariant DonorAccountDataModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	QVariant text;

	if (role != Qt::DisplayRole)
		return QVariant();

	if (orientation == Qt::Horizontal) {
		switch(section) {  // clang format off
			case 0:  text = QString(tr("DID"));              break;
			case 1:  text = QString(tr("Date"));             break;
			case 2:  text = QString(tr("Ammount"));          break;
			case 3:  text = QString(tr("Advantage"));        break;
			case 4:  text = QString(tr("Fee"));              break;
			case 5:  text = QString(tr("%"));                break;
			case 6:  text = QString(tr("Mission"));          break;
			case 7:  text = QString(tr("Year Total"));       break;
			case 8:  text = QString(tr("Year Receiptable")); break;
			case 9:  text = QString(tr("Anonymous"));        break;
			case 10: text = QString(tr("Type"));             break;
			case 11: text = QString(tr("Voided"));           break;
			case 12: text = QString(tr("Iif #"));            break;
		}  // clang format on
	} else {
		text = QVariant(QString());
	}

	return text;
}

QVariant DonorAccountDataModel::data(const QModelIndex &index, int role) const
{
	QVariant item;
	QString typeString;
	if (role == Qt::DisplayRole) {
		int row = account.size() - 1 - index.row();
		switch(index.column()) {  // clang format off
			case 0: item = account.at(row).id; break;
			case 1: item = account.at(row).date; break;
			case 2: item = convertCentsToString(account.at(row).ammount, true); break;
			case 3: item = convertCentsToString(account.at(row).advantage, true); break;
			case 4: item = convertCentsToString(account.at(row).fee, true); break;
			case 5: item = QString("%1%").arg(account.at(row).percentage, 0, 'f', 1); break;
			case 6: item = account.at(row).mission; break;
			case 7: item = convertCentsToString(account.at(row).balance, true); break;
			case 8: item = convertCentsToString(account.at(row).receiptable, true); break;
			case 9:
				if (account.at(row).anonymous)
					item = "Anonymous";
				break;
			case 10:
				switch (account.at(row).source) {  // clang-format off
					case DonationVia::Manual:    typeString = "Man. "; break;
					case DonationVia::Paypal:    typeString = "PP. ";  break;
					case DonationVia::Recurring: typeString = "Rec. "; break;
				}
				switch (account.at(row).type) {
					case DonationType::Normal:            typeString+= tr("Normal");     break;
					case DonationType::ThirdParty:        typeString+= tr("3rd Party");  break;
					case DonationType::Inkind:            typeString+= tr("Inkind");     break;
					case DonationType::ForeignThirdParty: typeString+= tr("F3rd Party"); break;
				}  // clang-format on
				item = typeString;
				break;
			case 11:
				if (account.at(row).voided)
					item = "Voided";
				break;
			case 12:
				if (account.at(row).exportId < 0)
					item = "-";
				else
					item = account.at(row).exportId;
				break;
		}  // clang format on
	}
	return item;
}
