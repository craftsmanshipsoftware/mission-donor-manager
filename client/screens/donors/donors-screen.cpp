// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QKeyEvent>
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QPlainTextEdit>
#include <QListView>
#include <QTableView>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QSortFilterProxyModel>
#include <QFileDialog>

#include "qslidingstackedwidget.h"
#include "../common/common.h"
#include "../common/permission.h"
#include "main-window.h"
#include "connection-manager.h"
#include "user-input.h"
#include "styler.h"
#include "theme.h"
#include "qexclusivebuttons.h"
#include "pdf-writer.h"
#include "donor-summary-widget.h"
#include "donor-info-widget.h"
#include "donor-mail-widget.h"
#include "centred-message-widget.h"
#include "donor-account-widget.h"
#include "donor-receipts-widget.h"
#include "name-selector-widget.h"
#include "donors-screen.h"

#include <QDebug>

namespace Content {
enum {
	SelectDonor,
	Summary,
	Edit,
	Mail,
	Notes,
	Acconut,
	Receipts,
};
}

namespace TopBottons {
enum  {
	Summary,
	Info,
	Mail,
	Notes,
	Account,
	Receipts,
};
}

DonorScreen::DonorScreen(MainWindow &mainWindow, Permission &p) : mainWindow(mainWindow), cm(mainWindow.connectionManager)
{
	QVBoxLayout *layout = new QVBoxLayout;
	setLayout(layout);
	int m = Theme::OuterMargin * qMin(screen()->geometry().width(), screen()->geometry().height());
	layout->setContentsMargins(m, m, m, m);
	QHBoxLayout *topLayout = new QHBoxLayout;
	layout->addLayout(topLayout);
	QHBoxLayout *bottomLayout = new QHBoxLayout;
	layout->addLayout(bottomLayout);

	// top Button layout
	topLayout->addStretch(1);
	QLabel *title = new QLabel(tr("Donors"));
	Styler::styleLabel(title, Theme::H1Size);
	topLayout->addWidget(title);
	topLayout->addStretch(1);

	topButtons = new QExclusiveButtons( { tr("Summary"), tr("Info"), tr("Communications"), tr("Notes"), tr("Account"), tr("Receipts") } );
	topLayout->addWidget(topButtons);
	for ( int i=0; i < topButtons->size(); i++) {
		Styler::styleButton(topButtons->buttonAt(i));
		topButtons->buttonAt(i)->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
	}
	enableTopButtons(false);

	// bottom layout
	QVBoxLayout *selectorLayout = new QVBoxLayout;
	bottomLayout->addLayout(selectorLayout);
	bottomLayout->setStretchFactor(selectorLayout, 2);
	bottomLayout->addStretch(1);
	QVBoxLayout *contentLayout = new QVBoxLayout;
	bottomLayout->addLayout(contentLayout);
	bottomLayout->setStretchFactor(contentLayout, 7);

	// selector sub layout
	donorListView = new NameSelectorWidget;
	donorListView->enabledIdShown(true);
	selectorLayout->addWidget(donorListView);

	newButton = new QPushButton(tr("New"));
	Styler::styleButton(newButton);
	selectorLayout->addWidget(newButton);
	selectorLayout->setAlignment(newButton, Qt::AlignHCenter);
	if (!p.hasPermission(Permission::DonorModify))
		newButton->hide();

	// conent sub layout
	content = new QSlidingStackedWidget();
	content->setDuration(500);
	contentLayout->addWidget(content);
	QWidget *askSelect = new CentredMessageWidget("Please Select a Donor");
	content->addWidget(askSelect);
	donorSummary = new DonorSummary;
	content->addWidget(donorSummary);
	donorInfo = new DonorInfo;
	content->addWidget(donorInfo);
	donorMail = new DonorMail;
	content->addWidget(donorMail);
	noteEdit = new QPlainTextEdit;
	Styler::stylePlainTextEdit(noteEdit);
	content->addWidget(noteEdit);
	donorAccount = new DonorAccount(p);
	content->addWidget(donorAccount);
	donorReceiptsWidget = new DonorReceiptsWidget(*this);
	content->addWidget(donorReceiptsWidget);
	content->setCurrentIndex(Content::SelectDonor);
	contentLayout->addStretch(1);
	QHBoxLayout *contentButtonLayout = new QHBoxLayout;
	contentLayout->addLayout(contentButtonLayout);
	contentButtonLayout->addStretch(1);
	editEnableButton = new QPushButton(tr("Edit"));
	Styler::styleButton(editEnableButton);
	editEnableButton->setEnabled(false);
	contentButtonLayout->addWidget(editEnableButton);
	contentButtonLayout->addStretch(1);
	cancelButton = new QPushButton(tr("Cancel"));
	Styler::styleButton(cancelButton);
	cancelButton->setEnabled(false);
	contentButtonLayout->addWidget(cancelButton);
	contentButtonLayout->addStretch(1);
	saveButton = new QPushButton(tr("Save"));
	Styler::styleButton(saveButton);
	saveButton->setEnabled(false);
	contentButtonLayout->addWidget(saveButton);
	if (!p.hasPermission(Permission::DonorModify)) {
		editEnableButton->hide();
		cancelButton->hide();
		saveButton->hide();
	}
	contentButtonLayout->addStretch(1);

	connect(saveButton, &QPushButton::clicked, this, &DonorScreen::saveClicked);
	connect(donorInfo, &DonorInfo::changed, [&]() {
		saveButton->setEnabled(true);
	});
	connect(donorMail, &DonorMail::changed, this, &DonorScreen::mailEditChanged);
	connect(donorListView, &NameSelectorWidget::nameSelected, this, &DonorScreen::donorSelected);
	//connect(donorListView, &NameSelectorWidget::filterEdited, this, &DonorScreen::clearSelection);
	connect(newButton, &QPushButton::clicked, this, &DonorScreen::newClicked);
	connect(editEnableButton, &QPushButton::clicked, [&]() {
		cm.foxmq.post("donors.lock", FoxHead::ContentType::IntAsString, QByteArray::number(donorUnderEdit));
	});
	connect(topButtons, &QExclusiveButtons::clicked, this, &DonorScreen::topButtonClicked);
	connect(cancelButton, &QPushButton::clicked, this, &DonorScreen::cancelClicked);
	connect(donorAccount->timePeriodButtons, &QExclusiveButtons::clicked, this, &DonorScreen::accountTimePeriodButtonClicked);
	connect(donorAccount, &DonorAccount::voidButtonClicked, this, &DonorScreen::accountVoidClicked);
	connect(donorAccount, &DonorAccount::iifButtonClicked, this, &DonorScreen::accountIifButtonClicked);

	topicsToReceive << "donors" << "donations.md.donor-accounts-updated" << "iif.mark-append";
	cm.registerApiForReceiver(this);
}

void DonorScreen::subscribe()
{
	cm.foxmq.subscription("donors.ro.list", true);
	cm.foxmq.subscription("donors.ro.info-updated", true);
	cm.foxmq.subscription("donors.ro.mail-updated", true);
	cm.foxmq.subscription("donors.ro.note-updated", true);
	cm.foxmq.subscription("donations.md.donor-accounts-updated", true);
	queryAsPerCurrentTab();
}

void DonorScreen::unsubscribe()
{
	cm.foxmq.subscription("donors.ro.list", false);
	cm.foxmq.subscription("donors.ro.info-updated", false);
	cm.foxmq.subscription("donors.ro.mail-updated", false);
	cm.foxmq.subscription("donors.ro.note-updated", false);
	cm.foxmq.subscription("donations.md.donor-accounts-updated", false);
}

void DonorScreen::donorSelected(int nameId, QModelIndex index)
{
	if (donorUnderEdit == -1) {
		content->setCurrentIndex(Content::Summary);
		topButtons->setChecked(TopBottons::Summary, true);
	}
	if (nameId != donorUnderEdit) {
		if (!checkForSave()) {
			indexOfDonorUnderEdit = index;
			donorUnderEdit = nameId;
			donorInfo->setReadOnly(true);
			donorMail->setReadOnly(true);
			noteEdit->setReadOnly(true);
			editEnableButton->setEnabled(true);
			enableTopButtons(true);
			queryAsPerCurrentTab();
		}
	}
}

void DonorScreen::queryAsPerCurrentTab()
{
	switch (topButtons->currentIndex() + 1) {  // clang-format off
		case Content::Summary:  sendRequestForSummaryUpdate(); break;
		case Content::Edit:     sendRequestForInfoUpdate();    break;
		case Content::Mail:     sendRequestForMailUpdate();    break;
		case Content::Notes:    sendRequestForNotesUpdate();   break;
		case Content::Acconut:  sendRequestForAccountUpdate(); break;
		case Content::Receipts: sendRequestForReceipts();      break;
	}  // clang-format on
}

void DonorScreen::topButtonClicked(int index)
{
	content->setCurrentIndex(index + 1);
	switch (index + 1) {  // clang-format off
		case Content::Summary:  sendRequestForSummaryUpdate(); break;
		case Content::Edit:
			if (!donorInfo->isModified)
				sendRequestForInfoUpdate();
			break;
		case Content::Mail:
			if (!donorMail->isModified)
				sendRequestForMailUpdate();
			break;
		case Content::Notes:
			if (!noteEdit->document()->isModified())
				sendRequestForNotesUpdate();
			break;
		case Content::Acconut:  sendRequestForAccountUpdate(); break;
		case Content::Receipts: sendRequestForReceipts();      break;
	}  // clang-format on
}

bool DonorScreen::checkForSave()
{
	bool changed = false;
	if (donorInfo->isModified || noteEdit->document()->isModified() || donorMail->isModified) {
		changed = true;
		connect(mainWindow.userInput, &UserInput::button1Clicked, this, &DonorScreen::saveConfirmDiscardClicked);
		connect(mainWindow.userInput, &UserInput::button2Clicked, this, &DonorScreen::saveConfirmCleanUp);
		connect(mainWindow.userInput, &UserInput::button3Clicked, this, &DonorScreen::saveConfirmSaveClicked);
		mainWindow.userInput->askForInput(tr("Changes have been made"), tr("Discard Changes"), tr("Continue Editing"), tr("      Save      "));
		mainWindow.showUserInput();
	} else if (cancelButton->isEnabled()) {
		doneEdits();  // editing has been enabled, but no changes, so unlock
	}
	return changed;
}

void DonorScreen::saveConfirmSaveClicked()
{
	saveConfirmCleanUp();
	saveClicked();
}

void DonorScreen::saveConfirmDiscardClicked()
{
	// on discard, refresh forms from server
	if (newFlag) {
		content->setCurrentIndex(Content::SelectDonor);
	} else {
		if (donorInfo->isModified)
			sendRequestForInfoUpdate();
		if (noteEdit->document()->isModified())
			sendRequestForNotesUpdate();
		if (donorMail->isModified)
			sendRequestForMailUpdate();
	}
	saveConfirmCleanUp();
	doneEdits();
}

void DonorScreen::saveConfirmCleanUp()
{
	disconnect(mainWindow.userInput, &UserInput::button1Clicked, this, &DonorScreen::saveConfirmDiscardClicked);
	disconnect(mainWindow.userInput, &UserInput::button2Clicked, this, &DonorScreen::saveConfirmCleanUp);
	disconnect(mainWindow.userInput, &UserInput::button3Clicked, this, &DonorScreen::saveConfirmSaveClicked);
	if (!newFlag)
		donorListView->setCurrentIndex(indexOfDonorUnderEdit);
	mainWindow.showMainScreen();
}

void DonorScreen::enableTopButtons(bool enabled)
{
	for (int i=0; i<topButtons->size(); i++)
		topButtons->setEnabled(i, enabled);
}

void DonorScreen::processPublish(const QByteArray &topic, ssize_t, FoxID, FoxHead::ContentType contentType, const QByteArray &content)
{
	int donorID = -1;
	QJsonArray jArray;
	if (contentType == FoxHead::ContentType::IntAsString)
		donorID = content.toInt();
	else if (contentType == FoxHead::ContentType::Json)
		jArray = QJsonDocument::fromJson(content).array();

	if (topic == "donors.ro.list") {
		donorListView->updateNameList(jArray);
		if (selectLastDonor) {
			selectLastDonor = false;
			donorListView->selectEndOfDataModel();
		}
		indexOfDonorUnderEdit = donorListView->indexOfId(donorUnderEdit);  // QModelIndex indexOfDonorUnderEdit is invalid from the updated list.  Fix using updated list
	} else if (donorID == donorUnderEdit && topic == "donors.ro.info-updated" && donorInfo->isReadOnly()) {
		sendRequestForInfoUpdate();
	} else if (donorID == donorUnderEdit && topic == "donors.ro.mail-updated" && donorMail->isReadOnly()) {
		sendRequestForMailUpdate();
	} else if (donorID == donorUnderEdit && topic == "donors.ro.note-updated" && donorInfo->isReadOnly()) {
		sendRequestForNotesUpdate();
	} else if (topic == "donations.md.donor-accounts-updated" && jArray.contains(donorUnderEdit)) {
		if (topButtons->buttonAt(TopBottons::Summary)->isChecked())
			sendRequestForSummaryUpdate();
		else if (topButtons->buttonAt(TopBottons::Account)->isChecked())
			sendRequestForAccountUpdate();
	}
}

void DonorScreen::sendRequestForSummaryUpdate()
{
	cm.foxmq.post("donors.ro.summary", FoxHead::ContentType::IntAsString, QByteArray::number(donorUnderEdit));
}

void DonorScreen::sendRequestForInfoUpdate()
{
	cm.foxmq.post("donors.ro.info", FoxHead::ContentType::IntAsString, QByteArray::number(donorUnderEdit));
}

void DonorScreen::sendRequestForMailUpdate()
{
	cm.foxmq.post("donors.ro.mail", FoxHead::ContentType::IntAsString, QByteArray::number(donorUnderEdit));
}

void DonorScreen::sendRequestForNotesUpdate()
{
	cm.foxmq.post("donors.ro.note", FoxHead::ContentType::IntAsString, QByteArray::number(donorUnderEdit));
}

void DonorScreen::sendRequestForReceipts()
{
	cm.foxmq.post("donors.ro.receipts", FoxHead::ContentType::IntAsString, QByteArray::number(donorUnderEdit));
}

void DonorScreen::processPostResponce(const QByteArray &topic, FoxID, FoxHead::ContentType, const QByteArray &content)
{
	if (topic == "donors.ro.info") {
		donorInfo->populateValues(QJsonDocument::fromJson(content).object());
	} else if (topic == "donors.ro.mail") {
		donorMail->populateValues(content.toInt());
	} else if (topic == "donors.ro.note") {
		noteEdit->setPlainText(content);
	} else if (topic == "donors.ro.account") {
		donorAccount->setDonations(content);
	} else if (topic == "donors.ro.summary") {
		donorSummary->update(content);
	} else if (topic == "donors.ro.receipts") {
		donorReceiptsWidget->setReceipts(content);
	} else if (topic == "donors.ro.receipt") {
		receiptToSave = content;
		saveFileDialog = QSharedPointer<QFileDialog>(new QFileDialog(nullptr, "Save File", "", "PDF (*.pdf)"), &QObject::deleteLater);
		saveFileDialog->setAcceptMode(QFileDialog::AcceptSave);
		connect(saveFileDialog.data(), &QFileDialog::fileSelected, this, &DonorScreen::saveReceiptSelected);
		connect(saveFileDialog.data(), &QFileDialog::finished, this, &DonorScreen::saveReceiptSelectorFinished);
		saveFileDialog->show();
		mainWindow.setEnabled(false);
	} else if (topic == "donors.ro.sign-receipt") {
		QJsonObject injObj = QJsonDocument::fromJson(content).object();
		QByteArray signature = QByteArray::fromBase64(injObj.value("signature").toVariant().toByteArray());
		QFile receiptSignatureFile(receiptSignatureFileName);
		if (receiptSignatureFile.open(QIODevice::WriteOnly)) {
			receiptSignatureFile.write(signature);
			receiptSignatureFile.close();
		}
	} else if (topic == "iif.mark-append") {
		int error = content.toInt();
		if (error) {
			switch (error) {  // clang-format off
				case IifAppendError::InvalidIif: mainWindow.userInput->askForInput(tr("IIF number does not exist"));                                  break;
				case IifAppendError::DonationNotFound: mainWindow.userInput->askForInput(tr("Donation does not exist"));                              break;
				case IifAppendError::IncomeAccountMissmatch: mainWindow.userInput->askForInput(tr("Donation's Income Account does not match IIF's")); break;
				case IifAppendError::DonationAlreadyGrouped: mainWindow.userInput->askForInput(tr("Donation is already grouped to IIF"));             break;
			}  // clang-format on
			connect(mainWindow.userInput, &UserInput::button1Clicked, this, &DonorScreen::okFromIifAppendError);
			mainWindow.showUserInput();
		}
	}
}

void DonorScreen::processAck(const QByteArray &topic)
{
	if (topic == "donors.lock") {
		donorInfo->setReadOnly(false);
		donorMail->setReadOnly(false);
		noteEdit->setReadOnly(false);
		newButton->setEnabled(false);
		editEnableButton->setEnabled(false);
		cancelButton->setEnabled(true);
		noteEdit->document()->setModified(false);
		connect(noteEdit->document(), &QTextDocument::modificationChanged, this, &DonorScreen::noteEditTextChanged);
	}
}

void DonorScreen::processNack(const QByteArray &topic)
{
	if (topic == "donors.lock") {
		connect(mainWindow.userInput, &UserInput::button1Clicked, this, &DonorScreen::okFromLockNack);
		mainWindow.userInput->askForInput(tr("Another user is editing this donor, try again later"));
		mainWindow.showUserInput();
	} else if (topic == "iif.mark-append") {
		connect(mainWindow.userInput, &UserInput::button1Clicked, this, &DonorScreen::okFromLockNack);
		mainWindow.userInput->askForInput(tr("Another user is editing IIFs, try again later"));
		mainWindow.showUserInput();
	}
}

void DonorScreen::mailEditChanged()
{
	saveButton->setEnabled(true);
}

void DonorScreen::noteEditTextChanged()
{
	saveButton->setEnabled(true);
}

void DonorScreen::saveClicked()
{
	if (newFlag) {
		donorListView->clearSearchFilter();
		QJsonDocument jdoc;
		QJsonObject json;
		donorInfo->buildJson(json);
		jdoc.setObject(json);
		cm.foxmq.post("donors.add", FoxHead::ContentType::Json, jdoc.toJson(QJsonDocument::Compact));
		QJsonObject jMail;
		donorMail->buildJson(jMail);
		jdoc.setObject(jMail);
		cm.foxmq.post("donors.mail-edit", FoxHead::ContentType::Json, jdoc.toJson(QJsonDocument::Compact));
	} else {
		if (donorInfo->isModified) {
			QJsonDocument jdoc;
			QJsonObject json;
			json.insert("id", donorUnderEdit);
			donorInfo->buildJson(json);
			jdoc.setObject(json);
			cm.foxmq.post("donors.info-edit", FoxHead::ContentType::Json, jdoc.toJson(QJsonDocument::Compact));
		}
		if (noteEdit->document()->isModified()) {
			QJsonDocument jdoc;
			QJsonObject json;
			json.insert("id", donorUnderEdit);
			json.insert("log", noteEdit->toPlainText());
			jdoc.setObject(json);
			cm.foxmq.post("donors.note-edit", FoxHead::ContentType::Json, jdoc.toJson(QJsonDocument::Compact));
		}
		if (donorMail->isModified) {
			QJsonDocument jdoc;
			QJsonObject jMail;
			jMail.insert("id", donorUnderEdit);
			donorMail->buildJson(jMail);
			jdoc.setObject(jMail);
			cm.foxmq.post("donors.mail-edit", FoxHead::ContentType::Json, jdoc.toJson(QJsonDocument::Compact));
		}
	}
	if (newFlag)
		selectLastDonor = true;
	doneEdits();
}

void DonorScreen::newClicked()
{
	donorListView->clearIndexAndSelection();
	donorUnderEdit = -1;
	newFlag = true;
	donorInfo->clear();
	donorMail->clear();
	donorInfo->setReadOnly(false);
	donorMail->setReadOnly(false);
	content->setCurrentIndex(Content::Edit);
	topButtons->setEnabled(TopBottons::Summary, false);
	topButtons->setEnabled(TopBottons::Info, true);
	topButtons->setEnabled(TopBottons::Notes, false);
	topButtons->setEnabled(TopBottons::Account, false);
	topButtons->setEnabled(TopBottons::Receipts, false);
	newButton->setEnabled(false);
	editEnableButton->setEnabled(false);
	cancelButton->setEnabled(true);
	topButtons->setChecked(TopBottons::Info, true);
}

void DonorScreen::cancelClicked()
{
	bool wasNew = newFlag;
	if (!checkForSave() && wasNew) {
		content->setCurrentIndex(Content::SelectDonor);
		donorListView->clearIndexAndSelection();
	}
}

void DonorScreen::doneEdits()
{
	disconnect(noteEdit->document(), &QTextDocument::modificationChanged, this, &DonorScreen::noteEditTextChanged);
	saveButton->setEnabled(false);
	noteEdit->setReadOnly(true);
	donorInfo->setReadOnly(true);
	donorMail->setReadOnly(true);
	donorInfo->isModified = false;
	donorMail->isModified = false;
	newButton->setEnabled(true);
	cancelButton->setEnabled(false);
	noteEdit->document()->setModified(false);
	if (!newFlag) {
		enableTopButtons(true);
		editEnableButton->setEnabled(true);
		cm.foxmq.post("donors.unlock", FoxHead::ContentType::IntAsString, QByteArray::number(donorUnderEdit));
	} else {
		enableTopButtons(false);
	}
	newFlag = false;
}

void DonorScreen::okFromLockNack()
{
	disconnect(mainWindow.userInput, &UserInput::button1Clicked, this, &DonorScreen::okFromLockNack);
	mainWindow.showMainScreen();
}

void DonorScreen::okFromIifAppendError()
{
	disconnect(mainWindow.userInput, &UserInput::button1Clicked, this, &DonorScreen::okFromIifAppendError);
	mainWindow.showMainScreen();
}

void DonorScreen::accountTimePeriodButtonClicked(int index)
{
	if (indexOfTimePeriodButtonSelected != index) {
		indexOfTimePeriodButtonSelected = index;
		sendRequestForAccountUpdate();
	}
}

void DonorScreen::sendRequestForAccountUpdate()
{
	int financialPeriod;
	switch (indexOfTimePeriodButtonSelected) {  // clang-format off
		case 0: financialPeriod = FinancialSpan::YTD;              break;
		case 1: financialPeriod = FinancialSpan::Previous365Days;  break;
		case 2: financialPeriod = FinancialSpan::PreviousYear;     break;
		case 3: financialPeriod = FinancialSpan::Previous3Years;   break;
		default:
		case 4: financialPeriod = FinancialSpan::All;              break;
	}  // clang-format on

	QJsonDocument jdoc;
	QJsonObject json;
	json.insert("id", donorUnderEdit);
	json.insert("financial-period", financialPeriod);
	jdoc.setObject(json);
	cm.foxmq.post("donors.ro.account", FoxHead::ContentType::Json, jdoc.toJson(QJsonDocument::Compact));
}

void DonorScreen::accountVoidClicked()
{
	QString msg;
	if (donorAccount->currentSelectionIsVoid())
		msg = QString(tr("Are you sure you want to Unvoid Donation %1")).arg(donorAccount->selectedDonationId);
	else
		msg = QString(tr("Are you sure you want to Void Donation %1")).arg(donorAccount->selectedDonationId);
	connect(mainWindow.userInput, &UserInput::button1Clicked, this, &DonorScreen::voidDonationConfirmedClicked);
	connect(mainWindow.userInput, &UserInput::button2Clicked, this, &DonorScreen::voidDonationDiscardClicked);
	mainWindow.userInput->askForInput(msg, tr("Yes"), tr("No"));
	mainWindow.showUserInput();
}

void DonorScreen::voidDonationConfirmedClicked()
{
	QJsonDocument jdoc;
	QJsonObject json;
	json.insert("donor-id", donorUnderEdit);
	json.insert("donation-id", donorAccount->selectedDonationId);
	json.insert("voided", !donorAccount->currentSelectionIsVoid());
	jdoc.setObject(json);
	cm.foxmq.post("donations.void", FoxHead::ContentType::Json, jdoc.toJson(QJsonDocument::Compact));
	voidDonationDiscardClicked();
}

void DonorScreen::voidDonationDiscardClicked()
{
	disconnect(mainWindow.userInput, &UserInput::button1Clicked, this, nullptr);
	disconnect(mainWindow.userInput, &UserInput::button2Clicked, this, &DonorScreen::voidDonationDiscardClicked);
	mainWindow.showMainScreen();
}

void DonorScreen::keyPressEvent(QKeyEvent *e)
{
	if (content->currentIndex() == Content::Acconut && e->type() == QKeyEvent::KeyPress && e->matches(QKeySequence::Copy))
		donorAccount->copySelectionToClipBoard();
}

void DonorScreen::receiptDownloadClicked(int receiptId)
{
	cm.foxmq.post("donors.ro.receipt", FoxHead::ContentType::IntAsString, QByteArray::number(receiptId));
}

void DonorScreen::saveReceiptSelected(QString fn)
{
	PDFWriter::write(fn, receiptToSave);
	receiptToSave.clear();

	QFile newPdf(fn);
	if (newPdf.open(QIODevice::ReadOnly)) {
		receiptSignatureFileName = fn + ".signature";
		QByteArray hash = QCryptographicHash::hash(newPdf.readAll(), QCryptographicHash::RealSha3_256);
		QJsonDocument jdoc;
		QJsonObject json;
		json.insert("hash", QString(hash.toBase64()));
		json.insert("year", donorReceiptsWidget->yearOfLastSelectedReceipt);
		jdoc.setObject(json);
		cm.foxmq.post("donors.ro.sign-receipt", FoxHead::ContentType::Json, jdoc.toJson(QJsonDocument::Compact));
	}
}

void DonorScreen::saveReceiptSelectorFinished()
{
	mainWindow.setEnabled(true);
	saveFileDialog.clear();
}

void DonorScreen::accountIifButtonClicked()
{
	QString msg;
	if (donorAccount->hasIif())
		msg = QString(tr("Are you sure you want to Ungroup Donation %1 from IIF %2?")).arg(donorAccount->selectedDonationId).arg(donorAccount->iif());
	else
		msg = QString(tr("Are you sure you want to add Donation %1 to IIF %2?")).arg(donorAccount->selectedDonationId).arg(donorAccount->enteredIif());
	connect(mainWindow.userInput, &UserInput::button1Clicked, this, &DonorScreen::accountIifButtonConfirmedClicked);
	connect(mainWindow.userInput, &UserInput::button2Clicked, this, &DonorScreen::voidDonationDiscardClicked);
	mainWindow.userInput->askForInput(msg, tr("Yes"), tr("No"));
	mainWindow.showUserInput();
}

void DonorScreen::accountIifButtonConfirmedClicked()
{
	QJsonDocument jdoc;
	QJsonObject json;
	json.insert("donor-id", donorUnderEdit);
	json.insert("donation-id", donorAccount->selectedDonationId);
	QByteArray topic;
	if (!donorAccount->hasIif()) {
		json.insert("iif", donorAccount->enteredIif());
		topic = "iif.mark-append";
	} else {
		topic = "iif.unmark-donation";
	}
	jdoc.setObject(json);
	cm.foxmq.post(topic, FoxHead::ContentType::Json, jdoc.toJson(QJsonDocument::Compact));
	voidDonationDiscardClicked();
}
