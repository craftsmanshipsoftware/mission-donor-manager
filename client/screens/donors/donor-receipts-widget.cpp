// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QVBoxLayout>
#include <QTableView>
#include <QHeaderView>
#include <QPushButton>
#include "theme.h"
#include "styler.h"
#include "donors-screen.h"
#include "donor-receipts-widget.h"

DonorReceiptsWidget::DonorReceiptsWidget(DonorScreen &donorScreen) : donorScreen(donorScreen)
{
	QVBoxLayout *layout = new QVBoxLayout;
	setLayout(layout);

	donorReceiptsView = new QTableView;
	Styler::styleTableView(donorReceiptsView);
	donorReceiptsView->setModel(&donorReceiptsDataModel);
	donorReceiptsView->setFocusPolicy(Qt::FocusPolicy::NoFocus);
	donorReceiptsView->verticalHeader()->hide();
	donorReceiptsView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	donorReceiptsView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
	donorReceiptsView->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
	donorReceiptsView->setAlternatingRowColors(true);
	donorReceiptsView->setSelectionMode(QAbstractItemView::SingleSelection);
	layout->addWidget(donorReceiptsView);
	layout->setAlignment(donorReceiptsView, Qt::AlignHCenter);

	downloadButton = new QPushButton(tr("Download PDF"));
	downloadButton->setEnabled(false);
	Styler::styleButton(downloadButton);
	layout->addWidget(downloadButton);
	layout->setAlignment(downloadButton, Qt::AlignHCenter);

	connect(donorReceiptsView, &QTableView::clicked, this, &DonorReceiptsWidget::receiptViewClicked);
	connect(downloadButton, &QPushButton::clicked, this, &DonorReceiptsWidget::downloadClicked);
}

void DonorReceiptsWidget::setReceipts(const QByteArray &receipts)
{
	donorReceiptsDataModel.setReceipts(receipts);
	donorReceiptsView->resizeColumnsToContents();
}

void DonorReceiptsWidget::receiptViewClicked(QModelIndex index)
{
	selectedReceiptIndex = index;
	int row = index.row();
	selectedReceiptId = donorReceiptsDataModel.getId(row);
	yearOfLastSelectedReceipt = donorReceiptsDataModel.getYear(row);
	downloadButton->setEnabled(true);
}

void DonorReceiptsWidget::downloadClicked()
{
	donorScreen.receiptDownloadClicked(selectedReceiptId);
	downloadButton->setEnabled(false);
}
