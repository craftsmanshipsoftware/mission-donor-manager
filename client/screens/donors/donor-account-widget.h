// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef MDM_DONOR_ACCOUNT_H_
#define MDM_DONOR_ACCOUNT_H_

#include <QWidget>
#include <QLineEdit>
#include "donor-account-dm.h"
#include "../common/permission.h"

class QLabel;
class QTableView;
class QExclusiveButtons;
class QPushButton;

class DonorAccount : public QWidget
{
	Q_OBJECT

public:
	DonorAccount(Permission &p);
	void setDonations(const QByteArray &donations);
	inline bool currentSelectionIsVoid() { return donorAccountDataModel.isVoid(selectedDonationIndex.row()); }
	void copySelectionToClipBoard();
	int enteredIif() { return iifEntry->text().toInt(); }
	bool hasIif() { return donorAccountDataModel.hasIif(selectedDonationIndex.row()); }
	int iif() { return donorAccountDataModel.iif(selectedDonationIndex.row()); }
	QTableView *donorAccountView;
	QExclusiveButtons *timePeriodButtons;
	int selectedDonationId = -1;

signals:
	void voidButtonClicked();
	void iifButtonClicked();

private:
	void accountViewClicked(QModelIndex index);
	void accountViewPressed(QModelIndex index);
	bool permissionForVoiding();
	QLabel *noActivity;
	QLabel *note;
	DonorAccountDataModel donorAccountDataModel;
	QPushButton *voidButton;
	QPushButton *iifButton;
	QLineEdit *iifEntry;
	QModelIndex selectedDonationIndex;
	Permission prm;
};

#endif
