// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QLabel>
#include <QTableView>
#include <QHeaderView>
#include <QJsonDocument>
#include <QJsonObject>
#include "theme.h"
#include "styler.h"
#include "qexclusivebuttons.h"
#include "copy-and-paste.h"
#include "donor-account-widget.h"

DonorAccount::DonorAccount(Permission &p) : prm(p)
{
	QVBoxLayout *layout = new QVBoxLayout;
	setLayout(layout);

	timePeriodButtons = new QExclusiveButtons({"    YTD    ", "Past 365 Days", " Last Year ", "Last 3 Years", "    All    "});
	timePeriodButtons->buttonAt(0)->setChecked(true);
	for (int i=0; i<timePeriodButtons->size(); i++)
		Styler::styleButton(timePeriodButtons->buttonAt(i));
	layout->addWidget(timePeriodButtons);

	noActivity = new QLabel(tr("No Financial Activity"));
	Styler::styleLabel(noActivity);
	layout->addWidget(noActivity);
	layout->setAlignment(noActivity, Qt::AlignHCenter);
	donorAccountView = new QTableView;
	Styler::styleTableView(donorAccountView);
	donorAccountView->setModel(&donorAccountDataModel);
	donorAccountView->verticalHeader()->hide();
	donorAccountView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	donorAccountView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
	donorAccountView->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
	donorAccountView->setAlternatingRowColors(true);
	layout->addWidget(donorAccountView);
	layout->setAlignment(donorAccountView, Qt::AlignHCenter);

	QHBoxLayout *bottomButtonsLayout = new QHBoxLayout;
	layout->addLayout(bottomButtonsLayout);

	bottomButtonsLayout->addStretch(1);
	voidButton = new QPushButton(tr("Void Donation"));
	Styler::styleButton(voidButton);
	bottomButtonsLayout->addWidget(voidButton);
	voidButton->setEnabled(false);
	if (!permissionForVoiding())
		voidButton->hide();

	iifButton = new QPushButton(tr("Add to IIF"));
	Styler::styleButton(iifButton);
	bottomButtonsLayout->addWidget(iifButton);
	iifButton->setEnabled(false);

	iifEntry = new QLineEdit;
	Styler::styleLineEdit(iifEntry);
	iifEntry->setValidator(new QIntValidator);
	bottomButtonsLayout->addWidget(iifEntry);
	if (!prm.hasPermission(Permission::IifExport)) {
		iifButton->hide();
		iifEntry->hide();
	}
	bottomButtonsLayout->addStretch(1);

	note = new QLabel();
	Styler::styleLabel(note);
	layout->addWidget(note);
	layout->setAlignment(note, Qt::AlignHCenter);

	connect(donorAccountView, &QTableView::clicked, this, &DonorAccount::accountViewClicked);
	connect(donorAccountView, &QTableView::pressed, this, &DonorAccount::accountViewPressed);
	connect(voidButton, &QPushButton::clicked, this, &DonorAccount::voidButtonClicked);
	connect(iifButton, &QPushButton::clicked, this, &DonorAccount::iifButtonClicked);

	layout->addStretch(1);
}

void DonorAccount::setDonations(const QByteArray &donations)
{
	donorAccountView->setCurrentIndex(QModelIndex());  // clear selected index if copy-and-paste is attempted after view is refreshed with smaller model.
	donorAccountDataModel.setDonations(donations);
	donorAccountView->resizeColumnsToContents();
	selectedDonationId = -1;
	voidButton->setEnabled(false);
	iifButton->setEnabled(false);
	iifEntry->setEnabled(false);
	iifEntry->clear();
	if (donorAccountDataModel.rowCount()) {
		noActivity->hide();
		donorAccountView->show();
		if (permissionForVoiding())
			voidButton->show();
		if (prm.hasPermission(Permission::IifExport)) {
			iifButton->show();
			iifEntry->show();
		}
	} else {
		noActivity->show();
		donorAccountView->hide();
		voidButton->hide();
		iifButton->hide();
		iifEntry->hide();
	}
}

void DonorAccount::accountViewClicked(QModelIndex index)
{
	selectedDonationIndex = index;
	int row = index.row();
	selectedDonationId = donorAccountDataModel.getId(row);
	voidButton->setEnabled(true);
	iifButton->setEnabled(true);
	if (donorAccountDataModel.isVoid(row))
		voidButton->setText(tr("UnVoid Donation"));
	else
		voidButton->setText(tr("Void Donation"));

	if (!donorAccountDataModel.hasIif(row)) {
		iifButton->setText(tr("Add to IIF"));
		iifEntry->setEnabled(true);
	} else {
		iifButton->setText(tr("Remove From IIF"));
	}
	note->setText(donorAccountDataModel.getNote(row));
}

void DonorAccount::accountViewPressed(QModelIndex)
{
	voidButton->setEnabled(false);
	iifButton->setEnabled(false);
	iifEntry->setEnabled(false);
}

void DonorAccount::copySelectionToClipBoard()
{
	copyFromTable(donorAccountView->selectionModel()->selectedIndexes());
}

bool DonorAccount::permissionForVoiding()
{
	return (prm.hasPermission(Permission::Donations) || prm.hasPermission(Permission::RecurringDonations));
}
