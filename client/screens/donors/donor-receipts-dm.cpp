// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include "../common/common.h"
#include "donor-receipts-dm.h"

void DonorReceiptsDataModel::setReceipts(const QByteArray &data)
{
	layoutAboutToBeChanged();
	receipts.clear();
	QJsonArray jArray = QJsonDocument::fromJson(data).array();
	for (auto jIt : jArray) {
		QJsonObject json = jIt.toObject();
		DonorReceipttItem item;
		item.receiptId = json.value("id").toInt();
		item.year = json.value("year").toInt();
		item.type = json.value("type").toInt();
		item.dateIssued = json.value("date-issued").toInt();
		item.amount = json.value("amount").toInt();
		receipts.append(item);
	}
	layoutChanged();
}

int DonorReceiptsDataModel::rowCount(const QModelIndex &) const
{
	return receipts.size();
}

int DonorReceiptsDataModel::columnCount(const QModelIndex &) const
{
	return 5;
}

QVariant DonorReceiptsDataModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	QVariant text;

	if (role != Qt::DisplayRole)
		return text;

	if (orientation == Qt::Horizontal) {
		switch(section) {  // clang-format off
			case 0:  text = QString(tr("Tax Year"));         break;
			case 1:  text = QString(tr("Receipt Number"));   break;
			case 2:  text = QString(tr("Type"));             break;
			case 3:  text = QString(tr("Date Issued"));      break;
			case 4:  text = QString(tr("Amount"));           break;
		}  // clang-format on
	}

	return text;
}

QVariant DonorReceiptsDataModel::data(const QModelIndex &index, int role) const
{
	QVariant item;
	QString typeString;
	if (role == Qt::DisplayRole) {
		switch(index.column()) {  // clang-format off
			case 0: item = receipts.at(index.row()).year; break;
			case 1: item = receipts.at(index.row()).receiptId; break;
			case 2: item = receipts.at(index.row()).type == DonationType::Inkind ? tr("Inkind") : ""; break;
			case 3: item = QDate::fromJulianDay(receipts.at(index.row()).dateIssued).toString("MMM d yyyy"); break;
			case 4: item = convertCentsToString(receipts.at(index.row()).amount, true); break;
		}  // clang-format on
	}
	return item;
}
