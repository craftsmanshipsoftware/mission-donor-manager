// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef MDM_DONOR_EDIT_H_
#define MDM_DONOR_EDIT_H_

#include <QWidget>

class DonorScreen;
class QLineEdit;
class QRadioButton;
class QCheckBox;

class DonorInfo : public QWidget
{
	Q_OBJECT

public:
	DonorInfo();
	void clear();
	void setReadOnly(bool ro);
	void buildJson(QJsonObject &json);
	void populateValues(QJsonObject json);
	bool isReadOnly() { return readOnly; }
	bool isModified = false;

signals:
	void changed();

private:
	void flagChanged();
	QLineEdit *first;
	QLineEdit *last;
	QLineEdit *salutation;
	QLineEdit *addressSalutation;
	QLineEdit *otherName;
	QLineEdit *address;
	QLineEdit *apt;
	QLineEdit *city;
	QLineEdit *province;
	QLineEdit *country;
	QLineEdit *postalCode;
	QLineEdit *email;
	QLineEdit *cellPhone;
	QLineEdit *homePhone;
	QLineEdit *workPhone;
	QLineEdit *importedId;
	QLineEdit *dateAdded;
	QLineEdit *contact;
	QLineEdit *ppa0;
	QLineEdit *ppa1;
	QLineEdit *ppa2;
	QLineEdit *ppa3;
	QLineEdit *ppa4;
	QLineEdit *ppa0Last;
	QLineEdit *ppa1Last;
	QLineEdit *ppa2Last;
	QLineEdit *ppa3Last;
	QLineEdit *ppa4Last;
	QRadioButton *typePerson;
	QRadioButton *typeCorporation;
	QRadioButton *typeGovernmentAgency;
	QRadioButton *typeCharity;
	QRadioButton *typeForeignCharity;
	QRadioButton *typeNonTaxPayer;
	QRadioButton *typeFoundation;
	QCheckBox *is3rdParty;
	QCheckBox *deceased;

	int type = 0;
	bool readOnly = true;
};

#endif
