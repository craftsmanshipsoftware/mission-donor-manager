// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QVBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QGroupBox>
#include <QRadioButton>
#include <QCheckBox>
#include <QJsonObject>
#include "../common/common.h"
#include "styler.h"
#include "theme.h"
#include "donors-screen.h"
#include "donor-mail-widget.h"

DonorMail::DonorMail()
{
	QVBoxLayout *layout = new QVBoxLayout;
	setLayout(layout);

	layout->addStretch(4);

	newsletter = new QCheckBox(tr("Receive Newsletter"));
	Styler::styleCheckBox(newsletter);
	layout->addWidget(newsletter);
	layout->setAlignment(newsletter, Qt::AlignHCenter);

	layout->addStretch(1);

//------- Tax Receipt delivery options
	QGroupBox *taxReceiptGroup = new QGroupBox(tr("Tax Receipt Delivery"));
	Styler::styleGroupBox(taxReceiptGroup);
	layout->addWidget(taxReceiptGroup);
	layout->setAlignment(taxReceiptGroup, Qt::AlignHCenter);
	QHBoxLayout *taxReceiptGroupLayout = new QHBoxLayout;
	taxReceiptGroup->setLayout(taxReceiptGroupLayout);

	taxReceiptBySnailMail = new QCheckBox(tr("Snail Mail"));
	Styler::styleCheckBox(taxReceiptBySnailMail);
	taxReceiptGroupLayout->addWidget(taxReceiptBySnailMail);

	taxReceiptByEmail = new QCheckBox(tr("Email"));
	Styler::styleCheckBox(taxReceiptByEmail);
	taxReceiptGroupLayout->addWidget(taxReceiptByEmail);

	layout->addStretch(1);

//------- Paypal thank you
	QGroupBox *thankyouPaypalGroup = new QGroupBox(tr("Paypal Web Page Donations"));
	Styler::styleGroupBox(thankyouPaypalGroup);
	layout->addWidget(thankyouPaypalGroup);
	layout->setAlignment(thankyouPaypalGroup, Qt::AlignHCenter);
	QVBoxLayout *thankyouPaypalGroupLayout = new QVBoxLayout;
	thankyouPaypalGroup->setLayout(thankyouPaypalGroupLayout);

	thankyouPaypalOneTime = new QCheckBox(tr("Enable Thank you for a One Time donation"));
	Styler::styleCheckBox(thankyouPaypalOneTime);
	thankyouPaypalGroupLayout->addWidget(thankyouPaypalOneTime);

	thankyouPaypalRecurring = new QCheckBox(tr("Enable Thank you for a Recurring donation"));
	Styler::styleCheckBox(thankyouPaypalRecurring);
	thankyouPaypalGroupLayout->addWidget(thankyouPaypalRecurring);

	layout->addStretch(1);

//------ Recurring donation thankyou
	auto addThankyouOption = [](const QString &name, QHBoxLayout *layout) {
		QRadioButton *option = new QRadioButton(name);
		Styler::styleRadioButton(option);
		layout->addWidget(option);
		return option;
	};

	QGroupBox *thankyouPreAuthGroup = new QGroupBox(tr("Recurring Thankyou"));
	Styler::styleGroupBox(thankyouPreAuthGroup);
	layout->addWidget(thankyouPreAuthGroup);
	layout->setAlignment(thankyouPreAuthGroup, Qt::AlignHCenter);
	QHBoxLayout *thankyouPreAuthGroupLayout = new QHBoxLayout;
	thankyouPreAuthGroup->setLayout(thankyouPreAuthGroupLayout);
	thankyouPreAuthEmail = addThankyouOption("Email", thankyouPreAuthGroupLayout);
	thankyouPreAuthMail = addThankyouOption("Snail Mail", thankyouPreAuthGroupLayout);
	thankyouPreAuthNone = addThankyouOption("None", thankyouPreAuthGroupLayout);

	layout->addStretch(1);

//------ Manual donation thankyou
	QGroupBox *thankyouManualGroup = new QGroupBox(tr("Manual Donation Thankyou"));
	Styler::styleGroupBox(thankyouManualGroup);
	layout->addWidget(thankyouManualGroup);
	layout->setAlignment(thankyouManualGroup, Qt::AlignHCenter);
	QHBoxLayout *thankyouManualGroupLayout = new QHBoxLayout;
	thankyouManualGroup->setLayout(thankyouManualGroupLayout);
	thankyouManualEmail = addThankyouOption("Email", thankyouManualGroupLayout);
	thankyouManualMail = addThankyouOption("Snail Mail", thankyouManualGroupLayout);
	thankyouManualNone = addThankyouOption("None", thankyouManualGroupLayout);

	layout->addStretch(4);

	connect(newsletter, &QCheckBox::clicked, this, &DonorMail::flagChanged);
	connect(taxReceiptBySnailMail, &QCheckBox::clicked, this, &DonorMail::flagChanged);
	connect(taxReceiptByEmail, &QCheckBox::clicked, this, &DonorMail::flagChanged);
	connect(thankyouManualMail, &QCheckBox::clicked, this, &DonorMail::flagChanged);
	connect(thankyouManualEmail, &QCheckBox::clicked, this, &DonorMail::flagChanged);
	connect(thankyouManualNone, &QCheckBox::clicked, this, &DonorMail::flagChanged);
	connect(thankyouPreAuthMail, &QCheckBox::clicked, this, &DonorMail::flagChanged);
	connect(thankyouPreAuthEmail, &QCheckBox::clicked, this, &DonorMail::flagChanged);
	connect(thankyouPreAuthNone, &QCheckBox::clicked, this, &DonorMail::flagChanged);
	connect(thankyouPaypalOneTime, &QCheckBox::clicked, this, &DonorMail::flagChanged);
	connect(thankyouPaypalRecurring, &QCheckBox::clicked, this, &DonorMail::flagChanged);
}

void DonorMail::flagChanged()
{
	if (!isModified) {
		isModified = true;
		emit changed();
	}
}

void DonorMail::clear()
{
	newsletter->setChecked(false);
	taxReceiptBySnailMail->setChecked(false);
	taxReceiptByEmail->setChecked(false);
	thankyouPreAuthNone->setChecked(false);
	thankyouPreAuthEmail->setChecked(false);
	thankyouPreAuthMail->setChecked(false);
	thankyouManualNone->setChecked(false);
	thankyouManualEmail->setChecked(false);
	thankyouManualMail->setChecked(false);
	thankyouPaypalOneTime->setChecked(false);
	thankyouPaypalRecurring->setChecked(false);
}

void DonorMail::setReadOnly(bool ro)
{
	readOnly = ro;
	newsletter->setEnabled(!ro);
	taxReceiptBySnailMail->setEnabled(!ro);
	taxReceiptByEmail->setEnabled(!ro);
	thankyouPreAuthNone->setEnabled(!ro);
	thankyouPreAuthEmail->setEnabled(!ro);
	thankyouPreAuthMail->setEnabled(!ro);
	thankyouManualNone->setEnabled(!ro);
	thankyouManualEmail->setEnabled(!ro);
	thankyouManualMail->setEnabled(!ro);
	thankyouPaypalOneTime->setEnabled(!ro);
	thankyouPaypalRecurring->setEnabled(!ro);
}

void DonorMail::populateValues(int mailOptions)
{
	isModified = false;

	newsletter->setChecked(mailOptions & Mail::NewsLetter);
	taxReceiptBySnailMail->setChecked(mailOptions & Mail::TaxReceiptBySnailMail);
	taxReceiptByEmail->setChecked(mailOptions & Mail::TaxReceiptByEmail);
	thankyouPreAuthNone->setChecked(!(mailOptions & (Mail::ThankyouPreAuthByEmail | Mail::ThankyouPreAuthByMail)));
	thankyouPreAuthEmail->setChecked(mailOptions & Mail::ThankyouPreAuthByEmail);
	thankyouPreAuthMail->setChecked(mailOptions & Mail::ThankyouPreAuthByMail);
	thankyouManualNone->setChecked(!(mailOptions & (Mail::ThankyouManualByEmail | Mail::ThankyouManualByEmail)));
	thankyouManualEmail->setChecked(mailOptions & Mail::ThankyouManualByEmail);
	thankyouManualMail->setChecked(mailOptions & Mail::ThankyouManualByMail);
	thankyouPaypalOneTime->setChecked(mailOptions & Mail::ThankyouPaypalOneTime);
	thankyouPaypalRecurring->setChecked(mailOptions & Mail::ThankyouPaypalRecurring);
}

void DonorMail::buildJson(QJsonObject &json)
{
	int mailOptions = 0;
	if (newsletter->isChecked())
		mailOptions|= Mail::NewsLetter;
	if (taxReceiptBySnailMail->isChecked())
		mailOptions|= Mail::TaxReceiptBySnailMail;
	if (taxReceiptByEmail->isChecked())
		mailOptions|= Mail::TaxReceiptByEmail;
	if (thankyouPreAuthEmail->isChecked())
		mailOptions|= Mail::ThankyouPreAuthByEmail;
	if (thankyouPreAuthMail->isChecked())
		mailOptions|= Mail::ThankyouPreAuthByMail;
	if (thankyouManualEmail->isChecked())
		mailOptions|= Mail::ThankyouManualByEmail;
	if (thankyouManualMail->isChecked())
		mailOptions|= Mail::ThankyouManualByMail;
	if (thankyouPaypalOneTime->isChecked())
		mailOptions|= Mail::ThankyouPaypalOneTime;
	if (thankyouPaypalRecurring->isChecked())
		mailOptions|= Mail::ThankyouPaypalRecurring;
	json.insert("news", mailOptions);
}
