// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QLabel>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include "theme.h"
#include "donor-summary-widget.h"

DonorSummary::DonorSummary()
{

}

void DonorSummary::update(const QByteArray &content)
{
	auto addLabel = [&](const QString text, int row, int col, float percentChange = 0.0f) {
		QLabel *label = new QLabel(text);
		if (percentChange > 0.0f)
			label->setStyleSheet(QString("QLabel {color: rgb(0, 255, 0); font-size: 12pt;}"));
		else if (percentChange < 0.0f)
			label->setStyleSheet(QString("QLabel {color: rgb(255, 0, 0); font-size: 12pt;}"));
		else
			label->setStyleSheet(QString("QLabel {color: rgb(%1, %2, %3); font-size: 12pt; border: 0;}").arg(Theme::Text.red()).arg(Theme::Text.green()).arg(Theme::Text.blue()));
		label->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
		layout->addWidget(label, row, col);
		//layout.setAlignment(askSelectLabel, Qt::AlignHCenter);
	};

	qDeleteAll(findChildren<QWidget *>(QString(), Qt::FindDirectChildrenOnly));
	layout = QSharedPointer<QGridLayout>::create();
	setLayout(layout.data());
	addLabel(tr("Quarter\nTotal"), 0, 1);
	addLabel(tr("YoY"), 0, 2);

	QJsonArray jList = QJsonDocument::fromJson(content).array();
	int i = 1;
	for (auto json : jList) {
		QJsonObject jQuarter = json.toObject();
		addLabel(jQuarter.value("quarter").toString(), i, 0);
		addLabel(QString("$%1").arg(jQuarter.value("amount").toDouble() / 100), i, 1);
		float percentChange = jQuarter.value("percent-change").toDouble();
		addLabel(QString("%1%").arg(percentChange * 100, 0, 'f', 1), i, 2, percentChange);
		i++;
	}
}
