// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef MDM_DONOR_SCREEN_H_
#define MDM_DONOR_SCREEN_H_

#include <QModelIndex>
#include "tab-screen.h"
#include "receiver.h"

class QPushButton;
class QSlidingStackedWidget;
class DonorSummary;
class DonorInfo;
class DonorMail;
class DonorAccount;
class DonorReceiptsWidget;
class QPlainTextEdit;
class MainWindow;
class ConnectionManager;
class QLineEdit;
class QListView;
class QSortFilterProxyModel;
class QFileDialog;
class QExclusiveButtons;
class Permission;
class NameSelectorWidget;

class QLabel;  // hack for now

class DonorScreen : public TabScreen, public Receiver
{
	Q_OBJECT

public:
	DonorScreen(MainWindow &mainWindow, Permission &p);
	void subscribe() override;
	void unsubscribe() override;
	void receiptDownloadClicked(int receiptId);

private:
	void processPublish(const QByteArray &topic, ssize_t count, FoxID id, FoxHead::ContentType contentType, const QByteArray &content) override;
	void processPostResponce(const QByteArray &topic, FoxID id, FoxHead::ContentType contentType, const QByteArray &content) override;
	void processAck(const QByteArray &topic) override;
	void processNack(const QByteArray &topic) override;
	void saveClicked();
	void newClicked();
	void cancelClicked();
	void mailEditChanged();
	void noteEditTextChanged();
	void donorSelected(int nameId, QModelIndex index);
	void queryAsPerCurrentTab();
	void topButtonClicked(int index);
	void doneEdits();
	void enableTopButtons(bool enabled);
	void okFromLockNack();
	void okFromIifAppendError();
	bool checkForSave();
	void saveConfirmSaveClicked();
	void saveConfirmDiscardClicked();
	void saveConfirmCleanUp();
	void accountTimePeriodButtonClicked(int index);
	void sendRequestForSummaryUpdate();
	void sendRequestForInfoUpdate();
	void sendRequestForMailUpdate();
	void sendRequestForNotesUpdate();
	void sendRequestForReceipts();
	void sendRequestForAccountUpdate();
	void accountVoidClicked();
	void voidDonationConfirmedClicked();
	void voidDonationDiscardClicked();
	void saveReceiptSelected(QString fn);
	void saveReceiptSelectorFinished();
	void keyPressEvent(QKeyEvent *e) override;
	void accountIifButtonClicked();
	void accountIifButtonConfirmedClicked();
	MainWindow &mainWindow;
	ConnectionManager &cm;
	QPushButton *newButton;
	QPushButton *editEnableButton;
	QPushButton *saveButton;
	QPushButton *cancelButton;
	QExclusiveButtons *topButtons;
	QSlidingStackedWidget *content;
	DonorSummary *donorSummary;
	DonorInfo *donorInfo;
	DonorMail *donorMail;
	QPlainTextEdit *noteEdit;
	DonorAccount *donorAccount;
	DonorReceiptsWidget *donorReceiptsWidget;
	NameSelectorWidget *donorListView;
	QSharedPointer<QFileDialog> saveFileDialog;
	QByteArray receiptToSave;
	QString receiptSignatureFileName;
	int donorUnderEdit = -1;
	QModelIndex indexOfDonorUnderEdit;
	int indexOfTimePeriodButtonSelected = 0;
	bool newFlag = false;
	bool selectLastDonor = false;
};

#endif
