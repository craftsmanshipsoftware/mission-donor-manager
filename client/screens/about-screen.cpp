// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QVBoxLayout>
#include <QPushButton>
#include <QLabel>
#include <QIcon>
#include <QCloseEvent>

#include "qcolorablesvg.h"
#include "theme.h"
#include "styler.h"
#include "version.h"
#include "about-screen.h"

AboutScreen::AboutScreen()
{
	QVBoxLayout *layout = new QVBoxLayout;

	auto addLabel = [&](const QString &text, int size) {
		QLabel *label = new QLabel(text);
		Styler::styleLabel(label, size);
		layout->addWidget(label);
		layout->setAlignment(label, Qt::AlignHCenter);;
	};

	auto addLabelWithUrl = [&](const QString &text) {
		QLabel *label = new QLabel(text);
		label->setTextFormat(Qt::RichText);
		label->setTextInteractionFlags(Qt::TextBrowserInteraction);
		label->setOpenExternalLinks(true);
		Styler::styleLabel(label);
		layout->addWidget(label);
		layout->setAlignment(label, Qt::AlignHCenter);
	};

	setLayout(layout);
	Styler::styleMainWidget(this);

	layout->addStretch(10);

	addLabel(QString("Mission Donor Manager - v%1").arg(VERSION), Theme::H1Size);

	layout->addStretch(1);

	addLabelWithUrl(tr("Mission Donor Manager is free and open source software licensed under the <a href=\"https://www.gnu.org/licenses/gpl-3.0.en.html\">GPLv3</a>"));
	addLabelWithUrl(tr("written by Craftsmanship Software Inc."));
	addLabelWithUrl(tr("The source code for Mission Donor Manager can be found <a href=\"https://gitlab.com/craftsmanshipsoftware/mission-donor-manager\">here</a>"));

	layout->addStretch(2);

	addLabel(tr("Mission Donor Manager uses the following open source libraries"), Theme::H2Size);
	addLabelWithUrl(tr("<a href=\"https://contribute.qt-project.org/\">Qt Project</a> <a href=\"https://code.qt.io/cgit/qt/qt5.git\">Source code</a>"));
	addLabelWithUrl(tr("<a href=\"https://www.openssl.org/\">OpenSSL</a> <a href=\"https://github.com/openssl/openssl.git\">Source code</a>"));
	addLabelWithUrl(tr("FoxMQ <a href=\"https://gitlab.com/craftsmanshipsoftware/foxmq\">Source code</a>"));
	addLabelWithUrl(tr("QtCraftsmanshipWidgets <a href=\"https://gitlab.com/craftsmanshipsoftware/qtcraftsmanshipwidgets\">Source code</a>"));
	addLabelWithUrl(tr("QtSslUtils <a href=\"https://github.com/jbagg/QtSslUtils\">Source code</a>"));
	addLabelWithUrl(tr("QtCipherSqlitePlugin <a href=\"https://github.com/devbean/QtCipherSqlitePlugin\">Source code</a>"));

	layout->addStretch(10);

	QPushButton *okButton = new QPushButton(tr("OK"));
	Styler::styleButton(okButton);
	layout->addWidget(okButton);
	layout->setAlignment(okButton, Qt::AlignHCenter);
	connect(okButton, &QPushButton::clicked, this, &AboutScreen::close);

	layout->addStretch(1);

	QIcon windowIcon(QColorableSvg::color(QString(":/mission.svg"), 256, 256, Theme::IconColor));
	setWindowIcon(windowIcon);
}

void AboutScreen::closeEvent(QCloseEvent *event)
{
	deleteLater();
	QWidget::closeEvent(event);
}
