// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QVBoxLayout>
#include <QLabel>
#include <QPushButton>
#include "theme.h"
#include "styler.h"
#include "year-selector.h"
#include "tax-screen.h"
#include "tax-generate-receipts-widget.h"

TaxGenerateReceiptsWidget::TaxGenerateReceiptsWidget(TaxScreen &taxScreen) : taxScreen(taxScreen)
{
	QVBoxLayout *layout = new QVBoxLayout;
	setLayout(layout);

	layout->addStretch(1);

	QLabel *label = new QLabel(tr("Choose Tax Year"));
	Styler::styleLabel(label);
	layout->addWidget(label);
	layout->setAlignment(label, Qt::AlignHCenter);

	yearSelector = new YearSelector;
	layout->addWidget(yearSelector);
	layout->setAlignment(yearSelector, Qt::AlignHCenter);

	layout->addStretch(1);

	generateButton = new QPushButton(tr("Create Receipts"));
	generateButton->setEnabled(false);
	Styler::styleButton(generateButton);
	layout->addWidget(generateButton);
	layout->setAlignment(generateButton, Qt::AlignHCenter);

	layout->addStretch(1);

	enableButton = new QPushButton(tr("Enable"));
	Styler::styleButton(enableButton);
	layout->addWidget(enableButton);
	layout->setAlignment(enableButton, Qt::AlignHCenter);

	layout->addStretch(5);

	connect(generateButton, &QPushButton::clicked, this, &TaxGenerateReceiptsWidget::generateClicked);
	connect(enableButton, &QPushButton::clicked, this, &TaxGenerateReceiptsWidget::enabledClicked);
}

void TaxGenerateReceiptsWidget::generateClicked()
{
	taxScreen.generateClicked(yearSelector->currentYearSelected());
	doneEdits();
}

void TaxGenerateReceiptsWidget::enabledClicked()
{
	if (taxScreen.haveLock)
		doneEdits();
	else
		taxScreen.sendLock();
}

void TaxGenerateReceiptsWidget::gotLock()
{
	generateButton->setEnabled(true);
	enableButton->setText(tr("Disable"));
}

void TaxGenerateReceiptsWidget::doneEdits()
{
	generateButton->setEnabled(false);
	enableButton->setText(tr("Enable"));
	taxScreen.sendUnlock();
}
