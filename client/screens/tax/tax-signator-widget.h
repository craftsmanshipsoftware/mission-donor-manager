// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef MDM_TAX_SIGNATOR_WIDGET_H_
#define MDM_TAX_SIGNATOR_WIDGET_H_

#include <QWidget>

class QLabel;
class QComboBox;
class QLineEdit;
class QPushButton;
class TaxScreen;
class QFileDialog;

class TaxSignatorWidget : public QWidget
{
	Q_OBJECT

public:
	TaxSignatorWidget(TaxScreen &taxscreen);
	void updateSignatorsList(const QByteArray &listData, ssize_t count);
	void updateCurrentSignator(const QByteArray &data, ssize_t count);
	void gotLock();
	void doneEdits();

private:
	void editButtonClicked();
	void newClicked();
	void saveClicked();
	void signatorSelectorClicked(int index);
	void pathButtonClicked();
	void previewButtonClicked();
	void updateSaveButton();
	void imageFileSelected(QString fn);
	void imageFileSelectorFinished();
	TaxScreen &taxScreen;
	QLabel *signatorSelectorLabel;
	QComboBox *signatorSelector;
	QList<int> signatorsList;
	QLineEdit *nameEdit;
	QPushButton *editButton;
	QPushButton *pathButton;
	QPushButton *previewButton;
	QPushButton *newButton;
	QPushButton *cancelButton;
	QPushButton *saveButton;
	QSharedPointer<QFileDialog> imageFileDialog;
	int currentSignatorId = -1;
	int signatorListPublishCount;
	int currentSignatorPublishCount;
	bool newFlag = false;
	bool modSignatureFlag = false;
};

#endif
