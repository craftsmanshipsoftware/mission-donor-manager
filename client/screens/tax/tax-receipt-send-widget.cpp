// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QVBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QJsonDocument>
#include <QJsonObject>
#include <QProgressBar>
#include "../common/common.h"
#include "theme.h"
#include "styler.h"
#include "year-selector.h"
#include "tax-screen.h"
#include "tax-receipt-send-widget.h"

TaxReceiptSendWidget::TaxReceiptSendWidget(TaxScreen &taxScreen) : taxScreen(taxScreen)
{
	QVBoxLayout *layout = new QVBoxLayout;
	setLayout(layout);

	layout->addStretch(1);

	QLabel *label = new QLabel(tr("Choose Tax Year"));
	Styler::styleLabel(label);
	layout->addWidget(label);
	layout->setAlignment(label, Qt::AlignHCenter);

	yearSelector = new YearSelector;
	layout->addWidget(yearSelector);
	layout->setAlignment(yearSelector, Qt::AlignHCenter);

	layout->addStretch(1);

	snailMailDownloadButton = new QPushButton(tr("Download Snail Mail Receipts"));
	Styler::styleButton(snailMailDownloadButton);
	layout->addWidget(snailMailDownloadButton);
	layout->setAlignment(snailMailDownloadButton, Qt::AlignHCenter);

	layout->addStretch(1);

	emailSendButton = new QPushButton(tr("Send Email Receipts"));
	Styler::styleButton(emailSendButton);
	layout->addWidget(emailSendButton);
	layout->setAlignment(emailSendButton, Qt::AlignHCenter);

	layout->addStretch(1);

	progressBar = new QProgressBar;
	progressBar->setMinimum(0);
	progressBar->hide();
	Styler::styleProgressBar(progressBar);
	layout->addWidget(progressBar);

	finishedLabel = new QLabel(tr("Finished"));
	Styler::styleLabel(finishedLabel, Theme::H2Size);
	finishedLabel->hide();
	layout->addWidget(finishedLabel);
	layout->setAlignment(finishedLabel, Qt::AlignHCenter);

	layout->addStretch(5);

	connect(snailMailDownloadButton, &QPushButton::clicked, this, &TaxReceiptSendWidget::snailMailDownloadClicked);
	connect(emailSendButton, &QPushButton::clicked, this, &TaxReceiptSendWidget::emailSendClicked);
}

void TaxReceiptSendWidget::snailMailDownloadClicked()
{
	taxScreen.snailMailDownloadClicked(yearSelector->currentYearSelected());
}

void TaxReceiptSendWidget::emailSendClicked()
{
	taxScreen.emailSendClicked(yearSelector->currentYearSelected());
}

void TaxReceiptSendWidget::updateProgress(const QByteArray &content)
{
	QJsonObject json = QJsonDocument::fromJson(content).object();
	progressBar->setMaximum(json.value("total").toInt());
	progressBar->setValue(json.value("count").toInt());
	int state = json.value("state").toInt();
	if (state == EmailingTaxReceiptState::Idle) {
		progressBar->hide();
		finishedLabel->hide();
	} else if (state == EmailingTaxReceiptState::Sending) {
		progressBar->show();
		finishedLabel->hide();
	} else if (state == EmailingTaxReceiptState::Finished) {
		progressBar->hide();
		finishedLabel->show();
	}
}
