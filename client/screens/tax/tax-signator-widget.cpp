// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QHBoxLayout>
#include <QLabel>
#include <QComboBox>
#include <QLineEdit>
#include <QPushButton>
#include <QFileDialog>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include "theme.h"
#include "styler.h"
#include "main-window.h"
#include "connection-manager.h"
#include "tax-screen.h"
#include "tax-signator-widget.h"

TaxSignatorWidget::TaxSignatorWidget(TaxScreen &taxscreen) : taxScreen(taxscreen)
{
	QHBoxLayout *layout = new QHBoxLayout;
	setLayout(layout);
	layout->addStretch(1);

	QVBoxLayout *vLayout = new QVBoxLayout;
	layout->addLayout(vLayout);
	vLayout->addStretch(5);
	QLabel *title = new QLabel(tr("Change Signatory"));
	Styler::styleLabel(title, Theme::H2Size);
	vLayout->addWidget(title);
	vLayout->setAlignment(title, Qt::AlignHCenter);
	vLayout->addStretch(1);
	signatorSelectorLabel = new QLabel(tr("Current Signatory"));
	Styler::styleLabel(signatorSelectorLabel);
	vLayout->addWidget(signatorSelectorLabel);
	signatorSelector = new QComboBox;
	Styler::styleComboBox(signatorSelector);
	vLayout->addWidget(signatorSelector);
	editButton = new QPushButton(tr("Change Signatory"));
	Styler::styleButton(editButton);
	vLayout->addWidget(editButton);
	vLayout->addStretch(9);

	layout->addStretch(1);

	QVBoxLayout *addLayout = new QVBoxLayout;
	layout->addLayout(addLayout);
	addLayout->addStretch(5);
	title = new QLabel(tr("Add Signatory"));
	Styler::styleLabel(title, Theme::H2Size);
	addLayout->addWidget(title);
	addLayout->setAlignment(title, Qt::AlignHCenter);
	addLayout->addStretch(1);
	QLabel *label = new QLabel(tr("Enter name for signatory"));
	Styler::styleLabel(label);
	addLayout->addWidget(label);
	nameEdit = new QLineEdit;
	Styler::styleLineEdit(nameEdit);
	addLayout->addWidget(nameEdit);

	QHBoxLayout *buttonLayout = new QHBoxLayout;
	addLayout->addLayout(buttonLayout);
	newButton = new QPushButton(tr("New"));
	Styler::styleButton(newButton);
	buttonLayout->addWidget(newButton);
	cancelButton = new QPushButton(tr("Cancel"));
	Styler::styleButton(cancelButton);
	buttonLayout->addWidget(cancelButton);
	saveButton = new QPushButton(tr("Save"));
	Styler::styleButton(saveButton);
	buttonLayout->addWidget(saveButton);
	addLayout->addStretch(9);

	layout->addStretch(1);

	QVBoxLayout *sigLayout = new QVBoxLayout;
	layout->addLayout(sigLayout);
	sigLayout->addStretch(5);
	title = new QLabel(tr("Change Current\nSignatory's Signature"));
	Styler::styleLabel(title, Theme::H2Size);
	sigLayout->addWidget(title);
	sigLayout->setAlignment(title, Qt::AlignHCenter);
	sigLayout->addStretch(1);
	label = new QLabel(tr("Choose signature file (*.png)"));
	Styler::styleLabel(label);
	sigLayout->addWidget(label);
	label = new QLabel(tr("Image resolution must be 150px in height\nand upto 700px in width.\nSignature should almost fill either height or width"));
	Styler::styleLabel(label);
	sigLayout->addWidget(label);
	pathButton = new QPushButton(tr("Choose Signature File"));
	pathButton->setEnabled(false);
	Styler::styleButton(pathButton);
	sigLayout->addWidget(pathButton);
	sigLayout->addStretch(1);

	previewButton = new QPushButton(tr("Preview Example Receipt"));
	Styler::styleButton(previewButton);
	sigLayout->addWidget(previewButton);

	sigLayout->addStretch(6);

	layout->addStretch(1);

	doneEdits();  // setup forms

	connect(nameEdit, &QLineEdit::textEdited, this, &TaxSignatorWidget::updateSaveButton);
	connect(pathButton, &QPushButton::clicked, this, &TaxSignatorWidget::pathButtonClicked);
	connect(previewButton, &QPushButton::clicked, this, &TaxSignatorWidget::previewButtonClicked);
	connect(editButton, &QPushButton::clicked, this, &TaxSignatorWidget::editButtonClicked);
	connect(newButton, &QPushButton::clicked, this, &TaxSignatorWidget::newClicked);
	connect(cancelButton, &QPushButton::clicked, this, &TaxSignatorWidget::doneEdits);
	connect(saveButton, &QPushButton::clicked, this, &TaxSignatorWidget::saveClicked);
	connect(signatorSelector, qOverload<int>(&QComboBox::activated), this, &TaxSignatorWidget::signatorSelectorClicked);
}

void TaxSignatorWidget::updateSignatorsList(const QByteArray &listData, ssize_t count)
{
	int indexOfCurrent = -1;
	signatorSelector->clear();
	signatorsList.clear();
	QJsonArray jArray = QJsonDocument::fromJson(listData).array();
	for (int i = 0; i < jArray.size(); i++) {
		QJsonObject json = jArray.at(i).toObject();
		int id = json.value("id").toInt();
		signatorsList.append(id);
		if (id == currentSignatorId)  // server sends current signator id before sending the list of signators
			indexOfCurrent = i;
		signatorSelector->addItem(json.value("name").toString());
	}

	if (count == currentSignatorPublishCount) // tax.signator-current was sent first, ok to update selector
		signatorSelector->setCurrentIndex(indexOfCurrent);
	signatorListPublishCount = count;
}

void TaxSignatorWidget::updateCurrentSignator(const QByteArray &data, ssize_t count)
{
	currentSignatorId = data.toInt();
	if (count != currentSignatorPublishCount && count == signatorListPublishCount) {  // tax.signators was sent first which deferred updating the selector so we need to update it now
		for (int i = 0; i< signatorsList.size(); i++) {
			if (signatorsList.value(i) == currentSignatorId) {
				signatorSelector->setCurrentIndex(i);
				break;
			}
		}
	}
	currentSignatorPublishCount = count;
	if (currentSignatorId > 0)
		pathButton->setEnabled(true);
}

void TaxSignatorWidget::gotLock()
{
	editButton->setEnabled(false);
	newButton->setEnabled(false);
	previewButton->setEnabled(false);
	if (modSignatureFlag) {
		imageFileDialog = QSharedPointer<QFileDialog>(new QFileDialog(nullptr, "Open Banner File", "", "PNG (*.png)"), &QObject::deleteLater);
		imageFileDialog->setAcceptMode(QFileDialog::AcceptOpen);
		imageFileDialog->setFileMode(QFileDialog::ExistingFile);
		connect(imageFileDialog.data(), &QFileDialog::fileSelected, this, &TaxSignatorWidget::imageFileSelected);
		connect(imageFileDialog.data(), &QFileDialog::finished, this, &TaxSignatorWidget::imageFileSelectorFinished);
		imageFileDialog->show();
		taxScreen.mainWindow.setEnabled(false);
	} else if (newFlag) {
		nameEdit->setEnabled(true);
		pathButton->setEnabled(false);
		cancelButton->setEnabled(true);
		signatorSelector->setEnabled(false);
	} else {
		signatorSelector->setEnabled(true);
		cancelButton->setEnabled(true);
		pathButton->setEnabled(false);
		signatorSelectorLabel->setText(tr("Choose Signatory"));
	}
}

void TaxSignatorWidget::editButtonClicked()
{
	taxScreen.sendLock();
}

void TaxSignatorWidget::newClicked()
{
	newFlag = true;
	taxScreen.sendLock();
}

void TaxSignatorWidget::saveClicked()
{
	taxScreen.addSignator(nameEdit->text());
	doneEdits();
}

void TaxSignatorWidget::signatorSelectorClicked(int index)
{
	taxScreen.signatorSelectorClicked(signatorsList.at(index));
	doneEdits();
}

void TaxSignatorWidget::pathButtonClicked()
{
	modSignatureFlag = true;
	taxScreen.sendLock();
}

void TaxSignatorWidget::previewButtonClicked()
{
	taxScreen.cm.foxmq.post("tax.preview", FoxHead::ContentType::Empty, "");
}

void TaxSignatorWidget::imageFileSelected(QString fn)
{
	QFile imageFile(fn);
	if (imageFile.open(QIODevice::ReadOnly)) {
		QJsonObject json;
		json.insert("id", currentSignatorId);
		json.insert("signature-image", QString(imageFile.readAll().toBase64()));
		QJsonDocument jDoc;
		jDoc.setObject(json);
		taxScreen.cm.foxmq.post("tax.signature-change", FoxHead::ContentType::Json, jDoc.toJson(QJsonDocument::Compact));
	} else {
		taxScreen.signatureFileOpenError();
	}
	imageFile.close();
}

void TaxSignatorWidget::imageFileSelectorFinished()
{
	taxScreen.mainWindow.setEnabled(true);
	imageFileDialog.clear();
	doneEdits();
}

void TaxSignatorWidget::updateSaveButton()
{
	if (!nameEdit->text().isEmpty())
		saveButton->setEnabled(true);
	else
		saveButton->setEnabled(false);
}

void TaxSignatorWidget::doneEdits()
{
	newFlag = false;
	modSignatureFlag = false;
	nameEdit->clear();
	nameEdit->setEnabled(false);
	if (currentSignatorId > 0)
		pathButton->setEnabled(true);
	newButton->setEnabled(true);
	saveButton->setEnabled(false);
	cancelButton->setEnabled(false);
	newButton->setEnabled(true);
	editButton->setEnabled(true);
	signatorSelector->setEnabled(false);
	signatorSelectorLabel->setText(tr("Current Signatory"));
	previewButton->setEnabled(true);
	taxScreen.sendUnlock();
}
