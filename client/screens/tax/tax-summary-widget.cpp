// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QHBoxLayout>
#include <QLabel>
#include <QTableView>
#include <QHeaderView>
#include <QJsonObject>
#include <QJsonArray>
#include <QRadioButton>
#include <QKeyEvent>
#include "../common/common.h"
#include "copy-and-paste.h"
#include "theme.h"
#include "styler.h"
#include "year-selector.h"
#include "tax-agency-list-dm.h"
#include "tax-summary-totals-widget.h"
#include "tax-summary-widget.h"

TaxSummaryWidget::TaxSummaryWidget()
{
	QHBoxLayout *layout = new QHBoxLayout;
	setLayout(layout);

	layout->addStretch(1);

	QVBoxLayout *leftLayout = new QVBoxLayout;
	layout->addLayout(leftLayout);
	leftLayout->addStretch(1);

	yearSelector = new YearSelector;
	leftLayout->addWidget(yearSelector);
	leftLayout->setAlignment(yearSelector, Qt::AlignHCenter);

	leftLayout->addStretch(1);

	QHBoxLayout *typeLayout = new QHBoxLayout;
	leftLayout->addLayout(typeLayout);
	typeLayout->addStretch(1);
	fiscalButton = new QRadioButton("Fiscal");
	Styler::styleRadioButton(fiscalButton);
	typeButtonGroup.addButton(fiscalButton);
	typeLayout->addWidget(fiscalButton);
	calendarButton = new QRadioButton("Calendar");
	Styler::styleRadioButton(calendarButton);
	typeButtonGroup.addButton(calendarButton);
	typeLayout->addWidget(calendarButton);
	fiscalButton->setChecked(true);
	typeLayout->addStretch(1);

	leftLayout->addStretch(1);
	donorTypeSummery = new TaxSummaryTotalsWidget(tr("Cash totals by donor type"));
	leftLayout->addWidget(donorTypeSummery);
	leftLayout->addStretch(10);

	layout->addStretch(1);

	QVBoxLayout *organizationLayout = new QVBoxLayout;
	layout->addLayout(organizationLayout);

	organizationLayout->addStretch(1);
	QLabel *titleLabel = new QLabel(tr("Organization Totals"));
	Styler::styleLabel(titleLabel, Theme::H1Size);
	organizationLayout->addWidget(titleLabel);
	organizationLayout->setAlignment(titleLabel, Qt::AlignHCenter);

	agencyListView = new QTableView;
	agencyListView->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Maximum);
	organizationLayout->addWidget(agencyListView);
	Styler::styleTableView(agencyListView);
	agencyListView->verticalHeader()->hide();
	agencyListView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	agencyListView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
	agencyListView->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
	agencyListView->setAlternatingRowColors(true);
	agencyListView->setModel(&taxAgencyListDataModel);
	organizationLayout->addStretch(10);

	layout->addStretch(1);

	connect(yearSelector, &YearSelector::yearChanged, this, &TaxSummaryWidget::rangeOrYearChanged);
	connect(&typeButtonGroup, &QButtonGroup::idClicked, this, &TaxSummaryWidget::rangeOrYearChanged);
}

void TaxSummaryWidget::updateTotals(const QJsonObject &json)
{
	agencyListView->setCurrentIndex(QModelIndex());  // clear selected index if copy-and-paste is attempted after view is refreshed with smaller model.
	donorTypeSummery->updateTotals(json.value("totals").toObject());
	taxAgencyListDataModel.layoutAboutToBeChanged();
	taxAgencyListDataModel.clear();
	taxAgencyListDataModel.update(json.value("agencies").toArray());
	taxAgencyListDataModel.layoutChanged();
	agencyListView->resizeColumnsToContents();
}

QJsonObject TaxSummaryWidget::getYearAndRange()
{
	QJsonObject json;
	json.insert("year", yearSelector->currentYearSelected());
	json.insert("fiscal", fiscalButton->isChecked());
	return json;
}

void TaxSummaryWidget::keyPressEvent(QKeyEvent *e)
{
	if (e->type() == QKeyEvent::KeyPress && e->matches(QKeySequence::Copy))
		copyFromTable(agencyListView->selectionModel()->selectedIndexes());
}

