// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef MDM_TAX_SUMMARY_TOTALS_WIDGET_H_
#define MDM_TAX_SUMMARY_TOTALS_WIDGET_H_

#include <QWidget>

class QLabel;

class TaxSummaryTotalsWidget : public QWidget
{
	Q_OBJECT

public:
	TaxSummaryTotalsWidget(const QString &title);
	void updateTotals(const QJsonObject &json);

private:
	QLabel *dateRangeLabel;
	QLabel *individualsLabel;
	QLabel *individualsTotal;
	QLabel *individualsNeftdLabel;
	QLabel *individualsNeftdTotal;
	QLabel *businessLabel;
	QLabel *businessTotal;
	QLabel *businessNeftdLabel;
	QLabel *businessNeftdTotal;
	QLabel *governmentLabel;
	QLabel *governmentTotal;
	QLabel *canadianCharityLabel;
	QLabel *canadianCharityTotal;
	QLabel *canadianFoundationLabel;
	QLabel *canadianFoundationTotal;
	QLabel *foreignCharityLabel;
	QLabel *foreignCharityTotal;
	QLabel *nonTaxpayerLabel;
	QLabel *nonTaxpayerTotal;
};

#endif
