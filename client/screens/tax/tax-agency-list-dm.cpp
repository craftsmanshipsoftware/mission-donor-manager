// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QJsonObject>
#include <QJsonArray>
#include "../common/common.h"
#include "tax-agency-list-dm.h"

int TaxAgencyListDataModel::rowCount(const QModelIndex &) const
{
	return agencyList.size();
}

int TaxAgencyListDataModel::columnCount(const QModelIndex &) const
{
	return 3;
}

QVariant TaxAgencyListDataModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	QVariant text;

	if (role != Qt::DisplayRole)
		return QVariant();

	if (orientation == Qt::Horizontal) {
		switch(section) {  // clang-format off
			case 0: text = QString(tr("ID")); break;
			case 1: text = QString(tr("Name")); break;
			case 2: text = QString(tr("Total")); break;
		}  // clang-format on
	}

	return text;
}

QVariant TaxAgencyListDataModel::data(const QModelIndex &index, int role) const
{
	QVariant item;
	if (role == Qt::DisplayRole) {
		switch(index.column()) {  // clang-format off
			case 0: item = QString::number(agencyList.at(index.row()).id); break;
			case 1: item = agencyList.at(index.row()).name; break;
			case 2: item = convertCentsToString(agencyList.at(index.row()).amount, true); break;
		}  // clang-format on
	}
	return item;
}

void TaxAgencyListDataModel::clear()
{
	agencyList.clear();
}

void TaxAgencyListDataModel::update(QJsonArray jArray)
{
	for (int i=0; i < jArray.size(); i++) {
		QJsonObject json = jArray.at(i).toObject();
		struct TaxAgencyListItem tali;
		tali.id = json.value("id").toInt();
		tali.amount = json.value("amount").toInt();
		tali.name = json.value("name").toString();
		agencyList.append(tali);
	}
}
