// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QVBoxLayout>
#include <QLabel>
#include <QJsonObject>
#include "../common/common.h"
#include "theme.h"
#include "styler.h"
#include "tax-summary-totals-widget.h"

TaxSummaryTotalsWidget::TaxSummaryTotalsWidget(const QString &title)
{
	QVBoxLayout *layout = new QVBoxLayout;
	setLayout(layout);

	QLabel *titleLabel = new QLabel(title);
	Styler::styleLabel(titleLabel, Theme::H1Size);
	layout->addWidget(titleLabel);
	layout->setAlignment(titleLabel, Qt::AlignHCenter);

	dateRangeLabel = new QLabel;
	Styler::styleLabel(dateRangeLabel);
	layout->addWidget(dateRangeLabel);
	layout->setAlignment(dateRangeLabel, Qt::AlignHCenter);

	QGridLayout *infoLayout = new QGridLayout;
	layout->addLayout(infoLayout);

	auto addLabel = [&](const QString &title, int row, int column) {
		QLabel *label = new QLabel(title);
		Styler::styleLabel(label, Theme::H2Size);
		infoLayout->addWidget(label, row, column);
		if (column)
			infoLayout->setAlignment(label, Qt::AlignLeft);
		else
			infoLayout->setAlignment(label, Qt::AlignRight);
		return label;
	};

	individualsLabel = addLabel(tr("Individual's Eligible Total:"), 0, 0);
	individualsTotal = addLabel("", 0, 1);
	individualsNeftdLabel = addLabel(tr("Individual's Non-Eligible Total:"), 1, 0);
	individualsNeftdTotal = addLabel("", 1, 1);
	businessLabel = addLabel("Business's Eligible Total:", 2, 0);
	businessTotal = addLabel("", 2, 1);
	businessNeftdLabel = addLabel("Business's Non-Eligible Total:", 3, 0);
	businessNeftdTotal = addLabel("", 3, 1);
	governmentLabel = addLabel("Government Agency's Total:", 4, 0);
	governmentTotal = addLabel("", 4, 1);
	canadianCharityLabel = addLabel("Canadian Charity's Total:", 5, 0);
	canadianCharityTotal = addLabel("", 5, 1);
	canadianFoundationLabel = addLabel("Canadian Foundation's Total:", 6, 0);
	canadianFoundationTotal = addLabel("", 6, 1);
	foreignCharityLabel = addLabel("Foreign Charity's Total:", 7, 0);
	foreignCharityTotal = addLabel("", 7, 1);
	nonTaxpayerLabel = addLabel("Non Tax payer's Total:", 8, 0);
	nonTaxpayerTotal = addLabel("", 8, 1);
	// fixme add 3rd party

	layout->addStretch(1);
}

void TaxSummaryTotalsWidget::updateTotals(const QJsonObject &json)
{
	QDate start = QDate::fromJulianDay(json.value("start-julian").toInt());
	QDate end = QDate::fromJulianDay(json.value("end-julian").toInt());
	dateRangeLabel->setText(QString(tr("%1 to %2").arg(start.toString("MMMM d yyyy")).arg(end.toString("MMMM d yyyy"))));
	individualsTotal->setText(convertCentsToString(json.value("individual").toInt(), true));
	individualsNeftdTotal->setText(convertCentsToString(json.value("individual-neftd").toInt(), true));
	businessTotal->setText(convertCentsToString(json.value("business").toInt(), true));
	businessNeftdTotal->setText(convertCentsToString(json.value("business-neftd").toInt(), true));
	governmentTotal->setText(convertCentsToString(json.value("government").toInt(), true));
	canadianCharityTotal->setText(convertCentsToString(json.value("canadian-charity").toInt(), true));
	canadianFoundationTotal->setText(convertCentsToString(json.value("canadian-foundation").toInt(), true));
	foreignCharityTotal->setText(convertCentsToString(json.value("foreign-charity").toInt(), true));
	nonTaxpayerTotal->setText(convertCentsToString(json.value("non-taxpayer").toInt(), true));
}
