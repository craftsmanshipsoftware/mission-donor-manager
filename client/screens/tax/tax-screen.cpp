// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QVBoxLayout>
#include <QJsonDocument>
#include <QJsonArray>
#include <QFileDialog>
#include "main-window.h"
#include "connection-manager.h"
#include "user-input.h"
#include "centred-message-widget.h"
#include "styler.h"
#include "theme.h"
#include "qexclusivebuttons.h"
#include "qslidingstackedwidget.h"
#include "pdf-writer.h"
#include "tax-summary-widget.h"
#include "tax-signator-widget.h"
#include "tax-receipt-send-widget.h"
#include "tax-generate-receipts-widget.h"
#include "tax-inkind-widget.h"
#include "tax-screen.h"

enum Tab {
	Summary,
	InKind,
	Create,
	Send,
	Signator,
};

TaxScreen::TaxScreen(MainWindow &mainWindow) : mainWindow(mainWindow), cm(mainWindow.connectionManager)
{
	QVBoxLayout *layout = new QVBoxLayout;
	setLayout(layout);
	int m = Theme::OuterMargin * qMin(screen()->geometry().width(), screen()->geometry().height());
	layout->setContentsMargins(m, m, m, m);

	topButtons = new QExclusiveButtons({tr("Cash Summary"), tr("In Kind Summary"), tr("Create Receipts"), tr("Send Receipts"), tr("Signatory")});
	layout->addWidget(topButtons);
	for ( int i=0; i < topButtons->size(); i++)
		Styler::styleButton(topButtons->buttonAt(i));
	topButtons->setChecked(0, true);  // default to summary tab

	sections = new QSlidingStackedWidget();
	layout->addWidget(sections);
	sections->setDuration(500);
	summary = new TaxSummaryWidget;
	sections->addWidget(summary);

	inkind = new TaxInKindWidget();
	sections->addWidget(inkind);

	generateReceiptWidget = new TaxGenerateReceiptsWidget(*this);
	sections->addWidget(generateReceiptWidget);

	receiprSendWidget = new TaxReceiptSendWidget(*this);
	sections->addWidget(receiprSendWidget);

	signatorWidget = new TaxSignatorWidget(*this);
	sections->addWidget(signatorWidget);

	connect(topButtons, &QExclusiveButtons::clicked, this, &TaxScreen::topButtonClicked);
	connect(summary, &TaxSummaryWidget::rangeOrYearChanged, this, &TaxScreen::sendTaxSummaryRequest);
	connect(inkind, &TaxInKindWidget::rangeOrYearChanged, this, &TaxScreen::sendInKindListRequest);

	topicsToReceive << "tax";
	cm.registerApiForReceiver(this);
}

void TaxScreen::subscribe()
{
	cm.foxmq.subscription("tax", true);
	if (sections->currentIndex() == Tab::Summary)
		sendTaxSummaryRequest();
}

void TaxScreen::unsubscribe()
{
	cm.foxmq.subscription("tax", false);
}

void TaxScreen::processPublish(const QByteArray &topic, ssize_t count, FoxID, FoxHead::ContentType, const QByteArray &content)
{
	if (topic == "tax.signator-current") {
		signatorWidget->updateCurrentSignator(content, count);
	} else if (topic == "tax.signators") {
		signatorWidget->updateSignatorsList(content, count);
	} else if (topic == "tax.receipt-sending-state") {
		receiprSendWidget->updateProgress(content);
	}
}

void TaxScreen::processPostResponce(const QByteArray &topic, FoxID, FoxHead::ContentType, const QByteArray &content)
{
	if (topic == "tax.summary") {
		summary->updateTotals(QJsonDocument::fromJson(content).object());
	} else if (topic == "tax.inkind") {
		inkind->update(QJsonDocument::fromJson(content).object());
	} else if (topic == "tax.snail-mail-receipts" || topic == "tax.preview") {
		receiptsToSave = content;
		saveFileDialog = QSharedPointer<QFileDialog>(new QFileDialog(nullptr, "Save File", "", "PDF (*.pdf)"), &QObject::deleteLater);
		saveFileDialog->setAcceptMode(QFileDialog::AcceptSave);
		connect(saveFileDialog.data(), &QFileDialog::fileSelected, this, &TaxScreen::saveReceiptsSelected);
		connect(saveFileDialog.data(), &QFileDialog::finished, this, &TaxScreen::saveReceiptsSelectorFinished);
		saveFileDialog->show();
		mainWindow.setEnabled(false);
	} else if (topic == "tax.generate-receipts") {
		QJsonObject json = QJsonDocument::fromJson(content).object();
		int donations = json.value("total-cash-donations").toInt();
		int receipts = json.value("total-cash-receipts").toInt();
		int inkind = json.value("total-inkind-receipts").toInt();
		QString resultString(QString("Total Cash Donations: %1\nTotal Cash Receipts: %2\nTotal Inkind Receipts: %3\n").arg(donations).arg(receipts).arg(inkind));

		connect(mainWindow.userInput, &UserInput::button1Clicked, this, &TaxScreen::okFromLockNack);
		mainWindow.userInput->askForInput(resultString);
		mainWindow.showUserInput();
	}
}

void TaxScreen::processAck(const QByteArray &topic)
{
	if (topic == "tax.lock") {
		haveLock = true;
		int activeTab = sections->currentIndex();
		if (activeTab == Tab::Create)
			generateReceiptWidget->gotLock();
		else if (activeTab == Tab::Signator)
			signatorWidget->gotLock();
	} else if (topic == "tax.unlock") {
		haveLock = false;
	}
}

void TaxScreen::processNack(const QByteArray &topic)
{
	if (topic == "tax.lock") {
		connect(mainWindow.userInput, &UserInput::button1Clicked, this, &TaxScreen::okFromLockNack);
		mainWindow.userInput->askForInput(tr("Another user is editing tax receipts, try again later"));
		mainWindow.showUserInput();
	}
}

void TaxScreen::sendLock()
{
	cm.foxmq.post("tax.lock", FoxHead::ContentType::Empty, "");
}

void TaxScreen::sendUnlock()
{
	cm.foxmq.post("tax.unlock", FoxHead::ContentType::Empty, "");
}

void TaxScreen::topButtonClicked(int i)
{
	int oldIndex = sections->currentIndex();
	if (haveLock  && oldIndex != i) {
		if (oldIndex == Tab::Create)
			generateReceiptWidget->doneEdits();
		if (oldIndex == Tab::Signator)
			signatorWidget->doneEdits();
	}
	sections->setCurrentIndex(i);

	if (i == Tab::Summary)
		sendTaxSummaryRequest();
	if (i == Tab::InKind)
		sendInKindListRequest();
}

void TaxScreen::sendTaxSummaryRequest()
{
	QJsonDocument jDoc;
	jDoc.setObject(summary->getYearAndRange());
	cm.foxmq.post("tax.summary", FoxHead::ContentType::Json, jDoc.toJson(QJsonDocument::Compact));
}

void TaxScreen::sendInKindListRequest()
{
	QJsonDocument jDoc;
	jDoc.setObject(inkind->getYearAndRange());
	cm.foxmq.post("tax.inkind", FoxHead::ContentType::Json, jDoc.toJson(QJsonDocument::Compact));
}

void TaxScreen::addSignator(QString name)
{
	QJsonObject json;
	json.insert("name", name);
	QJsonDocument jDoc;
	jDoc.setObject(json);
	cm.foxmq.post("tax.signator-add", FoxHead::ContentType::Json, jDoc.toJson(QJsonDocument::Compact));
}

void TaxScreen::signatureFileOpenError()
{
	connect(mainWindow.userInput, &UserInput::button1Clicked, this, &TaxScreen::okFromLockNack);
	mainWindow.userInput->askForInput(tr("Unable to open signature file"), tr("OK"));
	mainWindow.showUserInput();
}

void TaxScreen::okFromLockNack()
{
	disconnect(mainWindow.userInput, &UserInput::button1Clicked, this, &TaxScreen::okFromLockNack);
	mainWindow.showMainScreen();
}

void TaxScreen::signatorSelectorClicked(int id)
{
	cm.foxmq.post("tax.signator-set", FoxHead::ContentType::IntAsString, QByteArray::number(id));
}

void TaxScreen::snailMailDownloadClicked(int year)
{
	cm.foxmq.post("tax.snail-mail-receipts", FoxHead::ContentType::IntAsString, QByteArray::number(year));
}

void TaxScreen::emailSendClicked(int year)
{
	cm.foxmq.post("tax.send-email-receipts", FoxHead::ContentType::IntAsString, QByteArray::number(year));
}

void TaxScreen::saveReceiptsSelected(QString fn)
{
	PDFWriter::write(fn, receiptsToSave);
	receiptsToSave.clear();
}

void TaxScreen::saveReceiptsSelectorFinished()
{
	mainWindow.setEnabled(true);
	saveFileDialog.clear();
}

void TaxScreen::generateClicked(int year)
{
	cm.foxmq.post("tax.generate-receipts", FoxHead::ContentType::IntAsString, QByteArray::number(year));
}
