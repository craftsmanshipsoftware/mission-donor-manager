// clang-format off
/**************************************************************************************************
	Copyright (C) 2024  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QJsonObject>
#include <QJsonArray>
#include "../common/common.h"
#include "tax-inkind-dm.h"

int TaxInKindDataModel::rowCount(const QModelIndex &) const
{
	return list.size();
}

int TaxInKindDataModel::columnCount(const QModelIndex &) const
{
	return 7;
}

QVariant TaxInKindDataModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	QVariant text;

	if (role != Qt::DisplayRole)
		return QVariant();

	if (orientation == Qt::Horizontal) {
		switch(section) {  // clang-format off
			case 0: text = QString(tr("Date"));             break;
			case 1: text = QString(tr("DID"));              break;
			case 2: text = QString(tr("Donor ID"));         break;
			case 3: text = QString(tr("Name"));             break;
			case 4: text = QString(tr("Amount"));           break;
			case 5: text = QString(tr("receipt #"));        break;
			case 6: text = QString(tr("Note"));             break;
		}  // clang-format on
	}

	return text;
}

QVariant TaxInKindDataModel::data(const QModelIndex &index, int role) const
{
	QVariant item;
	if (role == Qt::DisplayRole) {
		switch(index.column()) {  // clang-format off
			case 0: item = list.at(index.row()).date; break;
			case 1: item = QString::number(list.at(index.row()).did); break;
			case 2: item = QString::number(list.at(index.row()).donorId); break;
			case 3: item = list.at(index.row()).name; break;
			case 4: item = convertCentsToString(list.at(index.row()).amount, true); break;
			case 5: { //item = QString::number(list.at(index.row()).rid);
				if (list.at(index.row()).rid <= 0)
					item = "-";
				else
					item = list.at(index.row()).rid;
			}	break;
			case 6: item = list.at(index.row()).note.left(60); break;
		}  // clang-format on
	}
	return item;
}

void TaxInKindDataModel::clear()
{
	list.clear();
}

void TaxInKindDataModel::update(QJsonArray jArray)
{
	for (int i=0; i < jArray.size(); i++) {
		QJsonObject json = jArray.at(i).toObject();
		struct TaxInKindListItem tikli;
		tikli.date = QDate::fromJulianDay(json.value("date").toInt());
		tikli.did = json.value("did").toInt();
		tikli.donorId = json.value("donor-id").toInt();
		tikli.amount = json.value("amount").toInt();
		tikli.name = json.value("name").toString();
		tikli.note = json.value("note").toString();
		tikli.rid = json.value("rid").toInt();
		list.append(tikli);
	}
}
