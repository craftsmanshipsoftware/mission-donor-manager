// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef MDM_TAX_SCREEN_H_
#define MDM_TAX_SCREEN_H_

#include "tab-screen.h"
#include "receiver.h"

class MainWindow;
class ConnectionManager;
class QExclusiveButtons;
class QSlidingStackedWidget;
class QFileDialog;
class TaxSummaryWidget;
class TaxSignatorWidget;
class TaxReceiptSendWidget;
class TaxGenerateReceiptsWidget;
class TaxInKindWidget;

class TaxScreen : public TabScreen, public Receiver
{
	Q_OBJECT

public:
	TaxScreen(MainWindow &mainWindow);
	void subscribe() override;
	void unsubscribe() override;
	void snailMailDownloadClicked(int year);
	void emailSendClicked(int year);
	void generateClicked(int year);

private:
	void processPublish(const QByteArray &topic, ssize_t count, FoxID id, FoxHead::ContentType contentType, const QByteArray &content) override;
	void processPostResponce(const QByteArray &topic, FoxID id, FoxHead::ContentType contentType, const QByteArray &content) override;
	void processAck(const QByteArray &topic) override;
	void processNack(const QByteArray &topic) override;
	void sendLock();
	void sendUnlock();
	void topButtonClicked(int i);
	void sendTaxSummaryRequest();
	void sendInKindListRequest();
	void addSignator(QString name);
	void signatorSelectorClicked(int id);
	void signatureFileOpenError();
	void okFromLockNack();
	void saveReceiptsSelected(QString fn);
	void saveReceiptsSelectorFinished();
	MainWindow &mainWindow;
	ConnectionManager &cm;
	QExclusiveButtons *topButtons;
	QSlidingStackedWidget *sections;
	TaxGenerateReceiptsWidget *generateReceiptWidget;
	TaxSummaryWidget *summary;
	TaxInKindWidget *inkind;
	TaxSignatorWidget *signatorWidget;
	TaxReceiptSendWidget *receiprSendWidget;
	QSharedPointer<QFileDialog> saveFileDialog;
	QByteArray receiptsToSave;
	bool haveLock = false;

friend TaxSignatorWidget;
friend TaxGenerateReceiptsWidget;
};

#endif
