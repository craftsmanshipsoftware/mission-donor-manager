// clang-format off
/**************************************************************************************************
	Copyright (C) 2024  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QVBoxLayout>
#include <QLabel>
#include <QTableView>
#include <QHeaderView>
#include <QRadioButton>
#include <QJsonObject>
#include <QJsonArray>
#include <QKeyEvent>
#include "styler.h"
#include "theme.h"
#include "copy-and-paste.h"
#include "year-selector.h"
#include "tax-summary-totals-widget.h"
#include "tax-inkind-widget.h"

TaxInKindWidget::TaxInKindWidget()
{
	QHBoxLayout *layout = new QHBoxLayout;
	setLayout(layout);

	layout->addStretch(1);

	QVBoxLayout *leftLayout = new QVBoxLayout;
	layout->addLayout(leftLayout);

	leftLayout->addStretch(1);
	yearSelector = new YearSelector;
	leftLayout->addWidget(yearSelector);
	leftLayout->setAlignment(yearSelector, Qt::AlignHCenter);
	leftLayout->addStretch(1);

	QHBoxLayout *typeLayout = new QHBoxLayout;
	leftLayout->addLayout(typeLayout);
	typeLayout->addStretch(1);
	fiscalButton = new QRadioButton("Fiscal");
	Styler::styleRadioButton(fiscalButton);
	typeButtonGroup.addButton(fiscalButton);
	typeLayout->addWidget(fiscalButton);
	calendarButton = new QRadioButton("Calendar");
	Styler::styleRadioButton(calendarButton);
	typeButtonGroup.addButton(calendarButton);
	typeLayout->addWidget(calendarButton);
	fiscalButton->setChecked(true);
	typeLayout->addStretch(1);
	leftLayout->addStretch(1);

	taxSummaryTotalsWidget = new TaxSummaryTotalsWidget(tr("InKind totals by donor type"));
	leftLayout->addWidget(taxSummaryTotalsWidget);
	leftLayout->addStretch(10);

	layout->addStretch(1);

	QVBoxLayout *rightLayout = new 	QVBoxLayout;
	layout->addLayout(rightLayout);

	rightLayout->addStretch(1);

	QLabel *titleLabel = new QLabel(tr("List of in kind donations"));
	Styler::styleLabel(titleLabel, Theme::H1Size);
	rightLayout->addWidget(titleLabel);
	rightLayout->setAlignment(titleLabel, Qt::AlignHCenter);

	inkindViewList = new QTableView;
	inkindViewList->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Maximum);
	rightLayout->addWidget(inkindViewList);
	Styler::styleTableView(inkindViewList);
	inkindViewList->verticalHeader()->hide();
	inkindViewList->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	inkindViewList->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
	inkindViewList->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
	inkindViewList->setAlternatingRowColors(true);
	inkindViewList->setModel(&taxInKindDataModel);
	rightLayout->addStretch(10);

	layout->addStretch(1);

	connect(yearSelector, &YearSelector::yearChanged, this, &TaxInKindWidget::rangeOrYearChanged);
	connect(&typeButtonGroup, &QButtonGroup::idClicked, this, &TaxInKindWidget::rangeOrYearChanged);
}

void TaxInKindWidget::update(const QJsonObject &json)
{
	inkindViewList->setCurrentIndex(QModelIndex());  // clear selected index if copy-and-paste is attempted after view is refreshed with smaller model.
	taxInKindDataModel.layoutAboutToBeChanged();
	taxInKindDataModel.clear();
	taxInKindDataModel.update(json.value("list").toArray());
	taxInKindDataModel.layoutChanged();
	inkindViewList->resizeColumnsToContents();
	taxSummaryTotalsWidget->updateTotals(json.value("totals").toObject());
}

QJsonObject TaxInKindWidget::getYearAndRange()
{
	QJsonObject json;
	json.insert("year", yearSelector->currentYearSelected());
	json.insert("fiscal", fiscalButton->isChecked());
	return json;
}

void TaxInKindWidget::keyPressEvent(QKeyEvent *e)
{
	if (e->type() == QKeyEvent::KeyPress && e->matches(QKeySequence::Copy))
		copyFromTable(inkindViewList->selectionModel()->selectedIndexes());
}
