// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QStyleOption>
#include <QHBoxLayout>
#include <QPainter>
#include "main-window.h"
#include "styler.h"
#include "theme.h"
#include "user-input.h"

UserInput::UserInput(const MainWindow &mainWindow) : mainWindow(mainWindow)
{
	QVBoxLayout *layout = new QVBoxLayout;
	setLayout(layout);

	Styler::styleMainWidget(this);

	msg = new QLabel;
	Styler::styleLabel(msg, Theme::H1Size);
	msg->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);

	lineEdit = new QLineEdit;
	lineEdit->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
	Styler::styleLineEdit(lineEdit);
	lineEdit->setVisible(false);

	QHBoxLayout *buttonLayout = new QHBoxLayout;

	layout->addStretch(10);
	layout->addWidget(msg);
	layout->setAlignment(msg, Qt::AlignHCenter);
	layout->addStretch(1);
	layout->addWidget(lineEdit);
	layout->setAlignment(lineEdit, Qt::AlignHCenter);
	layout->addStretch(1);
	layout->addLayout(buttonLayout);
	layout->addStretch(10);

	auto addButton = [&]() {
		QPushButton *button = new QPushButton;
		Styler::styleButton(button);
		QFont font = button->font();
		font.setPointSize(12);
		button->setFont(font);
		button->setFocusPolicy(Qt::NoFocus);
		button->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
		return button;
	};

	buttonLayout->addStretch(1);
	button1 = addButton();
	buttonLayout->addWidget(button1);

	button2 = addButton();
	buttonLayout->addWidget(button2);

	button3 = addButton();
	buttonLayout->addWidget(button3);
	buttonLayout->addStretch(1);

	connect(button1, &QPushButton::clicked, this, &UserInput::button1Clicked);
	connect(button2, &QPushButton::clicked, this, &UserInput::button2Clicked);
	connect(button3, &QPushButton::clicked, this, &UserInput::button3Clicked);
}

void UserInput::paintEvent(QPaintEvent *)
{
	QStyleOption o;
	o.initFrom(this);
	QPainter p(this);
	style()->drawPrimitive(QStyle::PE_Widget, &o, &p, this);
};


void UserInput::askForInput(QString msgLabel, QString b1Label, QString b2Label, QString b3Label)
{
	msg->setText(msgLabel);
	button1->setText(b1Label);
	if (b2Label.size()) {
		button2->setText(b2Label);
		button2->setVisible(true);
	} else {
		button2->setVisible(false);
	}

	if (b3Label.size()) {
		button3->setText(b3Label);
		button3->setVisible(true);
	} else {
		button3->setVisible(false);
	}
}

void UserInput::enableTextInput(QString preset)
{
	if (preset.size())
		lineEdit->setText(preset);

	lineEdit->setVisible(true);
}

void UserInput::disconnectAll()
{
	disconnect(this, &UserInput::button1Clicked, nullptr, nullptr);
	disconnect(this, &UserInput::button2Clicked, nullptr, nullptr);
	disconnect(this, &UserInput::button3Clicked, nullptr, nullptr);
	lineEdit->setVisible(false);
}
