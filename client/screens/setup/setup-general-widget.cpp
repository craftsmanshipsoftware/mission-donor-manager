// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QHBoxLayout>
#include <QGridLayout>
#include <QLabel>
#include <QPushButton>
#include <QLineEdit>
#include <QCheckBox>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QCalendarWidget>
#include <QFileDialog>
#include "../common/common.h"
#include "main-window.h"
#include "connection-manager.h"
#include "styler.h"
#include "theme.h"
#include "user-input.h"
#include "source-account-selector-widget.h"
#include "setup-general-widget.h"

SetupGeneralWidget::SetupGeneralWidget(MainWindow &mainWindow) : mainWindow(mainWindow), cm(mainWindow.connectionManager)
{
	QVBoxLayout *layout = new QVBoxLayout;
	setLayout(layout);

	layout->addStretch(2);
	QHBoxLayout *formOuterLayout = new QHBoxLayout;
	QGridLayout *gridLayout = new QGridLayout;
	formOuterLayout->addStretch(1);
	formOuterLayout->addLayout(gridLayout);
	formOuterLayout->setStretchFactor(gridLayout, 5);
	formOuterLayout->addStretch(1);
	layout->addLayout(formOuterLayout);
	layout->setAlignment(gridLayout, Qt::AlignHCenter);
	layout->addStretch(2);

	int row = 0;
	QLabel *label = new QLabel(tr("Outgoing Email Server"));
	Styler::styleLabel(label);
	gridLayout->addWidget(label, row, 0);
	outgoingEmailServer = new QLineEdit;
	Styler::styleLineEdit(outgoingEmailServer);
	gridLayout->addWidget(outgoingEmailServer, row++, 1);

	label = new QLabel(tr("Outgoing Email Server Port"));
	Styler::styleLabel(label);
	gridLayout->addWidget(label, row, 0);
	outgoingEmailPort = new QLineEdit;
	portValidator.setRange(1, 65535);
	outgoingEmailPort->setValidator(&portValidator);
	Styler::styleLineEdit(outgoingEmailPort);
	gridLayout->addWidget(outgoingEmailPort, row++, 1);

	label = new QLabel(tr("PayPal App Client Id"));
	Styler::styleLabel(label);
	gridLayout->addWidget(label, row, 0);
	paypalAppClientId = new QLineEdit;
	Styler::styleLineEdit(paypalAppClientId);
	gridLayout->addWidget(paypalAppClientId, row++, 1);

	label = new QLabel(tr("PayPal App Secret Key"));
	Styler::styleLabel(label);
	gridLayout->addWidget(label, row, 0);
	paypalAppSecretKey = new QLineEdit;
	Styler::styleLineEdit(paypalAppSecretKey);
	gridLayout->addWidget(paypalAppSecretKey, row++, 1);

	label = new QLabel(tr("PayPal Income Account"));
	Styler::styleLabel(label);
	gridLayout->addWidget(label, row, 0);
	sourceAccountSelector = new SourceAccountSelectorWidget;
	Styler::styleComboBox(sourceAccountSelector);
	gridLayout->addWidget(sourceAccountSelector, row++, 1);

	label = new QLabel(tr("Amount for Benefactor"));
	Styler::styleLabel(label);
	gridLayout->addWidget(label, row, 0);
	includePayPalFee = new QCheckBox(tr("Include (PayPal) Processing Fee"));
	Styler::styleCheckBox(includePayPalFee);
	includePayPalFee->setToolTip(tr("It is more accurate accounting to not enable this feature.\n\n"
								 "When this is enabled, with an operations rate of 10%, a donation from PayPal of $100 would be split $10 for operations and $90 for the benefactor mission.\n\n"
								 "When this is not enabled, with an operations rate of 10%, a donation from PayPal of $100 (subtract fee of $1.50) would be split $9.85 for operations and $88.65 for the benefactor mission.\n\n"
								 "The donor is receipted for $100 in either case."));
	gridLayout->addWidget(includePayPalFee, row++, 1);

	label = new QLabel(tr("Donor Relations Email"));
	Styler::styleLabel(label);
	gridLayout->addWidget(label, row, 0);
	donorRelationsEmail = new QLineEdit;
	Styler::styleLineEdit(donorRelationsEmail);
	gridLayout->addWidget(donorRelationsEmail, row++, 1);

	label = new QLabel(tr("Standard Rate"));
	Styler::styleLabel(label);
	gridLayout->addWidget(label, row, 0);
	standardRate = new QLineEdit;
	standardRateValidator.setRange(0, 100, 1);  // 0 --> 100, 1 decimal place
	standardRate->setValidator(&standardRateValidator);
	Styler::styleLineEdit(standardRate);
	gridLayout->addWidget(standardRate, row++, 1);

	label = new QLabel(tr("Operations Account Number"));
	Styler::styleLabel(label);
	gridLayout->addWidget(label, row, 0);
	operationsAccount = new QLineEdit;
	Styler::styleLineEdit(operationsAccount);
	gridLayout->addWidget(operationsAccount, row++, 1);

	row = 0;

	label = new QLabel(tr("Business Name"));
	Styler::styleLabel(label);
	gridLayout->addWidget(label, row, 2);
	businessNameEdit = new QLineEdit;
	Styler::styleLineEdit(businessNameEdit);
	gridLayout->addWidget(businessNameEdit, row++, 3);

	label = new QLabel(tr("CRA Charitable Business Number"));
	Styler::styleLabel(label);
	gridLayout->addWidget(label, row, 2);
	craBusinessNumberEdit = new QLineEdit;
	Styler::styleLineEdit(craBusinessNumberEdit);
	gridLayout->addWidget(craBusinessNumberEdit, row++, 3);

	label = new QLabel(tr("Address Line 1"));
	Styler::styleLabel(label);
	gridLayout->addWidget(label, row, 2);
	addressLine1Edit = new QLineEdit;
	Styler::styleLineEdit(addressLine1Edit);
	gridLayout->addWidget(addressLine1Edit, row++, 3);

	label = new QLabel(tr("Address Line 2"));
	Styler::styleLabel(label);
	gridLayout->addWidget(label, row, 2);
	addressLine2Edit = new QLineEdit;
	Styler::styleLineEdit(addressLine2Edit);
	gridLayout->addWidget(addressLine2Edit, row++, 3);

	label = new QLabel(tr("Address Line 3"));
	Styler::styleLabel(label);
	gridLayout->addWidget(label, row, 2);
	addressLine3Edit = new QLineEdit;
	Styler::styleLineEdit(addressLine3Edit);
	gridLayout->addWidget(addressLine3Edit, row++, 3);

	label = new QLabel(tr("Phone Number"));
	Styler::styleLabel(label);
	gridLayout->addWidget(label, row, 2);
	phoneEdit = new QLineEdit;
	Styler::styleLineEdit(phoneEdit);
	gridLayout->addWidget(phoneEdit, row++, 3);

	label = new QLabel(tr("Issue Location"));
	Styler::styleLabel(label);
	gridLayout->addWidget(label, row, 2);
	locationEdit = new QLineEdit;
	Styler::styleLineEdit(locationEdit);
	gridLayout->addWidget(locationEdit, row++, 3);

	label = new QLabel(tr("Donation Notification Threshold"));
	Styler::styleLabel(label);
	gridLayout->addWidget(label, row, 2);
	donationNotifyThresholdEdit = new QLineEdit;
	Styler::styleLineEdit(donationNotifyThresholdEdit);
	gridLayout->addWidget(donationNotifyThresholdEdit, row++, 3);

	label = new QLabel(tr("Export Gifts In Kind to IIF"));
	Styler::styleLabel(label);
	gridLayout->addWidget(label, row, 2);
	exportGik = new QCheckBox(tr("Enable"));
	Styler::styleCheckBox(exportGik);
	gridLayout->addWidget(exportGik, row++, 3);

	label = new QLabel(tr("InKind Income Account"));
	Styler::styleLabel(label);
	gridLayout->addWidget(label, row, 2);
	exportGikAccountSelector = new SourceAccountSelectorWidget;
	Styler::styleComboBox(exportGikAccountSelector);
	gridLayout->addWidget(exportGikAccountSelector, row++, 3);

	bannerButton = new QPushButton(tr("Upload Banner"));
	bannerButton->setEnabled(false);
	Styler::styleButton(bannerButton);
	layout->addWidget(bannerButton);
	layout->setAlignment(bannerButton, Qt::AlignHCenter);

	label = new QLabel(tr("1100x350 max"));
	Styler::styleLabel(label);
	layout->addWidget(label);
	layout->setAlignment(label, Qt::AlignHCenter);

	layout->addStretch(1);

	label = new QLabel(tr("Fiscal Year End"));
	Styler::styleLabel(label);
	layout->addWidget(label);
	layout->setAlignment(label, Qt::AlignHCenter);
	calendar = new QCalendarWidget;
	layout->addWidget(calendar);
	layout->setAlignment(calendar, Qt::AlignHCenter);

	enableForms(false);

	// bottom buttons
	layout->addStretch(4);
	QHBoxLayout *bottomButtonLayout = new QHBoxLayout;
	layout->addLayout(bottomButtonLayout);
	bottomButtonLayout->addStretch(1);
	editButton = new QPushButton(tr(" Edit "));
	Styler::styleButton(editButton);
	bottomButtonLayout->addWidget(editButton);
	cancelButton = new QPushButton(tr("Cancel"));
	Styler::styleButton(cancelButton);
	cancelButton->setEnabled(false);
	bottomButtonLayout->addWidget(cancelButton);
	saveButton = new QPushButton(tr(" Save "));
	Styler::styleButton(saveButton);
	saveButton->setEnabled(false);
	bottomButtonLayout->addWidget(saveButton);
	bottomButtonLayout->addStretch(1);

	connect(editButton, &QPushButton::clicked, this, &SetupGeneralWidget::editClicked);
	connect(cancelButton, &QPushButton::clicked, this, &SetupGeneralWidget::cancelClicked);
	connect(saveButton, &QPushButton::clicked, this, &SetupGeneralWidget::saveClicked);

	connect(bannerButton, &QPushButton::clicked, this, &SetupGeneralWidget::bannerButtonClicked);
	connect(outgoingEmailServer, &QLineEdit::textEdited, this, &SetupGeneralWidget::updateSaveEnabled);
	connect(outgoingEmailPort, &QLineEdit::textEdited, this, &SetupGeneralWidget::updateSaveEnabled);
	connect(paypalAppClientId, &QLineEdit::textEdited, this, &SetupGeneralWidget::updateSaveEnabled);
	connect(paypalAppSecretKey, &QLineEdit::textEdited, this, &SetupGeneralWidget::updateSaveEnabled);
	connect(includePayPalFee, &QCheckBox::clicked, this, &SetupGeneralWidget::paypalFlagNewClicked);
	connect(donorRelationsEmail, &QLineEdit::textEdited, this, &SetupGeneralWidget::updateSaveEnabled);
	connect(standardRate, &QLineEdit::textEdited, this, &SetupGeneralWidget::updateSaveEnabled);
	connect(operationsAccount, &QLineEdit::textEdited, this, &SetupGeneralWidget::updateSaveEnabled);
	connect(businessNameEdit, &QLineEdit::textEdited, this, &SetupGeneralWidget::updateSaveEnabled);
	connect(craBusinessNumberEdit, &QLineEdit::textEdited, this, &SetupGeneralWidget::updateSaveEnabled);
	connect(addressLine1Edit, &QLineEdit::textEdited, this, &SetupGeneralWidget::updateSaveEnabled);
	connect(addressLine2Edit, &QLineEdit::textEdited, this, &SetupGeneralWidget::updateSaveEnabled);
	connect(addressLine3Edit, &QLineEdit::textEdited, this, &SetupGeneralWidget::updateSaveEnabled);
	connect(phoneEdit, &QLineEdit::textEdited, this, &SetupGeneralWidget::updateSaveEnabled);
	connect(locationEdit, &QLineEdit::textEdited, this, &SetupGeneralWidget::updateSaveEnabled);
	connect(donationNotifyThresholdEdit, &QLineEdit::textEdited, this, &SetupGeneralWidget::updateSaveEnabled);
	connect(sourceAccountSelector, qOverload<int>(&QComboBox::activated), this, &SetupGeneralWidget::ppsaSelected);
	connect(exportGikAccountSelector, qOverload<int>(&QComboBox::activated), this, &SetupGeneralWidget::giksaSelected);
	connect(exportGik, &QCheckBox::clicked, this, &SetupGeneralWidget::paypalFlagNewClicked);
	connect(calendar, &QCalendarWidget::clicked, this, &SetupGeneralWidget::calendarClicked);

	topicsToReceive << "setup" << "income-accounts.list-enabled";
	cm.registerApiForReceiver(this);
}

void SetupGeneralWidget::selected()
{
	cm.foxmq.subscription("income-accounts.list-enabled", true);
	cm.foxmq.post("setup.get", FoxHead::ContentType::Empty, "");
	cm.foxmq.subscription("setup.updated", true);
}

void SetupGeneralWidget::unselected()
{
	cm.foxmq.subscription("income-accounts.list-enabled", false);
	cm.foxmq.subscription("setup.updated", false);
}

void SetupGeneralWidget::processPublish(const QByteArray &topic, ssize_t, FoxID, FoxHead::ContentType, const QByteArray &content)
{
	if (topic == "setup.updated") {
		cm.foxmq.post("setup.get", FoxHead::ContentType::Empty, "");
	} else if (topic == "income-accounts.list-enabled") {
		QJsonArray jArray = QJsonDocument::fromJson(content).array();
		sourceAccountSelector->update(jArray);
		sourceAccountSelector->setCurrentIndexByAccountId(paypalSourceAccount);
		exportGikAccountSelector->update(jArray);
		exportGikAccountSelector->setCurrentIndexByAccountId(gikSourceAccount);
	}
}

void SetupGeneralWidget::processPostResponce(const QByteArray &topic, FoxID, FoxHead::ContentType, const QByteArray &content)
{
	if (topic == "setup.get") {
		QJsonObject json = QJsonDocument::fromJson(content).object();
		outgoingEmailServer->setText(json.value("outgoing-email-server").toString());
		int port = json.value("outgoing-email-server-port").toInt();
		outgoingEmailPort->setText(QString::number(port));
		paypalAppClientId->setText(json.value("paypal-client-id").toString());
		paypalAppSecretKey->setText(json.value("paypal-secret-key").toString());
		paypalSourceAccount = json.value("paypal-sa").toInt();
		sourceAccountSelector->setCurrentIndexByAccountId(paypalSourceAccount);
		includePayPalFee->setChecked(json.value("include-fee").toBool());
		donorRelationsEmail->setText(json.value("donor-rel-email").toString());
		double standardRateValue = json.value("standard-rate").toDouble() * 100;
		standardRate->setText(QString::number(standardRateValue));
		operationsAccount->setText(json.value("operation-account").toString());
		businessNameEdit->setText(json.value("business-name").toString());
		craBusinessNumberEdit->setText(json.value("cra-business-number").toString());
		addressLine1Edit->setText(json.value("address-l1").toString());
		addressLine2Edit->setText(json.value("address-l2").toString());
		addressLine3Edit->setText(json.value("address-l3").toString());
		phoneEdit->setText(json.value("phone").toString());
		locationEdit->setText(json.value("issue-location").toString());
		donationNotifyThresholdEdit->setText(convertCentsToString(json.value("dn-threshold").toInt()));
		calendar->setSelectedDate(QDate::fromJulianDay(json.value("fiscal-end").toInt()));
		exportGik->setChecked(json.value("export-gik").toBool());
		gikSourceAccount = json.value("export-gik-account").toInt();
		exportGikAccountSelector->setCurrentIndexByAccountId(gikSourceAccount);
	}
}

void SetupGeneralWidget::processAck(const QByteArray &topic)
{
	if (topic == "setup.lock") {
		enableForms(true);
		editButton->setEnabled(false);
		cancelButton->setEnabled(true);
		bannerButton->setEnabled(true);
	}
}

void SetupGeneralWidget::processNack(const QByteArray &topic)
{
	if (topic == "setup.lock") {
		connect(mainWindow.userInput, &UserInput::button1Clicked, this, &SetupGeneralWidget::okFromLockNack);
		mainWindow.userInput->askForInput(tr("Another user is modifying general setup, try again later"));
		mainWindow.showUserInput();
	}
}

void SetupGeneralWidget::okFromLockNack()
{
	disconnect(mainWindow.userInput, &UserInput::button1Clicked, this, &SetupGeneralWidget::okFromLockNack);
	mainWindow.showMainScreen();
}

void SetupGeneralWidget::enableForms(bool enabled)
{
	outgoingEmailServer->setReadOnly(!enabled);
	outgoingEmailPort->setReadOnly(!enabled);
	paypalAppClientId->setReadOnly(!enabled);
	paypalAppSecretKey->setReadOnly(!enabled);
	sourceAccountSelector->setEnabled(enabled);
	includePayPalFee->setEnabled(enabled);
	exportGikAccountSelector->setEnabled(enabled);
	exportGik->setEnabled(enabled);
	donorRelationsEmail->setEnabled(enabled);
	standardRate->setEnabled(enabled);
	operationsAccount->setEnabled(enabled);
	businessNameEdit->setEnabled(enabled);
	craBusinessNumberEdit->setEnabled(enabled);
	addressLine1Edit->setEnabled(enabled);
	addressLine2Edit->setEnabled(enabled);
	addressLine3Edit->setEnabled(enabled);
	phoneEdit->setEnabled(enabled);
	locationEdit->setEnabled(enabled);
	donationNotifyThresholdEdit->setEnabled(enabled);
	calendar->setEnabled(enabled);
}

void SetupGeneralWidget::editClicked()
{
	cm.foxmq.post("setup.lock", FoxHead::ContentType::Empty, "");
}

void SetupGeneralWidget::cancelClicked()
{
	checkForSave();
}

bool SetupGeneralWidget::formsModified()
{
	return outgoingEmailServer->isModified() || outgoingEmailPort->isModified() || paypalAppClientId->isModified() || paypalAppSecretKey->isModified()
	        || donorRelationsEmail->isModified() || standardRate->isModified() || operationsAccount->isModified() || craBusinessNumberEdit->isModified()
	        || businessNameEdit->isModified() || addressLine1Edit->isModified() || addressLine2Edit->isModified() || addressLine3Edit->isModified()
	        || phoneEdit->isModified() || locationEdit->isModified() || donationNotifyThresholdEdit->isModified() || paypalSourceAccountModified
	        || paypalFlagNewModified || calendarModified || gikSourceAccountModified;
}

bool SetupGeneralWidget::checkForSave()
{
	bool changed = false;
	if (formsModified()) {
		changed = true;
		connect(mainWindow.userInput, &UserInput::button1Clicked, this, &SetupGeneralWidget::saveConfirmDiscardClicked);
		connect(mainWindow.userInput, &UserInput::button2Clicked, this, &SetupGeneralWidget::saveConfirmCancelClicked);
		connect(mainWindow.userInput, &UserInput::button3Clicked, this, &SetupGeneralWidget::saveConfirmSaveClicked);
		if (saveButton->isEnabled())
			mainWindow.userInput->askForInput(tr("Changes have been made"), tr("Discard Changes"), tr("Continue Editing"), tr("      Save      "));
		else
			mainWindow.userInput->askForInput(tr("Changes have been made"), tr("Discard Changes"), tr("Continue Editing"));
		mainWindow.showUserInput();
	} else if (cancelButton->isEnabled()) {
		doneEdits();  // editing has been enabled, but no changes, so unlock
	}
	return changed;
}

void SetupGeneralWidget::saveConfirmSaveClicked()
{
	saveConfirmCleanUp();
	saveClicked();
}

void SetupGeneralWidget::saveConfirmDiscardClicked()
{
	bool refreshForms = formsModified();
	saveConfirmCleanUp();
	doneEdits();
	if (refreshForms) { // on discard from edit only, refresh forms from server
		cm.foxmq.post("setup.get", FoxHead::ContentType::Empty, "");
	}
}

void SetupGeneralWidget::saveConfirmCancelClicked()
{
	saveConfirmCleanUp();
}

void SetupGeneralWidget::saveConfirmCleanUp()
{
	disconnect(mainWindow.userInput, &UserInput::button1Clicked, this, &SetupGeneralWidget::saveConfirmDiscardClicked);
	disconnect(mainWindow.userInput, &UserInput::button2Clicked, this, &SetupGeneralWidget::saveConfirmCancelClicked);
	disconnect(mainWindow.userInput, &UserInput::button3Clicked, this, &SetupGeneralWidget::saveConfirmSaveClicked);
	mainWindow.showMainScreen();
}

void SetupGeneralWidget::saveClicked()
{
	QJsonDocument jdoc;
	QJsonObject json;
	json.insert("outgoing-email-server", outgoingEmailServer->text());
	json.insert("outgoing-email-server-port", outgoingEmailPort->text().toInt());
	json.insert("paypal-client-id", paypalAppClientId->text());
	json.insert("paypal-secret-key", paypalAppSecretKey->text());
	json.insert("paypal-sa", sourceAccountSelector->accountIdAtCurrentIndex());
	json.insert("include-fee", includePayPalFee->isChecked());
	json.insert("donor-rel-email", donorRelationsEmail->text());
	double standardRateValue = standardRate->text().toDouble() / 100;
	json.insert("standard-rate", standardRateValue);
	json.insert("operation-account", operationsAccount->text());
	json.insert("business-name", businessNameEdit->text());
	json.insert("cra-business-number", craBusinessNumberEdit->text());
	json.insert("address-l1", addressLine1Edit->text());
	json.insert("address-l2", addressLine2Edit->text());
	json.insert("address-l3", addressLine3Edit->text());
	json.insert("phone", phoneEdit->text());
	json.insert("issue-location", locationEdit->text());
	json.insert("dn-threshold", convertStringToCents(donationNotifyThresholdEdit->text()));
	json.insert("export-gik-account", exportGikAccountSelector->accountIdAtCurrentIndex());
	json.insert("export-gik", exportGik->isChecked());
	json.insert("fiscal-end", calendar->selectedDate().toJulianDay());
	jdoc.setObject(json);
	cm.foxmq.post("setup.set", FoxHead::ContentType::Json, jdoc.toJson(QJsonDocument::Compact));
	doneEdits();
}

void SetupGeneralWidget::clearFormsModifiedFlags()
{
	outgoingEmailServer->setModified(false);
	outgoingEmailPort->setModified(false);
	paypalAppClientId->setModified(false);
	paypalAppSecretKey->setModified(false);
	donorRelationsEmail->setModified(false);
	standardRate->setModified(false);
	operationsAccount->setModified(false);
	craBusinessNumberEdit->setModified(false);
	paypalSourceAccountModified = false;
	gikSourceAccountModified = false;
	calendarModified = false;
	paypalFlagNewModified = false;
}

void SetupGeneralWidget::doneEdits()
{
	saveButton->setEnabled(false);
	enableForms(false);
	cancelButton->setEnabled(false);
	clearFormsModifiedFlags();
	editButton->setEnabled(true);
	bannerButton->setEnabled(false);
	cm.foxmq.post("setup.unlock", FoxHead::ContentType::Empty, "");
}

void SetupGeneralWidget::updateSaveEnabled()
{
	bannerButton->setEnabled(false);  // if editting forms, disable banner upload
	QString percentString = standardRate->text();
	QString portString = outgoingEmailPort->text();
	int pos = 0;
	if (standardRateValidator.validate(percentString, pos) == QValidator::Acceptable
	        && portValidator.validate(portString, pos) == QValidator::Acceptable
	        && formsModified())
		saveButton->setEnabled(true);
	else
		saveButton->setEnabled(false);
}

void SetupGeneralWidget::ppsaSelected(int)
{
	paypalSourceAccountModified = true;
	updateSaveEnabled();
}

void SetupGeneralWidget::giksaSelected(int)
{
	gikSourceAccountModified = true;
	updateSaveEnabled();
}

void SetupGeneralWidget::calendarClicked()
{
	calendarModified = true;
	updateSaveEnabled();
}

void SetupGeneralWidget::paypalFlagNewClicked()
{
	paypalFlagNewModified = true;
	updateSaveEnabled();
}

void SetupGeneralWidget::bannerButtonClicked()
{
	enableForms(false);  // if uploading a banner, disable forms
	bannerFileDialog = QSharedPointer<QFileDialog>(new QFileDialog(nullptr, "Open Banner File", "", "PNG (*.png)"), &QObject::deleteLater);
	bannerFileDialog->setAcceptMode(QFileDialog::AcceptOpen);
	bannerFileDialog->setFileMode(QFileDialog::ExistingFile);
	connect(bannerFileDialog.data(), &QFileDialog::fileSelected, this, &SetupGeneralWidget::bannerFileSelected);
	connect(bannerFileDialog.data(), &QFileDialog::finished, this, &SetupGeneralWidget::bannerFileSelectorFinished);
	bannerFileDialog->show();
	mainWindow.setEnabled(false);
}

void SetupGeneralWidget::bannerFileSelected(QString fn)
{
	QFile file(fn);
	if (file.open(QIODevice::ReadOnly)) {
		cm.foxmq.post("setup.set-banner", FoxHead::ContentType::Png, file.readAll());
		file.close();
	}
}

void SetupGeneralWidget::bannerFileSelectorFinished()
{
	doneEdits();
	mainWindow.setEnabled(true);
	bannerFileDialog.clear();
}
