// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef MDM_SETUP_GENERAL_WIDGET_H_
#define MDM_SETUP_GENERAL_WIDGET_H_

#include <QWidget>
#include <QDoubleValidator>
#include <QSharedPointer>
#include "tab-screen.h"
#include "receiver.h"

class MainWindow;
class ConnectionManager;
class QPushButton;
class QLineEdit;
class SourceAccountSelectorWidget;
class QCalendarWidget;
class QFileDialog;
class QCheckBox;

class SetupGeneralWidget : public QWidget, public Receiver
{
	Q_OBJECT

public:
	SetupGeneralWidget(MainWindow &mainWindow);
	void selected();
	void unselected();

private:
	void processPublish(const QByteArray &topic, ssize_t count, FoxID id, FoxHead::ContentType contentType, const QByteArray &content) override;
	void processPostResponce(const QByteArray &topic, FoxID id, FoxHead::ContentType contentType, const QByteArray &content) override;
	void processAck(const QByteArray &topic) override;
	void processNack(const QByteArray &topic) override;
	void enableForms(bool enabled);
	void editClicked();
	void cancelClicked();
	void saveClicked();
	void clearFormsModifiedFlags();
	void doneEdits();
	void okFromLockNack();
	bool formsModified();
	bool checkForSave();
	void updateSaveEnabled();
	void saveConfirmSaveClicked();
	void saveConfirmDiscardClicked();
	void saveConfirmCancelClicked();
	void saveConfirmCleanUp();
	void ppsaSelected(int idx);
	void giksaSelected(int);
	void calendarClicked();
	void bannerButtonClicked();
	void bannerFileSelected(QString fn);
	void bannerFileSelectorFinished();
	void paypalFlagNewClicked();
	MainWindow &mainWindow;
	ConnectionManager &cm;
	QPushButton *bannerButton;
	QPushButton *editButton;
	QPushButton *saveButton;
	QPushButton *cancelButton;
	QLineEdit *outgoingEmailServer;
	QLineEdit *outgoingEmailPort;
	QLineEdit *paypalAppClientId;
	QLineEdit *paypalAppSecretKey;
	QLineEdit *donorRelationsEmail;
	QLineEdit *standardRate;
	QLineEdit *operationsAccount;
	QLineEdit *businessNameEdit;
	QLineEdit *craBusinessNumberEdit;
	QLineEdit *addressLine1Edit;
	QLineEdit *addressLine2Edit;
	QLineEdit *addressLine3Edit;
	QLineEdit *phoneEdit;
	QLineEdit *locationEdit;
	QLineEdit *donationNotifyThresholdEdit;
	QCheckBox *includePayPalFee;
	QCheckBox *exportGik;
	SourceAccountSelectorWidget *exportGikAccountSelector;
	SourceAccountSelectorWidget *sourceAccountSelector;
	QCalendarWidget *calendar;
	int paypalSourceAccount = 0;
	int gikSourceAccount = 0;
	bool paypalSourceAccountModified = false;
	bool gikSourceAccountModified = false;
	bool calendarModified = false;
	bool paypalFlagNewModified = false;
	QDoubleValidator standardRateValidator;
	QIntValidator portValidator;
	QSharedPointer<QFileDialog> bannerFileDialog;
};

#endif
