// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QJsonObject>
#include <QJsonArray>
#include "setup-income-accounts-dm.h"

int IncomeAccountListDataModel::rowCount(const QModelIndex &) const
{
	return sourceAccountList.size();
}

int IncomeAccountListDataModel::columnCount(const QModelIndex &) const
{
	return 4;
}

QVariant IncomeAccountListDataModel::headerData(int, Qt::Orientation, int) const
{
	return QVariant();
}

QVariant IncomeAccountListDataModel::data(const QModelIndex &index, int role) const
{
	QVariant value;
	if (role == Qt::DisplayRole) {
		switch (index.column()) {  // clang format off
			case 0: value = sourceAccountList.at(index.row()).id;                                       break;
			case 1: value = sourceAccountList.at(index.row()).name;                                     break;
			case 2: value = sourceAccountList.at(index.row()).number;                                   break;
			case 3: value = sourceAccountList.at(index.row()).enabled == true ? "Enabled" : "Disabled"; break;
		}  // clang format on
	}
	return value;
}

void IncomeAccountListDataModel::clear()
{
	sourceAccountList.clear();
}

void IncomeAccountListDataModel::append(QJsonArray jArray)
{
	for (int i=0; i<jArray.size(); i++) {
		QJsonObject json = jArray.at(i).toObject();
		struct SourceAccountItem sai;
		sai.id = json.value("id").toInt();
		sai.name = json.value("name").toString();
		sai.number = json.value("number").toString();
		sai.enabled = json.value("enabled").toBool();
		sourceAccountList.append(sai);
	}
}
