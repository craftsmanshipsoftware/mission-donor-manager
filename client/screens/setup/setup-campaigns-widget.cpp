// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QHBoxLayout>
#include <QLineEdit>
#include <QCheckBox>
#include <QPushButton>
#include <QJsonDocument>
#include <QJsonArray>
#include <QTableView>
#include <QHeaderView>
#include "theme.h"
#include "styler.h"
#include "main-window.h"
#include "connection-manager.h"
#include "user-input.h"
#include "setup-campaigns-widget.h"

SetupCampaignsWidget::SetupCampaignsWidget(MainWindow &mainWindow) : mainWindow(mainWindow), cm(mainWindow.connectionManager)
{
	QHBoxLayout *layout = new QHBoxLayout;
	setLayout(layout);
	layout->addStretch(1);
	QVBoxLayout *selectorLayout = new QVBoxLayout;
	layout->addLayout(selectorLayout);
	layout->addStretch(1);
	QVBoxLayout *contentLayout = new QVBoxLayout;
	layout->addLayout(contentLayout);
	layout->addStretch(1);

	// left layout
	selectorLayout->addStretch(1);
	accountView = new QTableView;
	Styler::styleTableView(accountView);
	accountView->verticalHeader()->hide();
	accountView->horizontalHeader()->hide();
	accountView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	accountView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
	accountView->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
	accountView->setSelectionBehavior(QAbstractItemView::SelectRows);
	accountView->setSelectionMode(QAbstractItemView::SingleSelection);
	accountView->setAlternatingRowColors(true);
	accountView->setModel(&incomeAccountListDataModel);
	selectorLayout->addWidget(accountView);
	selectorLayout->addStretch(1);
	newButton = new QPushButton(tr("New"));
	Styler::styleButton(newButton);
	selectorLayout->addWidget(newButton);
	selectorLayout->setAlignment(newButton, Qt::AlignHCenter);

	// right layout
	contentLayout->addStretch(1);
	QGridLayout *inputLayout = new QGridLayout;
	contentLayout->addLayout(inputLayout);

	QLabel *label = new QLabel(tr("Name: "));
	Styler::styleLabel(label);
	inputLayout->addWidget(label, 0, 0);
	nameEdit = new QLineEdit;
	Styler::styleLineEdit(nameEdit);
	inputLayout->addWidget(nameEdit, 0, 1);

	accountEnabled = new QCheckBox(tr("Enabled"));
	Styler::styleCheckBox(accountEnabled);
	inputLayout->addWidget(accountEnabled, 2, 1);

	// button buttons
	contentLayout->addStretch(1);
	QHBoxLayout *bottomButtonLayout = new QHBoxLayout;
	contentLayout->addLayout(bottomButtonLayout);
	bottomButtonLayout->addStretch(1);
	editButton = new QPushButton(tr(" Edit "));
	Styler::styleButton(editButton);
	bottomButtonLayout->addWidget(editButton);
	cancelButton = new QPushButton(tr("Cancel"));
	Styler::styleButton(cancelButton);
	cancelButton->setEnabled(false);
	bottomButtonLayout->addWidget(cancelButton);
	saveButton = new QPushButton(tr(" Save "));
	Styler::styleButton(saveButton);
	saveButton->setEnabled(false);
	bottomButtonLayout->addWidget(saveButton);
	bottomButtonLayout->addStretch(1);

	enableForms(false);

	connect(accountView, &QTableView::clicked, this, &SetupCampaignsWidget::userSelected);
	connect(editButton, &QPushButton::clicked, this, &SetupCampaignsWidget::editClicked);
	connect(cancelButton, &QPushButton::clicked, this, &SetupCampaignsWidget::checkForSave);
	connect(saveButton, &QPushButton::clicked, this, &SetupCampaignsWidget::saveClicked);
	connect(nameEdit, &QLineEdit::textEdited, this, &SetupCampaignsWidget::updateSaveEnabled);
	connect(accountEnabled, &QCheckBox::clicked, this, &SetupCampaignsWidget::accountEnableClicked);
	connect(newButton, &QPushButton::clicked, this, &SetupCampaignsWidget::newClicked);

	topicsToReceive << "campaigns";
	cm.registerApiForReceiver(this);
}

void SetupCampaignsWidget::selected()
{
	cm.foxmq.subscription("campaigns.list-all", true);
}

void SetupCampaignsWidget::unselected()
{
	cm.foxmq.subscription("campaigns.list-all", false);
}

void SetupCampaignsWidget::newClicked()
{
	selectedAccountId = -1;
	newFlag = true;
	newButton->setEnabled(false);
	clearForms();
	clearFormsModifiedFlags();  // must be after clearForms() so form isModified() is false
	enableForms(true);
	accountEnabled->setChecked(true);
	editButton->setEnabled(false);
	cancelButton->setEnabled(true);
}

void SetupCampaignsWidget::processPublish(const QByteArray &topic, ssize_t, FoxID, FoxHead::ContentType, const QByteArray &content)
{
	if (topic == "campaigns.list-all") {
		incomeAccountListDataModel.layoutAboutToBeChanged();
		incomeAccountListDataModel.clear();
		incomeAccountListDataModel.append(QJsonDocument::fromJson(content).array());
		incomeAccountListDataModel.layoutChanged();
		accountView->resizeColumnsToContents();
		if (selectedAccountId > -1)
			populateFormsWithAccountInfo(indexOfSelectedAccount.row());
	}
}

void SetupCampaignsWidget::processAck(const QByteArray &topic)
{
	if (topic == "campaigns.lock") {
		enableForms(true);
		editButton->setEnabled(false);
		cancelButton->setEnabled(true);
		newButton->setEnabled(false);
	}
}

void SetupCampaignsWidget::processNack(const QByteArray &topic)
{
	if (topic == "campaigns.lock") {
		connect(mainWindow.userInput, &UserInput::button1Clicked, this, &SetupCampaignsWidget::okFromLockNack);
		mainWindow.userInput->askForInput(tr("Another user is modifying income account setup, try again later"));
		mainWindow.showUserInput();
	}
}

void SetupCampaignsWidget::okFromLockNack()
{
	disconnect(mainWindow.userInput, &UserInput::button1Clicked, this, &SetupCampaignsWidget::okFromLockNack);
	mainWindow.showMainScreen();
}

void SetupCampaignsWidget::editClicked()
{
	cm.foxmq.post("campaigns.lock", FoxHead::ContentType::Empty, "");
}

void SetupCampaignsWidget::enableForms(bool enabled)
{
	nameEdit->setReadOnly(!enabled);
	accountEnabled->setEnabled(enabled);
}

void SetupCampaignsWidget::clearForms()
{
	nameEdit->clear();
	accountEnabled->setChecked(false);
}

void SetupCampaignsWidget::clearFormsModifiedFlags()
{
	nameEdit->setModified(false);
	accountEnabledmodified = false;
}

bool SetupCampaignsWidget::formsModified()
{
	return nameEdit->isModified() || accountEnabledmodified;
}

void SetupCampaignsWidget::accountEnableClicked()
{
	accountEnabledmodified = true;
	updateSaveEnabled();
}

bool SetupCampaignsWidget::checkForSave()
{
	bool changed = false;
	if (formsModified()) {
		changed = true;
		connect(mainWindow.userInput, &UserInput::button1Clicked, this, &SetupCampaignsWidget::saveConfirmDiscardClicked);
		connect(mainWindow.userInput, &UserInput::button2Clicked, this, &SetupCampaignsWidget::saveConfirmCancelClicked);
		connect(mainWindow.userInput, &UserInput::button3Clicked, this, &SetupCampaignsWidget::saveConfirmSaveClicked);
		if (saveButton->isEnabled())
			mainWindow.userInput->askForInput(tr("Changes have been made"), tr("Discard Changes"), tr("Continue Editing"), tr("      Save      "));
		else
			mainWindow.userInput->askForInput(tr("Changes have been made"), tr("Discard Changes"), tr("Continue Editing"));
		mainWindow.showUserInput();
	} else if (cancelButton->isEnabled()) {
		doneEdits();  // editing has been enabled, but no changes, so unlock
	}
	return changed;
}

void SetupCampaignsWidget::saveConfirmSaveClicked()
{
	saveConfirmCleanUp();
	saveClicked();
}

void SetupCampaignsWidget::saveConfirmDiscardClicked()
{
	saveConfirmCleanUp();
	doneEdits();
}

void SetupCampaignsWidget::saveConfirmCancelClicked()
{
	saveConfirmCleanUp();
	accountView->setCurrentIndex(indexOfSelectedAccount);
}

void SetupCampaignsWidget::saveConfirmCleanUp()
{
	disconnect(mainWindow.userInput, &UserInput::button1Clicked, this, &SetupCampaignsWidget::saveConfirmDiscardClicked);
	disconnect(mainWindow.userInput, &UserInput::button2Clicked, this, &SetupCampaignsWidget::saveConfirmCancelClicked);
	disconnect(mainWindow.userInput, &UserInput::button3Clicked, this, &SetupCampaignsWidget::saveConfirmSaveClicked);
	mainWindow.showMainScreen();
}

void SetupCampaignsWidget::saveClicked()
{
	QJsonDocument jdoc;
	QJsonObject json;
	json.insert("name", nameEdit->text());
	json.insert("enabled", accountEnabled->isChecked());
	const char *api;
	if (newFlag) {
		api = "campaigns.add";
	} else {
		api = "campaigns.update";
		json.insert("id", selectedAccountId);
	}
	jdoc.setObject(json);
	cm.foxmq.post(api, FoxHead::ContentType::Json, jdoc.toJson(QJsonDocument::Compact));
	doneEdits();
}

void SetupCampaignsWidget::doneEdits()
{
	selectedAccountId = -1;
	enableForms(false);
	clearForms();
	clearFormsModifiedFlags();  // must be after clearForms() so form isModified() is false
	saveButton->setEnabled(false);
	cancelButton->setEnabled(false);
	editButton->setEnabled(false);
	newButton->setEnabled(true);
	accountView->setCurrentIndex(incomeAccountListDataModel.index(-1, -1));
	if (!newFlag)
		cm.foxmq.post("campaigns.unlock", FoxHead::ContentType::Empty, "");
	newFlag = false;
}

void SetupCampaignsWidget::updateSaveEnabled()
{
	if (!nameEdit->text().isEmpty())
		saveButton->setEnabled(true);
	else
		saveButton->setEnabled(false);
}

void SetupCampaignsWidget::userSelected(QModelIndex index)
{
	int row = index.row();
	int newSelectedAccountId = incomeAccountListDataModel.getID(row);
	if (newSelectedAccountId != selectedAccountId) {
		if (!checkForSave()) {
			indexOfSelectedAccount = index;
			selectedAccountId = newSelectedAccountId;
			populateFormsWithAccountInfo(row);
			enableForms(false);
			editButton->setEnabled(true);
		}
	}
}

void SetupCampaignsWidget::populateFormsWithAccountInfo(int row)
{
	nameEdit->setText(incomeAccountListDataModel.name(row));
	accountEnabled->setChecked(incomeAccountListDataModel.enabled(row));
}
