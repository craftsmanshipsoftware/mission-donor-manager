// clang-format off
/**************************************************************************************************
	Copyright (C) 2023-2024  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QLabel>
#include <QPushButton>
#include <QLineEdit>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include "../common/permission.h"
#include "main-window.h"
#include "connection-manager.h"
#include "styler.h"
#include "theme.h"
#include "qexclusivebuttons.h"
#include "qslidingstackedwidget.h"
#include "setup-general-widget.h"
#include "setup-income-accounts-widget.h"
#include "setup-advantage-accounts-widget.h"
#include "missions-setup-screen.h"
#include "setup-campaigns-widget.h"

#include "setup-screen.h"

SetupScreen::SetupScreen(MainWindow &mainWindow, Permission &p) : mainWindow(mainWindow)
{
	QVBoxLayout *layout = new QVBoxLayout;
	setLayout(layout);
	int m = Theme::OuterMargin * qMin(screen()->geometry().width(), screen()->geometry().height());
	layout->setContentsMargins(m, m, m, m);
	QHBoxLayout *topLayout = new QHBoxLayout;
	layout->addLayout(topLayout);
	QHBoxLayout *bottomLayout = new QHBoxLayout;
	layout->addLayout(bottomLayout);

	topLayout->addStretch(1);
	QLabel *title = new QLabel(tr("Setup"));
	Styler::styleLabel(title, Theme::H1Size);
	topLayout->addWidget(title);
	//layout->setAlignment(title, Qt::AlignHCenter);
	topLayout->addStretch(1);
	QStringList buttonTitles;
	if (p.hasPermission(Permission::Setup))
		buttonTitles+= { tr("General"), tr("Income Accounts"), tr("Advantage Accounts") };
	if (p.hasPermission(Permission::MissionRead))
		buttonTitles+= tr("Missions");
	if (p.hasPermission(Permission::Campaigns))
		buttonTitles+= tr("Campaigns");
	selectorButtons = new QExclusiveButtons(buttonTitles);
	topLayout->addWidget(selectorButtons);
	for ( int i=0; i < selectorButtons->size(); i++)
		Styler::styleButton(selectorButtons->buttonAt(i));
	selectorButtons->buttonAt(0)->setChecked(true);

	setups = new QSlidingStackedWidget();
	setups->setDuration(500);
	bottomLayout->addWidget(setups);
	if (p.hasPermission(Permission::Setup)) {
		setupGeneral = new SetupGeneralWidget(mainWindow);
		setups->addWidget(setupGeneral);
		setupIncomeAccounts = new SetupIncomeAccountsWidget(mainWindow);
		setups->addWidget(setupIncomeAccounts);
		setupAdvantageAccounts = new SetupAdvantageAccountsWidget(mainWindow);
		setups->addWidget(setupAdvantageAccounts);
	}
	if (p.hasPermission(Permission::MissionRead)) {
		missionsSetupScreen = new MissionsSetupScreen(mainWindow, p);
		setups->addWidget(missionsSetupScreen);
	}
	if (p.hasPermission(Permission::Campaigns)) {
		setupCampaigns = new SetupCampaignsWidget(mainWindow);
		setups->addWidget(setupCampaigns);
	}

	connect(selectorButtons, &QExclusiveButtons::clicked, [&](int i) {
		setups->setCurrentIndex(i);
	});
}

void SetupScreen::subscribe()
{
	if (setupGeneral) {
		setupGeneral->selected();
		setupIncomeAccounts->selected();
	}

	if (setupAdvantageAccounts)
		setupAdvantageAccounts->selected();

	if (setupCampaigns)
		setupCampaigns->selected();
	if (missionsSetupScreen)
		missionsSetupScreen->selected();
}

void SetupScreen::unsubscribe()
{
	if (setupGeneral) {
		setupGeneral->unselected();
		setupIncomeAccounts->unselected();
	}

	if (setupAdvantageAccounts)
		setupAdvantageAccounts->unselected();

	if (setupCampaigns)
		setupCampaigns->unselected();

	if (missionsSetupScreen)
		missionsSetupScreen->unselected();
}
