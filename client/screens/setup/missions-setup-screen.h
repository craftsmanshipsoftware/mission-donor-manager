// clang-format off
/**************************************************************************************************
	Copyright (C) 2023-2024  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef MDM_MISSIONARIES_SCREEN_H_
#define MDM_MISSIONARIES_SCREEN_H_

#include <QWidget>
#include <QModelIndex>
#include <QDoubleValidator>
#include "receiver.h"

class MainWindow;
class ConnectionManager;
class NameSelectorWidget;
class QPushButton;
class QLabel;
class QLineEdit;
class QComboBox;
class QRadioButton;
class QCheckBox;
class QDoubleValidator;
class Permission;

class MissionsSetupScreen : public QWidget, public Receiver
{
	Q_OBJECT

public:
	MissionsSetupScreen(MainWindow &mainWindow, Permission &p);
	void selected();
	void unselected();

private:
	void processPublish(const QByteArray &topic, ssize_t count, FoxID id, FoxHead::ContentType contentType, const QByteArray &content) override;
	void processPostResponce(const QByteArray &topic, FoxID id, FoxHead::ContentType contentType, const QByteArray &content) override;
	void processAck(const QByteArray &topic) override;
	void processNack(const QByteArray &topic) override;
	void groupChanged(int group);
	void missionSelected(int missionID, QModelIndex index);
	bool formsModified();
	bool checkForSave();
	void saveConfirmSaveClicked();
	void saveConfirmDiscardClicked();
	void saveConfirmCancelClicked();
	void saveConfirmCleanUp();
	void saveClicked();
	void newClicked();
	void editClicked();
	void cancelClicked();
	void clearFormsModifiedFlags();
	void doneEdits(bool blankForms = true);
	void clearForms();
	void okFromLockNack();
	void enableForms(bool enabled);
	void rateTypeChanged();
	void updateSaveEnabled();
	void neftdClicked();
	void isProxyClicked(bool enabled);
	void proxySelectorClicked();
	MainWindow &mainWindow;
	ConnectionManager &cm;
	NameSelectorWidget *missionSelector;
	QLabel *accountLabel;
	QPushButton *newButton;
	QPushButton *editButton;
	QPushButton *cancelButton;
	QPushButton *saveButton;
	QLabel *nameLabel;
	QLineEdit *name;
	QLineEdit *percent;
	QLineEdit *account;
	QLineEdit *buttonId;
	QRadioButton *standardRate;
	QRadioButton *customRate;
	QCheckBox *neftd;
	QCheckBox *isProxy;
	QComboBox *proxySelector;
	QList<int> proxyIdList;
	QDoubleValidator percentValidator;
	QModelIndex indexOfMissionUnderEdit;
	int missionUnderEdit = -1;
	bool newFlag = false;
	bool generalCustomChanged = false;
	bool selectLastMission = false;
	bool neftdModified = false;
	bool isProxyModified = false;
	bool proxySelectorModified = false;
};

#endif
