// clang-format off
/**************************************************************************************************
	Copyright (C) 2023-2014  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QScreen>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QRadioButton>
#include <QCheckBox>
#include <QComboBox>
#include <QGroupBox>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QDoubleValidator>
#include "permission.h"
#include "main-window.h"
#include "connection-manager.h"
#include "user-input.h"
#include "styler.h"
#include "theme.h"
#include "name-selector-widget.h"
#include "missions-setup-screen.h"

#include <QDebug>

MissionsSetupScreen::MissionsSetupScreen(MainWindow &mainWindow, Permission &p) : mainWindow(mainWindow), cm(mainWindow.connectionManager)
{
	QVBoxLayout *layout = new QVBoxLayout;
	setLayout(layout);
	int m = Theme::OuterMargin * qMin(screen()->geometry().width(), screen()->geometry().height());
	layout->setContentsMargins(m, m, m, m);
	QHBoxLayout *topLayout = new QHBoxLayout;
	layout->addLayout(topLayout);
	QHBoxLayout *bottomLayout = new QHBoxLayout;
	layout->addLayout(bottomLayout);

	topLayout->addStretch(1);
	QLabel *title = new QLabel(tr("Mission Setup"));
	Styler::styleLabel(title, Theme::H1Size);
	topLayout->addWidget(title);
	topLayout->addStretch(1);

	// bottom layout
	bottomLayout->addStretch(2);
	QVBoxLayout *selectorLayout = new QVBoxLayout;
	bottomLayout->addLayout(selectorLayout);
	bottomLayout->setStretchFactor(selectorLayout, 2);
	missionSelector = new NameSelectorWidget;
	selectorLayout->addWidget(missionSelector);
	newButton = new QPushButton(tr("new"));
	Styler::styleButton(newButton);
	newButton->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
	Styler::setRetainSizeWhenHidden(newButton, true);
	selectorLayout->addWidget(newButton);
	selectorLayout->setAlignment(newButton, Qt::AlignHCenter);
	if (!p.hasPermission(Permission::MissionModify))
		newButton->hide();

	bottomLayout->addStretch(1);

	QVBoxLayout *setupLayout = new QVBoxLayout;
	setupLayout->addStretch(3);
	bottomLayout->addLayout(setupLayout);
	bottomLayout->setStretchFactor(setupLayout, 2);

	nameLabel = new QLabel(tr("Mission Name"));
	Styler::styleLabel(nameLabel);
	Styler::setRetainSizeWhenHidden(nameLabel, true);
	setupLayout->addWidget(nameLabel);
	name = new QLineEdit;
	Styler::styleLineEdit(name);
	Styler::setRetainSizeWhenHidden(name, true);
	setupLayout->addWidget(name);
	setupLayout->addStretch(1);

	QGroupBox *percentageGroup = new QGroupBox(tr("Operations / Admin Fee %"));
	Styler::styleGroupBox(percentageGroup);
	setupLayout->addWidget(percentageGroup);
	QVBoxLayout *percentageLayout = new QVBoxLayout;
	percentageGroup->setLayout(percentageLayout);
	standardRate = new QRadioButton("Standard Rate");
	Styler::styleRadioButton(standardRate);
	percentageLayout->addWidget(standardRate);
	customRate = new QRadioButton("Custom Rate");
	Styler::styleRadioButton(customRate);
	percentageLayout->addWidget(customRate);
	percent = new QLineEdit;
	Styler::styleLineEdit(percent);
	percentValidator.setRange(0, 100, 1);  // 0 --> 100, 1 decimal place
	percent->setValidator(&percentValidator);
	percentageLayout->addWidget(percent);
	setupLayout->addStretch(1);
	accountLabel = new QLabel(tr("QuickBooks Account Number"));
	Styler::styleLabel(accountLabel);
	setupLayout->addWidget(accountLabel);
	account = new QLineEdit;
	Styler::styleLineEdit(account);
	setupLayout->addWidget(account);
	setupLayout->addStretch(1);
	QLabel *buttonIDLabel = new QLabel(tr("Paypal Button ID"));
	Styler::styleLabel(buttonIDLabel);
	setupLayout->addWidget(buttonIDLabel);
	buttonId = new QLineEdit;
	Styler::styleLineEdit(buttonId);
	setupLayout->addWidget(buttonId);

	setupLayout->addStretch(1);

	QLabel *proxyLabel = new QLabel(tr("Proxy this account to"));
	Styler::styleLabel(proxyLabel);
	setupLayout->addWidget(proxyLabel);
	proxySelector = new QComboBox;
	Styler::styleComboBox(proxySelector);
	setupLayout->addWidget(proxySelector);

	setupLayout->addStretch(1);

	isProxy = new QCheckBox(tr("Is a Proxy Account"));
	Styler::styleCheckBox(isProxy);
	setupLayout->addWidget(isProxy);

	setupLayout->addStretch(1);

	neftd = new QCheckBox(tr("Not Eligible for Tax Deduction"));
	Styler::styleCheckBox(neftd);
	setupLayout->addWidget(neftd);

	setupLayout->addStretch(3);

	QHBoxLayout *editButtonLayout = new QHBoxLayout;
	setupLayout->addLayout(editButtonLayout);
	editButton = new QPushButton(tr("Edit"));
	Styler::styleButton(editButton);
	editButton->setEnabled(false);
	editButtonLayout->addWidget(editButton);
	if (!p.hasPermission(Permission::MissionModify))
		editButton->hide();
	cancelButton = new QPushButton(tr("Cancel"));
	Styler::styleButton(cancelButton);
	cancelButton->setEnabled(false);
	editButtonLayout->addWidget(cancelButton);
	if (!p.hasPermission(Permission::MissionModify))
		cancelButton->hide();
	saveButton = new QPushButton(tr("Save"));
	Styler::styleButton(saveButton);
	saveButton->setEnabled(false);
	editButtonLayout->addWidget(saveButton);
	if (!p.hasPermission(Permission::MissionModify))
		saveButton->hide();

	bottomLayout->addStretch(6);

	enableForms(false);
	connect(newButton, &QPushButton::clicked, this, &MissionsSetupScreen::newClicked);
	connect(missionSelector, &NameSelectorWidget::nameSelected, this, &MissionsSetupScreen::missionSelected);
	connect(editButton, &QPushButton::clicked, this, &MissionsSetupScreen::editClicked);
	connect(cancelButton, &QPushButton::clicked, this, &MissionsSetupScreen::cancelClicked);
	connect(saveButton, &QPushButton::clicked, this, &MissionsSetupScreen::saveClicked);
	connect(standardRate, &QRadioButton::clicked, this, &MissionsSetupScreen::rateTypeChanged);
	connect(customRate, &QRadioButton::clicked, this, &MissionsSetupScreen::rateTypeChanged);
	connect(percent, &QLineEdit::textEdited, this, &MissionsSetupScreen::updateSaveEnabled);
	connect(buttonId, &QLineEdit::textEdited, this, &MissionsSetupScreen::updateSaveEnabled);
	connect(account, &QLineEdit::textEdited, this, &MissionsSetupScreen::updateSaveEnabled);
	connect(name, &QLineEdit::textEdited, this, &MissionsSetupScreen::updateSaveEnabled);
	connect(neftd, &QCheckBox::clicked, this, &MissionsSetupScreen::neftdClicked);
	connect(isProxy, &QCheckBox::clicked, this, &MissionsSetupScreen::isProxyClicked);
	connect(proxySelector, qOverload<int>(&QComboBox::activated), this, &MissionsSetupScreen::proxySelectorClicked);

	topicsToReceive << "mission";
	cm.registerApiForReceiver(this);
}

void MissionsSetupScreen::selected()
{
	cm.foxmq.subscription("mission.ro.list", true);
	cm.foxmq.subscription("mission.ro.updated", true);
}

void MissionsSetupScreen::unselected()
{
	cm.foxmq.subscription("mission.ro.list", false);
	cm.foxmq.subscription("mission.ro.updated", false);
}

void MissionsSetupScreen::processPublish(const QByteArray &topic, ssize_t, FoxID, FoxHead::ContentType, const QByteArray &content)
{
	QJsonArray jArray = QJsonDocument::fromJson(content).array();
	if (topic == "mission.ro.list") {
		missionSelector->updateNameList(jArray);
		if (selectLastMission) {
			selectLastMission = false;
			missionSelector->selectEndOfDataModel();
		}
		indexOfMissionUnderEdit = missionSelector->indexOfId(missionUnderEdit);  // QModelIndex indexOfMissionUnderEdit is invalid from the updated list.  Fix using updated list
	} else if(topic == "mission.ro.list-proxy") {
		proxySelector->clear();
		proxyIdList.clear();
		proxyIdList.append(-1);
		proxySelector->addItem(tr("Do not proxy"));
		for (int i=0; i<jArray.size(); i++) {
			QJsonObject json = jArray.at(i).toObject();
			proxySelector->addItem(json.value("forename").toString() + json.value("surname").toString());
			proxyIdList.append(json.value("id").toInt());
		}
	} else if (topic == "mission.ro.updated") {
		int missionID = content.toInt();
		if (missionID == missionUnderEdit)
			cm.foxmq.post("mission.ro.account", FoxHead::ContentType::IntAsString, QByteArray::number(missionUnderEdit));
	}
}

void MissionsSetupScreen::processPostResponce(const QByteArray &topic, FoxID, FoxHead::ContentType, const QByteArray &content)
{
	if (topic == "mission.ro.account") {
		QJsonObject json = QJsonDocument::fromJson(content).object();
		account->setText(json.value("account").toString());
		double percentValue = json.value("percent").toDouble();
		if (percentValue < 0.0f) {
			standardRate->setChecked(true);
			percent->clear();
		} else {
			customRate->setChecked(true);
			percent->setText(QString::number(percentValue * 100));
		}
		name->setText(json.value("name").toString());
		buttonId->setText(json.value("button-id").toString());
		neftd->setChecked(json.value("neftd").toBool());
		isProxy->setChecked(json.value("is-proxy").toBool());
		proxySelector->setCurrentIndex(0);
		int proxyId = json.value("proxy").toInt();
		if (proxyId > 0) {
			for (int i = 0; i < proxyIdList.size(); i++) {
				if (proxyIdList.at(i) == proxyId) {
					proxySelector->setCurrentIndex(i);
					break;
				}
			}
			accountLabel->setText(tr("Account Number"));
		} else {
			accountLabel->setText(tr("QuickBooks Account Number"));
		}
		clearFormsModifiedFlags();
	}
}

void MissionsSetupScreen::processAck(const QByteArray &topic)
{
	if (topic == "mission.lock") {
		enableForms(true);
		newButton->setEnabled(false);
		editButton->setEnabled(false);
		cancelButton->setEnabled(true);
	}
}

void MissionsSetupScreen::processNack(const QByteArray &topic)
{
	if (topic == "mission.lock") {
		connect(mainWindow.userInput, &UserInput::button1Clicked, this, &MissionsSetupScreen::okFromLockNack);
		mainWindow.userInput->askForInput(tr("Another user is editing this Mission, try again later"));
		mainWindow.showUserInput();
	} else if (topic == "mission.add" || topic == "mission.update") {
		connect(mainWindow.userInput, &UserInput::button1Clicked, this, &MissionsSetupScreen::okFromLockNack);
		mainWindow.userInput->askForInput(tr("Failed to Add or Update mission.  Check that mission name and button ID are unique \nand account does not conflict with an Income or Advantage account."));
		mainWindow.showUserInput();
	}
}

void MissionsSetupScreen::missionSelected(int missionID, QModelIndex index)
{
	if (missionID != missionUnderEdit) {
		if (!checkForSave()) {
			indexOfMissionUnderEdit = index;
			missionUnderEdit = missionID;
			enableForms(false);
			editButton->setEnabled(true);
			cm.foxmq.post("mission.ro.account", FoxHead::ContentType::IntAsString, QByteArray::number(missionID));
		}
	}
}

void MissionsSetupScreen::editClicked()
{
	cm.foxmq.post("mission.lock", FoxHead::ContentType::IntAsString, QByteArray::number(missionUnderEdit));
}

void MissionsSetupScreen::cancelClicked()
{
	if (newFlag)
		missionSelector->clearIndexAndSelection();
	checkForSave();
}

bool MissionsSetupScreen::formsModified()
{
	return name->isModified() || percent->isModified() || buttonId->isModified() || account->isModified() || generalCustomChanged || isProxyModified || proxySelectorModified || neftdModified;
}

bool MissionsSetupScreen::checkForSave()
{
	bool changed = false;
	if (formsModified()) {
		changed = true;
		connect(mainWindow.userInput, &UserInput::button1Clicked, this, &MissionsSetupScreen::saveConfirmDiscardClicked);
		connect(mainWindow.userInput, &UserInput::button2Clicked, this, &MissionsSetupScreen::saveConfirmCancelClicked);
		connect(mainWindow.userInput, &UserInput::button3Clicked, this, &MissionsSetupScreen::saveConfirmSaveClicked);
		if (saveButton->isEnabled())
			mainWindow.userInput->askForInput(tr("Changes have been made"), tr("Discard Changes"), tr("Continue Editing"), tr("      Save      "));
		else
			mainWindow.userInput->askForInput(tr("Changes have been made"), tr("Discard Changes"), tr("Continue Editing"));
		mainWindow.showUserInput();
	} else if (cancelButton->isEnabled()) {
		doneEdits(false);  // editing has been enabled, but no changes, so unlock
	}
	return changed;
}

void MissionsSetupScreen::saveConfirmSaveClicked()
{
	saveConfirmCleanUp();
	saveClicked();
}

void MissionsSetupScreen::saveConfirmDiscardClicked()
{
	bool refreshForms = formsModified() && !newFlag;
	saveConfirmCleanUp();
	doneEdits();
	if (refreshForms) { // on discard from edit only, refresh forms from server
		cm.foxmq.post("mission.ro.account", FoxHead::ContentType::IntAsString, QByteArray::number(missionUnderEdit));
		missionSelector->setCurrentIndex(indexOfMissionUnderEdit);
	}
}

void MissionsSetupScreen::saveConfirmCancelClicked()
{
	saveConfirmCleanUp();
}

void MissionsSetupScreen::saveConfirmCleanUp()
{
	disconnect(mainWindow.userInput, &UserInput::button1Clicked, this, &MissionsSetupScreen::saveConfirmDiscardClicked);
	disconnect(mainWindow.userInput, &UserInput::button2Clicked, this, &MissionsSetupScreen::saveConfirmCancelClicked);
	disconnect(mainWindow.userInput, &UserInput::button3Clicked, this, &MissionsSetupScreen::saveConfirmSaveClicked);
	mainWindow.showMainScreen();
}

void MissionsSetupScreen::saveClicked()
{
	if (newFlag)
		missionSelector->clearSearchFilter();
	QJsonDocument jdoc;
	QJsonObject json;
	json.insert("id", missionUnderEdit);
	json.insert("account", account->text());
	double percentValue;
	if (standardRate->isChecked())
		percentValue = -0.02;  // negative = use general rate
	else
		percentValue = percent->text().toDouble() / 100;
	json.insert("percent", percentValue);
	json.insert("name", name->text());
	json.insert("button-id", buttonId->text());
	json.insert("neftd", neftd->isChecked());
	json.insert("is-proxy", isProxy->isChecked());
	json.insert("proxy", proxyIdList.at(proxySelector->currentIndex()));

	jdoc.setObject(json);
	QByteArray api;
	if (newFlag)
		api = "mission.add";
	else
		api = "mission.update";
	cm.foxmq.post(api, FoxHead::ContentType::Json, jdoc.toJson(QJsonDocument::Compact));

	if (newFlag)
		selectLastMission = true;
	doneEdits(false);
}

void MissionsSetupScreen::newClicked()
{
	missionUnderEdit = -1;
	newFlag = true;
	clearForms();
	enableForms(true);
	accountLabel->setText(tr("QuickBooks Account Number"));
	proxySelector->setCurrentIndex(0);
	newButton->setEnabled(false);
	editButton->setEnabled(false);
	cancelButton->setEnabled(true);
	missionSelector->clearIndexAndSelection();
}

void MissionsSetupScreen::clearFormsModifiedFlags()
{
	generalCustomChanged = false;
	percent->setModified(false);
	buttonId->setModified(false);
	account->setModified(false);
	name->setModified(false);
	neftdModified = false;
	isProxyModified = false;
	proxySelectorModified = false;
}

void MissionsSetupScreen::doneEdits(bool blankForms)
{
	saveButton->setEnabled(false);
	enableForms(false);
	newButton->setEnabled(true);
	cancelButton->setEnabled(false);
	if (blankForms)
		clearForms();
	else
		clearFormsModifiedFlags();
	if (!newFlag) {
		editButton->setEnabled(true);
		cm.foxmq.post("mission.unlock", FoxHead::ContentType::IntAsString, QByteArray::number(missionUnderEdit));
	}
	newFlag = false;
}

void MissionsSetupScreen::clearForms()
{
	percent->clear();
	buttonId->clear();
	account->clear();
	name->clear();
	neftd->setChecked(false);
	isProxy->setChecked(false);
	clearFormsModifiedFlags();
}

void MissionsSetupScreen::okFromLockNack()
{
	disconnect(mainWindow.userInput, &UserInput::button1Clicked, this, &MissionsSetupScreen::okFromLockNack);
	mainWindow.showMainScreen();
}

void MissionsSetupScreen::enableForms(bool enabled)
{
	percent->setReadOnly(!enabled);
	buttonId->setReadOnly(!enabled);
	account->setReadOnly(!enabled);
	name->setReadOnly(!enabled);
	standardRate->setEnabled(enabled);
	customRate->setEnabled(enabled);
	neftd->setEnabled(enabled);
	isProxy->setEnabled(enabled);
	proxySelector->setEnabled(enabled && !isProxy->isChecked());
}

void MissionsSetupScreen::rateTypeChanged()
{
	percent->setEnabled(customRate->isChecked());
	generalCustomChanged = true;
	updateSaveEnabled();
}

void MissionsSetupScreen::updateSaveEnabled()
{
	QString percentString = percent->text();
	int pos = 0;
	if (((customRate->isChecked() && percentValidator.validate(percentString, pos) == QValidator::Acceptable) || standardRate->isChecked()) && formsModified())
		saveButton->setEnabled(true);
	else
		saveButton->setEnabled(false);
}

void MissionsSetupScreen::neftdClicked()
{
	neftdModified = true;
	updateSaveEnabled();
}

void MissionsSetupScreen::isProxyClicked(bool enabled)
{
	isProxyModified = true;
	updateSaveEnabled();
	proxySelector->setEnabled(!enabled);
	proxySelector->setCurrentIndex(0);
}

void MissionsSetupScreen::proxySelectorClicked()
{
	proxySelectorModified = true;
	updateSaveEnabled();
	if (proxySelector->currentIndex() != 0)
		accountLabel->setText(tr("Account Number"));
	else
		accountLabel->setText(tr("QuickBooks Account Number"));
}
