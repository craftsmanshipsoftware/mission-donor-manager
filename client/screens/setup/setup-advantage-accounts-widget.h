// clang-format off
/**************************************************************************************************
	Copyright (C) 2024  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef MDM_SETUP_ADVANTAGE_ACCOUNTS_WIDGET_H_
#define MDM_SETUP_ADVANTAGE_ACCOUNTS_WIDGET_H_

#include <QWidget>
//#include "tab-screen.h"
#include "receiver.h"
#include "setup-income-accounts-dm.h"

class QPushButton;
class QLineEdit;
class QCheckBox;
class QTableView;
class MainWindow;
class ConnectionManager;

class SetupAdvantageAccountsWidget : public QWidget, public Receiver
{
	Q_OBJECT

public:
	SetupAdvantageAccountsWidget(MainWindow &mainWindow);
	void selected();
	void unselected();

private:
	void accountSelected(QModelIndex index);
	void processPublish(const QByteArray &topic, ssize_t count, FoxID id, FoxHead::ContentType contentType, const QByteArray &content) override;
	void processPostResponce(const QByteArray &, FoxID, FoxHead::ContentType, const QByteArray &) override {}
	void processAck(const QByteArray &topic) override;
	void processNack(const QByteArray &topic) override;
	void okFromLockNack();
	void newClicked();
	void editClicked();
	bool checkForSave();
	void saveConfirmSaveClicked();
	void saveConfirmDiscardClicked();
	void saveConfirmCancelClicked();
	void saveConfirmCleanUp();
	void saveClicked();
	void enableForms(bool enabled);
	void clearForms();
	void clearFormsModifiedFlags();
	bool formsModified();
	void accountEnableClicked();
	void doneEdits();
	void updateSaveEnabled();
	void populateFormsWithAccountInfo(int row);
	MainWindow &mainWindow;
	ConnectionManager &cm;
	QLineEdit *nameEdit;
	QLineEdit *numberEdit;
	QCheckBox *accountEnabled;
	QPushButton *newButton;
	QPushButton *editButton;
	QPushButton *saveButton;
	QPushButton *cancelButton;
	QTableView *accountView;
	IncomeAccountListDataModel incomeAccountListDataModel;
	int selectedAccountId = -1;
	QModelIndex indexOfSelectedAccount;
	bool newFlag = false;
	bool accountEnabledmodified = false;

};

#endif
