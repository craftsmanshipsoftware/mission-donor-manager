// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef MDM_SETUP_SOURCE_ACCOUNTS_DM_H_
#define MDM_SETUP_SOURCE_ACCOUNTS_DM_H_

#include <QAbstractListModel>


class IncomeAccountListDataModel : public QAbstractTableModel
{
	Q_OBJECT

public:
	int rowCount(const QModelIndex &parent = QModelIndex()) const override ;
	int columnCount(const QModelIndex &parent = QModelIndex()) const override;
	QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
	void clear();
	void append(QJsonArray jArray);
	inline uint getID(uint row) { return sourceAccountList.at(row).id; }
	inline const QString &name(uint row)  { return sourceAccountList.at(row).name; }
	inline const QString &number(uint row)  { return sourceAccountList.at(row).number; }
	inline bool enabled(uint row)  { return sourceAccountList.at(row).enabled; }

private:
	struct SourceAccountItem {
		int id;
		QString name;
		QString number;
		bool enabled;
	};
	QList<SourceAccountItem> sourceAccountList;
};

#endif
