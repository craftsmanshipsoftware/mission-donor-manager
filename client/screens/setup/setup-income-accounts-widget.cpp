// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QHBoxLayout>
#include <QLineEdit>
#include <QCheckBox>
#include <QPushButton>
#include <QJsonDocument>
#include <QJsonArray>
#include <QTableView>
#include <QHeaderView>
#include "theme.h"
#include "styler.h"
#include "main-window.h"
#include "connection-manager.h"
#include "user-input.h"
#include "setup-income-accounts-widget.h"

SetupIncomeAccountsWidget::SetupIncomeAccountsWidget(MainWindow &mainWindow) : mainWindow(mainWindow), cm(mainWindow.connectionManager)
{
	QHBoxLayout *layout = new QHBoxLayout;
	setLayout(layout);
	layout->addStretch(1);
	QVBoxLayout *selectorLayout = new QVBoxLayout;
	layout->addLayout(selectorLayout);
	layout->addStretch(1);
	QVBoxLayout *contentLayout = new QVBoxLayout;
	layout->addLayout(contentLayout);
	layout->addStretch(1);

	// left layout
	selectorLayout->addStretch(1);
	accountView = new QTableView;
	Styler::styleTableView(accountView);
	accountView->verticalHeader()->hide();
	accountView->horizontalHeader()->hide();
	accountView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	accountView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
	accountView->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
	accountView->setSelectionBehavior(QAbstractItemView::SelectRows);
	accountView->setSelectionMode(QAbstractItemView::SingleSelection);
	accountView->setAlternatingRowColors(true);
	accountView->setModel(&incomeAccountListDataModel);
	selectorLayout->addWidget(accountView);
	selectorLayout->addStretch(1);
	newButton = new QPushButton(tr("New"));
	Styler::styleButton(newButton);
	selectorLayout->addWidget(newButton);
	selectorLayout->setAlignment(newButton, Qt::AlignHCenter);

	// right layout
	contentLayout->addStretch(1);
	QGridLayout *inputLayout = new QGridLayout;
	contentLayout->addLayout(inputLayout);

	QLabel *label = new QLabel(tr("Name: "));
	Styler::styleLabel(label);
	inputLayout->addWidget(label, 0, 0);
	nameEdit = new QLineEdit;
	Styler::styleLineEdit(nameEdit);
	inputLayout->addWidget(nameEdit, 0, 1);

	label = new QLabel(tr("Number: "));
	Styler::styleLabel(label);
	inputLayout->addWidget(label, 1, 0);
	numberEdit = new QLineEdit;
	Styler::styleLineEdit(numberEdit);
	inputLayout->addWidget(numberEdit, 1, 1);

	accountEnabled = new QCheckBox(tr("Enabled"));
	Styler::styleCheckBox(accountEnabled);
	inputLayout->addWidget(accountEnabled, 2, 1);

	// button buttons
	contentLayout->addStretch(1);
	QHBoxLayout *bottomButtonLayout = new QHBoxLayout;
	contentLayout->addLayout(bottomButtonLayout);
	bottomButtonLayout->addStretch(1);
	editButton = new QPushButton(tr(" Edit "));
	Styler::styleButton(editButton);
	editButton->setEnabled(false);
	bottomButtonLayout->addWidget(editButton);
	cancelButton = new QPushButton(tr("Cancel"));
	Styler::styleButton(cancelButton);
	cancelButton->setEnabled(false);
	bottomButtonLayout->addWidget(cancelButton);
	saveButton = new QPushButton(tr(" Save "));
	Styler::styleButton(saveButton);
	saveButton->setEnabled(false);
	bottomButtonLayout->addWidget(saveButton);
	bottomButtonLayout->addStretch(1);

	enableForms(false);

	connect(accountView, &QTableView::clicked, this, &SetupIncomeAccountsWidget::accountSelected);
	connect(editButton, &QPushButton::clicked, this, &SetupIncomeAccountsWidget::editClicked);
	connect(cancelButton, &QPushButton::clicked, this, &SetupIncomeAccountsWidget::checkForSave);
	connect(saveButton, &QPushButton::clicked, this, &SetupIncomeAccountsWidget::saveClicked);
	connect(nameEdit, &QLineEdit::textEdited, this, &SetupIncomeAccountsWidget::updateSaveEnabled);
	connect(numberEdit, &QLineEdit::textEdited, this, &SetupIncomeAccountsWidget::updateSaveEnabled);
	connect(accountEnabled, &QCheckBox::clicked, this, &SetupIncomeAccountsWidget::accountEnableClicked);
	connect(newButton, &QPushButton::clicked, this, &SetupIncomeAccountsWidget::newClicked);

	topicsToReceive << "income-accounts";
	cm.registerApiForReceiver(this);
}

void SetupIncomeAccountsWidget::selected()
{
	cm.foxmq.subscription("income-accounts.list-all", true);
}

void SetupIncomeAccountsWidget::unselected()
{
	cm.foxmq.subscription("income-accounts.list-all", false);
}

void SetupIncomeAccountsWidget::newClicked()
{
	selectedAccountId = -1;
	newFlag = true;
	newButton->setEnabled(false);
	clearForms();
	clearFormsModifiedFlags();  // must be after clearForms() so form isModified() is false
	enableForms(true);
	accountEnabled->setChecked(true);
	editButton->setEnabled(false);
	cancelButton->setEnabled(true);
}

void SetupIncomeAccountsWidget::processPublish(const QByteArray &topic, ssize_t, FoxID, FoxHead::ContentType, const QByteArray &content)
{
	if (topic == "income-accounts.list-all") {
		incomeAccountListDataModel.layoutAboutToBeChanged();
		incomeAccountListDataModel.clear();
		incomeAccountListDataModel.append(QJsonDocument::fromJson(content).array());
		incomeAccountListDataModel.layoutChanged();
		accountView->resizeColumnsToContents();
		if (selectedAccountId > -1)
			populateFormsWithAccountInfo(indexOfSelectedAccount.row());
	}
}

void SetupIncomeAccountsWidget::processAck(const QByteArray &topic)
{
	if (topic == "income-accounts.lock") {
		enableForms(true);
		editButton->setEnabled(false);
		cancelButton->setEnabled(true);
		newButton->setEnabled(false);
	}
}

void SetupIncomeAccountsWidget::processNack(const QByteArray &topic)
{
	if (topic == "income-accounts.lock") {
		connect(mainWindow.userInput, &UserInput::button1Clicked, this, &SetupIncomeAccountsWidget::okFromLockNack);
		mainWindow.userInput->askForInput(tr("Another user is modifying income account setup, try again later"));
		mainWindow.showUserInput();
	}
}

void SetupIncomeAccountsWidget::okFromLockNack()
{
	disconnect(mainWindow.userInput, &UserInput::button1Clicked, this, &SetupIncomeAccountsWidget::okFromLockNack);
	mainWindow.showMainScreen();
}

void SetupIncomeAccountsWidget::editClicked()
{
	cm.foxmq.post("income-accounts.lock", FoxHead::ContentType::Empty, "");
}

void SetupIncomeAccountsWidget::enableForms(bool enabled)
{
	nameEdit->setReadOnly(!enabled);
	numberEdit->setReadOnly(!enabled);
	accountEnabled->setEnabled(enabled);
}

void SetupIncomeAccountsWidget::clearForms()
{
	nameEdit->clear();
	numberEdit->clear();
	accountEnabled->setChecked(false);
}

void SetupIncomeAccountsWidget::clearFormsModifiedFlags()
{
	nameEdit->setModified(false);
	numberEdit->setModified(false);
	accountEnabledmodified = false;
}

bool SetupIncomeAccountsWidget::formsModified()
{
	return nameEdit->isModified() || numberEdit->isModified() || accountEnabledmodified;
}

void SetupIncomeAccountsWidget::accountEnableClicked()
{
	accountEnabledmodified = true;
	updateSaveEnabled();
}

bool SetupIncomeAccountsWidget::checkForSave()
{
	bool changed = false;
	if (formsModified()) {
		changed = true;
		connect(mainWindow.userInput, &UserInput::button1Clicked, this, &SetupIncomeAccountsWidget::saveConfirmDiscardClicked);
		connect(mainWindow.userInput, &UserInput::button2Clicked, this, &SetupIncomeAccountsWidget::saveConfirmCancelClicked);
		connect(mainWindow.userInput, &UserInput::button3Clicked, this, &SetupIncomeAccountsWidget::saveConfirmSaveClicked);
		if (saveButton->isEnabled())
			mainWindow.userInput->askForInput(tr("Changes have been made"), tr("Discard Changes"), tr("Continue Editing"), tr("      Save      "));
		else
			mainWindow.userInput->askForInput(tr("Changes have been made"), tr("Discard Changes"), tr("Continue Editing"));
		mainWindow.showUserInput();
	} else if (cancelButton->isEnabled()) {
		doneEdits();  // editing has been enabled, but no changes, so unlock
	}
	return changed;
}

void SetupIncomeAccountsWidget::saveConfirmSaveClicked()
{
	saveConfirmCleanUp();
	saveClicked();
}

void SetupIncomeAccountsWidget::saveConfirmDiscardClicked()
{
	saveConfirmCleanUp();
	doneEdits();
}

void SetupIncomeAccountsWidget::saveConfirmCancelClicked()
{
	saveConfirmCleanUp();
	accountView->setCurrentIndex(indexOfSelectedAccount);
}

void SetupIncomeAccountsWidget::saveConfirmCleanUp()
{
	disconnect(mainWindow.userInput, &UserInput::button1Clicked, this, &SetupIncomeAccountsWidget::saveConfirmDiscardClicked);
	disconnect(mainWindow.userInput, &UserInput::button2Clicked, this, &SetupIncomeAccountsWidget::saveConfirmCancelClicked);
	disconnect(mainWindow.userInput, &UserInput::button3Clicked, this, &SetupIncomeAccountsWidget::saveConfirmSaveClicked);
	mainWindow.showMainScreen();
}

void SetupIncomeAccountsWidget::saveClicked()
{
	QJsonDocument jdoc;
	QJsonObject json;
	json.insert("name", nameEdit->text());
	json.insert("number", numberEdit->text());
	json.insert("enabled", accountEnabled->isChecked());
	const char *api;
	if (newFlag) {
		api = "income-accounts.add";
	} else {
		api = "income-accounts.update";
		json.insert("id", selectedAccountId);
	}
	jdoc.setObject(json);
	cm.foxmq.post(api, FoxHead::ContentType::Json, jdoc.toJson(QJsonDocument::Compact));
	doneEdits();
}

void SetupIncomeAccountsWidget::doneEdits()
{
	selectedAccountId = -1;
	enableForms(false);
	clearForms();
	clearFormsModifiedFlags();  // must be after clearForms() so form isModified() is false
	saveButton->setEnabled(false);
	cancelButton->setEnabled(false);
	editButton->setEnabled(false);
	newButton->setEnabled(true);
	accountView->setCurrentIndex(incomeAccountListDataModel.index(-1, -1));
	if (!newFlag)
		cm.foxmq.post("income-accounts.unlock", FoxHead::ContentType::Empty, "");
	newFlag = false;
}

void SetupIncomeAccountsWidget::updateSaveEnabled()
{
	if (!nameEdit->text().isEmpty() && !numberEdit->text().isEmpty())
		saveButton->setEnabled(true);
	else
		saveButton->setEnabled(false);
}

void SetupIncomeAccountsWidget::accountSelected(QModelIndex index)
{
	int row = index.row();
	int newSelectedAccountId = incomeAccountListDataModel.getID(row);
	if (newSelectedAccountId != selectedAccountId) {
		if (!checkForSave()) {
			indexOfSelectedAccount = index;
			selectedAccountId = newSelectedAccountId;
			populateFormsWithAccountInfo(row);
			enableForms(false);
			editButton->setEnabled(true);
		}
	}
}

void SetupIncomeAccountsWidget::populateFormsWithAccountInfo(int row)
{
	nameEdit->setText(incomeAccountListDataModel.name(row));
	numberEdit->setText(incomeAccountListDataModel.number(row));
	accountEnabled->setChecked(incomeAccountListDataModel.enabled(row));
}
