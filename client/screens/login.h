// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef MDM_LOGIN_SCREEN_H_
#define MDM_LOGIN_SCREEN_H_

#include <QWidget>
#if !(defined(Q_OS_ANDROID) || defined(Q_OS_IOS))
#include "update-manager.h"
#endif

// fixme
// REF
// https://timschneeberger.me/qt/

class QLabel;
class QLineEdit;
class QPushButton;

class LoginScreen : public QWidget
{
	Q_OBJECT
public:
	explicit LoginScreen();
	void showAskForURL(QString currentHost, int currentPort);
	void showConnecting() { showMessage(State::Connecting, tr("Connecting to Host")); }
	void showNoConnection();
	void showDisconnected();
	void showAskForPassword();
	void showLoggingIn() { showMessage(State::Login, tr("Logging In")); }
	void showLoginFailed();
	void showForgotPassword();
	void showEnterCode();
	void showCodeSucceeded() { showMessage(State::OneTimeCodeSucceeded, tr("Code Accepted. Check email for new password"), tr("OK")); }
	void showCodeFailled() { showMessage(State::OneTimeCodeFailed, tr("Code Failed, Try password reset again?"), tr("Try Again")); }
	void setDataBasePassword();
	void enterDataBasePassword();

signals:
	void changeUrlClicked();
	void urlEntered(QString ip, int port);
	void databasePasswordSet(QString pwd);
	void databasePasswordEntered(QString pwd);
	void passwordEntered(QString email, QString pwd);
	void changePassword(QString email, QString currentPwd, QString newPwd);
	void forgotPassword(QString email);
	void oneTimeCode(QString email);
	void tryConnectionAgain();

private:
	enum State {
		NotConnected,
		SetDbPassword,
		EnterDbPassword,
		Url,
		Connecting,
		Password,
		Login,
		LoginFailed,
		ChangePassword,
		ForgotPassword,
		OneTimeCode,
		OneTimeCodeSucceeded,
		OneTimeCodeFailed,
		Rebooting,
	};
	void keyPressEvent(QKeyEvent *e) override;
	void showMessage(State newState, const QString &msg, const QString &buttonLabel = "");
	void showForms(uint8_t mask, bool passwordForm = true);
	void showLogin(const QString &titleString);
	void showDatabasePassword(const QString &titleString);
	void showChangePassword();
	void okPressed();
	void checkForValidPassword();
	void showHidePassword();
	void aboutClicked();
#if !(defined(Q_OS_ANDROID) || defined(Q_OS_IOS))
	void upgradeStateChanged(OtaUpdateManager::State state);
	void startUpgrade();
	QPushButton *upgradeButton;
	OtaUpdateManager upgradeManager;
#endif
	State state;
	QLabel *title;
	QLabel *pwdIssue;
	QList<QLabel *> entryLables;
	QList<QLineEdit *> entryLineEdits;
	QPushButton *okButton;
	QPushButton *urlEditButton;
	QPushButton *forgotButton;
	QPushButton *changePasswordButton;
	QPushButton *showHidePasswordButton;
	QPushButton *aboutButton;
	bool showPassword = true;
};

#endif
