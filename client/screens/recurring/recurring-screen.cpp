// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QScreen>
#include <QVBoxLayout>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include "main-window.h"
#include "connection-manager.h"
#include "user-input.h"
#include "styler.h"
#include "theme.h"
#include "recurring-selector-widget.h"
#include "recurring-donation-widget.h"
#include "recurring-screen.h"

#include <QDebug>

enum Screen {
	Select,
	Donation,
};

constexpr const char *titleText = "Recurring Donations";

RecurringScreen::RecurringScreen(MainWindow &mainWindow) : mainWindow(mainWindow), cm(mainWindow.connectionManager)
{
	QVBoxLayout *layout = new QVBoxLayout;
	setLayout(layout);
	int m = Theme::OuterMargin * qMin(screen()->geometry().width(), screen()->geometry().height());
	layout->setContentsMargins(m, m, m, m);

	QHBoxLayout *topLayout = new QHBoxLayout;  // fixme change to vbox and center title widget?
	layout->addLayout(topLayout);
	QHBoxLayout *bottomLayout = new QHBoxLayout;
	layout->addLayout(bottomLayout);

	topLayout->addStretch(1);
	title = new QLabel(tr(titleText));
	Styler::styleLabel(title, Theme::H1Size);
	topLayout->addWidget(title);
	topLayout->addStretch(1);

	widgetStack = new QStackedWidget;
	bottomLayout->addWidget(widgetStack);
	recurringSelectorWidget = new RecurringSelectorWidget;
	widgetStack->addWidget(recurringSelectorWidget);
	widgetStack->setCurrentIndex(Screen::Select);
	recurringDonationWidget = new RecurringDonationWidget;
	widgetStack->addWidget(recurringDonationWidget);

	connect(recurringSelectorWidget, &RecurringSelectorWidget::newListClicked, this, &RecurringScreen::newListClicked);
	connect(recurringSelectorWidget, &RecurringSelectorWidget::listNameEditClicked, this, &RecurringScreen::editListNameClicked);
	connect(recurringSelectorWidget, &RecurringSelectorWidget::editDonationsClicked, this, &RecurringScreen::editDonationsClicked);
	connect(recurringSelectorWidget, &RecurringSelectorWidget::runClicked, this, &RecurringScreen::runClicked);
	connect(recurringDonationWidget, &RecurringDonationWidget::done, this, &RecurringScreen::editDonationDoneClicked);
	connect(recurringDonationWidget, &RecurringDonationWidget::save, this, &RecurringScreen::updateDonation);
	connect(recurringDonationWidget, &RecurringDonationWidget::donationDeleted, this, &RecurringScreen::deleteDonation);
	connect(recurringDonationWidget, &RecurringDonationWidget::askUserForInput, this, &RecurringScreen::askUserForInput);

	topicsToReceive << "recurring" << "donors.ro.list" << "mission.ro.list" << "income-accounts.list-enabled";
	cm.registerApiForReceiver(this);
}

void RecurringScreen::subscribe()
{
	cm.foxmq.subscription("recurring.ro.list", true);
	cm.foxmq.subscription("recurring.ro.list-updated", true);
	cm.foxmq.subscription("donors.ro.list", true);
	cm.foxmq.subscription("mission.ro.list", true);
	cm.foxmq.subscription("income-accounts.list-enabled", true);
}

void RecurringScreen::unsubscribe()
{
	cm.foxmq.subscription("recurring.ro.list", false);
	cm.foxmq.subscription("recurring.ro.list-updated", false);
	cm.foxmq.subscription("donors.ro.list", false);
	cm.foxmq.subscription("mission.ro.list", false);
	cm.foxmq.subscription("income-accounts.list-enabled", false);
}

void RecurringScreen::reset()
{
	widgetStack->setCurrentIndex(Screen::Select);
	donationEditFlag = false;
	recurringSelectorWidget->reset();
	title->setText(tr(titleText));
}



void RecurringScreen::askUserForInput()
{
	connect(mainWindow.userInput, &UserInput::button1Clicked, this, &RecurringScreen::saveConfirmDiscardClicked);
	connect(mainWindow.userInput, &UserInput::button2Clicked, this, &RecurringScreen::saveConfirmCancelClicked);
	connect(mainWindow.userInput, &UserInput::button3Clicked, this, &RecurringScreen::saveConfirmSaveClicked);
	mainWindow.userInput->askForInput(tr("Changes have been made"), tr("Discard Changes"), tr("Continue Editing"), tr("      Save      "));
	mainWindow.showUserInput();
}

void RecurringScreen::saveConfirmSaveClicked()
{
	saveConfirmCleanUp();
	recurringDonationWidget->saveClicked();
}

void RecurringScreen::saveConfirmDiscardClicked()
{
	saveConfirmCleanUp();
	recurringDonationWidget->reset();
}

void RecurringScreen::saveConfirmCancelClicked()
{
	saveConfirmCleanUp();
	recurringDonationWidget->setDonationSelectorToCurrent();
}

void RecurringScreen::saveConfirmCleanUp()
{
	disconnect(mainWindow.userInput, &UserInput::button1Clicked, this, &RecurringScreen::saveConfirmDiscardClicked);
	disconnect(mainWindow.userInput, &UserInput::button2Clicked, this, &RecurringScreen::saveConfirmCancelClicked);
	disconnect(mainWindow.userInput, &UserInput::button3Clicked, this, &RecurringScreen::saveConfirmSaveClicked);
	mainWindow.showMainScreen();
}

void RecurringScreen::processPublish(const QByteArray &topic, ssize_t, FoxID, FoxHead::ContentType contentType, const QByteArray &content)
{
	QJsonArray jArray;
	int id = -1;
	if (contentType == FoxHead::ContentType::Json)
		jArray = QJsonDocument::fromJson(content).array();
	else if (contentType == FoxHead::ContentType::IntAsString)
		id = content.toInt();

	if (topic == "recurring.ro.list") {
		recurringSelectorWidget->updateList(jArray);
	} else if (topic == "recurring.ro.list-updated") {
		if (listIdUnderEdit > 0 && id == listIdUnderEdit)
			cm.foxmq.post("recurring.ro.donation-list", FoxHead::ContentType::IntAsString, QByteArray::number(listIdUnderEdit));
	} else if (topic == "donors.ro.list") {
		recurringDonationWidget->updateDonorsList(jArray);
	} else if (topic == "mission.ro.list") {
		recurringDonationWidget->updateMissionList(jArray);
	} else if (topic == "income-accounts.list-enabled") {
		recurringDonationWidget->updateSourceAccounts(jArray);
	}
}

void RecurringScreen::processPostResponce(const QByteArray &topic, FoxID, FoxHead::ContentType, const QByteArray &content)
{
	QJsonObject json = QJsonDocument::fromJson(content).object();
	if (topic == "recurring.ro.donation-list") {
		recurringDonationWidget->updateList(json);
	}
}

void RecurringScreen::processAck(const QByteArray &topic)
{
	if (topic == "recurring.lock") {
		if (donationEditFlag) {
			widgetStack->setCurrentIndex(Screen::Donation);
			cm.foxmq.post("recurring.ro.donation-list", FoxHead::ContentType::IntAsString, QByteArray::number(listIdUnderEdit));
		}
	} else if (topic == "recurring.run") {
		recurringSelectorWidget->enableSelectors(true);
		connect(mainWindow.userInput, &UserInput::button1Clicked, this, &RecurringScreen::okFromLockNack);
		mainWindow.userInput->askForInput(tr("Recurring list applied"));
		mainWindow.showUserInput();
	}
}

void RecurringScreen::processNack(const QByteArray &topic)
{
	if (topic == "recurring.lock") {
		if (!mainWindow.userInputIsOpen()) {
			connect(mainWindow.userInput, &UserInput::button1Clicked, this, &RecurringScreen::okFromLockNack);
			mainWindow.userInput->askForInput(tr("Another user is editing this list, try again later"));
			mainWindow.showUserInput();
		}
		reset();
	} else if (topic == "recurring.run") {
		recurringSelectorWidget->enableSelectors(true);
		if (!mainWindow.userInputIsOpen()) {
			connect(mainWindow.userInput, &UserInput::button1Clicked, this, &RecurringScreen::okFromLockNack);
			mainWindow.userInput->askForInput(tr("Failed to apply Recurring list!"));
			mainWindow.showUserInput();
		}
	}
}

void RecurringScreen::okFromLockNack()
{
	disconnect(mainWindow.userInput, &UserInput::button1Clicked, this, &RecurringScreen::okFromLockNack);
	mainWindow.showMainScreen();
}

void RecurringScreen::newListClicked(const QString listName)
{
	QJsonDocument jdoc;
	QJsonObject json;
	json.insert("name", listName);
	jdoc.setObject(json);
	cm.foxmq.post("recurring.list.add", FoxHead::ContentType::Json, jdoc.toJson(QJsonDocument::Compact));
}

void RecurringScreen::editListNameClicked(int id, const QString listName)
{
	cm.foxmq.post("recurring.lock", FoxHead::ContentType::Json, QByteArray::number(id));

	QJsonDocument jdoc;
	QJsonObject json;
	json.insert("id", id);
	json.insert("name", listName);
	jdoc.setObject(json);
	cm.foxmq.post("recurring.list.update", FoxHead::ContentType::Json, jdoc.toJson(QJsonDocument::Compact));

	cm.foxmq.post("recurring.unlock", FoxHead::ContentType::Json, QByteArray::number(id));
}

void RecurringScreen::editDonationsClicked(int listId, const QString &listName)
{
	listIdUnderEdit = listId;
	donationEditFlag = true;
	cm.foxmq.post("recurring.lock", FoxHead::ContentType::Json, QByteArray::number(listId));
	QString text = tr(titleText);
	text+= " - ";
	text+= listName;
	title->setText(text);
}

void RecurringScreen::editDonationDoneClicked()
{
	donationEditFlag = false;
	cm.foxmq.post("recurring.unlock", FoxHead::ContentType::Json, QByteArray::number(recurringSelectorWidget->listIdUnderEdit));
	reset();
}

void RecurringScreen::runClicked(int listId, QDate runDate)
{
	cm.foxmq.post("recurring.lock", FoxHead::ContentType::Json, QByteArray::number(listId));

	QJsonDocument jdoc;
	QJsonObject json;
	json.insert("id", listId);
	json.insert("julian-day", runDate.toJulianDay());
	jdoc.setObject(json);
	cm.foxmq.post("recurring.run", FoxHead::ContentType::Json, jdoc.toJson(QJsonDocument::Compact));

	// run auto unlocks ?
	// fixme rest or hold froms disabled until ack from unlock
}

void RecurringScreen::updateDonation(bool newFlag, QJsonObject json)
{
	json.insert("list-id", listIdUnderEdit);
	const char *api;
	if (newFlag)
		api = "recurring.donation.add";
	else
		api = "recurring.donation.update";
	QJsonDocument jdoc;
	jdoc.setObject(json);
	cm.foxmq.post(api, FoxHead::ContentType::Json, jdoc.toJson(QJsonDocument::Compact));
}

void RecurringScreen::deleteDonation()
{
	connect(mainWindow.userInput, &UserInput::button1Clicked, this, &RecurringScreen::deleteYes);
	connect(mainWindow.userInput, &UserInput::button2Clicked, this, &RecurringScreen::deleteNo);
	mainWindow.userInput->askForInput(QString(tr("Are you sure you want to delete donation %1")).arg(recurringDonationWidget->getDonationIdUnderEdit()), tr("Yes"), tr("No"));
	mainWindow.showUserInput();
}

void RecurringScreen::deleteYes()
{
	QJsonObject json;
	json.insert("list-id", listIdUnderEdit);
	json.insert("did", recurringDonationWidget->getDonationIdUnderEdit());
	QJsonDocument jdoc;
	jdoc.setObject(json);
	cm.foxmq.post("recurring.donation.delete", FoxHead::ContentType::Json, jdoc.toJson(QJsonDocument::Compact));
	deleteNo();
}

void RecurringScreen::deleteNo()
{
	disconnect(mainWindow.userInput, &UserInput::button1Clicked, this, &RecurringScreen::deleteYes);
	disconnect(mainWindow.userInput, &UserInput::button2Clicked, this, &RecurringScreen::deleteNo);
	recurringDonationWidget->reset();
	mainWindow.showMainScreen();
}
