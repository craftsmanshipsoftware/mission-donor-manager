// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef MDM_RECURRING_DONATION_LIST_DM_H_
#define MDM_RECURRING_DONATION_LIST_DM_H_

#include <QAbstractListModel>

class RecurringDonationDataModel : public QAbstractListModel
{
	Q_OBJECT

public:
	int rowCount(const QModelIndex &parent = QModelIndex()) const override ;
	int columnCount(const QModelIndex &parent = QModelIndex()) const override;
	QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
	void clear();
	void append(QJsonObject jItem, int donorId, int missionId, const QString &name);
	inline int size() { return recurringList.size(); }
	inline int idAt(int idx) { return recurringList.at(idx).id; }
	inline int donorIdAt(int idx) { return recurringList.at(idx).donorId; }
	inline int missionIdAt(int idx) { return recurringList.at(idx).missionId; }
	inline int amountAt(int idx) { return recurringList.at(idx).amount; }
	inline int sourceAccountAt(int idx) { return recurringList.at(idx).sa; }
	inline int inactiveAt(int idx) { return recurringList.at(idx).inactiveCount; }
	inline int enabledAt(int idx) { return recurringList.at(idx).enabled; }
	inline int anonymusAt(int idx) { return recurringList.at(idx).anonymus; }

private:
	struct RecurringDonationItem {
		int id;
		int donorId;
		int missionId;
		int amount;
		int sa;
		int inactiveCount;
		bool enabled;
		bool anonymus;
		QString name;
	};
	QList<RecurringDonationItem> recurringList;
};

#endif
