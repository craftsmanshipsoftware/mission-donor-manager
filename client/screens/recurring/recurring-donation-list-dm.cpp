// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QJsonObject>
#include "recurring-donation-list-dm.h"

int RecurringDonationDataModel::rowCount(const QModelIndex &) const
{
	return recurringList.size();
}

int RecurringDonationDataModel::columnCount(const QModelIndex &) const
{
	return 2;
}


QVariant RecurringDonationDataModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	QVariant text;

	if (role != Qt::DisplayRole)
		return QVariant();

	if (orientation == Qt::Horizontal) {  // fixme - why Horizontal check - test on tablet
		switch(section) {  // clang format off
			case 0: text = QString(tr("#")); break;
			case 1: text = QString(tr("Donor - Mission")); break;
		}  // clang format on
	}

	return text;
}

QVariant RecurringDonationDataModel::data(const QModelIndex &index, int role) const
{
	QVariant item;
	if (role == Qt::DisplayRole) {
		if (index.column() == 0)
			item = recurringList.at(index.row()).id;
		else
			item = recurringList.at(index.row()).name;
	}
	return item;
}

void RecurringDonationDataModel::clear()
{
	recurringList.clear();
}

void RecurringDonationDataModel::append(QJsonObject jItem, int donorId, int missionId, const QString &name)
{
	struct RecurringDonationItem dli;
	dli.id = jItem.value("id").toInt();
	dli.sa = jItem.value("sa").toInt();
	dli.amount = jItem.value("amount").toInt();
	dli.inactiveCount = jItem.value("inactive").toInt();
	dli.donorId = donorId;
	dli.missionId = missionId;
	dli.enabled = jItem.value("enabled").toBool();
	dli.anonymus = jItem.value("anonymus").toBool();
	dli.name = name;
	recurringList.append(dli);
}
