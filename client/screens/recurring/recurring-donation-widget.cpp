// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QScreen>
#include <QVBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QTableView>
#include <QHeaderView>
#include <QSortFilterProxyModel>
#include <QLineEdit>
#include <QCheckBox>
#include <QJsonArray>
#include "../common/common.h"
#include "styler.h"
#include "theme.h"
#include "recurring-donation-widget.h"

RecurringDonationWidget::RecurringDonationWidget()
{
	QVBoxLayout *layout = new QVBoxLayout;
	setLayout(layout);
	int m = Theme::OuterMargin * qMin(screen()->geometry().width(), screen()->geometry().height());
	layout->setContentsMargins(m, m, m, m);
	QHBoxLayout *contentLayout = new QHBoxLayout;
	layout->addLayout(contentLayout);
	contentLayout->addStretch(1);
	QVBoxLayout *donationsLayout = new QVBoxLayout;
	contentLayout->addLayout(donationsLayout, 7);
	contentLayout->addStretch(1);
	QVBoxLayout *donorLayout = new QVBoxLayout;
	contentLayout->addLayout(donorLayout, 7);
	contentLayout->addStretch(1);
	QVBoxLayout *missionLayout = new QVBoxLayout;
	contentLayout->addLayout(missionLayout, 7);
	contentLayout->addStretch(1);
	QVBoxLayout *detailsLayout = new QVBoxLayout;
	contentLayout->addLayout(detailsLayout, 7);
	contentLayout->addStretch(1);

// Donation Selector layout

	QLabel *title = new QLabel(tr("Select Donation"));
	Styler::styleLabel(title, Theme::H2Size);
	donationsLayout->addWidget(title);
	donationsLayout->setAlignment(title, Qt::AlignHCenter);

	donationFilterEdit = new QLineEdit;
	Styler::styleSearchLineEdit(donationFilterEdit);
	donationsLayout->addWidget(donationFilterEdit);
	donationSelector = new QTableView;
	Styler::styleTableView(donationSelector);
	donationFilter.setSourceModel(&donationsList);
	donationFilter.setFilterCaseSensitivity(Qt::CaseInsensitive);
	donationFilter.setFilterKeyColumn(1);
	donationSelector->setModel(&donationFilter);
	donationSelector->verticalHeader()->hide();
	//donationSelector->horizontalHeader()->hide();
	donationSelector->horizontalHeader()->setStretchLastSection(true);
	donationSelector->setSelectionBehavior(QAbstractItemView::SelectRows);
	donationSelector->setSelectionMode(QAbstractItemView::SingleSelection);
	donationSelector->setAlternatingRowColors(true);
	donationsLayout->addWidget(donationSelector);

	QHBoxLayout *totalLayout = new QHBoxLayout;
	donationsLayout->addLayout(totalLayout);
	QLabel *totalLabel = new QLabel(tr("Total:"));
	Styler::styleLabel(totalLabel);
	totalLayout->addWidget(totalLabel);
	totalEdit = new QLineEdit;
	Styler::styleLineEdit(totalEdit);
	totalEdit->setReadOnly(true);
	totalLayout->addWidget(totalEdit);
	totalLayout->addStretch(1);

// Donor Selection layout

	title = new QLabel(tr("Donor Selection"));
	Styler::styleLabel(title, Theme::H2Size);
	donorLayout->addWidget(title);
	donorLayout->setAlignment(title, Qt::AlignHCenter);

	donorSelector = new NameSelectorWidget;
	donorSelector->enabledIdShown(true);
	donorLayout->addWidget(donorSelector);

// Mission Selection layout

	title = new QLabel(tr("Mission Selection"));
	Styler::styleLabel(title, Theme::H2Size);
	missionLayout->addWidget(title);
	missionLayout->setAlignment(title, Qt::AlignHCenter);

	missionSelector = new NameSelectorWidget;
	missionLayout->addWidget(missionSelector);

// Donation Details layout

	title = new QLabel(tr("Details"));
	Styler::styleLabel(title, Theme::H2Size);
	detailsLayout->addWidget(title);

	detailsLayout->addStretch(1);

	title = new QLabel(tr("Donor:"));
	Styler::styleLabel(title);
	detailsLayout->addWidget(title);
	donorEdit = new QLineEdit;
	Styler::styleLineEdit(donorEdit);
	donorEdit->setReadOnly(true);
	detailsLayout->addWidget(donorEdit);
	detailsLayout->addStretch(1);

	title = new QLabel(tr("Mission:"));
	Styler::styleLabel(title);
	detailsLayout->addWidget(title);
	missionEdit = new QLineEdit;
	Styler::styleLineEdit(missionEdit);
	missionEdit->setReadOnly(true);
	detailsLayout->addWidget(missionEdit);
	detailsLayout->addStretch(1);

	title = new QLabel(tr("Amount:"));
	Styler::styleLabel(title);
	detailsLayout->addWidget(title);
	amountEdit = new QLineEdit;
	Styler::styleLineEdit(amountEdit);
	amountEdit->setValidator(new AmountValidator());
	detailsLayout->addWidget(amountEdit);
	detailsLayout->addStretch(1);

	title = new QLabel(tr("Income Account"));
	Styler::styleLabel(title);
	detailsLayout->addWidget((title));
	sourceAccountSelector = new SourceAccountSelectorWidget;
	Styler::styleComboBox(sourceAccountSelector);
	detailsLayout->addWidget(sourceAccountSelector);
	detailsLayout->addStretch(1);

	enabledCheckBox = new QCheckBox(tr("Include in Post"));
	Styler::styleCheckBox(enabledCheckBox);
	detailsLayout->addWidget(enabledCheckBox);
	detailsLayout->addStretch(1);

	title = new QLabel(tr("Disable for n posts:"));
	Styler::styleLabel(title);
	detailsLayout->addWidget(title);
	inactiveEdit = new QLineEdit;
	Styler::styleLineEdit(inactiveEdit);
	inactiveEdit->setValidator(new QIntValidator());
	detailsLayout->addWidget(inactiveEdit);
	detailsLayout->addStretch(1);

	anonymusCheckBox = new QCheckBox(tr("Anonymus"));
	Styler::styleCheckBox(anonymusCheckBox);
	detailsLayout->addWidget(anonymusCheckBox);

	detailsLayout->addStretch(4);

// bottom button layout

	QHBoxLayout *buttonLayout = new QHBoxLayout;
	layout->addLayout(buttonLayout);
	buttonLayout->addStretch(5);
	newButton = new QPushButton(tr("New"));
	Styler::styleButton(newButton);
	buttonLayout->addWidget(newButton);
	cancelButton = new QPushButton(tr("Cancel"));
	Styler::styleButton(cancelButton);
	buttonLayout->addWidget(cancelButton);
	saveButton = new QPushButton(tr("Save"));
	Styler::styleButton(saveButton);
	buttonLayout->addWidget(saveButton);
	deleteButton = new QPushButton(tr("Delete"));
	Styler::styleButton(deleteButton);
	buttonLayout->addWidget(deleteButton);
	buttonLayout->addStretch(4);
	doneButton = new QPushButton(tr("Exit List"));
	Styler::styleButton(doneButton);
	buttonLayout->addWidget(doneButton);
	buttonLayout->addStretch(5);

	reset();

	connect(doneButton, &QPushButton::clicked, this, &RecurringDonationWidget::doneClicked);
	connect(donationSelector, &QTableView::clicked, this, &RecurringDonationWidget::donationSelected);
	connect(donationFilterEdit, &QLineEdit::textEdited, &donationFilter, &QSortFilterProxyModel::setFilterFixedString);
	connect(donorSelector, &NameSelectorWidget::nameSelected, this, &RecurringDonationWidget::donorSelected);
	connect(missionSelector, &NameSelectorWidget::nameSelected, this, &RecurringDonationWidget::missionSelected);
	connect(newButton, &QPushButton::clicked, this, &RecurringDonationWidget::newClicked);
	connect(saveButton, &QPushButton::clicked, this, &RecurringDonationWidget::saveClicked);
	connect(cancelButton, &QPushButton::clicked, this, &RecurringDonationWidget::cancelClicked);
	connect(deleteButton, &QPushButton::clicked, this, &RecurringDonationWidget::deleteClicked);
	connect(amountEdit, &QLineEdit::textEdited, this, &RecurringDonationWidget::flagFormsModified);
	connect(inactiveEdit, &QLineEdit::textEdited, this, &RecurringDonationWidget::inactiveChanged);
	connect(sourceAccountSelector, qOverload<int>(&SourceAccountSelectorWidget::activated), this, &RecurringDonationWidget::flagFormsModified);
	connect(enabledCheckBox, &QCheckBox::clicked, this, &RecurringDonationWidget::flagFormsModified);
	connect(anonymusCheckBox, &QCheckBox::clicked, this, &RecurringDonationWidget::flagFormsModified);
}

void RecurringDonationWidget::updateList(const QJsonObject &json)
{
	donationsList.layoutAboutToBeChanged();
	donationsList.clear();
	QJsonArray jArray = json.value("list").toArray();
	for (auto jItem : jArray) {
		QJsonObject json = jItem.toObject();
		int donorId = json.value("donor-id").toInt();
		int missionId = json.value("mission-id").toInt();
		// Pulling the donor and mission names here works because they are populated when the user enters the recurring tab and only later the user edits a recurring list
		QString name = donorSelector->model().getFullName(donorId - 1) + " - " + missionSelector->model().getFullName(missionId - 1);
		donationsList.append(json, donorId, missionId, name);
	}
	donationsList.layoutChanged();
	donationSelector->resizeColumnsToContents();
	totalEdit->setText(convertCentsToString(json.value("total").toInt(), true));
}

void RecurringDonationWidget::reset()
{
	donationSelector->setCurrentIndex(donationsList.index(-1));
	donorSelector->clearIndexAndSelection();
	missionSelector->clearIndexAndSelection();
	sourceAccountSelector->setCurrentIndex(-1);
	donorEdit->clear();
	missionEdit->clear();
	amountEdit->clear();
	inactiveEdit->clear();
	anonymusCheckBox->setChecked(false);
	enabledCheckBox->setChecked(true);
	donationIndexUnderEdit = donationsList.index(-1);
	donationIdUnderEdit = -1;
	selectedDonorId = -1;
	selectedMissionId = -1;
	newFlag = false;
	isModified = false;
	saveButton->setEnabled(false);
	cancelButton->setEnabled(false);
	deleteButton->setEnabled(false);
	newButton->setEnabled(true);
	donationSelector->setEnabled(true);
	enableForms(false);
}

void RecurringDonationWidget::donationSelected(QModelIndex index)
{
	newFlag = false;
	int row = donationFilter.mapToSource(index).row();
	int id = donationsList.idAt(row);
	if (id != donationIdUnderEdit && !checkForSave()) {
		donationIndexUnderEdit = index;
		donationIdUnderEdit = id;
		enableForms(true);
		selectedDonorId = donationsList.donorIdAt(row);
		selectedMissionId = donationsList.missionIdAt(row);
		donorEdit->setText(donorSelector->model().getFullName(selectedDonorId - 1));
		// select donor in donorSelector???
		missionEdit->setText(missionSelector->model().getFullName(selectedMissionId - 1));
		amountEdit->setText(convertCentsToString(donationsList.amountAt(row)));
		int inactiveCount = donationsList.inactiveAt(row);
		inactiveEdit->setText(QString::number(inactiveCount));
		sourceAccountSelector->setCurrentIndexByAccountId(donationsList.sourceAccountAt(row));
		enabledCheckBox->setChecked(donationsList.enabledAt(row));
		enabledCheckBox->setEnabled(!inactiveCount);
		anonymusCheckBox->setChecked(donationsList.anonymusAt(row));
		cancelButton->setEnabled(true);
		deleteButton->setEnabled(true);
		newButton->setEnabled(false);
	}
}

void RecurringDonationWidget::donorSelected(int id, QModelIndex)
{
	donorEdit->setText(donorSelector->model().getFullName(id - 1));
	selectedDonorId = id;
	flagFormsModified();
}

void RecurringDonationWidget::missionSelected(int id, QModelIndex)
{
	missionEdit->setText(missionSelector->model().getFullName(id - 1));
	selectedMissionId = id;
	flagFormsModified();
}

void RecurringDonationWidget::newClicked()
{
	enableForms(true);
	newFlag = true;
	newButton->setEnabled(false);
	cancelButton->setEnabled(true);
	deleteButton->setEnabled(false);
	donationSelector->setEnabled(false);
}

void RecurringDonationWidget::doneClicked()
{
	if (!checkForSave()) {
		emit done();
		reset();
	}
}

void RecurringDonationWidget::cancelClicked()
{
	if (!checkForSave())
		reset();
}

void RecurringDonationWidget::deleteClicked()
{
	emit donationDeleted();
}

bool RecurringDonationWidget::checkForSave()
{
	if (isModified)
		emit askUserForInput();
	return isModified;
}

void RecurringDonationWidget::saveClicked()
{
	QJsonObject json;
	if (!newFlag)
		json.insert("id", donationIdUnderEdit);
	json.insert("donor-id", selectedDonorId);
	json.insert("mission-id", selectedMissionId);
	json.insert("sa", sourceAccountSelector->accountIdAtCurrentIndex());
	json.insert("amount", convertStringToCents(amountEdit->text()));
	json.insert("inactive", inactiveEdit->text().toInt());
	json.insert("enabled", enabledCheckBox->isChecked());
	json.insert("anonymus", anonymusCheckBox->isChecked());

	emit save(newFlag, json);
	reset();
}

void RecurringDonationWidget::updateSaveEnabled()
{
	if (!donorEdit->text().isEmpty() && !missionEdit->text().isEmpty() && !amountEdit->text().isEmpty() && sourceAccountSelector->currentIndex() > -1)
		saveButton->setEnabled(true);
	else
		saveButton->setEnabled(false);
}

void RecurringDonationWidget::flagFormsModified()
{
	isModified = true;
	updateSaveEnabled();
}

void RecurringDonationWidget::inactiveChanged()
{
	int inactiveCount = inactiveEdit->text().toInt();
	if (inactiveCount) {
		enabledCheckBox->setChecked(false);
		enabledCheckBox->setEnabled(false);
	} else {
		enabledCheckBox->setEnabled(true);
	}
	flagFormsModified();
}

void RecurringDonationWidget::enableForms(bool enabled)
{
	donorSelector->setEnabled(enabled);
	missionSelector->setEnabled(enabled);
	sourceAccountSelector->setEnabled(enabled);
	amountEdit->setEnabled(enabled);
	inactiveEdit->setEnabled(enabled);
	anonymusCheckBox->setEnabled(enabled);
	enabledCheckBox->setEnabled(enabled);
}
