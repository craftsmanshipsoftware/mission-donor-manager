// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef MDM_REOCURRING_DONATION_WIDGET_H_
#define MDM_REOCURRING_DONATION_WIDGET_H_

#include <QWidget>
#include <QJsonObject>
#include <QTableView>
#include <QSortFilterProxyModel>
#include "name-selector-widget.h"
#include "source-account-selector-widget.h"
#include "recurring-donation-list-dm.h"

class QPushButton;
class QLineEdit;
class QCheckBox;

class RecurringDonationWidget : public QWidget
{
	Q_OBJECT

public:
	RecurringDonationWidget();
	void updateList(const QJsonObject &json);
	void updateDonorsList(const QJsonArray &json) { donorSelector->updateNameList(json); }
	void updateMissionList(const QJsonArray &json) { missionSelector->updateNameList(json); }
	void updateSourceAccounts(const QJsonArray &json) { sourceAccountSelector->update(json); }
	void reset();
	void saveClicked();
	inline void setDonationSelectorToCurrent() { donationSelector->setCurrentIndex(donationIndexUnderEdit); }
	inline int getDonationIdUnderEdit() { return donationIdUnderEdit; }

signals:
	void done();
	void save(bool newFlag, QJsonObject json);
	void donationDeleted();
	void askUserForInput();

private:
	void donationSelected(QModelIndex index);
	void donorSelected(int id, QModelIndex);
	void missionSelected(int id, QModelIndex);
	void newClicked();
	void doneClicked();
	void cancelClicked();
	void deleteClicked();
	bool checkForSave();
	void updateSaveEnabled();
	void flagFormsModified();
	void inactiveChanged();
	void enableForms(bool enabled);
	QPushButton *newButton;
	QPushButton *cancelButton;
	QPushButton *saveButton;
	QPushButton *deleteButton;
	QPushButton *doneButton;
	NameSelectorWidget *donorSelector;
	NameSelectorWidget *missionSelector;
	QTableView *donationSelector;
	QLineEdit *donationFilterEdit;
	QLineEdit *donorEdit;
	QLineEdit *missionEdit;
	QLineEdit *amountEdit;
	QLineEdit *totalEdit;
	QLineEdit *inactiveEdit;
	SourceAccountSelectorWidget *sourceAccountSelector;
	QCheckBox *enabledCheckBox;
	QCheckBox *anonymusCheckBox;
	RecurringDonationDataModel donationsList;
	QSortFilterProxyModel donationFilter;
	QModelIndex donationIndexUnderEdit;
	int donationIdUnderEdit = -1;
	int selectedDonorId = -1;
	int selectedMissionId = -1;
	bool newFlag = false;
	bool isModified = false;
};

#endif
