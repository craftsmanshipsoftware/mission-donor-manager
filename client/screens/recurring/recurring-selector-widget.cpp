// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QScreen>
#include <QHBoxLayout>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QLabel>
#include <QPushButton>
#include <QLineEdit>
#include <QCalendarWidget>
#include "styler.h"
#include "theme.h"
#include "name-selector-widget.h"
#include "recurring-selector-widget.h"

RecurringSelectorWidget::RecurringSelectorWidget()
{
	QHBoxLayout *layout = new QHBoxLayout;
	setLayout(layout);
	int m = Theme::OuterMargin * qMin(screen()->geometry().width(), screen()->geometry().height());
	layout->setContentsMargins(m, m, m, m);

	layout->addStretch(2);

	QVBoxLayout *leftLayout = new QVBoxLayout;
	layout->addLayout(leftLayout);
	leftLayout->addStretch(2);

	QLabel *title = new QLabel(tr("Recurring Lists"));
	Styler::styleLabel(title);
	leftLayout->addWidget(title);
	listSelector = new NameSelectorWidget;
	listSelector->setSearchVisible(false);
	leftLayout->addWidget(listSelector);
	leftLayout->addStretch(1);
	title = new QLabel(tr("Date of Post"));
	Styler::styleLabel(title);
	leftLayout->addWidget(title);
	calendar = new QCalendarWidget;
	leftLayout->addWidget(calendar);
	leftLayout->addStretch(1);
	runButton = new QPushButton(tr("  Post  "));
	Styler::styleButton(runButton);
	runButton->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
	leftLayout->addWidget(runButton);
	leftLayout->setAlignment(runButton, Qt::AlignHCenter);
	leftLayout->addStretch(2);

	layout->addStretch(1);

	QVBoxLayout *rightLayout = new QVBoxLayout;
	layout->addLayout(rightLayout);
	rightLayout->addStretch(2);
	editDonations = new QPushButton(tr("Add / Edit Donations"));
	Styler::styleButton(editDonations);
	rightLayout->addWidget(editDonations);
	rightLayout->addStretch(1);
	nameEdit = new QLineEdit;
	Styler::styleLineEdit(nameEdit);
	rightLayout->addWidget(nameEdit);
	renameButton = new QPushButton(tr("Rename List"));
	Styler::styleButton(renameButton);
	rightLayout->addWidget(renameButton);
	rightLayout->addStretch(1);
	QLabel *label = new QLabel(tr("Enter New List Name Here"));
	Styler::styleLabel(label);
	rightLayout->addWidget(label);
	newNameEdit = new QLineEdit;
	Styler::styleLineEdit(newNameEdit);
	rightLayout->addWidget(newNameEdit);
	newNameButton = new QPushButton(tr("Create New List"));
	Styler::styleButton(newNameButton);
	rightLayout->addWidget(newNameButton);
	rightLayout->addStretch(2);

	layout->addStretch(2);

	connect(newNameButton, &QPushButton::clicked, this, &RecurringSelectorWidget::newListButtonClicked);
	connect(newNameEdit, &QLineEdit::textEdited, this, &RecurringSelectorWidget::newEditChanged);
	connect(renameButton, &QPushButton::clicked, this, &RecurringSelectorWidget::nameEditButtonClicked);
	connect(listSelector, &NameSelectorWidget::nameSelected, this, &RecurringSelectorWidget::listSelected);
	connect(editDonations, &QPushButton::clicked, this, &RecurringSelectorWidget::editDonationsButtonClicked);
	connect(runButton, &QPushButton::clicked, this, &RecurringSelectorWidget::runButtonClicked);
	connect(calendar, &QCalendarWidget::clicked, this, &RecurringSelectorWidget::calendarClicked);
	reset();
}

void RecurringSelectorWidget::updateList(const QJsonArray &jArray)
{
	listSelector->updateNameList(jArray);
	reset();
}

void RecurringSelectorWidget::newEditChanged()
{
	if (newNameEdit->text().isEmpty())
		newNameButton->setEnabled(false);
	else
		newNameButton->setEnabled(true);
}

void RecurringSelectorWidget::newListButtonClicked()
{
	emit newListClicked(newNameEdit->text());
	reset();
}

void RecurringSelectorWidget::listSelected(int listId, QModelIndex index)
{
	renameButton->setEnabled(true);
	nameEdit->setEnabled(true);
	editDonations->setEnabled(true);
	nameEdit->setText(listSelector->model().getFirstName(listSelector->mapToSourceRow(index)));
	listIdUnderEdit = listId;
	guiIndexOfListUnderEdit = index;
}

void RecurringSelectorWidget::calendarClicked(QDate date)
{
	runDateSelected = true;
	runDate = date;
	if (runDateSelected && listIdUnderEdit > 0)
		runButton->setEnabled(true);
}

void RecurringSelectorWidget::nameEditButtonClicked()
{
	emit listNameEditClicked(listIdUnderEdit, nameEdit->text());
	reset();
}

void RecurringSelectorWidget::editDonationsButtonClicked()
{
	int indexInDM = listSelector->mapToSourceRow(guiIndexOfListUnderEdit);
	emit editDonationsClicked(listIdUnderEdit, listSelector->model().getFirstName(indexInDM));
}

void RecurringSelectorWidget::runButtonClicked()
{
	emit runClicked(listIdUnderEdit, runDate);
	reset();
	enableSelectors(false);
}

void RecurringSelectorWidget::enableSelectors(bool enabled)
{
	listSelector->setEnabled(enabled);
	calendar->setEnabled(enabled);
}

void RecurringSelectorWidget::reset()
{
	nameEdit->clear();
	nameEdit->setEnabled(false);
	newNameEdit->clear();
	newNameEdit->setEnabled(true);
	newNameButton->setEnabled(false);
	renameButton->setEnabled(false);
	editDonations->setEnabled(false);
	runButton->setEnabled(false);
	listSelector->clearIndexAndSelection();
	runDateSelected = false;
	listIdUnderEdit = -1;
}
