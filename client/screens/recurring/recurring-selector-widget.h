// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef MDM_REOCURRING_SELECTOR_WIDGET_H_
#define MDM_REOCURRING_SELECTOR_WIDGET_H_

#include <QWidget>
#include <QDate>
#include <QModelIndex>

class NameSelectorWidget;
class QCalendarWidget;
class QPushButton;
class QLineEdit;

class RecurringSelectorWidget : public QWidget
{
	Q_OBJECT

public:
	RecurringSelectorWidget();
	void updateList(const QJsonArray &jArray);
	void reset();
	void enableSelectors(bool enabled);
	int listIdUnderEdit = -1;

signals:
	void newListClicked(const QString listName);
	void listNameEditClicked(int id, const QString listName);
	void runClicked(int listId, QDate runDate);
	void editDonationsClicked(int listId, const QString &listName);

private:
	void newEditChanged();
	void newListButtonClicked();
	void nameEditButtonClicked();
	void editDonationsButtonClicked();
	void runButtonClicked();
	void listSelected(int listId, QModelIndex index);
	void calendarClicked(QDate date);
	NameSelectorWidget *listSelector;
	QCalendarWidget *calendar;
	QPushButton *runButton;

	QPushButton *editDonations;
	QPushButton *renameButton;
	QPushButton *newNameButton;
	QLineEdit *nameEdit;
	QLineEdit *newNameEdit;
	QDate runDate;
	QModelIndex guiIndexOfListUnderEdit;
	bool runDateSelected = false;
};

#endif
