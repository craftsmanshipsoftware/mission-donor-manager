// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef MDM_REOCURRING_SCREEN_H_
#define MDM_REOCURRING_SCREEN_H_

#include "tab-screen.h"
#include "receiver.h"

class MainWindow;
class ConnectionManager;
class QLabel;
class UserInfo;
class QStackedWidget;
class RecurringSelectorWidget;
class RecurringDonationWidget;

class RecurringScreen : public TabScreen, public Receiver
{
	Q_OBJECT

public:
	RecurringScreen(MainWindow &mainWindow);
	void subscribe() override;
	void unsubscribe() override;

private:
	void processPublish(const QByteArray &topic, ssize_t count, FoxID id, FoxHead::ContentType contentType, const QByteArray &content) override;
	void processPostResponce(const QByteArray &topic, FoxID id, FoxHead::ContentType contentType, const QByteArray &content) override;
	void processAck(const QByteArray &topic) override;
	void processNack(const QByteArray &topic) override;
	void okFromLockNack();
	void askUserForInput();
	void saveConfirmSaveClicked();
	void saveConfirmDiscardClicked();
	void saveConfirmCancelClicked();
	void saveConfirmCleanUp();
	void newListClicked(const QString listName);
	void editListNameClicked(int id, const QString listName);
	void editDonationsClicked(int listId, const QString &listName);
	void runClicked(int listId, QDate runDate);
	void reset();
	void editDonationDoneClicked();
	void updateDonation(bool newFlag, QJsonObject json);
	void deleteDonation();
	void deleteYes();
	void deleteNo();
	MainWindow &mainWindow;
	ConnectionManager &cm;
	QStackedWidget *widgetStack;
	QLabel *title;
	RecurringSelectorWidget *recurringSelectorWidget;
	RecurringDonationWidget *recurringDonationWidget;
	int listIdUnderEdit = -1;
	bool donationEditFlag = false;
};

#endif
