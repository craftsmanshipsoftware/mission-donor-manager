// clang-format off
/**************************************************************************************************
	Copyright (C) 2024  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QJsonObject>
#include <QJsonArray>
#include "../common/common.h"
#include "reports-monthly-dm.h"

int ReportsMonthlyDataModel::rowCount(const QModelIndex &) const
{
	return list.size();
}

int ReportsMonthlyDataModel::columnCount(const QModelIndex &) const
{
	return 3;
}

QVariant ReportsMonthlyDataModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	QVariant text;

	if (role != Qt::DisplayRole)
		return QVariant();

	if (orientation == Qt::Horizontal) {
		switch(section) {  // clang-format off
			case 0: text = QString(tr("Date"));            break;
			case 1: text = QString(tr("Total"));           break;
			case 2: text = QString(tr("YoY Delta"));       break;
		}  // clang-format on
	}

	return text;
}

QVariant ReportsMonthlyDataModel::data(const QModelIndex &index, int role) const
{
	QVariant item;
	int row = list.size() - index.row() - 1;
	if (role == Qt::DisplayRole) {
		switch(index.column()) {  // clang-format off
			case 0: item = list.at(row).date.toString("MMMM");                break;
			case 1: item = convertCentsToString(list.at(row).amount, true);   break;
			case 2: item = QString::number(list.at(row).delta, 'f', 1) + '%'; break;
		}  // clang-format on
	}
	return item;
}

void ReportsMonthlyDataModel::update(const QJsonObject &json)
{
	list.clear();
	QDate endMonth = QDate::fromJulianDay(json.value("end-date").toInt()).addMonths(1);
	QJsonArray jMonthTotals = json.value("month-totals").toArray();
	QJsonArray jMonthDeltas = json.value("month-deltas").toArray();
	for (int i=0; i < jMonthTotals.size(); i++) {
		struct ReportsMonthlyListItem mtdli;
		mtdli.amount = jMonthTotals.at(i).toInt();
		mtdli.delta = jMonthDeltas.at(i).toDouble();
		mtdli.date = endMonth;
		endMonth = endMonth.addMonths(1);
		list.append(mtdli);
	}
}
