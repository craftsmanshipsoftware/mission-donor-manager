// clang-format off
/**************************************************************************************************
	Copyright (C) 2024  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QVBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QJsonDocument>
#include <QJsonObject>
#include "theme.h"
#include "styler.h"
#include "year-selector.h"
#include "reports-stats-widget.h"

ReportsStatsWidget::ReportsStatsWidget(FoxMQ &foxmq) : foxmq(foxmq)
{
	QVBoxLayout *layout = new QVBoxLayout;
	setLayout(layout);

	QLabel *title = new QLabel(tr("Donation Stats Export"));
	Styler::styleLabel(title, Theme::H1Size);
	layout->addWidget(title);
	layout->setAlignment(title, Qt::AlignHCenter);

	layout->addStretch(6);

	title = new QLabel(tr("Select Year"));
	Styler::styleLabel(title);
	layout->addWidget(title);
	layout->setAlignment(title, Qt::AlignHCenter);

	yearSelector = new YearSelector;
	layout->addWidget(yearSelector);
	layout->setAlignment(yearSelector, Qt::AlignHCenter);

	layout->addStretch(2);

	QPushButton *downloadButton = new QPushButton(tr("Download Stats"));
	Styler::styleButton(downloadButton);
	layout->addWidget(downloadButton);
	layout->setAlignment(downloadButton, Qt::AlignHCenter);

	layout->addStretch(8);

	connect(downloadButton, &QPushButton::clicked, this, &ReportsStatsWidget::downloadClicked);
}


void ReportsStatsWidget::downloadClicked()
{
	QJsonDocument jDoc;
	QJsonObject json;
	json.insert("year", yearSelector->currentYearSelected());
	jDoc.setObject(json);
	foxmq.post("reports.stats", FoxHead::ContentType::Json, jDoc.toJson(QJsonDocument::Compact));
}
