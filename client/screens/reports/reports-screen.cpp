// clang-format off
/**************************************************************************************************
	Copyright (C) 2024  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QFileDialog>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include "qexclusivebuttons.h"
#include "qslidingstackedwidget.h"
#include "main-window.h"
#include "connection-manager.h"
#include "styler.h"
#include "theme.h"
#include "reports-donations-widget.h"
#include "reports-stats-widget.h"
#include "reports-monthly-widget.h"
#include "reports-screen.h"

enum Content {
	Donations,
	Stats,
	Monthly,
};

ReportsScreen::ReportsScreen(MainWindow &mainWindow) : mainWindow(mainWindow), cm(mainWindow.connectionManager)
{
	QVBoxLayout *layout = new QVBoxLayout;
	setLayout(layout);
	int m = Theme::OuterMargin * qMin(screen()->geometry().width(), screen()->geometry().height());
	layout->setContentsMargins(m, m, m, m);
	QHBoxLayout *topLayout = new QHBoxLayout;
	layout->addLayout(topLayout);

	topLayout->addStretch(1);
	selectorButtons = new QExclusiveButtons({tr("Donations"), tr("Stats"), tr("Monthly")});
	topLayout->addWidget(selectorButtons);
	for ( int i=0; i < selectorButtons->size(); i++)
		Styler::styleButton(selectorButtons->buttonAt(i));
	selectorButtons->setChecked(0, true);

	content = new QSlidingStackedWidget();
	content->setDuration(500);
	layout->addWidget(content);

	ReportsDonationsWidget *donationsWidget = new ReportsDonationsWidget(cm.foxmq);
	content->addWidget(donationsWidget);

	ReportsStatsWidget *statsWidget = new ReportsStatsWidget(cm.foxmq);
	content->addWidget(statsWidget);

	monthlyWidget = new ReportsMonthlyWidget;
	content->addWidget(monthlyWidget);

	connect(selectorButtons, &QExclusiveButtons::clicked, this, &ReportsScreen::selectorClicked);

	topicsToReceive << "reports";
	cm.registerApiForReceiver(this);
}

void ReportsScreen::processPostResponce(const QByteArray &topic, FoxID id, FoxHead::ContentType, const QByteArray &content)
{
	if (topic == "reports.donations" || topic == "reports.stats") {
		exportToSave = content;
		saveFileDialog = QSharedPointer<QFileDialog>(new QFileDialog(nullptr, "Save File", "", "tsv (*.tsv)"), &QObject::deleteLater);
		saveFileDialog->setAcceptMode(QFileDialog::AcceptSave);
		connect(saveFileDialog.data(), &QFileDialog::fileSelected, this, &ReportsScreen::saveFileSelected);
		connect(saveFileDialog.data(), &QFileDialog::finished, this, &ReportsScreen::saveFileSelectorFinished);
		saveFileDialog->show();
		mainWindow.setEnabled(false);
	} else if (topic == "reports.monthly") {
		monthlyWidget->update(QJsonDocument::fromJson(content).object());
	}
}

void ReportsScreen::selectorClicked(int index)
{
	content->setCurrentIndex(index);
	if (index == Content::Monthly)
		cm.foxmq.post("reports.monthly", FoxHead::ContentType::Empty, "");
}

void ReportsScreen::saveFileSelected(QString fn)
{
	QFile file(fn);
	if (file.open(QIODevice::WriteOnly)) {
		file.write(exportToSave);
		file.close();
	}
	exportToSave.clear();
}

void ReportsScreen::saveFileSelectorFinished()
{
	mainWindow.setEnabled(true);
	saveFileDialog.clear();
}

