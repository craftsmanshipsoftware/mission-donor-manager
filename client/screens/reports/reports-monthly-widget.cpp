// clang-format off
/**************************************************************************************************
	Copyright (C) 2024  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QVBoxLayout>
#include <QLabel>
#include <QTableView>
#include <QHeaderView>
#include <QKeyEvent>
#include "styler.h"
#include "theme.h"
#include "copy-and-paste.h"
#include "reports-monthly-widget.h"

ReportsMonthlyWidget::ReportsMonthlyWidget()
{
	QVBoxLayout *layout = new QVBoxLayout;
	setLayout(layout);

	QLabel *titleLabel = new QLabel(tr("Monthly Donations Totals"));
	Styler::styleLabel(titleLabel, Theme::H1Size);
	layout->addWidget(titleLabel);
	layout->setAlignment(titleLabel, Qt::AlignHCenter);

	layout->addStretch(1);

	inkindViewList = new QTableView;
	inkindViewList->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Maximum);
	layout->addWidget(inkindViewList);
	layout->setAlignment(inkindViewList, Qt::AlignHCenter);
	Styler::styleTableView(inkindViewList);
	inkindViewList->verticalHeader()->hide();
	inkindViewList->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	inkindViewList->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	inkindViewList->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
	inkindViewList->setAlternatingRowColors(true);
	inkindViewList->setModel(&reportsMonthlyDataModel);

	layout->addStretch(1);
}

void ReportsMonthlyWidget::update(const QJsonObject &json)
{
	inkindViewList->setCurrentIndex(QModelIndex());  // clear selected index if copy-and-paste is attempted after view is refreshed with smaller model.
	reportsMonthlyDataModel.layoutAboutToBeChanged();
	reportsMonthlyDataModel.update(json);
	reportsMonthlyDataModel.layoutChanged();
	inkindViewList->resizeColumnsToContents();
}

void ReportsMonthlyWidget::keyPressEvent(QKeyEvent *e)
{
	if (e->type() == QKeyEvent::KeyPress && e->matches(QKeySequence::Copy))
		copyFromTable(inkindViewList->selectionModel()->selectedIndexes());
}
