// clang-format off
/**************************************************************************************************
	Copyright (C) 2024  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QVBoxLayout>
#include <QLabel>
#include <QCalendarWidget>
#include <QCheckBox>
#include <QPushButton>
#include <QJsonDocument>
#include <QJsonObject>
#include "theme.h"
#include "styler.h"
#include "reports-donations-widget.h"

#include <QDebug>

ReportsDonationsWidget::ReportsDonationsWidget(FoxMQ &foxmq) : foxmq(foxmq)
{
	QVBoxLayout *layout = new QVBoxLayout;
	setLayout(layout);

	QLabel *title = new QLabel(tr("Global Donations Export"));
	Styler::styleLabel(title, Theme::H1Size);
	layout->addWidget(title);
	layout->setAlignment(title, Qt::AlignHCenter);

	layout->addStretch(1);

	title = new QLabel(tr("From:"));
	Styler::styleLabel(title, Theme::H2Size);
	layout->addWidget(title);
	layout->setAlignment(title, Qt::AlignHCenter);

	fromCalendar = new QCalendarWidget;
	layout->addWidget(fromCalendar);
	layout->setAlignment(fromCalendar, Qt::AlignHCenter);

	layout->addStretch(1);

	title = new QLabel(tr("To:"));
	Styler::styleLabel(title, Theme::H2Size);
	layout->addWidget(title);
	layout->setAlignment(title, Qt::AlignHCenter);

	toCalendar = new QCalendarWidget;
	layout->addWidget(toCalendar);
	layout->setAlignment(toCalendar, Qt::AlignHCenter);

	layout->addStretch(1);

	includeVoided = new QCheckBox(tr("Include Voided"));
	Styler::styleCheckBox(includeVoided);
	layout->addWidget(includeVoided);
	layout->setAlignment(includeVoided, Qt::AlignHCenter);

	includeInkind = new QCheckBox(tr("Include Inkind"));
	Styler::styleCheckBox(includeInkind);
	layout->addWidget(includeInkind);
	layout->setAlignment(includeInkind, Qt::AlignHCenter);

	layout->addStretch(1);

	downloadButton = new QPushButton(tr("Download"));
	Styler::styleButton(downloadButton);
	layout->addWidget(downloadButton);
	layout->setAlignment(downloadButton, Qt::AlignHCenter);
	downloadButton->setEnabled(false);

	layout->addStretch(1);

	connect(fromCalendar, &QCalendarWidget::clicked, this, &ReportsDonationsWidget::validateDownloadButton);
	connect(toCalendar, &QCalendarWidget::clicked, this, &ReportsDonationsWidget::validateDownloadButton);
	connect(downloadButton, &QPushButton::clicked, this, &ReportsDonationsWidget::downloadClicked);
}

void ReportsDonationsWidget::validateDownloadButton()
{
	if (fromCalendar->selectedDate() > toCalendar->selectedDate())
		downloadButton->setEnabled(false);
	else
		downloadButton->setEnabled(true);
}

void ReportsDonationsWidget::downloadClicked()
{
	QJsonDocument jDoc;
	QJsonObject json;
	json.insert("from", fromCalendar->selectedDate().toJulianDay());
	json.insert("to", toCalendar->selectedDate().toJulianDay());
	json.insert("include-invalid", includeVoided->isChecked());
	json.insert("include-inkind", includeInkind->isChecked());
	jDoc.setObject(json);
	foxmq.post("reports.donations", FoxHead::ContentType::Json, jDoc.toJson(QJsonDocument::Compact));
}
