// clang-format off
/**************************************************************************************************
	Copyright (C) 2023-2024  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef MDM_DONOR_EXPORT_GENERAL_H_
#define MDM_DONOR_EXPORT_GENERAL_H_

#include <QWidget>
#include <QModelIndex>
#include <QDoubleValidator>
#include <QButtonGroup>
#include "name-selector-widget.h"

class MainWindow;
class ConnectionManager;
class QGroupBox;
class QPushButton;
class QCheckBox;
class QRadioButton;
class QComboBox;
class QCalendarWidget;
class QLabel;
class QFileDialog;

class DonorExportGeneral : public QWidget
{
	Q_OBJECT

public:
	DonorExportGeneral(MainWindow &mainWindow);
	inline void updateMissionList(const QJsonArray &jArray) { missionSelector->updateNameList(jArray); }
	inline void updateUsersList(const QJsonArray &jArray) { userSelector->updateNameList(jArray); }
	void postResponce(const QByteArray &content);

private:
	void filterByMissionChanged(QAbstractButton *);
	void filterByRegion1Clicked();
	void exportButtonClicked();
	void validateExportButton();
	void filterByTimeClicked();
	void filterByLastClicked();
	void filterByTimeDisableClicked();
	void saveFileSelected(QString fn);
	void saveFileSelectorFinished();
	MainWindow &mainWindow;
	ConnectionManager &cm;
	NameSelectorWidget *missionSelector;
	NameSelectorWidget *userSelector;
	QRadioButton *filterByTime;
	QRadioButton *filterByInactive;
	QRadioButton *filterByTimeNone;
	QRadioButton *filterBymissionNone;
	QRadioButton *filterByUser;
	QRadioButton *filterByMission;
	QButtonGroup missionGroup;
	QCheckBox *filterByNewsletter;
	QCheckBox *filterByType;
	QComboBox *typeSelector;
	QCheckBox *filterByRegion2;
	QCheckBox *filterByRegion3;
	QButtonGroup regionGroup;
	QRadioButton *regionNone;
	QRadioButton *regionInternational;
	QRadioButton *regionCanada;
	QComboBox *region1Selector;
	QComboBox *region2Selector;
	QComboBox *region3Selector;
	QLabel *oldestLabel;
	QLabel *newestLabel;
	QCalendarWidget *fromCalendar;
	QCalendarWidget *toCalendar;
	QPushButton *exportButton;
	QCheckBox *exportIdNumber;
	QCheckBox *exportFirstName;
	QCheckBox *exportLastName;
	QCheckBox *exportAddress;
	QCheckBox *exportEmail;
	QCheckBox *exportCellPhone;
	QCheckBox *exportHomePhone;
	QCheckBox *exportWorkPhone;
	QCheckBox *exportPaypalAccounts;
	QLabel *helperMessage;
	QSharedPointer<QFileDialog> saveFileDialog;
	QByteArray cvsToSave;
};

#endif
