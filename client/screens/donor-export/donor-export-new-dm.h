// clang-format off
/**************************************************************************************************
	Copyright (C) 2024  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef MDM_DONOR_EXPORT_NEW_DM_H_
#define MDM_DONOR_EXPORT_NEW_DM_H_

#include <QAbstractTableModel>
#include <QDate>

class DonorExportNewDataModel : public QAbstractTableModel
{
	Q_OBJECT

public:
	int rowCount(const QModelIndex &parent = QModelIndex()) const override ;
	int columnCount(const QModelIndex &parent = QModelIndex()) const override;
	QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
	void setDonors(const QByteArray &content);
//	uint getId(uint row) { return list.at(row).id; }
	using QAbstractItemModel::endInsertRows;

private:
	struct DonorExportItem {
		int id;
		QString first;
		QString last;
		QString address;
		QString email;
		QString cellPhone;
		QString homePhone;
		QString workPhone;
		QString paypalAccount;
	};
	QList<DonorExportItem> list;
};

#endif
