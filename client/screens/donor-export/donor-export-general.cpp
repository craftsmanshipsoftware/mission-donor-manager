// clang-format off
/**************************************************************************************************
	Copyright (C) 2023-2024  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QScreen>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QCheckBox>
#include <QRadioButton>
#include <QComboBox>
#include <QGroupBox>
#include <QCalendarWidget>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QFileDialog>
#include <QFile>
#include "../common/common.h"
#include "main-window.h"
#include "connection-manager.h"
#include "styler.h"
#include "theme.h"
#include "name-selector-widget.h"
#include "donor-export-general.h"

#include <QDebug>

DonorExportGeneral::DonorExportGeneral(MainWindow &mainWindow) : mainWindow(mainWindow), cm(mainWindow.connectionManager)
{
	QLabel *label;
	QStringList regionNames;
	regionNames << "Canada" << "Newfoundland" << "Prince Edward Island" << "Nova Scotia" << "New Brunswick" << "Quebec" << "Ontario" << "Manitoba" << "Saskatchewan"
	            << "Alberta" << "British Columbia" << "Yukon" << "Northwest Territories" << "Nunavut";

	auto addRegionSelector = [&](int defaultIndex) {
		QComboBox *cb = new QComboBox;
		cb->addItems(regionNames);
		Styler::styleComboBox(cb);
		cb->setCurrentIndex(defaultIndex);
		return cb;
	};

	QVBoxLayout *layout = new QVBoxLayout;
	setLayout(layout);
	int m = Theme::OuterMargin * qMin(screen()->geometry().width(), screen()->geometry().height());
	layout->setContentsMargins(m, m, m, m);

	label = new QLabel(tr("General"));
	Styler::styleLabel(label, Theme::H1Size);
	layout->addWidget(label);
	layout->setAlignment(label, Qt::AlignHCenter);

	QHBoxLayout *limitLayout = new QHBoxLayout;
	layout->addLayout(limitLayout);

	limitLayout->addStretch(1);
	QVBoxLayout *missionGroupLayout = new QVBoxLayout;
	limitLayout->addLayout(missionGroupLayout);
	QLabel *missionLimitText = new QLabel(tr("Limit to Mission"));
	Styler::styleLabel(missionLimitText);
	missionGroupLayout->addWidget(missionLimitText);

	filterBymissionNone = new QRadioButton(tr("None"));
	Styler::styleRadioButton(filterBymissionNone);
	filterBymissionNone->setChecked(true);
	missionGroupLayout->addWidget(filterBymissionNone);
	missionGroup.addButton(filterBymissionNone);
	filterByMission = new QRadioButton(tr("Missions"));
	Styler::styleRadioButton(filterByMission);
	missionGroupLayout->addWidget(filterByMission);
	missionGroup.addButton(filterByMission);
	filterByUser = new QRadioButton(tr("User's Missions"));
	Styler::styleRadioButton(filterByUser);
	missionGroupLayout->addWidget(filterByUser);
	missionGroup.addButton(filterByUser);

	missionSelector = new NameSelectorWidget;
	missionSelector->show();
	missionGroupLayout->addWidget(missionSelector);
	userSelector = new NameSelectorWidget;
	missionGroupLayout->addWidget(userSelector);
	userSelector->hide();

	limitLayout->addStretch(1);

	QVBoxLayout *middleLayout = new QVBoxLayout;
	limitLayout->addLayout(middleLayout);

	filterByNewsletter =  new QCheckBox(tr("Limit by Newsletter"));
	Styler::styleCheckBox(filterByNewsletter);
	middleLayout->addWidget(filterByNewsletter);
	middleLayout->addStretch(1);

	QVBoxLayout *timeGroupLayout = new QVBoxLayout;
	middleLayout->addLayout(timeGroupLayout);
	middleLayout->addStretch(1);

	filterByTime = new QRadioButton(tr("Limit by Donation Date"));
	Styler::styleRadioButton(filterByTime);
	timeGroupLayout->addWidget(filterByTime);

	filterByInactive = new QRadioButton(tr("Limit by Last Donation Date"));
	Styler::styleRadioButton(filterByInactive);
	timeGroupLayout->addWidget(filterByInactive);

	filterByTimeNone = new QRadioButton(tr("None"));
	Styler::styleRadioButton(filterByTimeNone);
	timeGroupLayout->addWidget(filterByTimeNone);
	filterByTimeNone->setChecked(true);

	oldestLabel = new QLabel;
	Styler::styleLabel(oldestLabel);
	timeGroupLayout->addWidget(oldestLabel);
	fromCalendar = new QCalendarWidget;
	timeGroupLayout->addWidget(fromCalendar);

	newestLabel = new QLabel;
	Styler::styleLabel(newestLabel);
	timeGroupLayout->addWidget(newestLabel);
	toCalendar = new QCalendarWidget;
	timeGroupLayout->addWidget(toCalendar);

	helperMessage = new QLabel;
	Styler::styleLabel(helperMessage);
	middleLayout->addWidget(helperMessage);
	middleLayout->setAlignment(helperMessage, Qt::AlignHCenter);
	middleLayout->addStretch(1);

	limitLayout->addStretch(1);

	QVBoxLayout *regionLayout = new QVBoxLayout;
	limitLayout->addLayout(regionLayout);
	QLabel *regionLimitText = new QLabel(tr("Limit to Region"));
	Styler::styleLabel(regionLimitText);
	regionLayout->addWidget(regionLimitText);

	regionNone = new QRadioButton(tr("None"));
	Styler::styleRadioButton(regionNone);
	regionNone->setChecked(true);
	regionLayout->addWidget(regionNone);
	regionGroup.addButton(regionNone);
	regionInternational = new QRadioButton(tr("International"));
	Styler::styleRadioButton(regionInternational);
	regionLayout->addWidget(regionInternational);
	regionGroup.addButton(regionInternational);
	regionCanada = new QRadioButton(tr("Canada"));
	Styler::styleRadioButton(regionCanada);
	regionLayout->addWidget(regionCanada);
	regionGroup.addButton(regionCanada);

	region1Selector = addRegionSelector(ProvinceAlphaCode::ON);
	regionLayout->addWidget(region1Selector);
	filterByRegion2 = new QCheckBox(tr("OR to Region"));
	Styler::styleCheckBox(filterByRegion2);
	regionLayout->addWidget(filterByRegion2);
	region2Selector = addRegionSelector(ProvinceAlphaCode::ON);
	regionLayout->addWidget(region2Selector);
	filterByRegion3 = new QCheckBox(tr("OR to Region"));
	Styler::styleCheckBox(filterByRegion3);
	regionLayout->addWidget(filterByRegion3);
	region3Selector = addRegionSelector(ProvinceAlphaCode::ON);
	regionLayout->addWidget(region3Selector);
	filterByRegion1Clicked();

	regionLayout->addStretch(1);

	filterByType = new QCheckBox(tr("Limit by Type"));
	Styler::styleCheckBox(filterByType);
	regionLayout->addWidget(filterByType);
	typeSelector = new QComboBox;
	typeSelector->addItems({tr("Individual"), tr("Business"), tr("Government Agency"), tr("Canadian Charity"), tr("Foreign Charity"), tr("Non Tax Payer"), tr("Canadian Foundation")}); // order same as DonorType enum
	Styler::styleComboBox(typeSelector);
	typeSelector->setCurrentIndex(0);
	regionLayout->addWidget(typeSelector);

	regionLayout->addStretch(1);

	label = new QLabel(tr("Enable Column"));
	Styler::styleLabel(label, Theme::H2Size);
	regionLayout->addWidget(label);

	exportIdNumber = new QCheckBox(tr("ID Number"));
	Styler::styleCheckBox(exportIdNumber);
	regionLayout->addWidget(exportIdNumber);
	exportFirstName = new QCheckBox(tr("First Name"));
	Styler::styleCheckBox(exportFirstName);
	regionLayout->addWidget(exportFirstName);
	exportLastName = new QCheckBox(tr("Last Name"));
	Styler::styleCheckBox(exportLastName);
	regionLayout->addWidget(exportLastName);
	exportAddress = new QCheckBox(tr("Address Line 1"));
	Styler::styleCheckBox(exportAddress);
	regionLayout->addWidget(exportAddress);
	exportEmail = new QCheckBox(tr("Email"));
	Styler::styleCheckBox(exportEmail);
	regionLayout->addWidget(exportEmail);
	exportCellPhone = new QCheckBox(tr("Cell Phone"));
	Styler::styleCheckBox(exportCellPhone);
	regionLayout->addWidget(exportCellPhone);
	exportHomePhone = new QCheckBox(tr("Home Phone"));
	Styler::styleCheckBox(exportHomePhone);
	regionLayout->addWidget(exportHomePhone);
	exportWorkPhone = new QCheckBox(tr("Work Phone"));
	Styler::styleCheckBox(exportWorkPhone);
	regionLayout->addWidget(exportWorkPhone);
	exportPaypalAccounts = new QCheckBox(tr("Paypal Accounts"));
	Styler::styleCheckBox(exportPaypalAccounts);
	regionLayout->addWidget(exportPaypalAccounts);

	limitLayout->addStretch(1);

	exportButton = new QPushButton(tr("Export List"));
	Styler::styleButton(exportButton);
	layout->addWidget(exportButton);
	layout->setAlignment(exportButton, Qt::AlignHCenter);

	connect(regionCanada, &QRadioButton::toggled, this, &DonorExportGeneral::filterByRegion1Clicked);
	connect(exportButton, &QPushButton::clicked, this, &DonorExportGeneral::exportButtonClicked);

	connect(exportIdNumber, &QCheckBox::clicked, this, &DonorExportGeneral::validateExportButton);
	connect(exportFirstName, &QCheckBox::clicked, this, &DonorExportGeneral::validateExportButton);
	connect(exportLastName, &QCheckBox::clicked, this, &DonorExportGeneral::validateExportButton);
	connect(exportAddress, &QCheckBox::clicked, this, &DonorExportGeneral::validateExportButton);
	connect(exportEmail, &QCheckBox::clicked, this, &DonorExportGeneral::validateExportButton);
	connect(exportCellPhone, &QCheckBox::clicked, this, &DonorExportGeneral::validateExportButton);
	connect(exportHomePhone, &QCheckBox::clicked, this, &DonorExportGeneral::validateExportButton);
	connect(exportWorkPhone, &QCheckBox::clicked, this, &DonorExportGeneral::validateExportButton);
	connect(exportPaypalAccounts, &QCheckBox::clicked, this, &DonorExportGeneral::validateExportButton);
	connect(&missionGroup, QOverload<QAbstractButton *>::of(&QButtonGroup::buttonClicked), this, &DonorExportGeneral::filterByMissionChanged);
	connect(missionSelector, &NameSelectorWidget::nameSelected, this, &DonorExportGeneral::validateExportButton);
	connect(missionSelector, &NameSelectorWidget::filterModified, this, &DonorExportGeneral::validateExportButton);
	connect(userSelector, &NameSelectorWidget::nameSelected, this, &DonorExportGeneral::validateExportButton);
	connect(userSelector, &NameSelectorWidget::filterModified, this, &DonorExportGeneral::validateExportButton);
	connect(filterByTime, &QRadioButton::clicked, this, &DonorExportGeneral::filterByTimeClicked);
	connect(filterByInactive, &QRadioButton::clicked, this, &DonorExportGeneral::filterByLastClicked);
	connect(filterByTimeNone, &QRadioButton::clicked, this, &DonorExportGeneral::filterByTimeDisableClicked);
	connect(fromCalendar, &QCalendarWidget::clicked, this, &DonorExportGeneral::validateExportButton);
	connect(toCalendar, &QCalendarWidget::clicked, this, &DonorExportGeneral::validateExportButton);

	filterByTimeDisableClicked();
}

void DonorExportGeneral::postResponce(const QByteArray &content)
{
	cvsToSave = content;
	saveFileDialog = QSharedPointer<QFileDialog>(new QFileDialog(nullptr, "Save File", "", "tsv (*.tsv)"), &QObject::deleteLater);
	saveFileDialog->setAcceptMode(QFileDialog::AcceptSave);
	connect(saveFileDialog.data(), &QFileDialog::fileSelected, this, &DonorExportGeneral::saveFileSelected);
	connect(saveFileDialog.data(), &QFileDialog::finished, this, &DonorExportGeneral::saveFileSelectorFinished);
	saveFileDialog->show();
	mainWindow.setEnabled(false);
}

void DonorExportGeneral::filterByMissionChanged(QAbstractButton *)
{
	if (filterByMission->isChecked()) {
		missionSelector->show();
		userSelector->hide();
		userSelector->clearIndexAndSelection();
	}

	if (filterByUser->isChecked()) {
		userSelector->show();
		missionSelector->hide();
		missionSelector->clearIndexAndSelection();
	}
	validateExportButton();
}

void DonorExportGeneral::filterByRegion1Clicked()
{
	bool enabled = regionCanada->isChecked();
	if (!enabled) {
		filterByRegion2->setChecked(false);
		filterByRegion3->setChecked(false);
	}
	filterByRegion2->setEnabled(enabled);
	filterByRegion3->setEnabled(enabled);
	region1Selector->setEnabled(enabled);
	region2Selector->setEnabled(enabled);
	region3Selector->setEnabled(enabled);
}

void DonorExportGeneral::exportButtonClicked()
{
	QJsonDocument jdoc;
	QJsonObject json;
	json.insert("filter-by-news", filterByNewsletter->isChecked());
	json.insert("filter-by-mission", filterByMission->isChecked());
	json.insert("filter-by-user", filterByUser->isChecked());
	json.insert("mission-id", missionSelector->idOfCurrentIndex());
	json.insert("user-id", userSelector->idOfCurrentIndex());
	json.insert("filter-by-time", filterByTime->isChecked());
	json.insert("filter-inactive", filterByInactive->isChecked());
	json.insert("toJulian", toCalendar->selectedDate().toJulianDay());
	json.insert("fromJulian", fromCalendar->selectedDate().toJulianDay());
	json.insert("filter-by-international", regionInternational->isChecked());
	json.insert("filter-by-region1", regionCanada->isChecked());
	json.insert("filter-by-region2", filterByRegion2->isChecked());
	json.insert("filter-by-region3", filterByRegion3->isChecked());
	json.insert("region1", region1Selector->currentIndex());
	json.insert("region2", region2Selector->currentIndex());
	json.insert("region3", region3Selector->currentIndex());
	json.insert("filter-by-type", filterByType->isChecked());
	json.insert("type", typeSelector->currentIndex());


	json.insert("export-id", exportIdNumber->isChecked());
	json.insert("export-firstname", exportFirstName->isChecked());
	json.insert("export-lastname", exportLastName->isChecked());
	json.insert("export-address", exportAddress->isChecked());
	json.insert("export-email", exportEmail->isChecked());
	json.insert("export-phone-cell", exportCellPhone->isChecked());
	json.insert("export-phone-home", exportHomePhone->isChecked());
	json.insert("export-phone-work", exportWorkPhone->isChecked());
	json.insert("export-paypal", exportPaypalAccounts->isChecked());

	jdoc.setObject(json);
	cm.foxmq.post("donor-export.generate", FoxHead::ContentType::Json, jdoc.toJson(QJsonDocument::Compact));
}

void DonorExportGeneral::validateExportButton()
{
	bool valid = true;

	auto updateButton = [&]() {
		exportButton->setEnabled(valid);
		if (valid)
			helperMessage->clear();
	};

	if (filterByMission->isChecked() && missionSelector->idOfCurrentIndex() == -1) {
		helperMessage->setText(tr("Please Choose a Mission"));
		valid = false;
		updateButton();
		return;
	}

	if (filterByUser->isChecked() && userSelector->idOfCurrentIndex() == -1) {
		helperMessage->setText(tr("Please Choose a User"));
		valid = false;
		updateButton();
		return;
	}

	if ((filterByTime->isChecked() || filterByInactive->isChecked()) && fromCalendar->selectedDate() > toCalendar->selectedDate())	{
		helperMessage->setText(tr("'Older Than' date is before 'Newer Than' date"));
		valid = false;
		updateButton();
		return;
	}

	if (!exportIdNumber->isChecked() &&!exportFirstName->isChecked() && !exportLastName->isChecked() && !exportAddress->isChecked() && !exportEmail->isChecked()
	        && !exportCellPhone->isChecked() && !exportHomePhone->isChecked() && !exportWorkPhone->isChecked() && !exportPaypalAccounts->isChecked()) {
		valid = false;
		helperMessage->setText(tr("Please Choose at least one column to export"));
	}
	updateButton();
}

void DonorExportGeneral::filterByTimeClicked()
{
	oldestLabel->setText(tr("Limit Donations NEWER than"));
	newestLabel->setText(tr("\nLimit Donations OLDER than"));
	fromCalendar->setEnabled(true);
	toCalendar->setEnabled(true);
	validateExportButton();
}

void DonorExportGeneral::filterByLastClicked()
{
	oldestLabel->setText(tr("Limit Last Donation Newer Than..."));
	newestLabel->setText(tr("\nLimit Last Donation Older Than..."));
	fromCalendar->setEnabled(true);
	toCalendar->setEnabled(true);
	validateExportButton();
}

void DonorExportGeneral::filterByTimeDisableClicked()
{
	oldestLabel->setText(tr("Disabled"));
	newestLabel->setText(tr("\nDisabled"));
	fromCalendar->setEnabled(false);
	toCalendar->setEnabled(false);
	validateExportButton();
}

void DonorExportGeneral::saveFileSelected(QString fn)
{
	QFile file(fn);
	if (file.open(QIODevice::WriteOnly)) {
		file.write(cvsToSave);
		file.close();
	}
	cvsToSave.clear();
}

void DonorExportGeneral::saveFileSelectorFinished()
{
	mainWindow.setEnabled(true);
	saveFileDialog.clear();
}
