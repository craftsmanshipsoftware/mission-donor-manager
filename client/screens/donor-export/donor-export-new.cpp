// clang-format off
/**************************************************************************************************
	Copyright (C) 2014  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QCalendarWidget>
#include <QJsonDocument>
#include <QJsonObject>
#include <QTableView>
#include <QHeaderView>
#include <QKeyEvent>
#include "copy-and-paste.h"
#include "connection-manager.h"
#include "styler.h"
#include "donor-export-new.h"

DonorExportNew::DonorExportNew(ConnectionManager &cm) : cm(cm)
{
	QHBoxLayout *layout = new QHBoxLayout;
	setLayout(layout);
	layout->addStretch(1);
	QVBoxLayout *calendarLayout = new QVBoxLayout;
	layout->addLayout(calendarLayout);
	layout->setStretchFactor(calendarLayout, 6);
	layout->addStretch(1);
	QVBoxLayout *donorLayout = new QVBoxLayout;
	layout->addLayout(donorLayout);
	layout->setStretchFactor(donorLayout, 18);
	layout->addStretch(1);


	calendarLayout->addStretch(1);
	QLabel *label = new QLabel(tr("Date FROM"));
	Styler::styleLabel(label);
	calendarLayout->addWidget(label);
	fromCalendar = new QCalendarWidget;
	calendarLayout->addWidget(fromCalendar);

	label = new QLabel(tr("Date TO"));
	Styler::styleLabel(label);
	calendarLayout->addWidget(label);
	toCalendar = new QCalendarWidget;
	calendarLayout->addWidget(toCalendar);
	calendarLayout->addStretch(1);

	donorLayout->addStretch(1);
	noActivity = new QLabel(tr("No New Donors for Period"));
	Styler::styleLabel(noActivity);
	donorLayout->addWidget(noActivity);
	donorLayout->setAlignment(noActivity, Qt::AlignHCenter);
	donorView = new QTableView;
	Styler::styleTableView(donorView);
	donorView->setModel(&donorExportNewDataModel);
	donorView->verticalHeader()->hide();
	donorView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	donorView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
	donorView->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
	donorView->setAlternatingRowColors(true);
	donorView->hide();
	donorLayout->addWidget(donorView);
	donorLayout->setAlignment(donorView, Qt::AlignHCenter);
	donorLayout->addStretch(1);

	connect(fromCalendar, &QCalendarWidget::clicked, this, &DonorExportNew::calendarClicked);
	connect(toCalendar, &QCalendarWidget::clicked, this, &DonorExportNew::calendarClicked);
}

void DonorExportNew::calendarClicked()
{
	if (toCalendar->selectedDate() >= fromCalendar->selectedDate()) {
		QJsonObject json;
		json.insert("toJulian", toCalendar->selectedDate().toJulianDay());
		json.insert("fromJulian", fromCalendar->selectedDate().toJulianDay());
		QJsonDocument jdoc;
		jdoc.setObject(json);
		cm.foxmq.post("donor-export.new-donors", FoxHead::ContentType::Json, jdoc.toJson(QJsonDocument::Compact));
	}
}

void DonorExportNew::updateDonorList(const QByteArray &content)
{
	donorView->setCurrentIndex(QModelIndex());  // clear selected index if copy-and-paste is attempted after view is refreshed with smaller model.
	donorExportNewDataModel.setDonors(content);
	donorView->resizeColumnsToContents();
	if (donorExportNewDataModel.rowCount()) {
		noActivity->hide();
		donorView->show();
	} else {
		noActivity->show();
		donorView->hide();
	}
}

void DonorExportNew::keyPressEvent(QKeyEvent *e)
{
	if (e->type() == QKeyEvent::KeyPress && e->matches(QKeySequence::Copy))
		copyFromTable(donorView->selectionModel()->selectedIndexes());
}
