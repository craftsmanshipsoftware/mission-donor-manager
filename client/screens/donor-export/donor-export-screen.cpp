// clang-format off
/**************************************************************************************************
	Copyright (C) 2023-2024  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QScreen> // for outer margin
#include <QVBoxLayout>
#include <QLabel>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include "main-window.h"
#include "connection-manager.h"
#include "qexclusivebuttons.h"
#include "qslidingstackedwidget.h"
#include "styler.h"
#include "theme.h"
#include "donor-export-general.h"
#include "donor-export-new.h"
#include "donor-export-screen.h"

DonorExportScreen::DonorExportScreen(MainWindow &mainWindow) : mainWindow(mainWindow), cm(mainWindow.connectionManager)
{
	QVBoxLayout *layout = new QVBoxLayout;
	setLayout(layout);
	int m = Theme::OuterMargin * qMin(screen()->geometry().width(), screen()->geometry().height());
	layout->setContentsMargins(m, m, m, m);
	QHBoxLayout *topLayout = new QHBoxLayout;
	layout->addLayout(topLayout);

	// top Button layout
	topLayout->addStretch(1);
	QLabel *title = new QLabel(tr("Export Donor Info"));
	Styler::styleLabel(title, Theme::H1Size);
	topLayout->addWidget(title);
	topLayout->addStretch(1);

	topButtons = new QExclusiveButtons( { tr("General"), tr("New Donors") } );
	topLayout->addWidget(topButtons);
	for ( int i=0; i < topButtons->size(); i++) {
		Styler::styleButton(topButtons->buttonAt(i));
		topButtons->buttonAt(i)->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
	}
	topButtons->setChecked(0, true);

	content = new QSlidingStackedWidget();
	content->setDuration(500);
	layout->addWidget(content);
	donorExportGeneral = new DonorExportGeneral(mainWindow);
	content->addWidget(donorExportGeneral);
	donorExportNew = new DonorExportNew(mainWindow.connectionManager);
	content->addWidget(donorExportNew);

	connect(topButtons, &QExclusiveButtons::clicked, this, &DonorExportScreen::topButtonClicked);

	topicsToReceive << "donor-export" << "mission.ro.list" << "users.ro.list";
	cm.registerApiForReceiver(this);
}

void DonorExportScreen::subscribe()
{
	cm.foxmq.subscription("mission.ro.list", true);
	cm.foxmq.subscription("users.ro.list", true);
}

void DonorExportScreen::unsubscribe()
{
	cm.foxmq.subscription("mission.ro.list", false);
	cm.foxmq.subscription("users.ro.list", false);
}

void DonorExportScreen::processPublish(const QByteArray &topic, ssize_t, FoxID, FoxHead::ContentType, const QByteArray &content)
{
	QJsonArray jArray = QJsonDocument::fromJson(content).array();
	if (topic == "mission.ro.list") {
		donorExportGeneral->updateMissionList(jArray);
	} else if (topic == "users.ro.list") {
		donorExportGeneral->updateUsersList(jArray);
	}
}

void DonorExportScreen::processPostResponce(const QByteArray &topic, FoxID, FoxHead::ContentType, const QByteArray &content)
{
	if (topic == "donor-export.generate")
		donorExportGeneral->postResponce(content);
	else if (topic == "donor-export.new-donors")
		donorExportNew->updateDonorList(content);
}

void DonorExportScreen::topButtonClicked(int index)
{
	content->setCurrentIndex(index);
}
