// clang-format off
/**************************************************************************************************
	Copyright (C) 2023-2014  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef MDM_DONOR_EXPORT_SCREEN_H_
#define MDM_DONOR_EXPORT_SCREEN_H_

#include "tab-screen.h"
#include "receiver.h"

class MainWindow;
class ConnectionManager;
class QExclusiveButtons;
class QSlidingStackedWidget;
class DonorExportGeneral;
class DonorExportNew;

class DonorExportScreen : public TabScreen, public Receiver
{
	Q_OBJECT

public:
	DonorExportScreen(MainWindow &mainWindow);
	void subscribe() override;
	void unsubscribe() override;

private:
	void processPublish(const QByteArray &topic, ssize_t count, FoxID id, FoxHead::ContentType contentType, const QByteArray &content) override;
	void processPostResponce(const QByteArray &topic, FoxID id, FoxHead::ContentType contentType, const QByteArray &content) override;
	void topButtonClicked(int index);
	MainWindow &mainWindow;
	ConnectionManager &cm;
	QExclusiveButtons *topButtons;
	QSlidingStackedWidget *content;
	DonorExportGeneral *donorExportGeneral;
	DonorExportNew *donorExportNew;
};

#endif
