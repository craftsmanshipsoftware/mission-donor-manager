// clang-format off
/**************************************************************************************************
	Copyright (C) 2024  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include "donor-export-new-dm.h"

void DonorExportNewDataModel::setDonors(const QByteArray &content)
{
	layoutAboutToBeChanged();
	list.clear();
	QJsonArray jDonors = QJsonDocument::fromJson(content).array();
	for (auto jDonor : jDonors) {
		QJsonObject json = jDonor.toObject();
		DonorExportItem item;
		item.id = json.value("id").toInt();
		item.first = json.value("first").toString();
		item.last = json.value("last").toString();
		item.address = json.value("address").toString();
		item.email = json.value("email").toString();
		item.cellPhone = json.value("cellphone").toString();
		item.homePhone = json.value("homephone").toString();
		item.workPhone = json.value("workphone").toString();
		item.paypalAccount = json.value("paypal").toString();
		list.append(item);
	}
	layoutChanged();
}

int DonorExportNewDataModel::rowCount(const QModelIndex &) const
{
	return list.size();
}

int DonorExportNewDataModel::columnCount(const QModelIndex &) const
{
	return 9;
}

QVariant DonorExportNewDataModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	QVariant text;

	if (role != Qt::DisplayRole)
		return QVariant();

	if (orientation == Qt::Horizontal) {
		switch(section) {  // clang format off
			case 0:  text = QString(tr("ID"));             break;
			case 1:  text = QString(tr("First"));          break;
			case 2:  text = QString(tr("Last"));           break;
			case 3:  text = QString(tr("Address"));        break;
			case 4:  text = QString(tr("Email"));          break;
			case 5:  text = QString(tr("Cell Phone"));     break;
			case 6:  text = QString(tr("Home Phone"));     break;
			case 7:  text = QString(tr("Work Phone"));     break;
			case 8:  text = QString(tr("PayPal"));         break;
		}  // clang format on
	} else {
		text = QVariant(QString());
	}

	return text;
}

QVariant DonorExportNewDataModel::data(const QModelIndex &index, int role) const
{
	QVariant item;
	QString typeString;
	if (role == Qt::DisplayRole) {
		switch(index.column()) {  // clang format off
			case 0: item = list.at(index.row()).id;              break;
			case 1: item = list.at(index.row()).first;           break;
			case 2: item = list.at(index.row()).last;            break;
			case 3: item = list.at(index.row()).address;         break;
			case 4: item = list.at(index.row()).email;           break;
			case 5: item = list.at(index.row()).cellPhone;       break;
			case 6: item = list.at(index.row()).homePhone;       break;
			case 7: item = list.at(index.row()).workPhone;       break;
			case 8: item = list.at(index.row()).paypalAccount;   break;
		}  // clang format on
	}
	return item;
}
