// clang-format off
/**************************************************************************************************
	Copyright (C) 2024  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include "tab-screen.h"

TabScreen::TabScreen()
{
	connect(&unsubTimer, &QTimer::timeout, this, &TabScreen::doUnsubscribe);
}

void TabScreen::selected()
{
	recentlySelected = true;
	if (unsubTimer.isActive())
		unsubTimer.stop();
	else
		subscribe();
}

void TabScreen::unselected()
{
	unsubTimer.start(600'000);  // 10min
}

void TabScreen::doUnsubscribe()
{
	unsubTimer.stop();
	unsubscribe();
	recentlySelected = false;
}
