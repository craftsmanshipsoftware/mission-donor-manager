INCLUDEPATH+= $$PWD

HEADERS+= $$PWD/centred-message-widget.h
SOURCES+= $$PWD/centred-message-widget.cpp

HEADERS+= $$PWD/name-selector-widget.h
SOURCES+= $$PWD/name-selector-widget.cpp

HEADERS+= $$PWD/source-account-selector-widget.h
SOURCES+= $$PWD/source-account-selector-widget.cpp

HEADERS+= $$PWD/year-selector.h
SOURCES+= $$PWD/year-selector.cpp
