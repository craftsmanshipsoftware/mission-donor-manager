// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef MDM_NAME_SELECTOR_WIDGET_H_
#define MDM_NAME_SELECTOR_WIDGET_H_

#include <QWidget>
#include <QListView>
#include <QSortFilterProxyModel>
#include "name-list-dm.h"

class QLineEdit;

class NameSelectorWidget : public QWidget
{
	Q_OBJECT

public:
	NameSelectorWidget();
	void updateNameList(const QJsonArray &jArray);
	void setCurrentIndex(QModelIndex index);
	void clearIndexAndSelection();                                   // changes index to -1
	inline void clearSelection() { nameListView->clearSelection(); } // clears, but does not change index
	void selectEndOfDataModel();
	void clearSearchFilter();
	inline NameListDataModel &model() { return nameListDataModel; }
	int idOfCurrentIndex();
	QModelIndex indexOfId(int id) {return nameSearchFilter->mapFromSource(nameListDataModel.index(id - 1)); }
	void setSearchVisible(bool visible);
	int mapToSourceRow(const QModelIndex &index) { return nameSearchFilter->mapToSource(index).row(); }
	inline void enabledIdShown(bool show) { nameListDataModel.enabledIdShown(show); }
	void enableSearchEdit(bool enabled);

signals:
	void filterModified();
	void nameSelected(int nameId, QModelIndex index);

private:
#if defined(Q_OS_ANDROID) || defined(Q_OS_IOS)
	void nameClicked(const QModelIndex index);
#else
	void nameClicked(const QModelIndex index, const QModelIndex &);
#endif
	void filterChanged();
	QLineEdit *nameSearchEdit;
	QSortFilterProxyModel *nameSearchFilter;
	QListView *nameListView;
	NameListDataModel nameListDataModel;
};

#endif
