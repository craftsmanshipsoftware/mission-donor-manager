// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QVBoxLayout>
#include <QLineEdit>
#include <QListView>
#include <QJsonObject>
#include <QJsonArray>
#include <QSortFilterProxyModel>
#include "styler.h"
#include "theme.h"
#include "name-selector-widget.h"

NameSelectorWidget::NameSelectorWidget()
{
	QVBoxLayout *layout = new QVBoxLayout;
	setLayout(layout);

	nameSearchEdit = new QLineEdit;
	Styler::styleSearchLineEdit(nameSearchEdit);
	layout->addWidget(nameSearchEdit);
	nameListView = new QListView;
	Styler::styleListView(nameListView);
#if defined(Q_OS_ANDROID) || defined(Q_OS_IOS)
	nameListView->setSpacing(Theme::ListLineSpacing);
	Styler::styleTouchScrolling(nameListView);
#endif
	layout->addWidget(nameListView);
	nameSearchFilter = new QSortFilterProxyModel;
	nameSearchFilter->setSourceModel(&nameListDataModel);
	nameSearchFilter->setFilterCaseSensitivity(Qt::CaseInsensitive);
	nameSearchFilter->sort(Qt::AscendingOrder);
	nameListView->setModel(nameSearchFilter);
#if defined(Q_OS_ANDROID) || defined(Q_OS_IOS)
	connect(nameListView, &QListView::clicked, this, &NameSelectorWidget::nameClicked);
#else
	connect(nameListView->selectionModel(), &QItemSelectionModel::currentChanged, this, &NameSelectorWidget::nameClicked);
#endif
	connect(nameSearchEdit, &QLineEdit::textEdited, nameSearchFilter, &QSortFilterProxyModel::setFilterFixedString);
	connect(nameSearchEdit, &QLineEdit::textEdited, this, &NameSelectorWidget::filterChanged);
}

void NameSelectorWidget::updateNameList(const QJsonArray &jArray)
{
	nameListDataModel.layoutAboutToBeChanged();
	nameListDataModel.clear();
	for (auto jName : jArray)
		nameListDataModel.append(jName.toObject());
	nameListDataModel.layoutChanged();
}

#if defined(Q_OS_ANDROID) || defined(Q_OS_IOS)
void NameSelectorWidget::nameClicked(const QModelIndex index)
#else
void NameSelectorWidget::nameClicked(const QModelIndex index, const QModelIndex &)
#endif
{
	if (!index.isValid())
		return;
	int selectedNameId = nameListDataModel.getID(nameSearchFilter->mapToSource(index).row());
	emit nameSelected(selectedNameId, index);
}

void NameSelectorWidget::filterChanged()
{
	clearIndexAndSelection();
	emit filterModified();
}

int NameSelectorWidget::idOfCurrentIndex()
{
	int row = nameSearchFilter->mapToSource(nameListView->currentIndex()).row();
	if (row >= 0)
		return nameListDataModel.getID(row);
	else
		return -1;
}

void NameSelectorWidget::setCurrentIndex(QModelIndex index)
{
	nameListView->setCurrentIndex(index);
}

void NameSelectorWidget::selectEndOfDataModel()
{
	QModelIndex indexOfEndOfDataModel = nameListDataModel.index(nameListDataModel.rowCount() - 1);
	nameListView->setCurrentIndex(nameSearchFilter->mapFromSource(indexOfEndOfDataModel));
}

void NameSelectorWidget::clearIndexAndSelection()
{
	nameListView->setCurrentIndex(nameListDataModel.index(-1));
}

void NameSelectorWidget::clearSearchFilter()
{
	nameSearchEdit->clear();
	nameSearchFilter->setFilterFixedString("");
}

void NameSelectorWidget::setSearchVisible(bool visible)
{
	nameSearchEdit->setVisible(visible);
}

void NameSelectorWidget::enableSearchEdit(bool enabled)
{
	nameSearchEdit->setEnabled(enabled);
}
