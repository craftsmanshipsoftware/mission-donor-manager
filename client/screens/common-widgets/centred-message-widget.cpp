// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QVBoxLayout>
#include <QLabel>
#include "theme.h"
#include "styler.h"
#include "centred-message-widget.h"

CentredMessageWidget::CentredMessageWidget(const QString &msg)
{
	QVBoxLayout *layout = new QVBoxLayout;
	setLayout(layout);
	QLabel *askSelectLabel = new QLabel(msg);
	Styler::styleLabel(askSelectLabel, Theme::H1Size);
	layout->addWidget(askSelectLabel);
	layout->setAlignment(askSelectLabel, Qt::AlignHCenter);
}
