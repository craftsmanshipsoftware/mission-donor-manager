// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QJsonArray>
#include <QJsonObject>
#include "source-account-selector-widget.h"

SourceAccountSelectorWidget::SourceAccountSelectorWidget()
{

}

void SourceAccountSelectorWidget::update(const QJsonArray &jArray, bool unselectAfter)
{
	clear();
	sourceAccountIds.clear();
	for (int i=0; i<jArray.size(); i++) {
		QJsonObject json = jArray.at(i).toObject();
		sourceAccountIds.append(json.value("id").toInt());
		addItem(json.value("number").toString() + " - " + json.value("name").toString());
	}
	if (unselectAfter)
		setCurrentIndex(-1);
}

int SourceAccountSelectorWidget::accountIdAtCurrentIndex()
{
	int sourceAccount = -1;
	int idx = currentIndex();
	if (idx < sourceAccountIds.size() && idx >= 0)
		sourceAccount = sourceAccountIds.at(idx);
	return sourceAccount;
}

void SourceAccountSelectorWidget::setCurrentIndexByAccountId(int accountId)
{
	int idx = -1;
	if (accountId > 0) {
		for (int i=0; i<sourceAccountIds.size(); i++) {
			if (sourceAccountIds.at(i) == accountId) {
				idx = i;
				break;
			}
		}
	}
	setCurrentIndex(idx);
}
