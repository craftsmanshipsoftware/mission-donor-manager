// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QDate>
#include "styler.h"
#include "year-selector.h"

YearSelector::YearSelector()
{
#ifndef Q_OS_MAC
	Styler::styleComboBox(this);
#endif
	populateAvailableYears(QDate::currentDate().year() + 1);
	setCurrentIndex(1);
	connect(this, qOverload<int>(&QComboBox::currentIndexChanged), this, &YearSelector::yearSelectorChanged);
}

void YearSelector::populateAvailableYears(int startYear)
{
	for (uint i=0; i < std::size(availableYears); i++) {
		addItem(QString::number(startYear));
		availableYears[i] = startYear;
		startYear--;
	}
}

void YearSelector::yearSelectorChanged(int index)
{
	emit yearChanged(availableYears[index]);
}

int YearSelector::currentYearSelected()
{
	return availableYears[currentIndex()];
}

