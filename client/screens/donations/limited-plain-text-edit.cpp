// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include "limited-plain-text-edit.h"
#include <QTextBlock>

void LimitedPlainTextEdit::keyPressEvent(QKeyEvent *event)
{
	bool ok = false;
	if (movementKeys.contains(event->key())) {
		ok = true;
	} else if (event->key() == Qt::Key_Return || event->key() == Qt::Key_Enter) {
		if (maximumLineCount == -1 || blockCount() < maximumLineCount)
			ok = true;
	} else if (maximumLineLength == -1 || document()->findBlock(textCursor().position()).length() < maximumLineLength) {
		ok = true;
	}

	if (ok)
		QPlainTextEdit::keyPressEvent(event);
}
