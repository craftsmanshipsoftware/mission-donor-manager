// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QScreen>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QLineEdit>
#include <QPlainTextEdit>
#include <QListView>
#include <QCheckBox>
#include <QRadioButton>
#include <QButtonGroup>
#include <QCalendarWidget>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QSortFilterProxyModel>
#include "../common/common.h"
#include "main-window.h"
#include "connection-manager.h"
#include "styler.h"
#include "theme.h"
#include "limited-plain-text-edit.h"
#include "name-selector-widget.h"
#include "source-account-selector-widget.h"
#include "user-input.h"
#include "donations-screen.h"

DonationsScreen::DonationsScreen(MainWindow &mainWindow) : mainWindow(mainWindow), cm(mainWindow.connectionManager)
{
	QHBoxLayout *layout = new QHBoxLayout;
	setLayout(layout);
	int m = Theme::OuterMargin * qMin(screen()->geometry().width(), screen()->geometry().height());
	layout->setContentsMargins(m, m, m, m);
	layout->addStretch(1);
	QVBoxLayout *donorLayout = new QVBoxLayout;
	layout->addLayout(donorLayout, 7);
	layout->addStretch(1);
	QVBoxLayout *missionLayout = new QVBoxLayout;
	layout->addLayout(missionLayout, 7);
	layout->addStretch(1);
	QVBoxLayout *detailsLayout = new QVBoxLayout;
	layout->addLayout(detailsLayout, 7);
	layout->addStretch(1);

// Donor Selection layout

	QLabel *title = new QLabel(tr("Select Donor"));
	Styler::styleLabel(title, Theme::H1Size);
	donorLayout->addWidget(title);

	donorSelector = new NameSelectorWidget;
	donorSelector->enabledIdShown(true);
	donorLayout->addWidget(donorSelector);

	lastThreeLabel = new QLabel;
	Styler::styleLabel(lastThreeLabel);
	donorLayout->addWidget(lastThreeLabel);

// Mission Selection layout

	title = new QLabel(tr("Select Mission"));
	Styler::styleLabel(title, Theme::H1Size);
	missionLayout->addWidget(title);

	missionSelector = new NameSelectorWidget;
	missionLayout->addWidget(missionSelector);

// Details layout

	title = new QLabel(tr("Donation Details"));
	Styler::styleLabel(title, Theme::H1Size);
	detailsLayout->addWidget(title);

	detailsLayout->addStretch(1);

	title = new QLabel(tr("Type"));
	Styler::styleLabel(title);
	detailsLayout->addWidget(title);
	QHBoxLayout *typeLayout = new QHBoxLayout;
	detailsLayout->addLayout(typeLayout);
	receiptable = new QRadioButton("Normal");
	Styler::styleRadioButton(receiptable);
	typeButtonGroup.addButton(receiptable);
	typeLayout->addWidget(receiptable);
	thirdParty = new QRadioButton("3rd Party");
	Styler::styleRadioButton(thirdParty);
	typeButtonGroup.addButton(thirdParty);
	typeLayout->addWidget(thirdParty);
	inKind = new QRadioButton("InKind");
	Styler::styleRadioButton(inKind);
	typeButtonGroup.addButton(inKind);
	typeLayout->addWidget(inKind);

	disableAdminCut = new QCheckBox(tr("Disable Admin Cut"));
	Styler::styleCheckBox(disableAdminCut);
	detailsLayout->addWidget(disableAdminCut);

	anonymous = new QCheckBox(tr("Anonymus"));
	Styler::styleCheckBox(anonymous);
	detailsLayout->addWidget(anonymous);

	thirdPartySelector = new QComboBox;
	thirdPartySelector->setEnabled(false);
	Styler::styleComboBox(thirdPartySelector);
	detailsLayout->addWidget(thirdPartySelector);

	detailsLayout->addStretch(1);
	QGridLayout *amountLayout = new QGridLayout;
	detailsLayout->addLayout(amountLayout);

	title = new QLabel(tr("Amount"));
	Styler::styleLabel(title);
	amountLayout->addWidget(title, 0, 0);
	amountEntry = new QLineEdit;
	Styler::styleLineEdit(amountEntry);
	amountEntry->setValidator(new AmountValidator());
	amountLayout->addWidget(amountEntry, 1, 0);

	title = new QLabel(tr("Advantage"));
	Styler::styleLabel(title);
	amountLayout->addWidget(title, 0, 1);
	advantageEntry = new QLineEdit;
	Styler::styleLineEdit(advantageEntry);
	advantageEntry->setValidator(new AmountValidator());
	amountLayout->addWidget(advantageEntry, 1, 1);

	title = new QLabel(tr("Fee"));
	Styler::styleLabel(title);
	amountLayout->addWidget(title, 0, 2);
	feeEntry = new QLineEdit;
	Styler::styleLineEdit(feeEntry);
	feeEntry->setValidator(new AmountValidator());
	amountLayout->addWidget(feeEntry, 1, 2);

	detailsLayout->addStretch(1);
	QHBoxLayout *incomeAccountLayout = new QHBoxLayout;
	detailsLayout->addLayout(incomeAccountLayout);
	title = new QLabel(tr("Income Account:"));
	Styler::styleLabel(title);
	incomeAccountLayout->addWidget((title));
	sourceAccountSelector = new SourceAccountSelectorWidget;
	sourceAccountSelector->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Maximum);
	Styler::styleComboBox(sourceAccountSelector);
	incomeAccountLayout->addWidget(sourceAccountSelector);

	detailsLayout->addStretch(1);
	QHBoxLayout *advantageAccountLayout = new QHBoxLayout;
	detailsLayout->addLayout(advantageAccountLayout);
	title = new QLabel(tr("Advantage Account:"));
	Styler::styleLabel(title);
	advantageAccountLayout->addWidget((title));
	advantageAccountSelector = new SourceAccountSelectorWidget;
	advantageAccountSelector->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Maximum);
	Styler::styleComboBox(advantageAccountSelector);
	advantageAccountSelector->setEnabled(false);
	advantageAccountLayout->addWidget(advantageAccountSelector);

	detailsLayout->addStretch(1);
	title = new QLabel(tr("Date of Donation"));
	Styler::styleLabel(title);
	detailsLayout->addWidget(title);
	QCalendarWidget *calendar = new QCalendarWidget;
	detailsLayout->addWidget(calendar);

	detailsLayout->addStretch(1);
	title = new QLabel(tr("Campaign"));
	Styler::styleLabel(title);
	detailsLayout->addWidget((title));
	campaignSelector = new QComboBox;
	Styler::styleComboBox(campaignSelector);
	detailsLayout->addWidget(campaignSelector);

	detailsLayout->addStretch(1);
	inKindLabel = new QLabel(tr("Note (5 lines, 72 line length)"));
	Styler::styleLabel(inKindLabel);
	detailsLayout->addWidget(inKindLabel);
	inKindEdit = new LimitedPlainTextEdit;
	inKindEdit->setMaximumLineLength(72);
	inKindEdit->setMaximumLineCount(5);
	Styler::stylePlainTextEdit(inKindEdit);
	detailsLayout->addWidget(inKindEdit);

	detailsLayout->addStretch(1);

	QHBoxLayout *postButtonLayout = new QHBoxLayout;
	detailsLayout->addLayout(postButtonLayout);

	helperMessage = new QLabel;
	Styler::styleLabel(helperMessage);
	postButtonLayout->addWidget(helperMessage);

	postButtonLayout->addStretch(1);

	postButton = new QPushButton(tr("Post"));
	postButton->setEnabled(false);
	Styler::styleButton(postButton);
	postButton->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
	postButtonLayout->addWidget(postButton);

	connect(donorSelector, &NameSelectorWidget::filterModified, this, &DonationsScreen::deselectDonor);
	connect(donorSelector, &NameSelectorWidget::nameSelected, this, &DonationsScreen::donorSelected);
	connect(missionSelector, &NameSelectorWidget::filterModified, this, &DonationsScreen::deselectMission);
	connect(missionSelector, &NameSelectorWidget::nameSelected, this, &DonationsScreen::missionSelected);
	connect(amountEntry, &QLineEdit::textEdited, this, &DonationsScreen::amountEntered);
	connect(advantageEntry, &QLineEdit::textEdited, this, &DonationsScreen::advantageEntered);
	connect(advantageAccountSelector, qOverload<int>(&QComboBox::currentIndexChanged), this, &DonationsScreen::advantageAccountSelected);
	connect(calendar, &QCalendarWidget::clicked, this, &DonationsScreen::dateSelected);
	connect(&typeButtonGroup, &QButtonGroup::idClicked, this, &DonationsScreen::typeSelected);
	connect(receiptable, &QRadioButton::clicked, this, &DonationsScreen::receiptableClicked);
	connect(thirdParty, &QRadioButton::clicked, this, &DonationsScreen::thirdPartyClicked);
	connect(inKind, &QRadioButton::clicked, this, &DonationsScreen::inkindClicked);
	connect(postButton, &QPushButton::clicked, this, &DonationsScreen::postClicked);

	updatePostButton();
	topicsToReceive << "donations" << "donors.ro.list" << "donors.ro.3rdparties" << "donors.ro.last-three" << "mission.ro" << "income-accounts.list-enabled" << "advantage-accounts.list-enabled" << "campaigns.list-enabled";
	cm.registerApiForReceiver(this);
}

void DonationsScreen::subscribe()
{
	cm.foxmq.subscription("donors.ro.list", true);
	cm.foxmq.subscription("donors.ro.3rdparties", true);
	cm.foxmq.subscription("donors.ro.last-three", true);
	cm.foxmq.subscription("mission.ro.list", true);
	cm.foxmq.subscription("income-accounts.list-enabled", true);
	cm.foxmq.subscription("campaigns.list-enabled", true);
	cm.foxmq.subscription("advantage-accounts.list-enabled", true);
}

void DonationsScreen::unsubscribe()
{
	cm.foxmq.subscription("donors.ro.list", false);
	cm.foxmq.subscription("donors.ro.3rdparties", false);
	cm.foxmq.subscription("donors.ro.last-three", false);
	cm.foxmq.subscription("mission.ro.list", false);
	cm.foxmq.subscription("income-accounts.list-enabled", false);
	cm.foxmq.subscription("campaigns.list-enabled", false);
	cm.foxmq.subscription("advantage-accounts.list-enabled", false);
}

void DonationsScreen::updatePostButton()
{
	bool valid = true;

	auto updateButton = [&]() {
		postButton->setEnabled(valid);
		if (valid)
			helperMessage->clear();
	};

	if (selectedDonorID == -1) {
		helperMessage->setText(tr("Please Choose a Donor"));
		valid = false;
		updateButton();
		return;
	}
	if (selectedMissionID == -1) {
		helperMessage->setText(tr("Please Choose a Mission"));
		valid = false;
		updateButton();
		return;
	}
	if (!typeWasSelected) {
		helperMessage->setText(tr("Please Select a Type"));
		valid = false;
		updateButton();
		return;
	}
	if (amountEntry->text().toDouble() == 0) {
		helperMessage->setText(tr("Please Enter Amount"));
		valid = false;
		updateButton();
		return;
	}
	if (sourceAccountSelector->accountIdAtCurrentIndex() < 1) {
		helperMessage->setText(tr("Please Select an Income Account"));
		valid = false;
		updateButton();
		return;
	}
	if (advantageEntry->text().toDouble() && advantageAccountSelector->accountIdAtCurrentIndex() < 1) {
		helperMessage->setText(tr("Please Select an Advantage Account"));
		valid = false;
		updateButton();
		return;
	}
	if (!dateWasSelected) {
		helperMessage->setText(tr("Please Choose a Date"));
		valid = false;
		updateButton();
		return;
	}
	updateButton();
}

void DonationsScreen::deselectDonor()
{
	selectedDonorID = -1;
	updatePostButton();
}

void DonationsScreen::deselectMission()
{
	selectedMissionID = -1;
	updatePostButton();
}

void DonationsScreen::donorSelected(int donorId, QModelIndex)
{
	selectedDonorID = donorId;
	updatePostButton();
	lastThreeLabel->clear();
	cm.foxmq.post("donors.ro.last-three", FoxHead::ContentType::IntAsString, QByteArray::number(donorId));
}

void DonationsScreen::missionSelected(int missionID, QModelIndex)
{
	selectedMissionID = missionID;
	updatePostButton();
}

void DonationsScreen::dateSelected(QDate date)
{
	dateOfDonation = date;
	dateWasSelected = true;
	updatePostButton();
}

void DonationsScreen::amountEntered()
{
	updatePostButton();
}

void DonationsScreen::advantageEntered()
{
	advantageAccountSelector->setEnabled(!advantageEntry->text().isEmpty());
	updatePostButton();
}

void DonationsScreen::advantageAccountSelected(int index)
{
	if (index > -1)
		updatePostButton();
}

void DonationsScreen::receiptableClicked()
{
	type = DonationType::Normal;
	thirdPartySelector->setEnabled(false);
	inKindLabel->setText(tr("Note (5 lines, 72 line length)"));
}

void DonationsScreen::thirdPartyClicked()
{
	type = DonationType::ThirdParty;
	thirdPartySelector->setEnabled(true);
	inKindLabel->setText(tr("Note (5 lines, 72 line length)"));
}

void DonationsScreen::inkindClicked()
{
	type = DonationType::Inkind;
	thirdPartySelector->setEnabled(false);
	inKindLabel->setText(tr("In-kind details (5 lines, 72 line length)"));
}

void DonationsScreen::typeSelected(int)
{
	typeWasSelected = true;
	updatePostButton();
}

void DonationsScreen::postClicked()
{
	QJsonDocument jDoc;
	QJsonObject json;
	json.insert("donor-id", selectedDonorID);
	json.insert("mission-id", selectedMissionID);
	json.insert("amount-cents", convertStringToCents(amountEntry->text()));
	json.insert("advantage-cents", convertStringToCents(advantageEntry->text()));
	json.insert("fee-cents", convertStringToCents(feeEntry->text()));
	json.insert("anonymous", anonymous->isChecked());
	json.insert("disable-admin-cut", disableAdminCut->isChecked());
	json.insert("source-account", sourceAccountSelector->accountIdAtCurrentIndex());
	json.insert("advantage-account", advantageAccountSelector->accountIdAtCurrentIndex());
	json.insert("campaign", campaignSelector->currentText());
	json.insert("type", type);
	if (type == DonationType::ThirdParty)
		json.insert("3rd-party-id", thirdPartyIdList.at(thirdPartySelector->currentIndex()));
	json.insert("date", dateOfDonation.toJulianDay());
	QString note = inKindEdit->toPlainText();
	if (!note.isEmpty())
		json.insert("in-kind-description", note);
	jDoc.setObject(json);
	cm.foxmq.post("donations.add", FoxHead::ContentType::Json, jDoc.toJson(QJsonDocument::Compact));

	selectedDonorID = -1;
	selectedMissionID = -1;
	donorSelector->clearIndexAndSelection();
	missionSelector->clearIndexAndSelection();
	typeButtonGroup.setExclusive(false);
	typeButtonGroup.checkedButton()->setChecked(false);
	typeButtonGroup.setExclusive(true);
	amountEntry->clear();
	advantageEntry->clear();
	advantageAccountSelector->setEnabled(false);
	feeEntry->clear();
	inKindEdit->clear();
	lastThreeLabel->clear();
	anonymous->setChecked(false);
	disableAdminCut->setChecked(false);
	thirdPartySelector->setEnabled(false);
	dateWasSelected = false;
	typeWasSelected = false;
	campaignSelector->setCurrentIndex(0);
	advantageAccountSelector->setCurrentIndex(-1);
	updatePostButton();
}

void DonationsScreen::processPublish(const QByteArray &topic, ssize_t, FoxID, FoxHead::ContentType, const QByteArray &content)
{
	QJsonArray jArray = QJsonDocument::fromJson(content).array();
	if (topic == "donors.ro.list") {
		donorSelector->updateNameList(jArray);
	} else if (topic == "donors.ro.3rdparties") {
		thirdPartyIdList.clear();
		thirdPartySelector->clear();
		for (int i = 0; i< jArray.size(); i++) {
			QJsonObject json = jArray.at(i).toObject();
			thirdPartyIdList.append(json.value("id").toInt());
			thirdPartySelector->addItem(json.value("forename").toString());
		}
		if (thirdPartySelector->count())
			thirdParty->setEnabled(true);
		else
			thirdParty->setEnabled(false);
	} else if (topic == "mission.ro.list") {
		missionSelector->updateNameList(jArray);
	} else if (topic == "income-accounts.list-enabled") {
		sourceAccountSelector->update(jArray);
	} else if (topic == "advantage-accounts.list-enabled") {
		advantageAccountSelector->update(jArray, true);
		if (advantageAccountSelector->count())
			advantageEntry->setEnabled(true);
		else
			advantageEntry->setEnabled(false);
		updatePostButton();
	} else if (topic == "campaigns.list-enabled") {
		campaignSelector->clear();
		campaignSelector->addItem("");
		for (int i=0; i<jArray.size(); i++) {
			QJsonObject json = jArray.at(i).toObject();
			campaignSelector->addItem(json.value("name").toString());
		}
	}
}

void DonationsScreen::processPostResponce(const QByteArray &topic, FoxID, FoxHead::ContentType, const QByteArray &content)
{
	if (topic == "donors.ro.last-three") {
		lastThreeLabel->setText(content);
	}
}

void DonationsScreen::processNack(const QByteArray &topic)
{
	if (topic == "donations.add") {
		connect(mainWindow.userInput, &UserInput::button1Clicked, this, &DonationsScreen::okFromNack);
		mainWindow.userInput->askForInput(tr("Server responed - Donation entry failed"));
		mainWindow.showUserInput();
	}
}

void DonationsScreen::okFromNack()
{
	disconnect(mainWindow.userInput, &UserInput::button1Clicked, this, &DonationsScreen::okFromNack);
	mainWindow.showMainScreen();
}
