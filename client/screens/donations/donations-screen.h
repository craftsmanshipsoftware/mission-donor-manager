// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef MDM_DONATIONS_SCREEN_H_
#define MDM_DONATIONS_SCREEN_H_

#include <QButtonGroup>
#include "tab-screen.h"
#include "receiver.h"

class QPushButton;
class SlidingStackedWidget;
class DonorInfo;
class LimitedPlainTextEdit;
class MainWindow;
class ConnectionManager;
class QLabel;
class QLineEdit;
class QCheckBox;
class SourceAccountSelectorWidget;
class QAbstractButton;
class QRadioButton;
class QListView;
class QComboBox;
class QSortFilterProxyModel;
class NameSelectorWidget;

class DonationsScreen : public TabScreen, public Receiver
{
	Q_OBJECT

public:
	DonationsScreen(MainWindow &mainWindow);
	void subscribe() override;
	void unsubscribe() override;

private:
	void processPublish(const QByteArray &topic, ssize_t count, FoxID id, FoxHead::ContentType contentType, const QByteArray &content) override;
	void processPostResponce(const QByteArray &topic, FoxID asker, FoxHead::ContentType contentType, const QByteArray &content) override;
	void processNack(const QByteArray &topic) override;
	void updatePostButton();
	void deselectDonor();
	void deselectMission();
	void donorSelected(int donorId, QModelIndex);
	void missionSelected(int missionID, QModelIndex index);
	void dateSelected(QDate date);
	void amountEntered();
	void advantageEntered();
	void advantageAccountSelected(int index);
	void receiptableClicked();
	void thirdPartyClicked();
	void inkindClicked();
	void typeSelected(int id);
	void postClicked();
	void okFromNack();
	MainWindow &mainWindow;
	ConnectionManager &cm;
	QLabel *inKindLabel;
	QLabel *lastThreeLabel;
	QLabel *helperMessage;
	QPushButton *postButton;
	QButtonGroup typeButtonGroup;
	QRadioButton *receiptable;
	QRadioButton *thirdParty;
	QRadioButton *inKind;
	LimitedPlainTextEdit *inKindEdit;
	QLineEdit *amountEntry;
	QLineEdit *advantageEntry;
	QLineEdit *feeEntry;
	QCheckBox *anonymous;
	QCheckBox *disableAdminCut;
	SourceAccountSelectorWidget *sourceAccountSelector;
	SourceAccountSelectorWidget *advantageAccountSelector;
	QComboBox *campaignSelector;
	QComboBox *thirdPartySelector;
	QList<int> thirdPartyIdList;
	NameSelectorWidget *donorSelector;
	int selectedDonorID = -1;
	NameSelectorWidget *missionSelector;
	int selectedMissionID = -1;
	QDate dateOfDonation;
	int type;
	bool dateWasSelected = false;
	bool typeWasSelected = false;
};

#endif
