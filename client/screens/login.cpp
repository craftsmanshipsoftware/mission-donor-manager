// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include <QFile>
#include <QKeyEvent>
#include "qcolorablesvg.h"
#include "qdualboxlayout.h"
#include "theme.h"
#include "styler.h"
#include "about-screen.h"
#include "login.h"

constexpr int TotalEntryLines = 4;
constexpr int MinPasswordLength = 8;

#if defined(Q_OS_ANDROID) || defined(Q_OS_IOS)
LoginScreen::LoginScreen()
#else
LoginScreen::LoginScreen() : upgradeManager("http://mdm.ota.craftsmanshipsoftware.ca/", "main")
#endif
{
	QVBoxLayout *layout = new QVBoxLayout;
	setLayout(layout);
	int m = Theme::OuterMargin * qMin(screen()->geometry().width(), screen()->geometry().height());
	layout->setContentsMargins(m, m, m, m);

	Styler::styleMainWidget(this);

	layout->addStretch(10);
	title = new QLabel;
	Styler::styleLabel(title, Theme::H2Size);
	layout->addWidget(title);
	layout->setAlignment(title, Qt::AlignHCenter);
	layout->addStretch(1);

	QGridLayout *entryLayout = new QGridLayout;
	layout->addLayout(entryLayout);
	layout->setAlignment(entryLayout, Qt::AlignHCenter);

	for (int i=0; i<TotalEntryLines; i++) {
		QLabel *label = new QLabel;
		Styler::styleLabel(label);
		entryLayout->addWidget(label, i, 0);
		entryLayout->setAlignment(label, Qt::AlignRight);
		entryLables.append(label);
		Styler::styleLabel(entryLables.at(i));
		QLineEdit *lineEdit = new QLineEdit;;
		Styler::styleLineEdit(lineEdit);
		entryLayout->addWidget(lineEdit, i, 1);
		entryLayout->setAlignment(lineEdit, Qt::AlignLeft);
		entryLineEdits.append(lineEdit);
	}

	layout->addStretch(1);
	showHidePasswordButton = new QPushButton(tr("Show Password"));
	Styler::styleButton(showHidePasswordButton);
	layout->addWidget(showHidePasswordButton);
	layout->setAlignment(showHidePasswordButton, Qt::AlignHCenter);

	layout->addStretch(2);

	okButton = new QPushButton;
	Styler::styleButton(okButton);
	layout->addWidget(okButton);
	layout->setAlignment(okButton, Qt::AlignHCenter);
	layout->addStretch(4);

	pwdIssue = new QLabel;
	Styler::styleLabel(pwdIssue);
	layout->addWidget(pwdIssue);
	layout->setAlignment(pwdIssue, Qt::AlignHCenter);

	layout->addStretch(10);

	QDualBoxLayout *bottomButtonLayout = new QDualBoxLayout;
	layout->addLayout(bottomButtonLayout);

#if !(defined(Q_OS_ANDROID) || defined(Q_OS_IOS))
	upgradeButton = new QPushButton(tr("Up to date"));
	upgradeButton->setEnabled(false);
	Styler::styleButton(upgradeButton);
	bottomButtonLayout->addWidget(upgradeButton);
#endif
	bottomButtonLayout->addStretch(1);
	forgotButton = new QPushButton(tr("Forgot Password"));
	Styler::styleButton(forgotButton);
	bottomButtonLayout->addWidget(forgotButton);
	bottomButtonLayout->setAlignment(forgotButton, Qt::AlignHCenter);
	changePasswordButton = new QPushButton(tr("Change Password"));
	Styler::styleButton(changePasswordButton);
	bottomButtonLayout->addWidget(changePasswordButton);
	bottomButtonLayout->setAlignment(changePasswordButton, Qt::AlignHCenter);
	urlEditButton = new QPushButton(tr("Change Host"));
	Styler::styleButton(urlEditButton);
	bottomButtonLayout->addWidget(urlEditButton);
	bottomButtonLayout->setAlignment(urlEditButton, Qt::AlignHCenter);
	bottomButtonLayout->addStretch(1);

	aboutButton = new QPushButton(tr("About"));
	Styler::styleButton(aboutButton);
	bottomButtonLayout->addWidget(aboutButton);
	bottomButtonLayout->setAlignment(aboutButton, Qt::AlignHCenter);

	connect(okButton, &QPushButton::clicked, this, &LoginScreen::okPressed);
	connect(forgotButton, &QPushButton::clicked, this, &LoginScreen::showForgotPassword);
	connect(changePasswordButton, &QPushButton::clicked, this, &LoginScreen::showChangePassword);
	connect(urlEditButton, &QPushButton::clicked, this, &LoginScreen::changeUrlClicked);
	connect(entryLineEdits.at(2), &QLineEdit::textEdited, this, &LoginScreen::checkForValidPassword);
	connect(entryLineEdits.at(3), &QLineEdit::textEdited, this, &LoginScreen::checkForValidPassword);
	connect(showHidePasswordButton, &QPushButton::clicked, this, &LoginScreen::showHidePassword);
	connect(aboutButton, &QPushButton::clicked, this, &LoginScreen::aboutClicked);
#if !(defined(Q_OS_ANDROID) || defined(Q_OS_IOS))
	connect(&upgradeManager, &OtaUpdateManager::upgradeStateChanged, this, &LoginScreen::upgradeStateChanged);
	connect(upgradeButton, &QPushButton::clicked, this, &LoginScreen::startUpgrade);
#endif

	QIcon windowIcon(QColorableSvg::color(QString(":/mission.svg"), 256, 256, Theme::IconColor));
	setWindowIcon(windowIcon);
	showMaximized();
}

void LoginScreen::showForms(uint8_t mask, bool passwordForm)
{
	int check = 0x1;
	for (int i=0; i<TotalEntryLines; i++) {
		if (mask & check) {
			entryLables.at(i)->show();
			entryLineEdits.at(i)->show();
		} else {
			entryLables.at(i)->hide();
			entryLineEdits.at(i)->hide();
		}
		if (i > 0) {
			if (passwordForm && showPassword)
				entryLineEdits.at(i)->setEchoMode(QLineEdit::Password);
			else
				entryLineEdits.at(i)->setEchoMode(QLineEdit::Normal);
		}
		check<<= 1;
	}

	if (passwordForm)
		showHidePasswordButton->show();
	else
		showHidePasswordButton->hide();
}

void LoginScreen::showAskForURL(QString currentHost, int currentPort)
{
	state = State::Url;
	title->setText(tr("Enter Host URL and Port"));
	showForms(0x3, false);
	entryLables.at(0)->setText(tr("Host Server URL: "));
	entryLables.at(1)->setText(tr("Port: "));
	okButton->setText(tr("OK"));
	okButton->show();
	okButton->setEnabled(true);
	forgotButton->hide();
	changePasswordButton->hide();
	urlEditButton->hide();
	entryLineEdits.at(0)->setText(currentHost);
	entryLineEdits.at(1)->setText(QString::number(currentPort));
	pwdIssue->hide();
}

void LoginScreen::showNoConnection()
{
	showMessage(State::NotConnected, tr("Unable to connect to Host"), tr("Retry"));
	urlEditButton->show();
}

void LoginScreen::showDisconnected()
{
	showMessage(State::NotConnected, tr("Disconnected from Server"), tr("Reconnect"));
}

void LoginScreen::showAskForPassword()
{
	state = State::Password;
	showLogin(tr("Enter Email and Password"));
}

void LoginScreen::showMessage(LoginScreen::State newState, const QString &msg, const QString &buttonLabel)
{
	state = newState;
	title->setText(msg);
	showForms(0, false);
	okButton->hide();
	forgotButton->hide();
	changePasswordButton->hide();
	urlEditButton->hide();
	pwdIssue->hide();
	if (!buttonLabel.isEmpty()) {
		okButton->setText(buttonLabel);
		okButton->show();
		okButton->setEnabled(true);
	}
}

void LoginScreen::showLoginFailed()
{
	state = State::LoginFailed;
	showLogin(tr("Login Failed, try again"));
}

void LoginScreen::showLogin(const QString &titleString)
{
	title->setText(titleString);
	showForms(0x3);
	entryLineEdits.at(0)->setFocus();
	entryLables.at(0)->setText(tr("Email: "));
	entryLables.at(1)->setText(tr("Password: "));
	okButton->setText(tr("Login"));
	okButton->show();
	okButton->setEnabled(true);
	forgotButton->show();
	changePasswordButton->show();
	urlEditButton->show();
	pwdIssue->hide();
	aboutButton->show();
}

void LoginScreen::setDataBasePassword()
{
	state = State::SetDbPassword;
	showDatabasePassword(tr("Set Database Password"));
}

void LoginScreen::enterDataBasePassword()
{
	state = State::EnterDbPassword;
	showDatabasePassword(tr("Enter Database Password"));
}

void LoginScreen::showDatabasePassword(const QString &titleString)
{
	title->setText(titleString);
	showForms(0x2, true);
	entryLables.at(1)->setText(tr("DataBase Password: "));
	entryLineEdits.at(1)->clear();
	okButton->setText(tr("Start"));
	okButton->show();
	okButton->setEnabled(true);
	forgotButton->hide();
	changePasswordButton->hide();
	urlEditButton->show();
	pwdIssue->hide();
	aboutButton->show();
}

void LoginScreen::showChangePassword()
{
	state = State::ChangePassword;
	title->setText(tr("Change Password"));
	showForms(0xF);
	entryLables.at(0)->setText(tr("Email: "));
	entryLables.at(1)->setText(tr("Current Password: "));
	entryLables.at(2)->setText(tr("New Password: "));
	entryLables.at(3)->setText(tr("New Password Confirm: "));
	okButton->setText(tr("Change"));
	okButton->show();
	okButton->setEnabled(false);
	forgotButton->show();
	changePasswordButton->show();
	urlEditButton->show();
	pwdIssue->show();
	aboutButton->show();
}

void LoginScreen::showForgotPassword()
{
	state = State::ForgotPassword;
	title->setText(tr("Reset Password"));
	showForms(0x1, false);
	entryLables.at(0)->setText(tr("Email: "));
	okButton->setText(tr("Reset"));
	okButton->show();
	okButton->setEnabled(true);
	forgotButton->hide();
	changePasswordButton->show();
	urlEditButton->show();
	pwdIssue->hide();
	aboutButton->hide();
}

void LoginScreen::showEnterCode()
{
	state = State::OneTimeCode;
	title->setText(tr("Check your email, then enter the one time password reset code"));
	showForms(0x1, false);
	entryLables.at(0)->setText(tr("Code: "));
	entryLineEdits.at(0)->clear();
	okButton->setText(tr("Send"));
	okButton->show();
	okButton->setEnabled(true);
	forgotButton->hide();
	changePasswordButton->hide();
	urlEditButton->show();
	pwdIssue->hide();
	aboutButton->hide();
}


void LoginScreen::okPressed()
{
	switch (state) {
		case Url:
			emit urlEntered(entryLineEdits.at(0)->text(), entryLineEdits.at(1)->text().toUInt());
			entryLineEdits.at(0)->clear();
			entryLineEdits.at(1)->clear();
			break;
		case Password:
			emit passwordEntered(entryLineEdits.at(0)->text().trimmed(), entryLineEdits.at(1)->text().trimmed());
			showLoggingIn();
			break;
		case Connecting:
		case NotConnected:
			emit tryConnectionAgain();
			break;
	    case SetDbPassword:
	        emit databasePasswordSet(entryLineEdits.at(1)->text());
	        entryLineEdits.at(1)->clear();
	        break;
	    case EnterDbPassword:
	        emit databasePasswordEntered(entryLineEdits.at(1)->text());
	        entryLineEdits.at(1)->clear();
	        break;
		case Login:
		case LoginFailed:
			emit passwordEntered(entryLineEdits.at(0)->text(), entryLineEdits.at(1)->text());
			break;
		case ChangePassword:
			emit changePassword(entryLineEdits.at(0)->text(), entryLineEdits.at(1)->text(), entryLineEdits.at(2)->text());
			break;
		case ForgotPassword:
			emit forgotPassword(entryLineEdits.at(0)->text());
			showEnterCode();
			break;
		case OneTimeCode:
			emit oneTimeCode(entryLineEdits.at(0)->text());
			break;
		case OneTimeCodeSucceeded:
			entryLineEdits.at(0)->clear();
			showAskForPassword();
			break;
		case OneTimeCodeFailed:
			entryLineEdits.at(0)->clear();
			showForgotPassword();
			break;
		case Rebooting: break;
	}
}

void LoginScreen::checkForValidPassword()
{
	bool valid = true;
	if (entryLineEdits.at(2)->text().size() < MinPasswordLength) {
		pwdIssue->setText(tr("Password must be 8 or more characters"));
		valid = false;
	} else if (entryLineEdits.at(2)->text() != entryLineEdits.at(3)->text()) {
		pwdIssue->setText(tr("Password confirm does not match"));
		valid = false;
	} else {
		pwdIssue->clear();
	}
	okButton->setEnabled(valid);
}

void LoginScreen::showHidePassword()
{
	if (showPassword)	{
		showPassword = false;
		entryLineEdits.at(1)->setEchoMode(QLineEdit::Normal);
		entryLineEdits.at(2)->setEchoMode(QLineEdit::Normal);
		entryLineEdits.at(3)->setEchoMode(QLineEdit::Normal);
		showHidePasswordButton->setText(tr("Hide Password"));
	} else {
		showPassword = true;
		entryLineEdits.at(1)->setEchoMode(QLineEdit::Password);
		entryLineEdits.at(2)->setEchoMode(QLineEdit::Password);
		entryLineEdits.at(3)->setEchoMode(QLineEdit::Password);
		showHidePasswordButton->setText(tr("Show Password"));
	}
}

void LoginScreen::keyPressEvent(QKeyEvent *e)
{
	if (e->type() == QKeyEvent::KeyPress) {
		if ((e->key() == Qt::Key_Return || e->key() == Qt::Key_Enter) && (state == Password || state == LoginFailed))
			emit passwordEntered(entryLineEdits.at(0)->text(), entryLineEdits.at(1)->text());
	}
}

#if !(defined(Q_OS_ANDROID) || defined(Q_OS_IOS))
void LoginScreen::upgradeStateChanged(OtaUpdateManager::State state)
{
	switch (state) {  // clang-format off
		case OtaUpdateManager::State::NoUpdate:
			upgradeButton->setText(tr("Up to date"));
			upgradeButton->setEnabled(false);
			break;
		case OtaUpdateManager::State::Downloading:
			upgradeButton->setText(tr("Downloading"));
			upgradeButton->setEnabled(false);
			break;
		case OtaUpdateManager::State::UpdateReady:
			upgradeButton->setText(tr("Start Update"));
			upgradeButton->setEnabled(true);
			break;
		case OtaUpdateManager::State::Updating:
			break;
	}  // clang-format on
}

void LoginScreen::startUpgrade()
{
	upgradeManager.startUpdate();
	upgradeButton->hide();
	showMessage(State::Rebooting, tr("Upgrade Complete, Restarting"), "");
}
#endif

void LoginScreen::aboutClicked()
{
	AboutScreen *about = new AboutScreen;
	about->showMaximized();
}
