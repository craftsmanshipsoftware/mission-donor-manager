// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QHBoxLayout>
#include <QJsonObject>
#include <QRadioButton>
#include "styler.h"
#include "name-selector-widget.h"
#include "thankyou-template-edit-widget.h"
#include "thankyou-screen.h"
#include "thankyou-missions-widget.h"

ThankyouMissionsWidget::ThankyouMissionsWidget(ThankyouScreen &thankyouScreen) : thankyouScreen(thankyouScreen)
{
	QHBoxLayout *layout = new QHBoxLayout;
	setLayout(layout);

	layout->addStretch(2);

	QVBoxLayout *leftLayout = new QVBoxLayout;
	layout->addLayout(leftLayout);
	layout->setStretchFactor(leftLayout, 4);

	// add mission selector
	missionSelector = new NameSelectorWidget;
	leftLayout->addWidget(missionSelector);

	useCommonThankyou = new QRadioButton(tr("Use Common Thank you"));
	Styler::styleRadioButton(useCommonThankyou);
	leftLayout->addWidget(useCommonThankyou);
	useCustomThankyou = new QRadioButton(tr("Use Custom Thank you"));
	Styler::styleRadioButton(useCustomThankyou);
	leftLayout->addWidget(useCustomThankyou);

	layout->addStretch(2);

	QVBoxLayout *rightLayout = new QVBoxLayout;
	layout->addLayout(rightLayout);
	layout->setStretchFactor(rightLayout, 12);

	emailTemplate = new ThankyouTemplateEditWidget(thankyouScreen, tr("Email Template:"), ThankyouTemplateEditWidget::Email, true);
	rightLayout->addWidget(emailTemplate);
	mailTemplate = new ThankyouTemplateEditWidget(thankyouScreen, tr("Mail Template:"), ThankyouTemplateEditWidget::Mail, true);
	rightLayout->addWidget(mailTemplate);

	layout->addStretch(1);

	setEnabled(false);

	connect(missionSelector, &NameSelectorWidget::nameSelected, &thankyouScreen, &ThankyouScreen::missionSelected);
	connect(missionSelector, &NameSelectorWidget::filterModified, &thankyouScreen, &ThankyouScreen::missionDeselected);
	connect(useCustomThankyou, &QRadioButton::clicked, this, &ThankyouMissionsWidget::customButtonsClicked);
	connect(useCommonThankyou, &QRadioButton::clicked, this, &ThankyouMissionsWidget::customButtonsClicked);
}

void ThankyouMissionsWidget::setEnabled(bool enabled)
{
	missionSelector->enableSearchEdit(!enabled);
	emailTemplate->setEnabled(enabled);
	mailTemplate->setEnabled(enabled);
	useCustomThankyou->setEnabled(enabled);
	useCommonThankyou->setEnabled(enabled);
}

void ThankyouMissionsWidget::customButtonsClicked()
{
	customButtonsModified = true;
	thankyouScreen.enableSaveButton();
}

bool ThankyouMissionsWidget::isModified()
{
	return emailTemplate->isModified || mailTemplate->isModified || customButtonsModified;
}

void ThankyouMissionsWidget::clearModified()
{
	emailTemplate->isModified = false;
	mailTemplate->isModified = false;
	customButtonsModified = false;
}

void ThankyouMissionsWidget::updateForms(const QJsonObject &json)
{
	emailTemplate->setText(json.value("email").toString());
	mailTemplate->setText(json.value("mail").toString());
	if (json.value("use-custom").toBool())
		useCustomThankyou->setChecked(true);
	else
		useCommonThankyou->setChecked(true);
}

void ThankyouMissionsWidget::updateFormsFromCommonCopy(const QJsonObject &json)
{
	if (emailTemplate->askedForCopy) {
		emailTemplate->setText(json.value("email").toString());
		emailTemplate->askedForCopy = false;
	} else if (mailTemplate->askedForCopy) {
		mailTemplate->setText(json.value("mail").toString());
		mailTemplate->askedForCopy = false;
	}
}

QJsonObject ThankyouMissionsWidget::buildJson()
{
	QJsonObject json;
	json.insert("email", emailTemplate->text());
	json.insert("mail", mailTemplate->text());
	json.insert("use-custom", useCustomThankyou->isChecked());
	return json;
}
