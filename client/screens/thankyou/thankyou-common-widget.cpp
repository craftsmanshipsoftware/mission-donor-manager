// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QCheckBox>
#include <QJsonObject>
#include "theme.h"
#include "styler.h"
#include "thankyou-screen.h"
#include "thankyou-template-edit-widget.h"
#include "thankyou-common-widget.h"

ThankyouCommonWidget::ThankyouCommonWidget(ThankyouScreen &thankyouScreen) : thankyouScreen(thankyouScreen)
{
	QHBoxLayout *layout = new QHBoxLayout;;
	setLayout(layout);

	layout->addStretch(2);

	QVBoxLayout *leftLayout = new QVBoxLayout;
	layout->addLayout(leftLayout);
	layout->setStretchFactor(leftLayout, 4);

	leftLayout->addStretch(11);
	mailWaitingLabel = new QLabel;
	Styler::styleLabel(mailWaitingLabel, Theme::H2Size);
	leftLayout->addWidget(mailWaitingLabel);
	leftLayout->setAlignment(mailWaitingLabel, Qt::AlignHCenter);
	//updateMailWaitingCount(0);

	leftLayout->addStretch(1);
	downloadButton = new QPushButton(tr("Download Mailing"));
	Styler::styleButton(downloadButton);
	leftLayout->addWidget(downloadButton);
	leftLayout->setAlignment(downloadButton, Qt::AlignHCenter);

	leftLayout->addStretch(1);
	clearWaitingButton = new QPushButton(tr("Clear Waiting"));
	Styler::styleButton(clearWaitingButton);
	leftLayout->addWidget(clearWaitingButton);
	leftLayout->setAlignment(clearWaitingButton, Qt::AlignHCenter);
	leftLayout->addStretch(11);

	emailEnabledCheckBox = new QCheckBox(tr("Email Thankyou Enabled"));
	Styler::styleCheckBox(emailEnabledCheckBox);
	leftLayout->addWidget(emailEnabledCheckBox);
	leftLayout->setAlignment(emailEnabledCheckBox, Qt::AlignHCenter);
	mailEnabledCheckBox = new QCheckBox(tr("Mail Thankyou Enabled"));
	Styler::styleCheckBox(mailEnabledCheckBox);
	leftLayout->addWidget(mailEnabledCheckBox);
	leftLayout->setAlignment(mailEnabledCheckBox, Qt::AlignHCenter);

	layout->addStretch(2);

	QVBoxLayout *rightLayout = new QVBoxLayout;
	layout->addLayout(rightLayout);
	layout->setStretchFactor(rightLayout, 12);

	emailTemplate = new ThankyouTemplateEditWidget(thankyouScreen, tr("Email Template:"), ThankyouTemplateEditWidget::Email, false);
	rightLayout->addWidget(emailTemplate);
	mailTemplate = new ThankyouTemplateEditWidget(thankyouScreen, tr("Mail Template:"), ThankyouTemplateEditWidget::Mail, false);
	rightLayout->addWidget(mailTemplate);

	layout->addStretch(1);

	setEnabled(false);

	connect(downloadButton, &QPushButton::clicked, &thankyouScreen, &ThankyouScreen::downloadClicked);
	connect(clearWaitingButton, &QPushButton::clicked, &thankyouScreen, &ThankyouScreen::clearClicked);
	connect(emailEnabledCheckBox, &QCheckBox::clicked, this, &ThankyouCommonWidget::enabledChanged);
	connect(mailEnabledCheckBox, &QCheckBox::clicked, this, &ThankyouCommonWidget::enabledChanged);
}

void ThankyouCommonWidget::setEnabled(bool enabled)
{
	emailTemplate->setEnabled(enabled);
	mailTemplate->setEnabled(enabled);
	emailEnabledCheckBox->setEnabled(enabled);
	mailEnabledCheckBox->setEnabled(enabled);
}

bool ThankyouCommonWidget::isModified()
{
	return emailTemplate->isModified || mailTemplate->isModified;
}

void ThankyouCommonWidget::clearModified()
{
	emailTemplate->isModified = false;
	mailTemplate->isModified = false;
	enabledIsModified = false;
}

void ThankyouCommonWidget::updateForms(const QJsonObject &json)
{
	emailTemplate->setText(json.value("email").toString());
	mailTemplate->setText(json.value("mail").toString());
}

void ThankyouCommonWidget::updateEnabled(const QJsonObject &json)
{
	emailEnabledCheckBox->setChecked(json.value("email-enabled").toBool());
	mailEnabledCheckBox->setChecked(json.value("mail-enabled").toBool());
}

QJsonObject ThankyouCommonWidget::buildJson()
{
	QJsonObject json;
	json.insert("email", emailTemplate->text());
	json.insert("mail", mailTemplate->text());
	return json;
}

QJsonObject ThankyouCommonWidget::buildEnabledJson()
{
	QJsonObject json;
	json.insert("email-enabled", emailEnabledCheckBox->isChecked());
	json.insert("mail-enabled", mailEnabledCheckBox->isChecked());
	return json;
}

void ThankyouCommonWidget::updateMailWaitingCount(int count)
{
	mailWaitingLabel->setText(mailWaitingMsg.arg(count));
}

void ThankyouCommonWidget::enabledChanged()
{
	enabledIsModified = true;
	thankyouScreen.enableSaveButton();
}
