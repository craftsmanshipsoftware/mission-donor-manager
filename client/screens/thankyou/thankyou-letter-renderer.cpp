// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QPainter>
#include <QFontDatabase>
#include <QJsonObject>
#include <QJsonArray>
#include <QPrinter>
#include "../common/common.h"
#include "thankyou-letter-renderer.h"

constexpr QSize pageSize = QSize(2400, 3425);
constexpr int BannerIndent = 865;
constexpr int NameYPos = 190;
constexpr int AddressYPos = 280;
constexpr int PrintDateYPos = 625;
constexpr int MailingYPos = 785;
constexpr int StartSectionYPos = 950;

void LetterRenderer::renderLetterList(const QString &fileName, const QJsonObject json)
{
	QPrinter printer;
	printer.setOutputFormat(QPrinter::PdfFormat);
	printer.setResolution(300);
	printer.setOutputFileName(fileName);

	name = json.value("name").toString();
	phone = json.value("phone").toString();
	email = json.value("email").toString();
	addressLine1 = json.value("address-line-1").toString();
	addressLine2 = json.value("address-line-2").toString();
	addressLine3 = json.value("address-line-3").toString();
	banner = QImage::fromData(QByteArray::fromBase64(json.value("banner").toString().toUtf8()));

#if defined(Q_OS_WIN) || defined(Q_OS_MAC)
	fdb.addApplicationFont(":Roboto-Bold.ttf");
	fdb.addApplicationFont(":Roboto-Regular.ttf");
#endif

	QJsonArray jLetters = json.value("letters").toArray();
	int pages = jLetters.size();

	painter.begin(&printer);

	for (int i = 0; i < jLetters.size(); i++) {
		QJsonObject jLetter = jLetters.at(i).toObject();
		addressSalutation = jLetter.value("address-salutation").toString();
		donorAddressLine1 = jLetter.value("address-l1").toString();
		donorApt = jLetter.value("apt").toString();
		donorCity = jLetter.value("city").toString();
		donorProvince = jLetter.value("province").toString();
		donorPostalCode = jLetter.value("postalcode").toString();
		donorCountry = jLetter.value("country").toString();
		body =  jLetter.value("body").toString();
		missionName = jLetter.value("mission-name").toString();
		amount = jLetter.value("amount").toInt();
		date = QDate::fromJulianDay(jLetter.value("date").toInt());
		renderPage();
		pages--;
		if (pages)
			printer.newPage();
	}
	painter.end();
}

void LetterRenderer::renderPage()
{
// Header
	painter.drawImage(BannerIndent, NameYPos, banner);

	painter.setFont(fdb.font("RobotoBold", "", 10));
	painter.drawText(0, NameYPos, name);

	constexpr int advance = 80;
	int advancePos = 0;
	painter.setFont(fdb.font("RobotoRegular", "", 9));
	if (!addressLine1.isEmpty()) {
		painter.drawText(0, AddressYPos, addressLine1);
		advancePos+= advance;
	}
	if (!addressLine2.isEmpty()) {
		painter.drawText(0, AddressYPos + advancePos, addressLine2);
		advancePos+= advance;
	}
	if (!addressLine3.isEmpty()) {
		painter.drawText(0, AddressYPos + advancePos, addressLine3);
		advancePos+= advance;
	}
	if (!phone.isEmpty() || !email.isEmpty()) {
		QString str = phone;
		if (!phone.isEmpty() && !email.isEmpty())
			str+= " / ";
		str+= email;
		painter.drawText(0, AddressYPos + advancePos, str);
	}

	painter.drawText(0, PrintDateYPos, QDate::currentDate().toString("MMMM d, yyyy"));

// Envelop address box

	auto drawAddress = [&](int yPos) {
		int lineOffset = 0;
		painter.setFont(fdb.font("RobotoRegular", "", 9));
		painter.drawText(0, yPos + lineOffset, addressSalutation);
		lineOffset+= 50;
		painter.drawText(0, yPos + lineOffset, donorAddressLine1 + " " + donorApt);
		lineOffset+= 50;
		painter.drawText(0, yPos + lineOffset, donorCity + ' ' + donorProvince + ' ' + donorPostalCode);
	};
	drawAddress(MailingYPos);

// Body

	int sectionPosition = StartSectionYPos;

	auto drawSlip = [&]() {
		constexpr int TitleMsgYPos = 15;
		constexpr int PreviousMsgYPos = 195;
		constexpr int GiftYPos= 275;
		constexpr int AddressYPos = 475;
		constexpr int Box1YPos = GiftYPos;
		constexpr int Box2YPos = Box1YPos + 50;
		constexpr int Line1YPos = GiftYPos + 150;
		constexpr int EnclosedMsgYPos = 500;
		constexpr int Line2YPos = EnclosedMsgYPos + 100;
		constexpr int RightIndent = 1100;

		QPen pen = painter.pen();
		pen.setWidth(3);
		painter.setPen(pen);
		painter.drawRect(0, sectionPosition, pageSize.width(), 75);

		painter.drawRect(RightIndent, sectionPosition + Box1YPos, 30, 30);
		painter.drawRect(RightIndent, sectionPosition + Box2YPos, 30, 30);

		painter.drawLine(RightIndent, sectionPosition + Line1YPos, 1800, sectionPosition + Line1YPos);
		painter.drawLine(RightIndent + 50, sectionPosition + Line2YPos, 1800, sectionPosition + Line2YPos);

		painter.setFont(fdb.font("RobotoBold", "", 10));
		painter.drawText(QRect(0, sectionPosition + TitleMsgYPos, pageSize.width(), 50), Qt::AlignHCenter, "Please Send this Portion with your Next Gift");

		painter.setFont(fdb.font("RobotoBold", "", 8));
		painter.drawText(0, sectionPosition + PreviousMsgYPos, "Previous Gift");
		painter.drawText(RightIndent, sectionPosition + PreviousMsgYPos, "Gift Preference");
		painter.drawText(RightIndent, sectionPosition + EnclosedMsgYPos, "Amount Enclosed:");

		painter.setFont(fdb.font("RobotoRegular", "", 9));
		painter.drawText(0, sectionPosition + GiftYPos, convertCentsToString(amount, true));
		painter.drawText(0, sectionPosition + GiftYPos + 50, date.toString("MMMM d, yyyy"));
		painter.drawText(0, sectionPosition + GiftYPos + 100, missionName);

		drawAddress(sectionPosition + AddressYPos);

		painter.drawText(RightIndent + 50, sectionPosition + Box1YPos + 30, "The same as before");
		painter.drawText(RightIndent + 50, sectionPosition + Box2YPos + 30, "New designation");
		painter.drawText(RightIndent, sectionPosition + Line2YPos, "$");
	};

	auto drawSection = [&](const QString &section) {
		QRect bounding;
		QRect available = QRect(0, sectionPosition, pageSize.width(), 1500);
		painter.drawText(available, Qt::TextWordWrap, section, &bounding);
		sectionPosition+= bounding.height();
	};

	QStringList sectionss = body.split("[[");

	for (int i = 0; i < sectionss.size(); i++) {
		QString section = sectionss.at(i);
		int stop = section.indexOf("]]");
		if (stop > -1) {
			QString var = section.mid(0, stop);
			section.remove(0, stop + 2);
			if (var.startsWith("bold")) {
				painter.setFont(fdb.font("RobotoBold", "", var.mid(4, var.size()).toInt()));
				drawSection(section);
			} else if (var.startsWith("font")) {
				painter.setFont(fdb.font("RobotoRegular", "", var.mid(4, var.size()).toInt()));
				drawSection(section);
			} else if (var.startsWith("slip")) {
				drawSlip();
			}
		} else {
			drawSection(section);
		}

	}
}
