// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QLabel>
#include <QJsonDocument>
#include <QJsonArray>
#include <QFileDialog>
#include "main-window.h"
#include "connection-manager.h"
#include "user-input.h"
#include "styler.h"
#include "theme.h"
#include "qexclusivebuttons.h"
#include "qslidingstackedwidget.h"
#include "name-selector-widget.h"
#include "thankyou-common-widget.h"
#include "thankyou-missions-widget.h"
#include "thankyou-letter-renderer.h"
#include "thankyou-screen.h"

enum Content {
	Common,
	Missions,
};

ThankyouScreen::ThankyouScreen(MainWindow &mainWindow) : mainWindow(mainWindow), cm(mainWindow.connectionManager)
{
	QVBoxLayout *layout = new QVBoxLayout;
	setLayout(layout);
	int m = Theme::OuterMargin * qMin(screen()->geometry().width(), screen()->geometry().height());
	layout->setContentsMargins(m, m, m, m);

// Top tab selction buttons
	QHBoxLayout *topLayout = new QHBoxLayout;
	layout->addLayout(topLayout);

	topLayout->addStretch(1);
	QLabel *title = new QLabel(tr("Thank you Letters"));
	Styler::styleLabel(title, Theme::H1Size);
	topLayout->addWidget(title);
	topLayout->addStretch(1);
	selectorButtons = new QExclusiveButtons({tr("Common"), tr("Mission")});
	topLayout->addWidget(selectorButtons);
	for ( int i=0; i < selectorButtons->size(); i++)
		Styler::styleButton(selectorButtons->buttonAt(i));
	selectorButtons->setChecked(Content::Common, true);

// content tab widgets
	widgetStack = new QSlidingStackedWidget();
	widgetStack->setDuration(500);
	layout->addWidget(widgetStack);

	common = new ThankyouCommonWidget(*this);
	widgetStack->addWidget(common);
	missions = new ThankyouMissionsWidget(*this);
	widgetStack->addWidget(missions);
	widgetStack->setCurrentIndex(Content::Common);

// bottom edit buttons
	QHBoxLayout *editButtonLayout = new QHBoxLayout;
	layout->addLayout(editButtonLayout);

	editButtonLayout->addStretch(1);
	editEnableButton = new QPushButton(tr("Edit"));
	Styler::styleButton(editEnableButton);
	editButtonLayout->addWidget(editEnableButton);
	cancelButton = new QPushButton(tr("Cancel"));
	Styler::styleButton(cancelButton);
	cancelButton->setEnabled(false);
	editButtonLayout->addWidget(cancelButton);
	saveButton = new QPushButton(tr("Save"));
	Styler::styleButton(saveButton);
	saveButton->setEnabled(false);
	editButtonLayout->addWidget(saveButton);
	editButtonLayout->addStretch(1);

	connect(selectorButtons, &QExclusiveButtons::clicked, this, &ThankyouScreen::selectorButtonClicked);
	connect(editEnableButton, &QPushButton::clicked, this, &ThankyouScreen::editEnabledClicked);
	connect(cancelButton, &QPushButton::clicked, this, &ThankyouScreen::checkForSave);
	connect(saveButton, &QPushButton::clicked, this, &ThankyouScreen::saveClicked);

	topicsToReceive << "thankyou" << "mission.ro.list" << "mission.ro.updated";
	cm.registerApiForReceiver(this);
}

void ThankyouScreen::subscribe()
{
	cm.foxmq.subscription("thankyou", true);
	cm.foxmq.subscription("mission.ro", true);
	if (widgetStack->currentIndex() == Content::Common && !common->isModified()) {
		sendRequestForCommonThankyouTemplates();
		sendRequestForEnabled();
	} else if (widgetStack->currentIndex() == Content::Missions && !missions->isModified()) {
		sendRequestForMissionThankyouTemplates();
	}
}

void ThankyouScreen::unsubscribe()
{
	cm.foxmq.subscription("thankyou", false);
	cm.foxmq.subscription("mission.ro", false);
}

void ThankyouScreen::processPublish(const QByteArray &topic, ssize_t, FoxID, FoxHead::ContentType, const QByteArray &content)
{
	if (topic == "thankyou.mail-waiting-count") {
		common->updateMailWaitingCount(content.toInt());
	} else if (topic == "thankyou.updated") {
		if (widgetStack->currentIndex() == Content::Common) {
			sendRequestForCommonThankyouTemplates();
			sendRequestForEnabled();
		}
	} else if (topic == "mission.ro.updated") {
		if (content.toInt() == missionUnderEdit)
			sendRequestForMissionThankyouTemplates();
	} else if (topic == "mission.ro.list") {
		missions->missionSelector->updateNameList(QJsonDocument::fromJson(content).array());
		indexOfMissionUnderEdit = missions->missionSelector->indexOfId(missionUnderEdit);  // QModelIndex indexOfMissionUnderEdit is invalid from the updated list.  Fix using updated list
	}
}

void ThankyouScreen::processPostResponce(const QByteArray &topic, FoxID, FoxHead::ContentType, const QByteArray &content)
{
	if (topic == "thankyou.get-common-templates") {
		common->updateForms(QJsonDocument::fromJson(content).object());
	} else if (topic == "thankyou.get-mission-templates") {
		missions->updateForms(QJsonDocument::fromJson(content).object());
	} else if (topic == "thankyou.copy-common") {
		missions->updateFormsFromCommonCopy(QJsonDocument::fromJson(content).object());
	} else if (topic == "thankyou.download") {
		LetterRenderer letterRenderer;
		letterRenderer.renderLetterList(saveFileName, QJsonDocument::fromJson(content).object());
	} else if (topic == "thankyou.get-enabled") {
		common->updateEnabled(QJsonDocument::fromJson(content).object());
	}
}

void ThankyouScreen::processAck(const QByteArray &topic)
{
	if (topic == "thankyou.lock") {
		editEnableButton->setEnabled(false);
		cancelButton->setEnabled(true);
		common->setEnabled(true);
		missions->setEnabled(true);
	}
}

void ThankyouScreen::processNack(const QByteArray &topic)
{
	if (topic == "thankyou.lock") {
		connect(mainWindow.userInput, &UserInput::button1Clicked, this, &ThankyouScreen::okFromLockNack);
		mainWindow.userInput->askForInput(tr("Another user is editing thank you letters, try again later"));
		mainWindow.showUserInput();
	}
}

void ThankyouScreen::okFromLockNack()
{
	disconnect(mainWindow.userInput, &UserInput::button1Clicked, this, &ThankyouScreen::okFromLockNack);
	mainWindow.showMainScreen();
}

void ThankyouScreen::sendLock()
{
	cm.foxmq.post("thankyou.lock", FoxHead::ContentType::Empty, "");
}

void ThankyouScreen::sendUnlock()
{
	cm.foxmq.post("thankyou.unlock", FoxHead::ContentType::Empty, "");
}

void ThankyouScreen::sendRequestForCommonThankyouTemplates()
{
	cm.foxmq.post("thankyou.get-common-templates", FoxHead::ContentType::Empty, "");
}

void ThankyouScreen::sendRequestForMissionThankyouTemplates()
{
	cm.foxmq.post("thankyou.get-mission-templates", FoxHead::ContentType::IntAsString, QByteArray::number(missionUnderEdit));
}

void ThankyouScreen::sendRequestForEnabled()
{
	cm.foxmq.post("thankyou.get-enabled", FoxHead::ContentType::Empty, "");
}

void ThankyouScreen::selectorButtonClicked(int i)
{
	if (!checkForSave()) {
		widgetStack->setCurrentIndex(i);
		if (i == Content::Common || missionUnderEdit > -1)
			editEnableButton->setEnabled(true);
		else
			editEnableButton->setEnabled(false);
	} else {
		selectorButtons->setChecked(widgetStack->currentIndex(), true);
	}
}

void ThankyouScreen::missionDeselected()  // this can't execute after edit button is clicked as the Misison selector's search filter line edit gets disabled
{
	missionUnderEdit = -1;
	indexOfMissionUnderEdit = missions->missionSelector->indexOfId(-1);
	editEnableButton->setEnabled(false);
}

void ThankyouScreen::editEnabledClicked()
{
	sendLock();
}

void ThankyouScreen::saveClicked()
{
	if (common->isModified()) {
		QJsonDocument jdoc;
		jdoc.setObject(common->buildJson());
		cm.foxmq.post("thankyou.save-common-templates", FoxHead::ContentType::Json, jdoc.toJson(QJsonDocument::Compact));
	}
	if (common->isEnabledModified()) {
		QJsonDocument jdoc;
		jdoc.setObject(common->buildEnabledJson());
		cm.foxmq.post("thankyou.set-enabled", FoxHead::ContentType::Json, jdoc.toJson(QJsonDocument::Compact));
	}
	if (missions->isModified()) {
		QJsonDocument jdoc;
		QJsonObject json = missions->buildJson();
		json.insert("id", missionUnderEdit);
		jdoc.setObject(json);
		cm.foxmq.post("thankyou.save-mission-templates", FoxHead::ContentType::Json, jdoc.toJson(QJsonDocument::Compact));
	}
	doneEdits();
}

bool ThankyouScreen::checkForSave()
{
	bool changed = false;
	if (common->isModified() || missions->isModified() || common->isEnabledModified()) {
		changed = true;
		connect(mainWindow.userInput, &UserInput::button1Clicked, this, &ThankyouScreen::saveConfirmDiscardClicked);
		connect(mainWindow.userInput, &UserInput::button2Clicked, this, &ThankyouScreen::saveConfirmCleanUp);
		connect(mainWindow.userInput, &UserInput::button3Clicked, this, &ThankyouScreen::saveConfirmSaveClicked);
		mainWindow.userInput->askForInput(tr("Changes have been made"), tr("Discard Changes"), tr("Continue Editing"), tr("      Save      "));
		mainWindow.showUserInput();
	} else if (cancelButton->isEnabled()) {
		doneEdits();  // editing has been enabled, but no changes, so unlock
	}
	return changed;
}

void ThankyouScreen::saveConfirmSaveClicked()
{
	saveClicked();
	saveConfirmCleanUp();
}

void ThankyouScreen::saveConfirmDiscardClicked()
{
	if (common->isModified())
		sendRequestForCommonThankyouTemplates();
	if (common->isEnabledModified())
		sendRequestForEnabled();
	if (missions->isModified())
		sendRequestForMissionThankyouTemplates();
	saveConfirmCleanUp();
	doneEdits();
}

void ThankyouScreen::saveConfirmCleanUp()
{
	disconnect(mainWindow.userInput, &UserInput::button1Clicked, this, &ThankyouScreen::saveConfirmDiscardClicked);
	disconnect(mainWindow.userInput, &UserInput::button2Clicked, this, &ThankyouScreen::saveConfirmCleanUp);
	disconnect(mainWindow.userInput, &UserInput::button3Clicked, this, &ThankyouScreen::saveConfirmSaveClicked);
	mainWindow.showMainScreen();
	missions->missionSelector->setCurrentIndex(indexOfMissionUnderEdit);
}

void ThankyouScreen::doneEdits()
{
	editEnableButton->setEnabled(true);
	cancelButton->setEnabled(false);
	saveButton->setEnabled(false);
	common->setEnabled(false);
	common->clearModified();
	missions->setEnabled(false);
	missions->clearModified();
	sendUnlock();
}

void ThankyouScreen::enableSaveButton()
{
	saveButton->setEnabled(true);
}

void ThankyouScreen::missionSelected(int missionID, QModelIndex index)
{
	if (missionID != missionUnderEdit) {
		if (!checkForSave()) {
			indexOfMissionUnderEdit = index;
			missionUnderEdit = missionID;
			editEnableButton->setEnabled(true);
			sendRequestForMissionThankyouTemplates();
		}
	}
}

void ThankyouScreen::downloadClicked()
{
	saveFileDialog = QSharedPointer<QFileDialog>(new QFileDialog(nullptr, "Save File", "", "PDF (*.pdf)"), &QObject::deleteLater);
	saveFileDialog->setAcceptMode(QFileDialog::AcceptSave);
	connect(saveFileDialog.data(), &QFileDialog::fileSelected, this, &ThankyouScreen::saveThankyousSelected);
	connect(saveFileDialog.data(), &QFileDialog::finished, this, &ThankyouScreen::saveThankyousSelectorFinished);
	saveFileDialog->show();
	mainWindow.setEnabled(false);
}

void ThankyouScreen::saveThankyousSelected(QString fn)
{
	saveFileName = fn;
	cm.foxmq.post("thankyou.download", FoxHead::ContentType::Empty, "");
}

void ThankyouScreen::saveThankyousSelectorFinished()
{
	mainWindow.setEnabled(true);
	saveFileDialog.clear();
}

void ThankyouScreen::clearClicked()
{
	cm.foxmq.post("thankyou.clear", FoxHead::ContentType::Empty, "");
}

void ThankyouScreen::copyCommonClicked()
{
	cm.foxmq.post("thankyou.copy-common", FoxHead::ContentType::Empty, "");
}
