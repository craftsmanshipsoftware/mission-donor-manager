// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef MDM_THANKYOU_SCREEN_H_
#define MDM_THANKYOU_SCREEN_H_

#include <QModelIndex>
#include "tab-screen.h"
#include "receiver.h"

class MainWindow;
class ConnectionManager;
class QPushButton;
class QFileDialog;
class QExclusiveButtons;
class QSlidingStackedWidget;
class ThankyouCommonWidget;
class ThankyouMissionsWidget;
class ThankyouTemplateEditWidget;

class ThankyouScreen : public TabScreen, public Receiver
{
	Q_OBJECT

public:
	ThankyouScreen(MainWindow &mainWindow);
	void subscribe() override;
	void unsubscribe() override;
	void enableSaveButton();

private:
	void processPublish(const QByteArray &topic, ssize_t count, FoxID id, FoxHead::ContentType contentType, const QByteArray &content) override;
	void processPostResponce(const QByteArray &topic, FoxID id, FoxHead::ContentType contentType, const QByteArray &content) override;
	void processAck(const QByteArray &topic) override;
	void processNack(const QByteArray &topic) override;
	void okFromLockNack();
	void sendLock();
	void sendUnlock();
	void sendRequestForCommonThankyouTemplates();
	void sendRequestForMissionThankyouTemplates();
	void sendRequestForEnabled();
	void selectorButtonClicked(int i);
	void editEnabledClicked();
	void saveClicked();
	bool checkForSave();
	void saveConfirmSaveClicked();
	void saveConfirmDiscardClicked();
	void saveConfirmCleanUp();
	void doneEdits();
	void missionSelected(int missionID, QModelIndex index);
	void missionDeselected();
	void downloadClicked();
	void saveThankyousSelected(QString fn);
	void saveThankyousSelectorFinished();
	void clearClicked();
	void copyCommonClicked();
	MainWindow &mainWindow;
	ConnectionManager &cm;
	QExclusiveButtons *selectorButtons;
	QPushButton *editEnableButton;
	QPushButton *cancelButton;
	QPushButton *saveButton;
	QSlidingStackedWidget *widgetStack;
	ThankyouCommonWidget *common;
	ThankyouMissionsWidget *missions;
	int missionUnderEdit = -1;
	QModelIndex indexOfMissionUnderEdit;
	QSharedPointer<QFileDialog> saveFileDialog;
	QString saveFileName;

friend ThankyouCommonWidget;
friend ThankyouMissionsWidget;
friend ThankyouTemplateEditWidget;
};

#endif
