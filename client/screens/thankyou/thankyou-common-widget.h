// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef MDM_THANKYOU_COMMON_WIDGET_H_
#define MDM_THANKYOU_COMMON_WIDGET_H_

#include <QWidget>

class QPushButton;
class QLabel;
class QCheckBox;
class ThankyouScreen;
class ThankyouTemplateEditWidget;

class ThankyouCommonWidget : public QWidget
{
	Q_OBJECT

public:
	ThankyouCommonWidget(ThankyouScreen &thankyouScreen);
	void setEnabled(bool enabled);
	void updateForms(const QJsonObject &json);
	void updateEnabled(const QJsonObject &json);
	QJsonObject buildJson();
	QJsonObject buildEnabledJson();
	void updateMailWaitingCount(int count);
	bool isModified();
	bool isEnabledModified() { return enabledIsModified; }
	void clearModified();

private:
	void enabledChanged();
	ThankyouScreen &thankyouScreen;
	ThankyouTemplateEditWidget *emailTemplate;
	ThankyouTemplateEditWidget *mailTemplate;
	QLabel *mailWaitingLabel;
	QPushButton *downloadButton;
	QPushButton *clearWaitingButton;
	QCheckBox *emailEnabledCheckBox;
	QCheckBox *mailEnabledCheckBox;
	QString mailWaitingMsg = tr("%1 letters waiting to be sent");
	bool enabledIsModified = false;
};

#endif
