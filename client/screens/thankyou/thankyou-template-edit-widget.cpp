// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QHBoxLayout>
#include <QLabel>
#include <QPlainTextEdit>
#include <QPushButton>
#include "theme.h"
#include "styler.h"
#include "thankyou-screen.h"
#include "thankyou-template-edit-widget.h"

ThankyouTemplateEditWidget::ThankyouTemplateEditWidget(ThankyouScreen &thankyouScreen, const QString &title, int helpType, bool showCopyCommon) : thankyouScreen(thankyouScreen)
{
	QHBoxLayout *layout = new QHBoxLayout;
	setLayout(layout);

	QVBoxLayout *leftLayout = new QVBoxLayout;
	layout->addLayout(leftLayout);

// title layout
	QHBoxLayout *titleLayout = new QHBoxLayout;
	leftLayout->addLayout(titleLayout);
	QLabel *label = new QLabel(title);
	Styler::styleLabel(label);
	titleLayout->addWidget(label);
	titleLayout->addStretch(1);


	copyCommonButton = new QPushButton(tr("Copy Common"));
	Styler::styleButton(copyCommonButton);
	titleLayout->addWidget(copyCommonButton);
	connect(copyCommonButton, &QPushButton::clicked, this, &ThankyouTemplateEditWidget::copyCommonClicked);
	if (!showCopyCommon)
		copyCommonButton->hide();


// QPlainTextEdit
	templateEdit = new QPlainTextEdit;
	Styler::stylePlainTextEdit(templateEdit);
	leftLayout->addWidget(templateEdit);

// help
	label = new QLabel;
	if (helpType == 0)
		label->setText(tr("Available variables:\n\n[[address-salutation]]\n[[salutation]]\n[[amount]]\n[[date]]\n[[mission]]"));
	else
		label->setText(tr("Additional line formatting:\n\n[[font]] eg. font9\n[[bold]] eg. bold11\n[[slip]] - add remittance slip"));
	Styler::styleLabel(label);
	layout->addWidget(label);

	setEnabled(false);
}

void ThankyouTemplateEditWidget::setEnabled(bool enabled)
{
	templateEdit->setEnabled(enabled);
	copyCommonButton->setEnabled(enabled);
	if (enabled)
		connect(templateEdit, &QPlainTextEdit::modificationChanged, this, &ThankyouTemplateEditWidget::templateChanged);
	else
		disconnect(templateEdit, &QPlainTextEdit::modificationChanged, this, &ThankyouTemplateEditWidget::templateChanged);
}

void ThankyouTemplateEditWidget::templateChanged()
{
	isModified = true;
	thankyouScreen.enableSaveButton();
}

void ThankyouTemplateEditWidget::setText(const QString &text)
{
	templateEdit->setPlainText(text);
}

QString ThankyouTemplateEditWidget::text()
{
	return templateEdit->toPlainText();
}

void ThankyouTemplateEditWidget::copyCommonClicked()
{
	askedForCopy = true;
	thankyouScreen.copyCommonClicked();
}
