// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef MDM_MISSION_ABSTRACT_DONOR_DM_H_
#define MDM_MISSION_ABSTRACT_DONOR_DM_H_

#include <QAbstractListModel>

class MissionAbstractDonorDataModel : public QAbstractListModel
{
	Q_OBJECT

public:
	virtual int rowCount(const QModelIndex &parent = QModelIndex()) const override = 0;
	virtual int columnCount(const QModelIndex &parent = QModelIndex()) const override = 0;
	virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const override = 0;
	virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override = 0;
	virtual void clear() = 0;
	virtual void append(QJsonObject jDonor) = 0;
	virtual int getId(uint row) = 0;
	virtual QString getNote(uint) { return QString(); }
	virtual int amount(uint row) = 0;
	virtual int amountInColumn() = 0;
	virtual int inKindAmount(uint row) = 0;
	virtual int inKindAmountInColumn() = 0;
};

#endif
