// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QJsonObject>
#include "../common/common.h"
#include "mission-donations-list-dm.h"

int MissionDonationsDataModel::rowCount(const QModelIndex &) const
{
	return donationsList.size();
}

int MissionDonationsDataModel::columnCount(const QModelIndex &) const
{
	return 4;
}


QVariant MissionDonationsDataModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	QVariant text;

	if (role != Qt::DisplayRole)
		return QVariant();

	if (orientation == Qt::Horizontal) {
		switch(section) {  // clang format off
    		case 0: text = QString(tr("Date"));   break;
			case 1: text = QString(tr("First"));  break;
			case 2: text = QString(tr("Last"));   break;
			case 3: text = QString(tr("Amount")); break;
		}  // clang format on
	} else {
		text = QVariant(QString());
	}

	return text;
}

QVariant MissionDonationsDataModel::data(const QModelIndex &index, int role) const
{
	QVariant item;
	QString typeString;
	if (role == Qt::DisplayRole) {
		switch(index.column()) {  // clang format off
			case 0: item = QDate::fromJulianDay(donationsList.at(index.row()).date).toString("yyyy/MM/dd"); break;
			case 1: item = donationsList.at(index.row()).firstName.left(MaxFirstNameDisplayLengthInMissionView); break;
			case 2: item = donationsList.at(index.row()).lastName; break;
			case 3:
				QString string = convertCentsToString(donationsList.at(index.row()).amount, true);
				if (donationsList.at(index.row()).inkind)
					string.append(" IK");
				item = string;
				break;
		}  // clang format on
	}
	return item;
}

void MissionDonationsDataModel::append(QJsonObject jDonor)
{
	struct DonationListItem dli;
	dli.firstName = jDonor.value("name").toString();
	dli.lastName = jDonor.value("lastname").toString();
	dli.amount = jDonor.value("amount").toInt();
	dli.inkind = jDonor.value("inkind").toBool();
	dli.date = jDonor.value("date").toInt();
	dli.id = jDonor.value("id").toInt();
	dli.note = jDonor.value("note").toString();
	donationsList.append(dli);
}
