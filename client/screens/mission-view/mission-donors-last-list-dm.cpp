// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QJsonObject>
#include <QDate>
#include "../common/common.h"
#include "mission-donors-last-list-dm.h"

int MissionDonorLastListDataModel::rowCount(const QModelIndex &) const
{
	return donorLastList.size();
}

int MissionDonorLastListDataModel::columnCount(const QModelIndex &) const
{
	return 3;
}


QVariant MissionDonorLastListDataModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	QVariant text;

	if (role != Qt::DisplayRole)
		return QVariant();

	if (orientation == Qt::Horizontal) {
		switch(section) {  // clang format off
			case 0: text = QString(tr("First")); break;
			case 1: text = QString(tr("Last")); break;
			case 2: text = QString(tr("Date")); break;
		}  // clang format on
	} else {
		text = QVariant(QString());
	}

	return text;
}

QVariant MissionDonorLastListDataModel::data(const QModelIndex &index, int role) const
{
	QVariant item;
	QString typeString;
	if (role == Qt::DisplayRole) {
		switch(index.column()) {  // clang format off
			case 0: item = donorLastList.at(index.row()).firstName.left(MaxFirstNameDisplayLengthInMissionView); break;
			case 1: item = donorLastList.at(index.row()).lastName; break;
			case 2: item = QDate::fromJulianDay(donorLastList.at(index.row()).date).toString("yyyy/MM/dd"); break;
		}  // clang format on
	}
	return item;
}

void MissionDonorLastListDataModel::append(QJsonObject jDonor)
{
	struct DonorLastListItem dsli;
	dsli.firstName = jDonor.value("name").toString();
	dsli.lastName = jDonor.value("lastname").toString();
	dsli.date = jDonor.value("date").toInt();
	dsli.id = jDonor.value("id").toInt();
	dsli.note = jDonor.value("note").toString();
	donorLastList.append(dsli);
}
