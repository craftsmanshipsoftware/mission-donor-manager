// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QScroller>
#include <QLabel>
#include <QTableView>
#include <QHeaderView>
#include <QJsonArray>
#include <QJsonObject>
#include <QRadioButton>
#include "qdualboxlayout.h"
#include "../common/common.h"
#include "theme.h"
#include "styler.h"
#if (defined(Q_OS_ANDROID) || defined(Q_OS_IOS))
#include "qexclusivebuttons.h"
#else
#include <QKeyEvent>
#include "copy-and-paste.h"
#endif
#include "mission-donors-widget.h"

MissionDonorsWidget::MissionDonorsWidget(MissionAbstractDonorDataModel &dm, const QString &title) : dm(dm)
{
	QDualBoxLayout *layout = new QDualBoxLayout;
	setLayout(layout);
#if (defined(Q_OS_ANDROID) || defined(Q_OS_IOS))
	Q_UNUSED(title);
	layout->addStretch(1);
#else
	layout->setLayoutDirections(QBoxLayout::TopToBottom, QBoxLayout::TopToBottom);

	titleLabel = new QLabel(title);
	Styler::styleLabel(titleLabel, Theme::H2Size);
	layout->addWidget(titleLabel);
	layout->setAlignment(titleLabel, Qt::AlignHCenter);

	// span selectors go before view on Desktop
	QGridLayout *spanButtonLayout = new QGridLayout;
	layout->addLayout(spanButtonLayout);
	auto addSpanRadioButton = [&](const QString &title, int id, bool selected) {
		QRadioButton *spanButton = new QRadioButton(title);
		Styler::styleRadioButton(spanButton);
		spanButtonLayout->addWidget(spanButton, id % 4, id / 4);
		spanButtonGroup.addButton(spanButton, id);
		spanButton->setChecked(selected);
	};
	addSpanRadioButton(tr("Past 30 Days"), 0, false);
	addSpanRadioButton(tr("Past 90 Days"), 1, false);
	addSpanRadioButton(tr("Past 120 Days"), 2, false);
	addSpanRadioButton(tr("Past 365 Days"), 3, true);
	addSpanRadioButton(tr("Year to Date"), 4, false);
	addSpanRadioButton(tr("Last Year"), 5, false);
	addSpanRadioButton(tr("Past 3 Years"), 6, false);
	addSpanRadioButton(tr("All"), 7, false);
#endif
	noActivity = new QLabel(tr("No Financial Activity"));
	Styler::styleLabel(noActivity);
	layout->addWidget(noActivity);
	layout->setAlignment(noActivity, Qt::AlignCenter);
	donorView = new QTableView;
	Styler::styleTableView(donorView);
	donorView->verticalHeader()->hide();
	donorView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	donorView->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
	donorView->setAlternatingRowColors(true);
#if (defined(Q_OS_ANDROID) || defined(Q_OS_IOS))
	donorView->setSelectionMode(QAbstractItemView::NoSelection);
	Styler::styleTouchScrolling(donorView);
#else
	donorView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
#endif
	donorView->hide();
	layout->addWidget(donorView);

#if (defined(Q_OS_ANDROID) || defined(Q_OS_IOS))
	layout->setAlignment(donorView, Qt::AlignCenter);
#endif

	sort.setDynamicSortFilter(true);
	sort.setSourceModel(&dm);
	donorView->setModel(&sort);
	donorView->setSortingEnabled(true);

#if (defined(Q_OS_ANDROID) || defined(Q_OS_IOS))
	// span selectors go after view on Mobile
	spanButtons = new QExclusiveButtons( { tr("Past 30 Days"), tr("Past 90 Days"), tr("Past 120 Days"), tr("Past 365 Days"), tr("Year to Date") });
	spanButtons->setLayoutDirections(QBoxLayout::TopToBottom, QBoxLayout::TopToBottom);
	layout->addStretch(1);
	for ( int i=0; i < spanButtons->size(); i++) {
		Styler::styleButton(spanButtons->buttonAt(i));
		spanButtons->buttonAt(i)->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
	}
	layout->addWidget(spanButtons);
	spanButtons->setChecked(3, true);
#endif
	financialSpan = FinancialSpan::Previous365Days;
	layout->addStretch(1);

#if (defined(Q_OS_ANDROID) || defined(Q_OS_IOS))
	connect(spanButtons, &QExclusiveButtons::clicked, this, &MissionDonorsWidget::spanButtonClicked);
#else
	connect(&spanButtonGroup, &QButtonGroup::idClicked, this, &MissionDonorsWidget::spanButtonClicked);
#endif
	connect(donorView, &QTableView::clicked, this, &MissionDonorsWidget::donorClicked);
}

void MissionDonorsWidget::populateValues(QJsonArray jArray)
{
	if (jArray.size()) {
		noActivity->hide();
		donorView->show();
		donorView->setCurrentIndex(QModelIndex());  // clear selected index if copy-and-paste is attempted after view is refreshed with smaller model.
		dm.layoutAboutToBeChanged();
		dm.clear();
		for (auto jIt : jArray)
			dm.append(jIt.toObject());
		dm.layoutChanged();
		donorView->resizeColumnsToContents();
	} else {
		noActivity->show();
		donorView->hide();
	}
}

void MissionDonorsWidget::spanButtonClicked(int buttonIndex)
{
	switch (buttonIndex) {  // clang format off
		case 0: financialSpan = FinancialSpan::Previous30Days;   break;
		case 1: financialSpan = FinancialSpan::Previous90Days;   break;
		case 2: financialSpan = FinancialSpan::Previous120Days;  break;
		case 3: financialSpan = FinancialSpan::Previous365Days;  break;
		case 4: financialSpan = FinancialSpan::YTD;              break;
		case 5: financialSpan = FinancialSpan::PreviousYear;     break;
		case 6: financialSpan = FinancialSpan::Previous3Years;   break;
		case 7: financialSpan = FinancialSpan::All;              break;
	}  // clang format on

	emit financialSpanChanged(financialSpan);
}

void MissionDonorsWidget::donorClicked(QModelIndex index)
{
	int row = sort.mapToSource(index).row();
	emit donorSelected(dm.getId(row), dm.getNote(row));
}

#if !(defined(Q_OS_ANDROID) || defined(Q_OS_IOS))
void MissionDonorsWidget::keyPressEvent(QKeyEvent *e)
{
	if (e->type() == QKeyEvent::KeyPress && e->matches(QKeySequence::Copy))
		copyFromTable(donorView->selectionModel()->selectedIndexes());
}
#endif
