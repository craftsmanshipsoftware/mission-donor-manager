// clang-format off
/**************************************************************************************************
	Copyright (C) 2024  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef MDM_MISSION_DONATION_SORT_PM_H_
#define MDM_MISSION_DONATION_SORT_PM_H_

#include <QSortFilterProxyModel>
#include "mission-abstract-donors-dm.h"

class MissionDonationSortProxyModel : public QSortFilterProxyModel
{
	Q_OBJECT

public:
	MissionDonationSortProxyModel(QObject *parent = nullptr): QSortFilterProxyModel(parent)	{}
	bool lessThan(const QModelIndex &left, const QModelIndex &right) const
	{
		bool lessThan = false;
		MissionAbstractDonorDataModel *dm = qobject_cast<MissionAbstractDonorDataModel *>(sourceModel());
		if (dm->amountInColumn() == left.column())
			lessThan = dm->amount(left.row()) < dm->amount(right.row());
		else if (dm->inKindAmountInColumn() == left.column())
			lessThan = dm->inKindAmount(left.row()) < dm->inKindAmount(right.row());
		else
			lessThan = QSortFilterProxyModel::lessThan(right, left);
		return lessThan;
	}
};

#endif
