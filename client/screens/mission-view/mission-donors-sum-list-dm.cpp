// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QJsonObject>
#include "../common/common.h"
#include "mission-donors-sum-list-dm.h"

int MissionDonorSumListDataModel::rowCount(const QModelIndex &) const
{
	return donorSumList.size();
}

int MissionDonorSumListDataModel::columnCount(const QModelIndex &) const
{
	return 4;
}


QVariant MissionDonorSumListDataModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	QVariant text;

	if (role != Qt::DisplayRole)
		return QVariant();

	if (orientation == Qt::Horizontal) {
		switch(section) {  // clang format off
			case 0: text = QString(tr("First")); break;
			case 1: text = QString(tr("Last")); break;
			case 2: text = QString(tr("Total")); break;
			case 3: text = QString(tr("Inkind")); break;
		}  // clang format on
	} else {
		text = QVariant(QString());
	}

	return text;
}

QVariant MissionDonorSumListDataModel::data(const QModelIndex &index, int role) const
{
	QVariant item;
	QString typeString;
	if (role == Qt::DisplayRole) {
		switch(index.column()) {  // clang format off
			case 0: item = donorSumList.at(index.row()).firstName.left(MaxFirstNameDisplayLengthInMissionView); break;
			case 1: item = donorSumList.at(index.row()).lastName; break;
			case 2: item = convertCentsToString(donorSumList.at(index.row()).amount, true); break;
			case 3: item = convertCentsToString(donorSumList.at(index.row()).inkind, true); break;
		}  // clang format on
	}
	return item;
}

void MissionDonorSumListDataModel::append(QJsonObject jDonor)
{
	struct DonorSumListItem dsli;
	dsli.firstName = jDonor.value("name").toString();
	dsli.lastName = jDonor.value("lastname").toString();
	dsli.amount = jDonor.value("amount").toInt();
	dsli.inkind = jDonor.value("inkind").toInt();
	dsli.id = jDonor.value("id").toInt();
	donorSumList.append(dsli);
}
