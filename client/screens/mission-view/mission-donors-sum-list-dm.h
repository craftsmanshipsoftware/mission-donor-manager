// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef MDM_MISSION_SUM_LIST_DM_H_
#define MDM_MISSION_SUM_LIST_DM_H_

#include "mission-abstract-donors-dm.h"

class MissionDonorSumListDataModel : public MissionAbstractDonorDataModel
{
	Q_OBJECT

public:
	int rowCount(const QModelIndex &parent = QModelIndex()) const override;
	int columnCount(const QModelIndex &parent = QModelIndex()) const override;
	QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
	inline void clear() override { donorSumList.clear(); }
	void append(QJsonObject jDonor) override;
	int getId(uint row) override { return donorSumList.at(row).id; }
	int amount(uint row) override { return donorSumList.at(row).amount; }
	int amountInColumn() override { return 2; }
	int inKindAmount(uint row) override { return donorSumList.at(row).inkind; }
	int inKindAmountInColumn() override { return 3; }

private:
	struct DonorSumListItem {
		QString firstName;
		QString lastName;
		int id;
		int amount;
		int inkind;
	};
	QList<DonorSumListItem> donorSumList;
};

#endif
