// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef MDM_MISSION_VIEW_SCREEN_H_
#define MDM_MISSION_VIEW_SCREEN_H_

#include "tab-screen.h"
#include "receiver.h"
#include "mission-donors-sum-list-dm.h"
#include "mission-donors-last-list-dm.h"
#include "mission-donations-list-dm.h"

class QLabel;
class QPushButton;
class QSlidingStackedWidget;
class MainWindow;
class Permission;
class ConnectionManager;
class NameSelectorWidget;
class MissionFinancialsWidget;
class MissionDonorsWidget;
class MissionDonorInfoWidget;

class MissionViewScreen : public TabScreen, public Receiver
{
	Q_OBJECT

public:
	MissionViewScreen(MainWindow &mainWindow, Permission &p);
	void subscribe() override;
	void unsubscribe() override;

private:
	void processPublish(const QByteArray &topic, ssize_t, FoxID asker, FoxHead::ContentType contentType, const QByteArray &content) override;
	void processPostResponce(const QByteArray &topic, FoxID asker, FoxHead::ContentType contentType, const QByteArray &content) override;
	void missionSelected(int missionID, QModelIndex index);
	void grabFinancials();
	void donorSumFinancialSpanChanged(int fs);
	void donorLastFinancialSpanChanged(int fs);
	void donationsListSpanChanged(int fs);
	void setTitle(const QString &missionName, const QString &title);
	void donorSelected(int donorId, QString note);
#if defined(Q_OS_ANDROID) || defined(Q_OS_IOS)
	void orientationChanged(Qt::ScreenOrientation so);
	void backClicked();
	void keyPressEvent(QKeyEvent *event) override;
	void donorTotalsClicked();
	void donorLastClicked();
	void donationsClicked();
#endif
	MainWindow &mainWindow;
	ConnectionManager &cm;
#if defined(Q_OS_ANDROID) || defined(Q_OS_IOS)
	QSlidingStackedWidget *content;
	QString missionName;
	QString title;
	QPushButton *backButton;
	MissionDonorInfoWidget *donorInfoWidget;
#endif
	QLabel *titleLabel;
	NameSelectorWidget *missionSelector;
	MissionFinancialsWidget *missionFinancialsWidget;
	MissionDonorsWidget *missionDonorsSumWidget;
	MissionDonorSumListDataModel missionDonorSumListDataModel;
	MissionDonorsWidget *missionDonorsLastWidget;
	MissionDonorLastListDataModel missionDonorLastListDataModel;
	MissionDonorsWidget *missionDonationsWidget;
	MissionDonationsDataModel missionDonationsListDataModel;
	int selectedMissionID = -1;
	bool updateFinancialsWhenTabSelected = false;
	bool updateMissionListWhenTabSelected = true;
};

#endif
