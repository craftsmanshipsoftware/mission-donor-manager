// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QJsonObject>
#include "../common/common.h"
#include "mission-financials-dm.h"

int MissionFinancialsDataModel::rowCount(const QModelIndex &) const
{
	return monthItemList.size();
}

int MissionFinancialsDataModel::columnCount(const QModelIndex &) const
{
	return 2;
}

QVariant MissionFinancialsDataModel::data(const QModelIndex &index, int role) const
{
	QVariant item;
	QString typeString;
	if (role == Qt::DisplayRole) {
		switch(index.column()) {  // clang format off
			case 0: item = QDate::fromJulianDay(monthItemList.at(index.row()).date).toString("MMMM yyyy"); break;
			case 1: item = convertCentsToString(monthItemList.at(index.row()).amount, true);               break;
		}  // clang format on
	}
	return item;
}

void MissionFinancialsDataModel::append(QJsonObject jDonor)
{
	struct MonthItem mi;
	mi.date = jDonor.value("date").toInt();
	mi.amount = jDonor.value("amount").toInt();
	monthItemList.append(mi);
}
