// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QApplication>
#include <QScreen>
#include <QVBoxLayout>
#include <QLabel>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include "permission.h"
#include "main-window.h"
#include "connection-manager.h"
#include "styler.h"
#include "theme.h"
#include "name-selector-widget.h"
#include "mission-financials-widget.h"
#include "mission-donors-widget.h"
#include "mission-view-screen.h"
#if defined(Q_OS_ANDROID) || defined(Q_OS_IOS)
#include <QPushButton>
#include <QKeyEvent>
#include "qslidingstackedwidget.h"
#include "mission-donor-info-widget-mobile.h"

enum Stack {
	Selector,
	Monthy,
	DonorTotals,
	DonorLast,
	Donations,
	Info,
};
#endif

MissionViewScreen::MissionViewScreen(MainWindow &mainWindow, Permission &p) : mainWindow(mainWindow), cm(mainWindow.connectionManager)
{
	QVBoxLayout *layout = new QVBoxLayout;
	setLayout(layout);
	int m = Theme::OuterMargin * qMin(screen()->geometry().width(), screen()->geometry().height());
	layout->setContentsMargins(m, m, m, m);

	missionSelector = new NameSelectorWidget;
	missionFinancialsWidget = new MissionFinancialsWidget;
	missionDonorsSumWidget = new MissionDonorsWidget(missionDonorSumListDataModel, tr("Donor Totals"));
	missionDonorsLastWidget = new MissionDonorsWidget(missionDonorLastListDataModel, tr("Last Donation"));
	missionDonationsWidget = new MissionDonorsWidget(missionDonationsListDataModel, tr("Donation List"));

	QHBoxLayout *titleLayout = new QHBoxLayout;
	layout->addLayout(titleLayout);
#if defined(Q_OS_ANDROID) || defined(Q_OS_IOS)
	QIcon backIcon(":/back.svg");
	backButton = new QPushButton(backIcon, "");
	backButton->setIconSize(QSize(32, 32));
	backButton->setFocusPolicy(Qt::NoFocus);
	backButton->setStyleSheet("QPushButton { background-color: rgba(255, 255, 255, 0); border: 0px;}");
	titleLayout->addWidget(backButton);
	backButton->hide();
#endif
	titleLayout->addStretch(1);
	titleLabel = new QLabel(tr("Select Mission"));
	Styler::styleLabel(titleLabel, Theme::H2Size);
	titleLayout->addWidget(titleLabel);
	titleLayout->addStretch(1);

#if defined(Q_OS_ANDROID) || defined(Q_OS_IOS)
	content = new QSlidingStackedWidget();
	layout->addWidget(content);

	missionSelector->setSearchVisible(false);
	content->setDuration(500);
	content->addWidget(missionSelector);
	content->addWidget(missionFinancialsWidget);
	content->addWidget(missionDonorsSumWidget);
	content->addWidget(missionDonorsLastWidget);
	content->addWidget(missionDonationsWidget);
	donorInfoWidget = new MissionDonorInfoWidget;
	content->addWidget(donorInfoWidget);
	content->setCurrentIndex(Stack::Selector);
	connect(qApp->primaryScreen(), &QScreen::orientationChanged, this, &MissionViewScreen::orientationChanged);
	connect(backButton, &QPushButton::clicked, this, &MissionViewScreen::backClicked);
	connect(missionFinancialsWidget, &MissionFinancialsWidget::donorTotalsClicked, this, &MissionViewScreen::donorTotalsClicked);
	connect(missionFinancialsWidget, &MissionFinancialsWidget::donorLastClicked, this, &MissionViewScreen::donorLastClicked);
	connect(missionFinancialsWidget, &MissionFinancialsWidget::donationsClicked, this, &MissionViewScreen::donationsClicked);
#else
	QHBoxLayout *contentLayout = new QHBoxLayout;
	layout->addLayout(contentLayout);
	contentLayout->addStretch(1);
	contentLayout->addWidget(missionSelector);
	contentLayout->addStretch(1);
	contentLayout->addWidget(missionFinancialsWidget);
	contentLayout->addStretch(1);
	contentLayout->addWidget(missionDonorsSumWidget);
	contentLayout->addStretch(1);
	contentLayout->addWidget(missionDonorsLastWidget);
	contentLayout->addStretch(1);
	contentLayout->addWidget(missionDonationsWidget);
	contentLayout->addStretch(1);
#endif

	connect(missionSelector, &NameSelectorWidget::nameSelected, this, &MissionViewScreen::missionSelected);
	connect(missionDonorsSumWidget, &MissionDonorsWidget::financialSpanChanged, this, &MissionViewScreen::donorSumFinancialSpanChanged);
	connect(missionDonorsLastWidget, &MissionDonorsWidget::financialSpanChanged, this, &MissionViewScreen::donorLastFinancialSpanChanged);
	connect(missionDonationsWidget, &MissionDonorsWidget::financialSpanChanged, this, &MissionViewScreen::donationsListSpanChanged);
	connect(missionDonorsSumWidget, &MissionDonorsWidget::donorSelected, this, &MissionViewScreen::donorSelected);
	connect(missionDonorsLastWidget, &MissionDonorsWidget::donorSelected, this, &MissionViewScreen::donorSelected);
	connect(missionDonationsWidget, &MissionDonorsWidget::donorSelected, this, &MissionViewScreen::donorSelected);

	topicsToReceive << "mission" << "users.ro.missions-updated" << "donations.md.mission-accounts-updated";
	cm.registerApiForReceiver(this);
	cm.foxmq.subscription("donations.md.mission-accounts-updated", true);
	cm.foxmq.subscription("users.ro.missions-updated", true);
	if (p.hasPermission(Permission::MissionViewAll))
		cm.foxmq.subscription("mission.full-list-updated", true);
}

void MissionViewScreen::subscribe()
{
	if (updateFinancialsWhenTabSelected) {
		updateFinancialsWhenTabSelected = false;
		grabFinancials();
	}
	if (updateMissionListWhenTabSelected) {
		updateMissionListWhenTabSelected = false;
		cm.foxmq.post("mission.my-list", FoxHead::ContentType::Empty, "");
	}
}

void MissionViewScreen::unsubscribe()
{

}

void MissionViewScreen::missionSelected(int missionId, QModelIndex index)
{
	selectedMissionID = missionId;
	int row = missionSelector->mapToSourceRow(index);
#if defined(Q_OS_ANDROID) || defined(Q_OS_IOS)
	content->setCurrentIndex(Stack::Monthy);
	setTitle(missionSelector->model().getFirstName(row), tr(" Financials"));
	backButton->show();
#else
	setTitle(missionSelector->model().getFirstName(row), tr(" Financials"));
	missionFinancialsWidget->clearDonorInfo();
#endif
	grabFinancials();
}

void MissionViewScreen::processPublish(const QByteArray &topic, ssize_t, FoxID, FoxHead::ContentType, const QByteArray &content)
{
	if (topic == "users.ro.missions-updated" || topic == "mission.full-list-updated") {
		if (recentlySelected)
			cm.foxmq.post("mission.my-list", FoxHead::ContentType::Empty, "");
		else
			updateMissionListWhenTabSelected = true;
	} else if (topic == "donations.md.mission-accounts-updated" && QJsonDocument::fromJson(content).array().contains(selectedMissionID)) {
		if (recentlySelected)
			grabFinancials();
		else
			updateFinancialsWhenTabSelected = true;
	}
}

void MissionViewScreen::grabFinancials()
{
#if defined(Q_OS_ANDROID) || defined(Q_OS_IOS)
	if (content->currentIndex() <= Stack::Monthy) {
		cm.foxmq.post(QString("mission.%1.financials").arg(selectedMissionID, 4, 10, QChar('0')).toUtf8(), FoxHead::ContentType::Empty, "");
	} else if (content->currentIndex() == Stack::DonorTotals) {
		int donorSumFincancialSpan = missionDonorsSumWidget->currentFinancialSpan();
		cm.foxmq.post(QString("mission.%1.donors-by-sum").arg(selectedMissionID, 4, 10, QChar('0')).toUtf8(), FoxHead::ContentType::IntAsString, QByteArray::number(donorSumFincancialSpan));
	} else if (content->currentIndex() == Stack::DonorLast) {
		int donorLastFincancialSpan = missionDonorsLastWidget->currentFinancialSpan();
		cm.foxmq.post(QString("mission.%1.donors-by-latest").arg(selectedMissionID, 4, 10, QChar('0')).toUtf8(), FoxHead::ContentType::IntAsString, QByteArray::number(donorLastFincancialSpan));
	} else if (content->currentIndex() == Stack::DonorLast) {
		int donationsFincancialSpan = missionDonationsWidget->currentFinancialSpan();
		cm.foxmq.post(QString("mission.%1.donations-list").arg(selectedMissionID, 4, 10, QChar('0')).toUtf8(), FoxHead::ContentType::IntAsString, QByteArray::number(donationsFincancialSpan));
	}
#else
	int donorSumFincancialSpan = missionDonorsSumWidget->currentFinancialSpan();
	int donorLastFincancialSpan = missionDonorsLastWidget->currentFinancialSpan();
	int donationsFincancialSpan = missionDonationsWidget->currentFinancialSpan();
	cm.foxmq.post(QString("mission.%1.financials").arg(selectedMissionID, 4, 10, QChar('0')).toUtf8(), FoxHead::ContentType::Empty, "");
	cm.foxmq.post(QString("mission.%1.donors-by-sum").arg(selectedMissionID, 4, 10, QChar('0')).toUtf8(), FoxHead::ContentType::IntAsString, QByteArray::number(donorSumFincancialSpan));
	cm.foxmq.post(QString("mission.%1.donors-by-latest").arg(selectedMissionID, 4, 10, QChar('0')).toUtf8(), FoxHead::ContentType::IntAsString, QByteArray::number(donorLastFincancialSpan));
	cm.foxmq.post(QString("mission.%1.donations-list").arg(selectedMissionID, 4, 10, QChar('0')).toUtf8(), FoxHead::ContentType::IntAsString, QByteArray::number(donationsFincancialSpan));
#endif
}

void MissionViewScreen::processPostResponce(const QByteArray &topic, FoxID, FoxHead::ContentType, const QByteArray &content)
{
	QJsonArray jArray = QJsonDocument::fromJson(content).array();
	if (topic == "mission.my-list") {
		missionSelector->updateNameList(jArray);
	} else if (topic == "mission.donor-info") {
		QJsonObject json = QJsonDocument::fromJson(content).object();
#if defined(Q_OS_ANDROID) || defined(Q_OS_IOS)
		donorInfoWidget->showDonorInfo(json);
		MissionViewScreen::content->setCurrentIndex(Stack::Info);
#else
		missionFinancialsWidget->showDonorInfo(json);
#endif
	} else if (topic.startsWith("mission")) {
		if (topic.endsWith("financials"))
			missionFinancialsWidget->populateValues(jArray);
		else if (topic.endsWith("donors-by-sum"))
			missionDonorsSumWidget->populateValues(jArray);
		else if (topic.endsWith("donors-by-latest"))
			missionDonorsLastWidget->populateValues(jArray);
		else if (topic.endsWith("donations-list"))
			missionDonationsWidget->populateValues(jArray);
	}
}

void MissionViewScreen::donorSumFinancialSpanChanged(int fs)
{
	if (selectedMissionID >= 0)
		cm.foxmq.post(QString("mission.%1.donors-by-sum").arg(selectedMissionID, 4, 10, QChar('0')).toUtf8(), FoxHead::ContentType::IntAsString, QByteArray::number(fs));
}

void MissionViewScreen::donorLastFinancialSpanChanged(int fs)
{
	if (selectedMissionID >= 0)
		cm.foxmq.post(QString("mission.%1.donors-by-latest").arg(selectedMissionID, 4, 10, QChar('0')).toUtf8(), FoxHead::ContentType::IntAsString, QByteArray::number(fs));
}

void MissionViewScreen::donationsListSpanChanged(int fs)
{
	if (selectedMissionID >= 0)
		cm.foxmq.post(QString("mission.%1.donations-list").arg(selectedMissionID, 4, 10, QChar('0')).toUtf8(), FoxHead::ContentType::IntAsString, QByteArray::number(fs));
}

void MissionViewScreen::setTitle(const QString &newMissionName, const QString &newTitle)
{
#if defined(Q_OS_ANDROID) || defined(Q_OS_IOS)
	missionName = newMissionName;
	title = newTitle;
	titleLabel->setText(missionName + title);
	orientationChanged(qApp->primaryScreen()->orientation());
#else
	titleLabel->setText(newMissionName + newTitle);
#endif
}

#if defined(Q_OS_ANDROID) || defined(Q_OS_IOS)

void MissionViewScreen::orientationChanged(Qt::ScreenOrientation so)
{
	if (qApp->primaryScreen()->isLandscape(so))
		titleLabel->setText(missionName + title);
	else
		titleLabel->setText(missionName + '\n' + title);
}

void MissionViewScreen::backClicked()
{
	if (content->currentIndex() == Stack::Monthy) {
		content->setCurrentIndex(Stack::Selector);
		setTitle("", tr("Select Mission"));
		backButton->hide();
	} else if  (content->currentIndex() == Stack::DonorTotals || content->currentIndex() == Stack::DonorLast || content->currentIndex() == Stack::Donations) {
		content->setCurrentIndex(Stack::Monthy);
		setTitle(missionName, tr(" Financials"));
	} else if (content->currentIndex() == Stack::Info) {
		content->previousBySelectionOrder();
	}
}

void MissionViewScreen::keyPressEvent(QKeyEvent *event)
{
	if (event->key() == Qt::Key_Back) {
		event->accept();
		backClicked();
	}
}

void MissionViewScreen::donorTotalsClicked()
{
	content->setCurrentIndex(Stack::DonorTotals);
	setTitle(missionName, tr(" Donor Totals"));
	int donorSumFincancialSpan = missionDonorsSumWidget->currentFinancialSpan();
	cm.foxmq.post(QString("mission.%1.donors-by-sum").arg(selectedMissionID, 4, 10, QChar('0')).toUtf8(), FoxHead::ContentType::IntAsString, QByteArray::number(donorSumFincancialSpan));
}

void MissionViewScreen::donorLastClicked()
{
	content->setCurrentIndex(Stack::DonorLast);
	setTitle(missionName, tr(" Donor's Last Donation"));
	int donorLastFincancialSpan = missionDonorsLastWidget->currentFinancialSpan();
	cm.foxmq.post(QString("mission.%1.donors-by-latest").arg(selectedMissionID, 4, 10, QChar('0')).toUtf8(), FoxHead::ContentType::IntAsString, QByteArray::number(donorLastFincancialSpan));
}

void MissionViewScreen::donationsClicked()
{
	content->setCurrentIndex(Stack::Donations);
	setTitle(missionName, tr(" Donations"));
	int donationsFincancialSpan = missionDonorsLastWidget->currentFinancialSpan();
	cm.foxmq.post(QString("mission.%1.donations-list").arg(selectedMissionID, 4, 10, QChar('0')).toUtf8(), FoxHead::ContentType::IntAsString, QByteArray::number(donationsFincancialSpan));
}
#endif

void MissionViewScreen::donorSelected(int donorId, QString note)
{
	cm.foxmq.post("mission.donor-info", FoxHead::ContentType::IntAsString, QByteArray::number(donorId));
#if !(defined(Q_OS_ANDROID) || defined(Q_OS_IOS))
	missionFinancialsWidget->setNote(note);
#endif
}
