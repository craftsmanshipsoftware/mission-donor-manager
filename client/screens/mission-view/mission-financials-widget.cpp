// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QScroller>
#include <QLabel>
#include <QTableView>
#include <QHeaderView>
#include <QJsonArray>
#include <QJsonObject>
#include <QPlainTextEdit>
#include "qdualboxlayout.h"
#include "../common/common.h"
#include "theme.h"
#include "styler.h"
#include "mission-financials-widget.h"
#if (defined(Q_OS_ANDROID) || defined(Q_OS_IOS))
#include <QPushButton>
#else
#include <QKeyEvent>
#include "copy-and-paste.h"
#endif

MissionFinancialsWidget::MissionFinancialsWidget()
{
	QDualBoxLayout *layout = new QDualBoxLayout;

	auto addLabel = [&](const QString &text) {
		QLabel *label = new QLabel(text);
		Styler::styleLabel(label);
		layout->addWidget(label);
		return label;
	};

	setLayout(layout);
#if !(defined(Q_OS_ANDROID) || defined(Q_OS_IOS))
	layout->setLayoutDirections(QBoxLayout::TopToBottom, QBoxLayout::TopToBottom);

	titleLabel = new QLabel(tr("Financials"));
	Styler::styleLabel(titleLabel, Theme::H2Size);
	layout->addWidget(titleLabel);
	layout->setAlignment(titleLabel, Qt::AlignHCenter);

	ytdLabel = addLabel(tr("Year-to-Date:"));
#else
	layout->addStretch(1);
#endif
	monthView = new QTableView;
	Styler::styleTableView(monthView);
	monthView->setModel(&missionFinancialsDataModel);
	monthView->verticalHeader()->hide();
	monthView->horizontalHeader()->hide();
	monthView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	monthView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
#if (defined(Q_OS_ANDROID) || defined(Q_OS_IOS))
	monthView->setSelectionMode(QAbstractItemView::NoSelection);
	QScroller::grabGesture(monthView, QScroller::LeftMouseButtonGesture);
#endif
	monthView->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Minimum);
	monthView->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
	monthView->setAlternatingRowColors(true);
	layout->addWidget(monthView);
#if (defined(Q_OS_ANDROID) || defined(Q_OS_IOS))
	layout->setAlignment(monthView, Qt::AlignCenter);
#endif
	layout->addStretch(1);
#if !(defined(Q_OS_ANDROID) || defined(Q_OS_IOS))

	addLabel("Donation Note:");
	noteDisplay = new QPlainTextEdit;
	noteDisplay->setReadOnly(true);
	Styler::stylePlainTextEdit(noteDisplay);
	layout->addWidget(noteDisplay);

	addressLabel = addLabel("\n\n\n");
	addressLabel->setTextInteractionFlags(Qt::TextSelectableByMouse);
	emailLabel = addLabel("");
	emailLabel->setTextInteractionFlags(Qt::TextSelectableByMouse);
#endif

#if (defined(Q_OS_ANDROID) || defined(Q_OS_IOS))
	QVBoxLayout *buttonLayout = new QVBoxLayout;
	layout->addLayout(buttonLayout);

	buttonLayout->addStretch(4);

	ytdLabel = new QLabel(tr("Year-to-Date:"));
	Styler::styleLabel(ytdLabel);
	buttonLayout->addWidget(ytdLabel);
	buttonLayout->addStretch(1);

	QPushButton *sumTotalButton = new QPushButton(tr("Donor Totals"));
	Styler::styleButton(sumTotalButton);
	buttonLayout->addWidget(sumTotalButton);
	QPushButton *lastButton = new QPushButton(tr("Donor's Last"));
	Styler::styleButton(lastButton);
	buttonLayout->addWidget(lastButton);
	QPushButton *donationsButton = new QPushButton(tr("Donations"));
	Styler::styleButton(donationsButton);
	buttonLayout->addWidget(donationsButton);
#if (defined(Q_OS_ANDROID) || defined(Q_OS_IOS))
	buttonLayout->setAlignment(ytdLabel, Qt::AlignHCenter);
	buttonLayout->setAlignment(sumTotalButton, Qt::AlignHCenter);
	buttonLayout->setAlignment(lastButton, Qt::AlignHCenter);
	buttonLayout->setAlignment(donationsButton, Qt::AlignHCenter);
#endif
	buttonLayout->addStretch(4);
	layout->addStretch(1);

	connect(sumTotalButton, &QPushButton::clicked, this, &MissionFinancialsWidget::donorTotalsClicked);
	connect(lastButton, &QPushButton::clicked, this, &MissionFinancialsWidget::donorLastClicked);
	connect(donationsButton, &QPushButton::clicked, this, &MissionFinancialsWidget::donationsClicked);
#endif
}

void MissionFinancialsWidget::populateValues(QJsonArray jArray)
{
	int i = 0;
	monthView->setCurrentIndex(QModelIndex());  // clear selected index if copy-and-paste is attempted after view is refreshed with smaller model.
	missionFinancialsDataModel.layoutAboutToBeChanged();
	missionFinancialsDataModel.clear();
	for (auto jIt : jArray) {
		QJsonObject json = jIt.toObject();
		if (i < NumberOfMonthsInMissionView)
			missionFinancialsDataModel.append(jIt.toObject());
		else
			ytdLabel->setText(QString(tr("Year-to-Date: %1").arg(convertCentsToString(json.value("amount").toInt(), true))));
		i++;
	}
	missionFinancialsDataModel.layoutChanged();
	monthView->resizeColumnsToContents();
}

#if !((defined(Q_OS_ANDROID) || defined(Q_OS_IOS)))
void MissionFinancialsWidget::showDonorInfo(QJsonObject json)
{
	QString address = json.value("address").toString();
	if (address.isEmpty())
		addressLabel->setText("\n\n\n");
	else
		addressLabel->setText(address);
	emailLabel->setText(json.value("email").toString());
}

void MissionFinancialsWidget::clearDonorInfo()
{
	addressLabel->setText("\n\n\n");
	emailLabel->setText("");
	noteDisplay->clear();
}

void MissionFinancialsWidget::setNote(const QString &note)
{
	noteDisplay->setPlainText(note);
}

void MissionFinancialsWidget::keyPressEvent(QKeyEvent *e)
{
	if (e->type() == QKeyEvent::KeyPress && e->matches(QKeySequence::Copy))
		copyFromTable(monthView->selectionModel()->selectedIndexes());
}

#endif
