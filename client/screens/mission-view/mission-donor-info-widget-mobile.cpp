// clang-format off
/**************************************************************************************************
	Copyright (C) 2024  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include<QVBoxLayout>
#include<QLabel>
#include <QJsonObject>
#include "styler.h"
#include "mission-donor-info-widget-mobile.h"

MissionDonorInfoWidget::MissionDonorInfoWidget()
{
	QVBoxLayout *layout = new QVBoxLayout;
	setLayout(layout);

	layout->addStretch(1);

	addressLabel = new QLabel;
	Styler::styleLabel(addressLabel);
	layout->addWidget(addressLabel);
	layout->setAlignment(addressLabel, Qt::AlignHCenter);

	layout->addStretch(1);

	emailLabel = new QLabel;
	Styler::styleLabel(emailLabel);
	layout->addWidget(emailLabel);
	layout->setAlignment(emailLabel, Qt::AlignHCenter);

	layout->addStretch(1);
}

void MissionDonorInfoWidget::showDonorInfo(QJsonObject json)
{
	addressLabel->setText(json.value("address").toString());
	emailLabel->setText(json.value("email").toString());
}
