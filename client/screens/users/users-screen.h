// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef MDM_USERS_SCREEN_H_
#define MDM_USERS_SCREEN_H_

#include <QModelIndex>
#include "tab-screen.h"
#include "receiver.h"

class MainWindow;
class ConnectionManager;
class QPushButton;
class QLineEdit;
class QSortFilterProxyModel;
class QListView;
class QSlidingStackedWidget;
class UserInfo;
class UserPermissionsWidget;
class UserMissionsWidget;
class QExclusiveButtons;
class NameSelectorWidget;
class Permission;

class UsersScreen : public TabScreen, public Receiver
{
	Q_OBJECT

public:
	UsersScreen(MainWindow &mainWindow, Permission &p);
	void subscribe() override;
	void unsubscribe() override;

private:
	void processPublish(const QByteArray &topic, ssize_t count, FoxID id, FoxHead::ContentType contentType, const QByteArray &content) override;
	void processPostResponce(const QByteArray &topic, FoxID id, FoxHead::ContentType contentType, const QByteArray &content) override;
	void processAck(const QByteArray &topic) override;
	void processNack(const QByteArray &topic) override;
	void saveClicked();
	void newClicked();
	void cancelClicked();
	void userSelected(int nameId, QModelIndex index);
	void clearSelection();
	void doneEdits();
	void enableTopButtons(bool enabled);
	void okFromLockNack();
	bool checkForSave();
	void saveConfirmSaveClicked();
	void saveConfirmDiscardClicked();
	void saveConfirmCleanUp();
	void passwordResetClicked();
	void disconnectUserClicked();
	void passwordResetConfirmClicked();
	void passwordResetCleanUp();
	void selectorButtonClicked(int i);
	void sendRequestForCurrentTab();
	void sendRequestForInfoUpdate();
	void sendRequestForPermissionsUpdate();
	void sendRequestForMissionsUpdate();
	MainWindow &mainWindow;
	ConnectionManager &cm;
	QPushButton *newButton;
	QPushButton *editEnableButton;
	QPushButton *saveButton;
	QPushButton *cancelButton;
	QExclusiveButtons *selectorButtons;
	QSlidingStackedWidget *content;
	UserInfo *userInfo;
	UserPermissionsWidget *userPermissions;
	UserMissionsWidget *userMissions;
	NameSelectorWidget *userListView;
	bool userSelectedFlag = false;
	int userUnderEdit = -1;
	QModelIndex indexOfUserUnderEdit;
	bool newFlag = false;
	bool selectLastUser = false;

};

#endif
