// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QHBoxLayout>
#include <QPushButton>
#include <QListView>
#include <QLabel>
#include <QJsonArray>
#include "styler.h"
#include "theme.h"
#include "user-missions-widget.h"

UserMissionsWidget::UserMissionsWidget()
{
	QHBoxLayout *layout = new QHBoxLayout;
	setLayout(layout);

	layout->addStretch(1);

	QVBoxLayout *leftLayout = new QVBoxLayout;
	layout->addLayout(leftLayout);
	missionSelector = new NameSelectorWidget;
	leftLayout->addWidget(missionSelector);
	add = new QPushButton(tr("Add"));
	Styler::styleButton(add);
	leftLayout->addWidget(add);
	leftLayout->setAlignment(add, Qt::AlignHCenter);

	layout->addStretch(1);

	QVBoxLayout *rightLayout = new QVBoxLayout;
	layout->addLayout(rightLayout);
	QLabel *enabledMissionsLabel = new QLabel(tr("Enabled Missions"));
	Styler::styleLabel(enabledMissionsLabel, Theme::H2Size);
	rightLayout->addWidget(enabledMissionsLabel);
	rightLayout->setAlignment(enabledMissionsLabel, Qt::AlignHCenter);
	enabledMissionsView = new QListView;
	enabledMissionsView->setModel(&enalbedMissions);
	Styler::styleListView(enabledMissionsView);
	rightLayout->addWidget(enabledMissionsView);
	remove = new QPushButton(tr("Remove"));
	Styler::styleButton(remove);
	rightLayout->addWidget(remove);
	rightLayout->setAlignment(remove, Qt::AlignHCenter);

	layout->addStretch(1);

	setEnabled(false);
	connect(add, &QPushButton::clicked, this, &UserMissionsWidget::addClicked);
	connect(remove, &QPushButton::clicked, this, &UserMissionsWidget::removeClicked);
	connect(missionSelector, &NameSelectorWidget::nameSelected, this, &UserMissionsWidget::missionSelectedInAddList);
	connect(enabledMissionsView, &QListView::clicked, this, &UserMissionsWidget::missionSelectedInRemoveList);
}

void UserMissionsWidget::setEnabled(bool enabled)
{
	missionSelector->setEnabled(enabled);
	missionSelector->clearIndexAndSelection();
	enabledMissionsView->setEnabled(enabled);
	enabledMissionsView->setCurrentIndex(enalbedMissions.index(-1));
	if (!enabled) {
		add->setEnabled(false);
		remove->setEnabled(false);
	}
}

void UserMissionsWidget::missionSelectedInAddList(int missionID, QModelIndex)
{
	add->setEnabled(true);
	missionIdSelectedForAdd = missionID;
}

void UserMissionsWidget::missionSelectedInRemoveList(QModelIndex index)
{
	remove->setEnabled(true);
	indexSelectedForRemove = index.row();
}

void UserMissionsWidget::addClicked()
{
	AddOrRemoveClicked();
	if (!enalbedMissions.contains(missionIdSelectedForAdd)) {
		enalbedMissions.layoutAboutToBeChanged();
		enalbedMissions.append(missionIdSelectedForAdd, missionSelector->model().getFirstName(missionIdSelectedForAdd - 1), "");
		enalbedMissions.layoutChanged();
		flagChanged();
	}
}

void UserMissionsWidget::removeClicked()
{
	AddOrRemoveClicked();
	enalbedMissions.layoutAboutToBeChanged();
	enalbedMissions.removeAt(indexSelectedForRemove);
	enalbedMissions.layoutChanged();
	flagChanged();
}

void UserMissionsWidget::AddOrRemoveClicked()
{
	enabledMissionsView->setCurrentIndex(enalbedMissions.index(-1));
	missionSelector->clearIndexAndSelection();
	add->setEnabled(false);
	remove->setEnabled(false);
}

void UserMissionsWidget::updateEnabledMissions(QJsonArray json)
{
	isModified = false;
	enalbedMissions.layoutAboutToBeChanged();
	enalbedMissions.clear();
	for (auto jMission : json) {
		int id = jMission.toInt();
		enalbedMissions.append(id, missionSelector->model().getFirstName(id - 1), "");
	}
	enalbedMissions.layoutChanged();
}

QJsonArray UserMissionsWidget::buildJson()
{
	QJsonArray json;
	for (int i=0; i<enalbedMissions.size(); i++)
		json.append((int)enalbedMissions.getID(i));
	return json;
}

void UserMissionsWidget::flagChanged()
{
	if (!isModified) {
		isModified = true;
		emit changed();
	}
}
