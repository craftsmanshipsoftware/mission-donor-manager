// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QGroupBox>
#include <QCheckBox>
#include <QJsonObject>
#include "styler.h"
#include "theme.h"
#include "user-info-widget.h"

UserInfo::UserInfo()
{
	QGridLayout *layout = new QGridLayout;
	setLayout(layout);

	auto addLabel = [&](const QString &title, uint row) {
		QLabel *label = new QLabel(title);
		QLineEdit *lineEdit = new QLineEdit;
		Styler::styleLabel(label);
		Styler::styleLineEdit(lineEdit);
		layout->addWidget(label, row, 0);
		layout->addWidget(lineEdit, row, 1);
		return lineEdit;
	};

	auto addCheckBox = [&](const QString title, int pos) {
		QCheckBox *cb = new QCheckBox(title);
		Styler::styleCheckBox(cb);
		layout->addWidget(cb, pos, 1);
		return cb;
	};

	uint i = 0;
	first = addLabel(tr("First:"), i++);
	last = addLabel(tr("Last:"), i++);
	address = addLabel(tr("Address:"), i++);
	apt = addLabel(tr("Apt:"), i++);
	city = addLabel(tr("City:"), i++);
	province = addLabel(tr("Province:"), i++);
	province->setMaxLength(2);
	country = addLabel(tr("Country Code:"), i++);
	country->setMaxLength(2);
	postalCode = addLabel(tr("Postal Code:"), i++);
	email = addLabel(tr("Email:"), i++);
	cellPhone = addLabel(tr("Cell Phone:"), i++);
	homePhone = addLabel(tr("Home Phone:"), i++);
	workPhone = addLabel(tr("Work Phone:"), i++);
	enabled = addCheckBox(tr("Enabled"), i++);

	connect(first, &QLineEdit::textEdited, this, &UserInfo::flagChanged);
	connect(last, &QLineEdit::textEdited, this, &UserInfo::flagChanged);
	connect(address, &QLineEdit::textEdited, this, &UserInfo::flagChanged);
	connect(apt, &QLineEdit::textEdited, this, &UserInfo::flagChanged);
	connect(city, &QLineEdit::textEdited, this, &UserInfo::flagChanged);
	connect(province, &QLineEdit::textEdited, this, &UserInfo::flagChanged);
	connect(country, &QLineEdit::textEdited, this, &UserInfo::flagChanged);
	connect(postalCode, &QLineEdit::textEdited, this, &UserInfo::flagChanged);
	connect(email, &QLineEdit::textEdited, this, &UserInfo::flagChanged);
	connect(cellPhone, &QLineEdit::textEdited, this, &UserInfo::flagChanged);
	connect(homePhone, &QLineEdit::textEdited, this, &UserInfo::flagChanged);
	connect(workPhone, &QLineEdit::textEdited, this, &UserInfo::flagChanged);
	connect(enabled, &QCheckBox::clicked, this, &UserInfo::flagChanged);
}

void UserInfo::flagChanged()
{
	if (!isModified) {
		isModified = true;
		emit changed();
	}
}

void UserInfo::clear()
{
	first->clear();
	last->clear();
	address->clear();
	apt->clear();
	city->clear();
	province->clear();
	country->clear();
	postalCode->clear();
	email->clear();
	cellPhone->clear();
	homePhone->clear();
	workPhone->clear();
	enabled->setChecked(true);
}

void UserInfo::setReadOnly(bool ro)
{
	readOnly = ro;
	first->setReadOnly(ro);
	last->setReadOnly(ro);
	address->setReadOnly(ro);
	apt->setReadOnly(ro);
	city->setReadOnly(ro);
	province->setReadOnly(ro);
	country->setReadOnly(ro);
	postalCode->setReadOnly(ro);
	email->setReadOnly(ro);
	cellPhone->setReadOnly(ro);
	homePhone->setReadOnly(ro);
	workPhone->setReadOnly(ro);
	enabled->setEnabled(!ro);
}

void UserInfo::populateValues(QJsonObject json)
{
	isModified = false;

	first->setText(json.value("forename").toString());
	last->setText(json.value("surname").toString());
	address->setText(json.value("address").toString());
	apt->setText(json.value("apt").toString());
	city->setText(json.value("city").toString());
	province->setText(json.value("province").toString());
	country->setText(json.value("country").toString());
	postalCode->setText(json.value("postalcode").toString());
	email->setText(json.value("email").toString());
	cellPhone->setText(json.value("cellphone").toString());
	homePhone->setText(json.value("homephone").toString());
	workPhone->setText(json.value("workphone").toString());
	enabled->setChecked(json.value("is-enabled").toBool());
}

void UserInfo::buildJson(QJsonObject &json)
{
	json.insert("forename", first->text());
	json.insert("surname", last->text());
	json.insert("address", address->text());
	json.insert("apt", apt->text());
	json.insert("city", city->text());
	json.insert("province", province->text());
	json.insert("country", country->text());
	json.insert("postalcode", postalCode->text());
	json.insert("email", email->text());
	json.insert("cellphone", cellPhone->text());
	json.insert("homephone", homePhone->text());
	json.insert("workphone", workPhone->text());
	json.insert("is-enabled", enabled->isChecked());
}
