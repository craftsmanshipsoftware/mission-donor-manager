// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef MDM_USER_PERMISSIONS_WIDGET_H_
#define MDM_USER_PERMISSIONS_WIDGET_H_

#include <QWidget>
#include <QPushButton>

class QCheckBox;
class Permission;

class UserPermissionsWidget : public QWidget
{
	Q_OBJECT

public:
	UserPermissionsWidget(Permission &p);
	void clear();
	void setReadOnly(bool ro);
	void buildJson(QJsonObject &json);
	void populateValues(QJsonObject json);
	bool isReadOnly() { return readOnly; }
	void setPasswordResetButtonEnabled(bool enabled) { passwordResetButton->setEnabled(enabled); }
	bool isModified = false;

signals:
	void changed();
	void passwordResetClicked();
	void disconnectUserClicked();

private:
	void flagChanged();
	void clearCheckBoxesOnDisable(QCheckBox *toTest, const QList<QCheckBox *> &checkBoxesToClear);
	void setCheckBoxesOnEnable(QCheckBox *toTest, const QList<QCheckBox *> &checkBoxesToClear);
	void clearCheckBoxesOnEnable(QCheckBox *toTest, const QList<QCheckBox *> &checkBoxesToMakeOpposite);
	void userReadClicked();
	void userModifyClicked();
	void userPasswordResetClicked();
	void userForceLogOutClicked();
	void missionReadClicked();
	void missionModifyClicked();
	void donorReadClicked();
	void donorModifyClicked();
	void donationsClicked();
	void recurringDonationsClicked();
	void missionViewClicked();
	void allMissionViewClicked();
	QCheckBox *userRead;
	QCheckBox *userModify;
	QCheckBox *userPasswordReset;
	QCheckBox *userForceLogOut;
	QCheckBox *donorRead;
	QCheckBox *donorModify;
	QCheckBox *missionRead;
	QCheckBox *missionModify;
	QCheckBox *donations;
	QCheckBox *recurringDonations;
	QCheckBox *iifExport;
	QCheckBox *donorExport;
	QCheckBox *setup;
	QCheckBox *missionView;
	QCheckBox *allMissionView;
	QCheckBox *taxReceipts;
	QCheckBox *thankyou;
	QCheckBox *campaigns;
	QCheckBox *reports;
	QPushButton *passwordResetButton;
	QPushButton *disconnectButton;
	bool readOnly = true;
};

#endif
