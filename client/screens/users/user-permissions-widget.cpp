// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QVBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QCheckBox>
#include <QJsonObject>
#include "styler.h"
#include "theme.h"
#include "../common/permission.h"
#include "user-permissions-widget.h"

UserPermissionsWidget::UserPermissionsWidget(Permission &p)
{
	QVBoxLayout *layout = new QVBoxLayout;
	setLayout(layout);

	auto addCheckBox = [&](const QString name) {
		QCheckBox *cb = new QCheckBox(name);
		Styler::styleCheckBox(cb);
		return cb;
	};

	layout->addStretch(1);
	userRead = addCheckBox(tr("User Read"));
	layout->addWidget(userRead);
	userModify = addCheckBox(tr("User Modify"));
	layout->addWidget(userModify);
	userPasswordReset = addCheckBox(tr("User Password Reset"));
	layout->addWidget(userPasswordReset);
	userForceLogOut = addCheckBox(tr("User Force Log Out"));
	layout->addWidget(userForceLogOut);
	donorRead = addCheckBox(tr("Donor Read"));;
	layout->addWidget(donorRead);
	donorModify = addCheckBox(tr("Donor Modify"));;
	layout->addWidget(donorModify);
	donations = addCheckBox(tr("Manual Donations"));;
	layout->addWidget(donations);
	recurringDonations = addCheckBox(tr("Recurring Donations"));
	layout->addWidget(recurringDonations);
	missionRead = addCheckBox(tr("Mission Read Access"));
	layout->addWidget(missionRead);
	missionModify = addCheckBox(tr("Mission Modify"));
	layout->addWidget(missionModify);
	iifExport = addCheckBox(tr("IIF Export"));
	layout->addWidget(iifExport);
	donorExport = addCheckBox(tr("Donor Export"));
	layout->addWidget(donorExport);
	setup = addCheckBox(tr("Setup"));
	layout->addWidget(setup);
	campaigns = addCheckBox(tr("Campaigns"));
	layout->addWidget(campaigns);
	reports = addCheckBox(tr("Reports"));
	layout->addWidget(reports);
	missionView = addCheckBox(tr("Mission Financial View"));
	layout->addWidget(missionView);
	allMissionView = addCheckBox(tr("All Missions Financial View"));
	layout->addWidget(allMissionView);
	taxReceipts = addCheckBox(tr("Tax Receipts"));
	layout->addWidget(taxReceipts);
	thankyou = addCheckBox(tr("Thankyou"));
	layout->addWidget(thankyou);
	layout->addStretch(1);
	passwordResetButton = new QPushButton(tr("Reset Password"));
	Styler::styleButton(passwordResetButton);
	layout->addWidget(passwordResetButton);
	if (!p.hasPermission(Permission::UserPasswordReset))
		passwordResetButton->hide();
	layout->setAlignment(passwordResetButton, Qt::AlignLeft);
	layout->addStretch(1);

	disconnectButton = new QPushButton(tr("Force Log Out"));
	Styler::styleButton(disconnectButton);
	layout->addWidget(disconnectButton);
	if (!p.hasPermission(Permission::UserForceLogout))
		disconnectButton->hide();
	layout->setAlignment(disconnectButton, Qt::AlignLeft);
	layout->addStretch(1);

	connect(userRead, &QCheckBox::clicked, this, &UserPermissionsWidget::userReadClicked);
	connect(userModify, &QCheckBox::clicked, this, &UserPermissionsWidget::userModifyClicked);
	connect(userPasswordReset, &QCheckBox::clicked, this, &UserPermissionsWidget::userPasswordResetClicked);
	connect(userForceLogOut, &QCheckBox::clicked, this, &UserPermissionsWidget::userForceLogOutClicked);
	connect(missionRead, &QCheckBox::clicked, this, &UserPermissionsWidget::missionReadClicked);
	connect(missionModify, &QCheckBox::clicked, this, &UserPermissionsWidget::missionModifyClicked);
	connect(donorRead, &QCheckBox::clicked, this, &UserPermissionsWidget::donorReadClicked);
	connect(donorModify, &QCheckBox::clicked, this, &UserPermissionsWidget::donorModifyClicked);
	connect(donations, &QCheckBox::clicked, this, &UserPermissionsWidget::donationsClicked);
	connect(recurringDonations, &QCheckBox::clicked, this, &UserPermissionsWidget::recurringDonationsClicked);
	connect(iifExport, &QCheckBox::clicked, this, &UserPermissionsWidget::flagChanged);
	connect(donorExport, &QCheckBox::clicked, this, &UserPermissionsWidget::flagChanged);
	connect(setup, &QCheckBox::clicked, this, &UserPermissionsWidget::flagChanged);
	connect(campaigns, &QCheckBox::clicked, this, &UserPermissionsWidget::flagChanged);
	connect(reports, &QCheckBox::clicked, this, &UserPermissionsWidget::flagChanged);
	connect(missionView, &QCheckBox::clicked, this, &UserPermissionsWidget::missionViewClicked);
	connect(allMissionView, &QCheckBox::clicked, this, &UserPermissionsWidget::allMissionViewClicked);
	connect(taxReceipts, &QCheckBox::clicked, this, &UserPermissionsWidget::flagChanged);
	connect(thankyou, &QCheckBox::clicked, this, &UserPermissionsWidget::flagChanged);

	connect(passwordResetButton, &QPushButton::clicked, this, &UserPermissionsWidget::passwordResetClicked);
	connect(disconnectButton, &QPushButton::clicked, this, &UserPermissionsWidget::disconnectUserClicked);
}

void UserPermissionsWidget::flagChanged()
{
	if (!isModified) {
		isModified = true;
		emit changed();
	}
}

void UserPermissionsWidget::clearCheckBoxesOnDisable(QCheckBox *toTest, const QList<QCheckBox *> &checkBoxesToClear)
{
	if (!toTest->isChecked()) {
		for (auto cb : checkBoxesToClear)
			cb->setChecked(false);
	}
}

void UserPermissionsWidget::setCheckBoxesOnEnable(QCheckBox *toTest, const QList<QCheckBox *> &checkBoxesToClear)
{
	if (toTest->isChecked()) {
		for (auto cb : checkBoxesToClear)
			cb->setChecked(true);
	}
}

void UserPermissionsWidget::clearCheckBoxesOnEnable(QCheckBox *toTest, const QList<QCheckBox *> &checkBoxesToMakeOpposite)
{
	if (toTest->isChecked()) {
		for (auto cb : checkBoxesToMakeOpposite)
			cb->setChecked(false);
	}
}

void UserPermissionsWidget::userReadClicked()
{
	clearCheckBoxesOnDisable(userRead, QList<QCheckBox *>{userModify, userPasswordReset, userForceLogOut});
	flagChanged();
}

void UserPermissionsWidget::userModifyClicked()
{
	setCheckBoxesOnEnable(userModify, QList<QCheckBox *>{userRead});
	flagChanged();
}

void UserPermissionsWidget::userPasswordResetClicked()
{
	setCheckBoxesOnEnable(userPasswordReset, QList<QCheckBox *>{userRead});
	flagChanged();
}

void UserPermissionsWidget::userForceLogOutClicked()
{
	setCheckBoxesOnEnable(userForceLogOut, QList<QCheckBox *>{userRead});
	flagChanged();
}

void UserPermissionsWidget::missionReadClicked()
{
	clearCheckBoxesOnDisable(missionRead, QList<QCheckBox *>{missionModify});
	flagChanged();
}

void UserPermissionsWidget::missionModifyClicked()
{
	setCheckBoxesOnEnable(missionModify, QList<QCheckBox *>{missionRead});
	flagChanged();
}

void UserPermissionsWidget::donorReadClicked()
{
	clearCheckBoxesOnDisable(donorRead, QList<QCheckBox *>{donorModify, donations, recurringDonations});
	flagChanged();
}

void UserPermissionsWidget::donorModifyClicked()
{
	setCheckBoxesOnEnable(donorModify, QList<QCheckBox *>{donorRead});
	flagChanged();
}

void UserPermissionsWidget::donationsClicked()
{
	setCheckBoxesOnEnable(donations, QList<QCheckBox *>{donorRead});
	flagChanged();
}

void UserPermissionsWidget::recurringDonationsClicked()
{
	setCheckBoxesOnEnable(recurringDonations, QList<QCheckBox *>{donorRead});
	flagChanged();
}

void UserPermissionsWidget::missionViewClicked()
{
	clearCheckBoxesOnEnable(missionView, QList<QCheckBox *>{allMissionView});
	flagChanged();
}

void UserPermissionsWidget::allMissionViewClicked()
{
	clearCheckBoxesOnEnable(allMissionView, QList<QCheckBox *>{missionView});
	flagChanged();
}

void UserPermissionsWidget::clear()
{
	userRead->setChecked(false);
	userModify->setChecked(false);
	userPasswordReset->setChecked(false);
	userForceLogOut->setChecked(false);
	donorRead->setChecked(false);
	donorModify->setChecked(false);
	missionRead->setChecked(false);
	missionModify->setChecked(false);
	donations->setChecked(false);
	recurringDonations->setChecked(false);
	iifExport->setChecked(false);
	donorExport->setChecked(false);
	setup->setChecked(false);
	campaigns->setChecked(false);
	reports->setChecked(false);
	missionView->setChecked(false);
	allMissionView->setChecked(false);
	taxReceipts->setChecked(false);
	thankyou->setChecked(false);
}

void UserPermissionsWidget::setReadOnly(bool ro)
{
	readOnly = ro;
	userRead->setEnabled(!ro);
	userModify->setEnabled(!ro);
	userPasswordReset->setEnabled(!ro);
	userForceLogOut->setEnabled(!ro);
	donorRead->setEnabled(!ro);
	donorModify->setEnabled(!ro);
	missionRead->setEnabled(!ro);
	missionModify->setEnabled(!ro);
	donations->setEnabled(!ro);
	recurringDonations->setEnabled(!ro);
	iifExport->setEnabled(!ro);
	donorExport->setEnabled(!ro);
	setup->setEnabled(!ro);
	campaigns->setEnabled(!ro);
	reports->setEnabled(!ro);
	missionView->setEnabled(!ro);
	allMissionView->setEnabled(!ro);
	taxReceipts->setEnabled(!ro);
	thankyou->setEnabled(!ro);
}

void UserPermissionsWidget::populateValues(QJsonObject json)
{
	isModified = false;
	int permissions = json.value("permissions").toInt();
	userRead->setChecked(permissions & Permission::UserRead);
	userModify->setChecked(permissions & Permission::UserModify);
	userPasswordReset->setChecked(permissions & Permission::UserPasswordReset);
	userForceLogOut->setChecked(permissions & Permission::UserForceLogout);
	donorRead->setChecked(permissions & Permission::DonorRead);
	donorModify->setChecked(permissions & Permission::DonorModify);
	missionRead->setChecked(permissions & Permission::MissionRead);
	missionModify->setChecked(permissions & Permission::MissionModify);
	donations->setChecked(permissions & Permission::Donations);
	recurringDonations->setChecked(permissions & Permission::RecurringDonations);
	iifExport->setChecked(permissions & Permission::IifExport);
	donorExport->setChecked(permissions & Permission::DonorExport);
	setup->setChecked(permissions & Permission::Setup);
	campaigns->setChecked(permissions & Permission::Campaigns);
	reports->setChecked(permissions & Permission::Reports);
	missionView->setChecked(permissions & Permission::MissionView);
	allMissionView->setChecked(permissions & Permission::MissionViewAll);
	taxReceipts->setChecked(permissions & Permission::Tax);
	thankyou->setChecked(permissions & Permission::Thankyou);
}

void UserPermissionsWidget::buildJson(QJsonObject &json)
{
	int permissions = 0;
	if (userRead->isChecked())
		permissions|= Permission::UserRead;
	if (userModify->isChecked())
		permissions|= Permission::UserModify;
	if (userPasswordReset->isChecked())
		permissions|= Permission::UserPasswordReset;
	if (userForceLogOut->isChecked())
		permissions|= Permission::UserForceLogout;
	if (donorRead->isChecked())
		permissions|= Permission::DonorRead;
	if (donorModify->isChecked())
		permissions|= Permission::DonorModify;
	if (missionRead->isChecked())
		permissions|= Permission::MissionRead;
	if (missionModify->isChecked())
		permissions|= Permission::MissionModify;
	if (donations->isChecked())
		permissions|= Permission::Donations;
	if (recurringDonations->isChecked())
		permissions|= Permission::RecurringDonations;
	if (iifExport->isChecked())
		permissions|= Permission::IifExport;
	if (donorExport->isChecked())
		permissions|= Permission::DonorExport;
	if (setup->isChecked())
		permissions|= Permission::Setup;
	if (campaigns->isChecked())
		permissions|= Permission::Campaigns;
	if (reports->isChecked())
		permissions|= Permission::Reports;
	if (missionView->isChecked())
		permissions|= Permission::MissionView;
	if (allMissionView->isChecked())
		permissions|= Permission::MissionViewAll;
	if (taxReceipts->isChecked())
		permissions|= Permission::Tax;
	if (thankyou->isChecked())
		permissions|= Permission::Thankyou;
	json.insert("permissions", permissions);
}
