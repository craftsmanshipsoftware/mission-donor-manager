// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QListView>
#include <QTableView>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QSortFilterProxyModel>
#include "qslidingstackedwidget.h"
#include "main-window.h"
#include "connection-manager.h"
#include "../common/permission.h"
#include "user-input.h"
#include "styler.h"
#include "theme.h"
#include "centred-message-widget.h"
#include "user-info-widget.h"
#include "user-permissions-widget.h"
#include "user-missions-widget.h"
#include "qexclusivebuttons.h"
#include "name-selector-widget.h"
#include "users-screen.h"

#include <QDebug>

enum Content {
	SelectUser,
	Info,
	Permissions,
	Missions,
};

UsersScreen::UsersScreen(MainWindow &mainWindow, Permission &p) : mainWindow(mainWindow), cm(mainWindow.connectionManager)
{
	QVBoxLayout *layout = new QVBoxLayout;
	setLayout(layout);
	int m = Theme::OuterMargin * qMin(screen()->geometry().width(), screen()->geometry().height());
	layout->setContentsMargins(m, m, m, m);
	QHBoxLayout *topLayout = new QHBoxLayout;
	layout->addLayout(topLayout);
	QHBoxLayout *bottomLayout = new QHBoxLayout;
	layout->addLayout(bottomLayout);

	topLayout->addStretch(1);
	QLabel *title = new QLabel(tr("Users"));
	Styler::styleLabel(title, Theme::H1Size);
	topLayout->addWidget(title);
	topLayout->addStretch(1);
	selectorButtons = new QExclusiveButtons({tr("Info"), tr("Permissions"), tr("Missions")});
	topLayout->addWidget(selectorButtons);
	for ( int i=0; i < selectorButtons->size(); i++)
		Styler::styleButton(selectorButtons->buttonAt(i));
	enableTopButtons(false);

	// bottom layout
	QVBoxLayout *selectorLayout = new QVBoxLayout;
	bottomLayout->addLayout(selectorLayout);
	bottomLayout->setStretchFactor(selectorLayout, 2);
	bottomLayout->addStretch(1);
	QVBoxLayout *contentLayout = new QVBoxLayout;
	bottomLayout->addLayout(contentLayout);
	bottomLayout->setStretchFactor(contentLayout, 7);

	// selector sub layout
	userListView = new NameSelectorWidget;
	selectorLayout->addWidget(userListView);
	userListView->enabledIdShown(true);

	newButton = new QPushButton(tr("New"));
	Styler::styleButton(newButton);
	selectorLayout->addWidget(newButton);
	selectorLayout->setAlignment(newButton, Qt::AlignHCenter);
	if (!p.hasPermission(Permission::UserModify))
		newButton->hide();

	// conent sub layout
	content = new QSlidingStackedWidget();
	content->setDuration(500);
	contentLayout->addWidget(content);
	CentredMessageWidget *askSelect = new CentredMessageWidget(tr("Please Select a User"));
	content->addWidget(askSelect);
	userInfo = new UserInfo;
	content->addWidget(userInfo);
	userPermissions = new UserPermissionsWidget(p);
	content->addWidget(userPermissions);
	userMissions = new UserMissionsWidget;
	content->addWidget(userMissions);
	content->setCurrentIndex(Content::SelectUser);

	contentLayout->addStretch(1);
	QHBoxLayout *contentButtonLayout = new QHBoxLayout;
	contentLayout->addLayout(contentButtonLayout);
	contentButtonLayout->addStretch(1);
	editEnableButton = new QPushButton(tr("Edit"));
	Styler::styleButton(editEnableButton);
	editEnableButton->setEnabled(false);
	contentButtonLayout->addWidget(editEnableButton);
	contentButtonLayout->addStretch(1);
	cancelButton = new QPushButton(tr("Cancel"));
	Styler::styleButton(cancelButton);
	cancelButton->setEnabled(false);
	contentButtonLayout->addWidget(cancelButton);
	contentButtonLayout->addStretch(1);
	saveButton = new QPushButton(tr("Save"));
	Styler::styleButton(saveButton);
	saveButton->setEnabled(false);
	contentButtonLayout->addWidget(saveButton);
	if (!p.hasPermission(Permission::UserModify)) {
		editEnableButton->hide();
		cancelButton->hide();
		saveButton->hide();
	}
	contentButtonLayout->addStretch(1);

	connect(selectorButtons, &QExclusiveButtons::clicked, this, &UsersScreen::selectorButtonClicked);
	connect(saveButton, &QPushButton::clicked, this, &UsersScreen::saveClicked);
	connect(userInfo, &UserInfo::changed, [&]() {
		saveButton->setEnabled(true);
	});
	connect(userPermissions, &UserPermissionsWidget::changed, [&]() {
		saveButton->setEnabled(true);
	});
	connect(userMissions, &UserMissionsWidget::changed, [&]() {
		saveButton->setEnabled(true);
	});
	connect(userListView, &NameSelectorWidget::nameSelected, this, &UsersScreen::userSelected);
	connect(userListView, &NameSelectorWidget::filterModified, this, &UsersScreen::clearSelection);
	connect(newButton, &QPushButton::clicked, this, &UsersScreen::newClicked);
	connect(editEnableButton, &QPushButton::clicked, [&]() {
		cm.foxmq.post("users.lock", FoxHead::ContentType::IntAsString, QByteArray::number(userUnderEdit));
	});
	connect(cancelButton, &QPushButton::clicked, this, &UsersScreen::cancelClicked);
	connect(userPermissions, &UserPermissionsWidget::passwordResetClicked, this, &UsersScreen::passwordResetClicked);
	connect(userPermissions, &UserPermissionsWidget::disconnectUserClicked, this, &UsersScreen::disconnectUserClicked);

	topicsToReceive << "users" << "mission";
	cm.registerApiForReceiver(this);
}

void UsersScreen::subscribe()
{
	cm.foxmq.subscription("users.ro.list", true);
	cm.foxmq.subscription("users.ro.info-updated", true);
	cm.foxmq.subscription("users.ro.permissions-updated", true);
	cm.foxmq.subscription("users.ro.missions-updated", true);
	cm.foxmq.subscription("mission.ro.list", true);
}

void UsersScreen::unsubscribe()
{
	cm.foxmq.subscription("users.ro.list", false);
	cm.foxmq.subscription("users.ro.info-updated", false);
	cm.foxmq.subscription("users.ro.permissions-updated", false);
	cm.foxmq.subscription("users.ro.missions-updated", false);
	cm.foxmq.subscription("mission.ro.list", false);
}

void UsersScreen::userSelected(int nameId, QModelIndex index)
{
	userPermissions->setPasswordResetButtonEnabled(true);
	if (userUnderEdit == -1) {
		content->setCurrentIndex(Content::Info);
		selectorButtons->setChecked(Content::Info - 1, true);
	}
	if (nameId != userUnderEdit || !userSelectedFlag) {
		if (!checkForSave()) {
			indexOfUserUnderEdit = index;
			userUnderEdit = nameId;
			userInfo->setReadOnly(true);
			userPermissions->setReadOnly(true);
			editEnableButton->setEnabled(true);
			enableTopButtons(true);
			sendRequestForCurrentTab();  // query as per current tab
		}
	}
	userSelectedFlag = true;
}

void UsersScreen::clearSelection()
{
	userSelectedFlag = false;
	userListView->clearIndexAndSelection();
	userPermissions->setPasswordResetButtonEnabled(false);
}

bool UsersScreen::checkForSave()
{
	bool changed = false;
	if (userInfo->isModified || userPermissions->isModified || userMissions->isModified) {
		changed = true;
		connect(mainWindow.userInput, &UserInput::button1Clicked, this, &UsersScreen::saveConfirmDiscardClicked);
		connect(mainWindow.userInput, &UserInput::button2Clicked, this, &UsersScreen::saveConfirmCleanUp);
		connect(mainWindow.userInput, &UserInput::button3Clicked, this, &UsersScreen::saveConfirmSaveClicked);
		mainWindow.userInput->askForInput(tr("Changes have been made"), tr("Discard Changes"), tr("Continue Editing"), tr("      Save      "));
		mainWindow.showUserInput();
	} else if (cancelButton->isEnabled()) {
		doneEdits();  // editing has been enabled, but no changes, so unlock
	}
	return changed;
}

void UsersScreen::saveConfirmSaveClicked()
{
	saveClicked();
	saveConfirmCleanUp();
}

void UsersScreen::saveConfirmDiscardClicked()
{
	// on discard, refresh forms from server
	if (newFlag) {
		content->setCurrentIndex(Content::SelectUser);
	} else {
		if (userInfo->isModified)
			sendRequestForInfoUpdate();
		if (userPermissions->isModified)
			sendRequestForPermissionsUpdate();
		if (userMissions->isModified)
			sendRequestForMissionsUpdate();
	}
	saveConfirmCleanUp();
	doneEdits();
}

void UsersScreen::saveConfirmCleanUp()
{
	disconnect(mainWindow.userInput, &UserInput::button1Clicked, this, &UsersScreen::saveConfirmDiscardClicked);
	disconnect(mainWindow.userInput, &UserInput::button2Clicked, this, &UsersScreen::saveConfirmCleanUp);
	disconnect(mainWindow.userInput, &UserInput::button3Clicked, this, &UsersScreen::saveConfirmSaveClicked);
	if (!newFlag)
		userListView->setCurrentIndex(indexOfUserUnderEdit);
	userPermissions->setPasswordResetButtonEnabled(false);
	mainWindow.showMainScreen();
}

void UsersScreen::enableTopButtons(bool enabled)
{
	for (int i=0; i<selectorButtons->size(); i++)
		selectorButtons->setEnabled(i, enabled);
}


void UsersScreen::processPublish(const QByteArray &topic, ssize_t, FoxID, FoxHead::ContentType, const QByteArray &content)
{
	if (topic == "users.ro.list") {
		QJsonArray jArray = QJsonDocument::fromJson(content).array();
		userListView->updateNameList(jArray);
		if (selectLastUser) {
			selectLastUser = false;
			userListView->selectEndOfDataModel();
		}
		indexOfUserUnderEdit = userListView->indexOfId(userUnderEdit);  // QModelIndex indexOfUserUnderEdit is invalid from the updated list.  Fix using updated list
	} else if (topic == "users.ro.info-updated" && userInfo->isReadOnly()) {
		int userID = content.toInt();
		if (userID == userUnderEdit)
			sendRequestForInfoUpdate();
	} else if (topic == "users.ro.permissions-updated" && userInfo->isReadOnly()) {
		int userID = content.toInt();
		if (userID == userUnderEdit)
			sendRequestForPermissionsUpdate();
	} else if (topic == "users.ro.missions-updated" && userInfo->isReadOnly()) {
		int userID = content.toInt();
		if (userID == userUnderEdit)
			sendRequestForMissionsUpdate();
	} else if (topic == "mission.ro.list") {
		userMissions->updateMissionList(QJsonDocument::fromJson(content).array());
	}
}

void UsersScreen::processPostResponce(const QByteArray &topic, FoxID, FoxHead::ContentType, const QByteArray &content)
{
	if (topic == "users.ro.info") {
		userInfo->populateValues(QJsonDocument::fromJson(content).object());
	} else if (topic == "users.ro.permissions") {
		userPermissions->populateValues(QJsonDocument::fromJson(content).object());
	} else if (topic == "users.ro.missions") {
		userMissions->updateEnabledMissions(QJsonDocument::fromJson(content).array());
	}
}

void UsersScreen::processAck(const QByteArray &topic)
{
	if (topic == "users.lock") {
		userInfo->setReadOnly(false);  //fixme add permissions check
		userPermissions->setReadOnly(false);  //fixme add permissions check
		newButton->setEnabled(false);
		editEnableButton->setEnabled(false);
		cancelButton->setEnabled(true);
		userMissions->setEnabled(true);
	}
}

void UsersScreen::processNack(const QByteArray &topic)
{
	if (topic == "users.lock") {
		connect(mainWindow.userInput, &UserInput::button1Clicked, this, &UsersScreen::okFromLockNack);
		mainWindow.userInput->askForInput(tr("Another user is editing this user, try again later"));
		mainWindow.showUserInput();
	} else if (topic == "users.permissions-edit") {
		connect(mainWindow.userInput, &UserInput::button1Clicked, this, &UsersScreen::okFromLockNack);
		mainWindow.userInput->askForInput(tr("Permission change failed - You are likely trying to remove the last user with user modify"));
		mainWindow.showUserInput();
		sendRequestForPermissionsUpdate();
	} else if (topic == "users.info-edit") {
		connect(mainWindow.userInput, &UserInput::button1Clicked, this, &UsersScreen::okFromLockNack);
		mainWindow.userInput->askForInput(tr("User info change failed - You are likely trying to disabled the last user with user modify"));
		mainWindow.showUserInput();
		sendRequestForInfoUpdate();
	}
}

void UsersScreen::saveClicked()
{
	if (newFlag) {
		userListView->clearSearchFilter();
		QJsonDocument jdoc;
		QJsonObject json;
		userInfo->buildJson(json);
		jdoc.setObject(json);
		cm.foxmq.post("users.add", FoxHead::ContentType::Json, jdoc.toJson(QJsonDocument::Compact));
	} else {
		if (userInfo->isModified) {
			QJsonDocument jdoc;
			QJsonObject json;
			json.insert("id", userUnderEdit);
			userInfo->buildJson(json);
			jdoc.setObject(json);
			cm.foxmq.post("users.info-edit", FoxHead::ContentType::Json, jdoc.toJson(QJsonDocument::Compact));
		}
		if (userPermissions->isModified) {
			QJsonDocument jdoc;
			QJsonObject json;
			json.insert("id", userUnderEdit);
			userPermissions->buildJson(json);
			jdoc.setObject(json);
			cm.foxmq.post("users.permissions-edit", FoxHead::ContentType::Json, jdoc.toJson(QJsonDocument::Compact));
		}
		if (userMissions->isModified) {
			QJsonDocument jdoc;
			QJsonObject json;
			json.insert("id", userUnderEdit);
			json.insert("missions", userMissions->buildJson());
			jdoc.setObject(json);
			cm.foxmq.post("users.missions-edit", FoxHead::ContentType::Json, jdoc.toJson(QJsonDocument::Compact));
		}
	}
	if (newFlag)
		selectLastUser = true;
	doneEdits();
}

void UsersScreen::newClicked()
{
	clearSelection();
	userUnderEdit = -1;
	newFlag = true;
	userInfo->clear();
	userInfo->setReadOnly(false);
	content->setCurrentIndex(Content::Info);
	enableTopButtons(false);
	selectorButtons->setEnabled(Content::Info - 1, true);  // only enable Info
	newButton->setEnabled(false);
	editEnableButton->setEnabled(false);
	cancelButton->setEnabled(true);
	selectorButtons->buttonAt(Content::Info - 1)->setChecked(true);
}

void UsersScreen::cancelClicked()
{
	bool wasNew = newFlag;
	if (!checkForSave() && wasNew) {
		content->setCurrentIndex(Content::SelectUser);
		userListView->clearIndexAndSelection();
	}
}

void UsersScreen::doneEdits()
{
	saveButton->setEnabled(false);
	userPermissions->setReadOnly(true);
	userInfo->setReadOnly(true);
	userInfo->isModified = false;
	userPermissions->isModified = false;
	userMissions->isModified = false;
	newButton->setEnabled(true);
	cancelButton->setEnabled(false);
	userMissions->setEnabled(false);
	if (!newFlag) {
		enableTopButtons(true);
		editEnableButton->setEnabled(true);
		cm.foxmq.post("users.unlock", FoxHead::ContentType::IntAsString, QByteArray::number(userUnderEdit));
	} else {
		enableTopButtons(false);
	}
	newFlag = false;
}

void UsersScreen::okFromLockNack()
{
	disconnect(mainWindow.userInput, &UserInput::button1Clicked, this, &UsersScreen::okFromLockNack);
	mainWindow.showMainScreen();
}

void UsersScreen::passwordResetClicked()
{
	connect(mainWindow.userInput, &UserInput::button1Clicked, this, &UsersScreen::passwordResetConfirmClicked);
	connect(mainWindow.userInput, &UserInput::button2Clicked, this, &UsersScreen::passwordResetCleanUp);
	QString name = userListView->model().getFullName(userListView->mapToSourceRow(indexOfUserUnderEdit));
	mainWindow.userInput->askForInput(QString(tr("Are you sure you want to reset %1's password?")).arg(name), tr("Reset"), tr("Cancel"));
	mainWindow.showUserInput();
}

void UsersScreen::disconnectUserClicked()
{
	cm.foxmq.post("users.force-log-out", FoxHead::ContentType::IntAsString, QByteArray::number(userUnderEdit));
}

void UsersScreen::passwordResetConfirmClicked()
{
	passwordResetCleanUp();
	cm.foxmq.post("users.reset-password", FoxHead::ContentType::IntAsString, QByteArray::number(userUnderEdit));
}

void UsersScreen::passwordResetCleanUp()
{
	disconnect(mainWindow.userInput, &UserInput::button1Clicked, this, &UsersScreen::passwordResetConfirmClicked);
	disconnect(mainWindow.userInput, &UserInput::button2Clicked, this, &UsersScreen::passwordResetCleanUp);
	mainWindow.showMainScreen();
}

void UsersScreen::selectorButtonClicked(int i)
{
	content->setCurrentIndex(i + 1);
	sendRequestForCurrentTab();
}

void UsersScreen::sendRequestForCurrentTab()
{
	switch (selectorButtons->currentIndex() + 1) {  // clang-format off
		case Content::Info:        sendRequestForInfoUpdate();        break;
		case Content::Permissions: sendRequestForPermissionsUpdate(); break;
		case Content::Missions:    sendRequestForMissionsUpdate();    break;
	}  // clang-format on
}

void UsersScreen::sendRequestForInfoUpdate()
{
	cm.foxmq.post("users.ro.info", FoxHead::ContentType::IntAsString, QByteArray::number(userUnderEdit));
}

void UsersScreen::sendRequestForPermissionsUpdate()
{
	cm.foxmq.post("users.ro.permissions", FoxHead::ContentType::IntAsString, QByteArray::number(userUnderEdit));
}

void UsersScreen::sendRequestForMissionsUpdate()
{
	cm.foxmq.post("users.ro.missions", FoxHead::ContentType::IntAsString, QByteArray::number(userUnderEdit));
}
