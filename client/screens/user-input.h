// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef USER_INPUT_H
#define USER_INPUT_H

#include <QWidget>
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>

//class Device;
class MainWindow;


class UserInput : public QWidget
{
	Q_OBJECT

public:
	explicit UserInput(const MainWindow &mainWindow);
	void askForInput(QString msgLabel, QString b1Label = tr("OK"), QString b2Label = "", QString b3Label = "");
	inline void setMsgText(QString text) { msg->setText(text); }
	void enableTextInput(QString preset = "");
	void disconnectAll();
	inline QString text() { return lineEdit->text(); }

signals:
	void button1Clicked();
	void button2Clicked();
	void button3Clicked();

private:
	void paintEvent(QPaintEvent *pe);
	const MainWindow &mainWindow;
	QLabel *msg;
	QPushButton *button1;
	QPushButton *button2;
	QPushButton *button3;
	QLineEdit *lineEdit;
};

#endif
