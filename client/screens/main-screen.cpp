// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QScreen>
#include <QScrollArea>
#include <QJsonArray>
#include "highlighted-tab/qhighlightedtab.h"
#include "connection-manager.h"
#include "theme.h"
#include "donors-screen.h"
#include "users-screen.h"
#include "missions-setup-screen.h"
#include "donations-screen.h"
#include "recurring-screen.h"
#include "iif-export-screen.h"
#include "donor-export-screen.h"
#include "setup-screen.h"
#include "mission-view-screen.h"
#include "tax-screen.h"
#include "thankyou-screen.h"
#include "reports-screen.h"
#include "../common/permission.h"
#include "main-screen.h"

MainScreen::MainScreen(MainWindow &mainWindow) : mainWindow(mainWindow)
{
	QHBoxLayout *layout = new QHBoxLayout;
	setLayout(layout);
	layout->setContentsMargins(0, 0, 0, 0);
	tab = new QHighlightedTab;
	layout->addWidget(tab);
	stackWidget = new QStackedWidget;
	layout->addWidget(stackWidget);
	tab->setArrangementMode(QHighlightedTab::ArrangementMode::TabsFirstCentered);
	//tab->setTabSizePercent(0.05);
	tab->setBackgroundColor(Theme::TabAreaBackground);
	tab->setSelectedColors(Theme::TabSelected, Theme::Text, Theme::TabStrip, Theme::TabSelectedBackground);
	tab->setUnselectedColors(Theme::TabUnselected, Theme::TabUnselected);
	tab->setHoverColor(Theme::TabHover);

	connect(tab, &QHighlightedTab::tabSelected, this, &MainScreen::tabSelected);
	connect(tab, &QHighlightedTab::tabUnselected, this, &MainScreen::tabUnselected);
}

void MainScreen::tabSelected(int tabIndex)
{
#if defined(Q_OS_ANDROID) || defined(Q_OS_IOS)
	dynamic_cast<TabScreen *>(stackWidget->currentWidget())->selected();
#else
	stackWidget->setCurrentIndex(tabIndex);
	QScrollArea *sa = dynamic_cast<QScrollArea *>(stackWidget->currentWidget());
	dynamic_cast<TabScreen *>(sa->widget())->selected();
#endif
}

void MainScreen::tabUnselected(int tabIndex)
{
#if defined(Q_OS_ANDROID) || defined(Q_OS_IOS)
	dynamic_cast<TabScreen *>(stackWidget->widget(tabIndex))->unselected();
#else
	QScrollArea *sa = dynamic_cast<QScrollArea *>(stackWidget->widget(tabIndex));
	dynamic_cast<TabScreen *>(sa->widget())->unselected();
#endif
}

void MainScreen::buildFromPermissions(int permissions)
{
	auto addScreen = [&](QWidget *newScreen, const QString &name, const QString &toolTip, const char *unselectedIconFilename, const char *selectedIconFilename) {
#if defined(Q_OS_ANDROID) || defined(Q_OS_IOS)
		stackWidget->addWidget(newScreen);
#else
		QScrollArea *sa = new QScrollArea;
#ifdef Q_OS_LINUX
		sa->setStyleSheet("QScrollArea {background-color: transparent;}");
#else
		sa->setStyleSheet("QScrollArea { background: transparent; } QScrollArea > QWidget > QWidget { background: transparent; } ");
#endif
		sa->setWidgetResizable(true);
		sa->setFrameShape(QFrame::NoFrame);
		sa->setWidget(newScreen);
		stackWidget->addWidget(sa);
#endif
		tab->appendTab(name, unselectedIconFilename, selectedIconFilename);
		tab->setTabToolTip(tab->tabCount() - 1, toolTip);
	};

	// the app might be re-connecting to the server so clear previous before re-building
	tab->removeAllTabs();
	for (int i = stackWidget->count() - 1; i > -1; i--) {
		QWidget *widget = stackWidget->widget(i);
		stackWidget->removeWidget(widget);
		delete widget;
	}

	Permission p(permissions);
	if (p.hasPermission(Permission::MissionView) || p.hasPermission(Permission::MissionViewAll))
		addScreen(new MissionViewScreen(mainWindow, p), tr("My Msns"), tr("Mission Financials"), ":mission.svg", ":mission.svg");
#if !(defined(Q_OS_ANDROID) || defined(Q_OS_IOS))
	if (p.hasPermission(Permission::DonorRead))
		addScreen(new DonorScreen(mainWindow, p), tr("Donors"), tr("Edit Donors"), ":donors.svg", ":donors.svg");

	if (p.hasPermission(Permission::Donations))
		addScreen(new DonationsScreen(mainWindow), tr("Manual"), tr("Manual Donation"), ":donations.svg", ":donations.svg");

	if (p.hasPermission(Permission::RecurringDonations))
		addScreen(new RecurringScreen(mainWindow), tr("Recurring"), tr("Recurring Donations"), ":recurring.svg", ":recurring.svg");

	if (p.hasPermission(Permission::IifExport))
		addScreen(new IifExportScreen(mainWindow), tr("IIF Export"), tr("Export to Quickbooks"), ":export.svg", ":export.svg");

	if (p.hasPermission(Permission::Tax))
		addScreen(new TaxScreen(mainWindow), tr("Tax"), tr("Tax Receipts"), ":tax.svg", ":tax.svg");

	if (p.hasPermission(Permission::DonorExport))
		addScreen(new DonorExportScreen(mainWindow), tr("Export"), tr("Export Donor Contact Info"), ":email.svg", ":email.svg");

	if (p.hasPermission(Permission::UserRead))
		addScreen(new UsersScreen(mainWindow, p), tr("Users"), tr("Edit Users"), ":user.svg", ":user.svg");

	if (p.hasPermission(Permission::Setup) || p.hasPermission(Permission::Campaigns) || p.hasPermission(Permission::MissionRead))
		addScreen(new SetupScreen(mainWindow, p), tr("Setup"), tr("General Setup"), ":setup.svg", ":setup.svg");

	if (p.hasPermission(Permission::Thankyou))
		addScreen(new ThankyouScreen(mainWindow), tr("Thankyou"), tr("Thank you Replies"), ":thankyou.svg", ":thankyou.svg");

	if (p.hasPermission(Permission::Reports))
		addScreen(new ReportsScreen(mainWindow), tr("Reports"), tr("Download Reports"), ":reports.svg", ":reports.svg");
#endif
	if (stackWidget->count()) {
		tabSelected(0);
		tab->setCurrentTab(0);
	}
	if (stackWidget->count() == 1)
		tab->hide();  // only one screen so tab selector is not needed
}
