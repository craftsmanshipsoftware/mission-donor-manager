// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QDir>
#include "update-manager.h"
#include "downloader.h"

OtaDownloader::OtaDownloader(OtaUpdateManager &updateManager, OtaWorker &otaWorker) : updateManager(updateManager), otaWorker(otaWorker)
{
	retryDelayTimer.setSingleShot(true);
	connect(&retryDelayTimer, &QTimer::timeout, this, &OtaDownloader::startNextFile);
}

void OtaDownloader::downloadList(OtaFileListContainer &files)
{
	filesToDownload = files;
	currentFileIndex = 0;
	retryCounter = 0;
	QDir localCachepath(updateManager.localCachePath());
	if (!localCachepath.exists()) {
		localCachepath.mkpath(updateManager.localCachePath());
	}
	startNextFile();
}

void OtaDownloader::startNextFile()
{
	if (retryCounter)
		qInfo() << "OTA - Retry" << retryCounter << "for Download of" << filesToDownload.at(currentFileIndex).first;
	currentFailed = false;
	currentFile.setFileName(updateManager.localCachePath() + filesToDownload.at(currentFileIndex).second + ".zz");
	if (currentFile.open(QIODevice::WriteOnly)) {
		QNetworkRequest request(QUrl(updateManager.baseUrl + filesToDownload.at(currentFileIndex).second + ".zz"));
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
		request.setAttribute(QNetworkRequest::RedirectPolicyAttribute, QNetworkRequest::NoLessSafeRedirectPolicy);
#endif
		currentReply = updateManager.nam->get(request);
		connect(currentReply, &QNetworkReply::readyRead, this, &OtaDownloader::processCurrentDownloadReadReady);
		connect(currentReply, &QNetworkReply::finished, this, &OtaDownloader::processCurrentDownloadFinished);
		connect(currentReply, &QNetworkReply::errorOccurred, this, &OtaDownloader::processCurrentDownloadError);
	} else {
		qInfo("OTA - Can't write download file");
		emit error();
	}
}

void OtaDownloader::processCurrentDownloadReadReady()
{
	currentFile.write(currentReply->readAll());
}

void OtaDownloader::processCurrentDownloadFinished()
{
	if (currentFailed)
		return;

	currentFile.close();
	currentReply->deleteLater();
	qInfo() << "OTA - Downloaded -" << filesToDownload.at(currentFileIndex).first << '(' << filesToDownload.at(currentFileIndex).second << ')';
	currentFileIndex++;
	if (currentFileIndex < filesToDownload.size())
		startNextFile();
	else
		QMetaObject::invokeMethod(&otaWorker, "uncompressDownloadedFiles", Qt::QueuedConnection, Q_ARG(OtaFileListContainer, filesToDownload));
}

void OtaDownloader::processCurrentDownloadError()
{
	currentFile.close();
	currentReply->deleteLater();
	currentFailed  =true;
	if (retryCounter < std::size(retryBackOffTimes)) {
		retryDelayTimer.start(retryBackOffTimes[retryCounter]);
		retryCounter++;
	} else {
		qInfo("OTA - Download failed");
		emit error();
	}
}
