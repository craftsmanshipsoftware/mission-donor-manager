// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QApplication>
#include "update-manager.h"
#include "ota-worker.h"

OtaWorker::OtaWorker(OtaUpdateManager &otaUpdateManager) : updateManager(otaUpdateManager)
{
	qRegisterMetaType<OtaFileListContainer>("OtaFileListContainer");
	thread.setObjectName("OTA worker");
	moveToThread(&thread);
}

OtaWorker::~OtaWorker()
{
	thread.quit();
	thread.wait();
}

void OtaWorker::compareLocalFilesToManifest(QJsonObject manifestJson)
{
	OtaFileListContainer changedFiles;

#ifdef Q_OS_MAC
	// MacOS we need to ascend in the path until reaching the root of the app bundle.
	bundleDir = QApplication::applicationDirPath();
	QString bundleName = QFileInfo(qApp->applicationFilePath()).fileName();
	bundleName.append(".app");
	while (!bundleDir.dirName().isNull() && bundleDir.dirName() != bundleName)
		bundleDir.cdUp();
	QString appDirPath = bundleDir.path();
#else
	QString appDirPath = QApplication::applicationDirPath();
#endif

	for (auto jFile : manifestJson.keys()) {
		QString localFilename = appDirPath + '/' + jFile;
		QByteArray localHash = hashFile(localFilename);
		QByteArray manifestHash = manifestJson.value(jFile).toString().toUtf8();
		if (!updateManager.compareMd5s(localHash, manifestHash)) {
			changedFiles.append(QPair<QString, QByteArray>(jFile, manifestHash));
			if (jFile != "recovery.exe")  // exclude recovery from copying itself to installation dir
#ifdef Q_OS_WIN
				toCopy.append(jFile.toUtf8() + '\n');
#else
				toCopy.append(jFile);
#endif
		}
	}

	emit compareComplete(changedFiles);
}

void OtaWorker::compareLocalCache(OtaFileListContainer changedFiles)
{
	OtaFileListContainer downloadList;
	for (auto file : changedFiles) {
		QByteArray localHash = hashFile(updateManager.localCachePath() + file.first);
		if (!updateManager.compareMd5s(localHash, file.second)) {
			downloadList.append(file);
		} else {
			qInfo() << "OTA -" << file.first << "already in local cache";
		}
	}
	emit compareLocalCacheComplete(downloadList);
}

void OtaWorker::uncompressDownloadedFiles(OtaFileListContainer changedFiles)
{
	bool failure = false;
	for (auto file : changedFiles) {
		qInfo() << "OTA - Uncompressing" << file.first;
		if (file.first.contains('/')) {  // check if a dir is needed
			int pos = file.first.lastIndexOf('/');
			QString pathOnly = file.first.left(pos);
			QDir dir(updateManager.localCachePath() + '/' + pathOnly);
			if (!dir.exists())
				dir.mkpath(pathOnly);
		}
		QFile compressedFile(updateManager.localCachePath() + file.second + ".zz");
		if (!compressedFile.open(QIODevice::ReadOnly)) {
			failure = true;
			break;
		}
		QFile uncompressedFile(updateManager.localCachePath() + file.first);
		if (!uncompressedFile.open(QIODevice::WriteOnly)) {
			failure = true;
			break;
		}
		uncompressedFile.write(qUncompress(compressedFile.readAll()));
		compressedFile.close();
		uncompressedFile.close();
		uncompressedFile.flush();
		QFile::remove(updateManager.localCachePath() + file.second + ".zz");
	}

	if (failure)
		emit error();
	else
		verifyDownloads(changedFiles);
}

void OtaWorker::verifyDownloads(OtaFileListContainer downloadedFiles)
{
	bool failure = false;
	for (auto file : downloadedFiles) {
		QByteArray localHash = hashFile(updateManager.localCachePath() + file.first);
		if (!updateManager.compareMd5s(localHash, file.second)) {
			failure = true;
			qInfo() << "OTA - Verify" << file.first << "- Failed";
		} else {
			qInfo() << "OTA - Verify" << file.first << "- OK";
		}
	}

	if (failure)
		emit error();
	else
		emit verifyDownloadsComplete();
}

QByteArray OtaWorker::hashFile(QString filename)
{
	QByteArray hash;
	QFile file(filename);
	if (file.open(QIODevice::ReadOnly)) {
		hash = QCryptographicHash::hash(file.readAll(), QCryptographicHash::Md5).toHex();
		file.close();
	}
	return hash;
}
