// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef OTA_DOWNLOADER_H
#define OTA_DOWNLOADER_H

#include <QNetworkReply>
#include <QFile>
#include <QTimer>
#include "ota-common.h"

class OtaUpdateManager;
class OtaWorker;

class OtaDownloader : public QObject
{
	Q_OBJECT

public:
	OtaDownloader(OtaUpdateManager &updateManager, OtaWorker &otaWorker);
	void downloadList(OtaFileListContainer &filesToDownload);

signals:
	void error();

private:
	void startNextFile();
	void processCurrentDownloadReadReady();
	void processCurrentDownloadFinished();
	void processCurrentDownloadError();
	QTimer retryDelayTimer;
	OtaUpdateManager &updateManager;
	OtaWorker &otaWorker;
	OtaFileListContainer filesToDownload;
	int currentFileIndex;
	QNetworkReply *currentReply;
	QFile currentFile;
	size_t retryCounter;
	bool currentFailed;
	int retryBackOffTimes[5] = {5000, 25'000, 60'000, 180'000, 300'000};  // retry times
};

#endif
