// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef PERIODIC_CHECKER_H
#define PERIODIC_CHECKER_H

#include <QTime>
#include <QTimer>

class OtaUpdateManager;
class QNetworkReply;

class OtaPeriodicChecker : public QObject
{
	Q_OBJECT

public:
	OtaPeriodicChecker(OtaUpdateManager &otaUpdateManager);
	void setFeed(QString feed);
	void checkForUpdates();

private:
	void setupTimerForNextCheck();
	void periodicCheck();
	void processManifestMd5Reply();
	void processManifestMd5ReplyError();
	void processManifestReply();
	void processManifestReplyError();
	bool compareManifestMd5s();
	void writeManifestToCache();
	void removeCurrentManifestFromCache();
	OtaUpdateManager &otaUpdateManager;
	QTime nextCheckTime;
	QTimer periodicCheckTimer;
	QByteArray downloadedManifestMd5;
	QByteArray manifest;
	QString serverManifest;
	QNetworkReply *md5Reply;
	QNetworkReply *manifestReply;
	bool md5ReplyError;
	bool manifestReplyError;

friend OtaUpdateManager;
};

#endif
