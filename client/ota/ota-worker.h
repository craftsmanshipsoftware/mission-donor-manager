// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef OTA_WORKER_H
#define OTA_WORKER_H

#include <QThread>
#include <QJsonObject>
#include <QDir>
#include "ota-common.h"

class OtaUpdateManager;

class OtaWorker : public QObject
{
	Q_OBJECT

public:
	OtaWorker(OtaUpdateManager &otaUpdateManager);
	~OtaWorker();
	void enable() { thread.start(QThread::Priority::IdlePriority); }
	void disable() { thread.quit(); }
	Q_INVOKABLE void compareLocalFilesToManifest(QJsonObject manifestJson);
	Q_INVOKABLE void compareLocalCache(OtaFileListContainer changedFiles);
	Q_INVOKABLE void uncompressDownloadedFiles(OtaFileListContainer downloadedFiles);
	void verifyDownloads(OtaFileListContainer downloadedFiles);
#ifdef Q_OS_WIN
	QByteArray toCopy;
#else
	QStringList toCopy;
#endif
#ifdef Q_OS_MAC
	QDir bundleDir;
#endif

signals:
	void compareComplete(OtaFileListContainer changedFiles);
	void compareLocalCacheComplete(OtaFileListContainer filesNeeded);
	void verifyDownloadsComplete();
	void error();

private:
	QByteArray hashFile(QString filename);
	OtaUpdateManager &updateManager;
	QThread thread;

};

#endif
