// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QApplication>
#include <QDir>
#include <QDirIterator>
#include <QFile>
#include <QProcess>
#include <QJsonDocument>
#include <QJsonObject>
#include "update-manager.h"

#define FEED_FILE_NAME "feed.txt"

#define CHECK_ON_BOOT

OtaUpdateManager::OtaUpdateManager(const QString &url, const QString &otaID) : periodicChecker(*this), otaWorker(*this), downloader(*this, otaWorker)
{
	baseUrl = url;
	restartDelayTimer.setSingleShot(true);
	QDir checkLocalCache(localCachePath());
	if (!checkLocalCache.exists())
		checkLocalCache.mkdir(localCachePath());

	connect(&otaWorker, &OtaWorker::compareComplete, this, &OtaUpdateManager::compareComplete);
	connect(&otaWorker, &OtaWorker::compareLocalCacheComplete, this, &OtaUpdateManager::compareLocalCacheComplete);
	connect(&otaWorker, &OtaWorker::verifyDownloadsComplete, this, &OtaUpdateManager::finishedDownloadActivities);
	connect(&downloader, &OtaDownloader::error, this, &OtaUpdateManager::cacheError);
	connect(&otaWorker, &OtaWorker::error, this, &OtaUpdateManager::cacheError);
	connect(&restartDelayTimer, &QTimer::timeout, this, &OtaUpdateManager::restart);

	QString feed = otaID;
	if (!otaID.isEmpty()) {
		// write new ota ID to feed.txt in local cache
		QFile feedFile(localCachePath() + FEED_FILE_NAME);
		if (feedFile.open(QIODevice::WriteOnly)) {
			feedFile.write(otaID.toUtf8());
			feedFile.close();
		}
	} else {
		// read current feed from local cache
		if (QFile::exists(localCachePath() + FEED_FILE_NAME)) {
			QFile feedFile(localCachePath() + FEED_FILE_NAME);
			if (feedFile.open(QIODevice::ReadOnly)) {
				feed = feedFile.readLine(64);
				feedFile.close();
			}
		} else {  // if no feed file exists then feed is main / public
			feed = "main";
		}
	}
	periodicChecker.setFeed(feed);

#ifdef CHECK_ON_BOOT
	periodicChecker.checkForUpdates();
#else
	if (!otaID.isNull())
		periodicChecker.checkForUpdates();
	else
		periodicChecker.setupTimerForNextCheck();
#endif
}

void OtaUpdateManager::prepareForNewManifest()
{
	otaWorker.toCopy.clear();
	removedFiles.clear();
	update = false;
	currentState = State::Downloading;
	emit upgradeStateChanged(currentState);
}

void OtaUpdateManager::setNewManifest(QByteArray data)
{
	prepareForNewManifest();
	QJsonObject mdjObj = QJsonDocument::fromJson(data).object();
	QJsonObject jManifestFiles = mdjObj.value("files").toObject();
	otaWorker.enable();
	QMetaObject::invokeMethod(&otaWorker, "compareLocalFilesToManifest", Qt::QueuedConnection, Q_ARG(QJsonObject, jManifestFiles));
	checkForLocalFilesNotInManifest(jManifestFiles);
}

void OtaUpdateManager::compareComplete(OtaFileListContainer changedFiles)
{
#ifdef Q_OS_WIN
	if (changedFiles.size() > 1)  // recovery.exe is always flagged because it is not in the local, so must be > 1
#else
	if (changedFiles.size())
#endif
		update = true;
	QMetaObject::invokeMethod(&otaWorker, "compareLocalCache", Qt::QueuedConnection, Q_ARG(OtaFileListContainer, changedFiles));
}


void OtaUpdateManager::compareLocalCacheComplete(OtaFileListContainer filesNeeded)
{
	if (filesNeeded.size())
		downloader.downloadList(filesNeeded);
	else
		finishedDownloadActivities();
}

void OtaUpdateManager::checkForLocalFilesNotInManifest(QJsonObject manifestJson)
{
	QDirIterator currentFiles(QApplication::applicationDirPath(), QDirIterator::Subdirectories);
	while (currentFiles.hasNext()) {
		if (currentFiles.fileInfo().isFile()) {
			QString onlyRelative = currentFiles.filePath().remove(currentFiles.path());
			if (onlyRelative.startsWith('/') || onlyRelative.startsWith("\\"))
				onlyRelative.remove(0, 1);  // remove leeding /
			if (!manifestJson.contains(onlyRelative) && onlyRelative != "manifest.md5")
				removedFiles.append(onlyRelative.toUtf8() + '\n');
		}
		currentFiles.next();
	}
}

void OtaUpdateManager::finishedDownloadActivities()
{
	disableDownloadResources();

#ifdef Q_OS_WIN
	// write a list of files to copy for recovery.exe
	if (otaWorker.toCopy.size()) {
		QFile toCopy(localCachePath() + "to-copy.txt");
		if (!toCopy.open(QIODevice::WriteOnly)) {
			qInfo("OTA - Could not write to-copy.txt");
			return;
		}
		toCopy.write(QApplication::applicationDirPath().toUtf8() + '\n');  // first line is the path where to copy the files to (where the app was run from)
		toCopy.write(otaWorker.toCopy);
		toCopy.close();
	}

	// write a list of files to remove for recovery.exe
	if (removedFiles.size()) {
		QFile toRemove(localCachePath() + "to-remove.txt");
		if (!toRemove.open(QIODevice::WriteOnly)) {
			qInfo("OTA - Could not write to-remove.txt");
			return;
		}
		toRemove.write(QApplication::applicationDirPath().toUtf8() + '\n');  // first line is the path where to copy the files to (where the app was run from)
		toRemove.write(removedFiles);
		toRemove.close();
	}
#endif

	// write manifest to local cache last after all files were downloaded successfully
	periodicChecker.writeManifestToCache();

	if (update)
		currentState = State::UpdateReady;
	else
		currentState = State::NoUpdate;
	emit upgradeStateChanged(currentState);
}

void OtaUpdateManager::cacheError()
{
	qInfo("OTA - Cache Error Ocurred");
	update = false;
	disableDownloadResources();
	currentState = State::NoUpdate;
	emit upgradeStateChanged(currentState);
}

void OtaUpdateManager::disableDownloadResources()
{
	otaWorker.disable();
	nam.clear();
}


bool OtaUpdateManager::compareMd5s(const QByteArray &a, const QByteArray &b)
{
	constexpr int Md5textLength = 32;
	bool same = true;

	int min = qMin(Md5textLength, qMin(a.size(), b.size()));
	if (min < Md5textLength) {
		same = false;
	} else {
		for (int i=0; i<min; i++) {
			if (a.at(i) != b.at(i)) {
				same = false;
				break;
			}
		}
	}

	return same;
}

// ---------- Above this line is for preparing for the update, below this line is actually doing the update -----------

void OtaUpdateManager::startUpdate()
{
	qInfo("OTA - Upgrade Started");
	// count steps
	if (update)
		totalSteps++;
	currentStep = 1;
	currentState = State::Updating;
	emit upgradeStateChanged(currentState);
	emit upgradeProgressChanged(totalSteps, currentStep, 0.0f, tr("Restarting %1").arg(qApp->applicationDisplayName()));
	restartDelayTimer.start(2000);
}

void OtaUpdateManager::restart()
{
#ifdef Q_OS_WIN
	QProcess::startDetached(localCachePath() + "recovery.exe", QStringList(), localCachePath());
#endif
#ifdef Q_OS_MAC
	for (auto fileName : otaWorker.toCopy) {
		QFile::remove(otaWorker.bundleDir.path() + '/' + fileName);
		QFile::copy(localCachePath() + fileName,  otaWorker.bundleDir.path() + '/' + fileName);
		QString exeName = QFileInfo(qApp->applicationFilePath()).fileName();
		if (fileName.endsWith(exeName) || fileName.endsWith("dylib") || fileName.endsWith(".sh")) {
			QFile file(otaWorker.bundleDir.path() + '/' + fileName);
			file.setPermissions(QFile::ReadOwner | QFile::WriteOwner | QFile::ExeOwner | QFile::ReadGroup | QFile::ExeGroup | QFile::ReadOther | QFile::ExeOther);
		}
		QFile::remove(localCachePath() + fileName);  // remove from cache after copied to app bundle
	}
	QProcess::startDetached(otaWorker.bundleDir.path() + "/Contents/MacOS/recovery.sh", QStringList(), otaWorker.bundleDir.path() + "/Contents/MacOS");
#endif
	QApplication::quit();
}
