QT+= network

INCLUDEPATH+= $$PWD
HEADERS+= $$PWD/periodic-checker.h
SOURCES+= $$PWD/periodic-checker.cpp

HEADERS+= $$PWD/update-manager.h
SOURCES+= $$PWD/update-manager.cpp

HEADERS+= $$PWD/ota-worker.h
SOURCES+= $$PWD/ota-worker.cpp

HEADERS+= $$PWD/downloader.h
SOURCES+= $$PWD/downloader.cpp
