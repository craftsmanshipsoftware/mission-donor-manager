// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QApplication>
#include <QRandomGenerator>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include "update-manager.h"
#include "periodic-checker.h"

#ifdef Q_OS_WIN
#define ManifestFilename "manifest-win"
#elif defined (Q_OS_MAC)
#define ManifestFilename "manifest-mac"
#elif defined (Q_OS_LINUX)
#define ManifestFilename "manifest-linux"
#endif
#define ManifestFilenameExt ".zz"
#define ManifestMd5FilenameExt ".md5"

OtaPeriodicChecker::OtaPeriodicChecker(OtaUpdateManager &otaUpdateManager) : otaUpdateManager(otaUpdateManager)
{
	periodicCheckTimer.setSingleShot(true);
	connect(&periodicCheckTimer, &QTimer::timeout, this, &OtaPeriodicChecker::periodicCheck);
}

void OtaPeriodicChecker::setFeed(QString feed)
{
	serverManifest = ManifestFilename  "-" + feed;
}

void OtaPeriodicChecker::setupTimerForNextCheck()
{
	// The app ideally checks for updates at night time.  We also don't want to hit the server all at the same time and cause a DoS.
	// EarliestCheckHour sets the earliest hour the app can check
	// spreadCheckHours sets how much variation in hours the app can add past the EarliestCheckHour
	constexpr int EarliestCheckHour = 1;  // 1AM
	constexpr int SpreadCheckHours = 6;  // vary from 1AM to 7AM

	QTime t = QTime::currentTime();
	int ms = t.msecsTo(QTime(EarliestCheckHour, 0, 0));  // set earliest possible check
	if (ms < 0)  // if check happens after 1AM, api returns negative, so add 1 full day of ms
		ms+= 86'400'000;
	int randomMs = QRandomGenerator::global()->bounded(0, SpreadCheckHours * 3'600'000);  // add a random ammount to make the check happen over a range
	periodicCheckTimer.start(ms + randomMs);

	// store next check time for "was in standby check"
	nextCheckTime = QTime(EarliestCheckHour, 0, 0);
	nextCheckTime = nextCheckTime.addMSecs(randomMs);
	qInfo() << "OTA - check for update at" << nextCheckTime;
}

void OtaPeriodicChecker::periodicCheck()
{
	// we may been in standby
	int secondsDiff = nextCheckTime.secsTo(QTime::currentTime());
	if (secondsDiff > -15 && secondsDiff < 15) {
		// not in standby
		checkForUpdates();
	} else {
		// was in standby - random time to check to avoid continental wake up DoS
		int randomMs = QRandomGenerator::global()->bounded(0, 1'800'000);
		periodicCheckTimer.start(randomMs);
		nextCheckTime = QTime::currentTime();
		nextCheckTime = nextCheckTime.addMSecs(randomMs);
	}
}

void OtaPeriodicChecker::checkForUpdates()
{
	setupTimerForNextCheck();
	if (otaUpdateManager.state() == OtaUpdateManager::State::Updating)  // don't grab a new manifest if in the middle of an update
		return;
	QNetworkRequest request(QUrl(otaUpdateManager.baseUrl + serverManifest + ManifestMd5FilenameExt));
	md5ReplyError = false;
	otaUpdateManager.nam = QSharedPointer<QNetworkAccessManager>::create();
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
	request.setAttribute(QNetworkRequest::RedirectPolicyAttribute, QNetworkRequest::NoLessSafeRedirectPolicy);
#endif
	md5Reply = otaUpdateManager.nam->get(request);
	connect(md5Reply, &QNetworkReply::finished, this, &OtaPeriodicChecker::processManifestMd5Reply);
	connect(md5Reply, &QNetworkReply::errorOccurred, this, &OtaPeriodicChecker::processManifestMd5ReplyError);
}

void OtaPeriodicChecker::processManifestMd5ReplyError()
{
	qInfo("OTA - Unable to download maniftest md5sum");
	md5ReplyError = true;
	md5Reply->deleteLater();
	otaUpdateManager.disableDownloadResources();
}

void OtaPeriodicChecker::processManifestMd5Reply()
{
	bool disableDownloadResources = false;
	downloadedManifestMd5 = md5Reply->readAll().trimmed();
	if (!md5ReplyError && !compareManifestMd5s()) {
		qInfo("OTA - New upgrade maniftest on server - Downloading");
		QNetworkRequest request(QUrl(otaUpdateManager.baseUrl + serverManifest + ManifestFilenameExt));
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
		request.setAttribute(QNetworkRequest::RedirectPolicyAttribute, QNetworkRequest::NoLessSafeRedirectPolicy);
#endif
		manifestReplyError = false;
		manifestReply = otaUpdateManager.nam->get(request);
		connect(manifestReply, &QNetworkReply::finished, this, &OtaPeriodicChecker::processManifestReply);
		connect(manifestReply, &QNetworkReply::errorOccurred, this, &OtaPeriodicChecker::processManifestReplyError);
	} else {
		if (md5ReplyError)
			qInfo("OTA - Failed to download manifest's MD5");

		// load current manifest from cache
		QFile localManifest(otaUpdateManager.localCachePath() +  ManifestFilename ManifestFilenameExt);
		if (localManifest.open(QIODevice::ReadOnly)) {
			qInfo("OTA - Using manifest in local cache");
			manifest = localManifest.readAll();
			otaUpdateManager.setNewManifest(qUncompress(manifest));
			localManifest.close();
		} else {
			qInfo("OTA - Failed to open manifest in local cache");
			disableDownloadResources = true;
			 // delete the local md5 if the local manifest does not exist for some reason.  This forces a re-downlaod next time
			QFile::remove(otaUpdateManager.localCachePath() +  ManifestFilename ManifestMd5FilenameExt);
		}
	}
	md5Reply->deleteLater();
	if (disableDownloadResources)
		otaUpdateManager.disableDownloadResources();
}

void OtaPeriodicChecker::processManifestReplyError()
{
	qInfo("OTA - Unable to download maniftest");
	manifestReplyError = true;
	manifestReply->deleteLater();
	otaUpdateManager.disableDownloadResources();
}

void OtaPeriodicChecker::processManifestReply()
{
	if (manifestReplyError)
		return;
	QByteArray manifestCompressed = manifestReply->readAll();
	QByteArray hash = QCryptographicHash::hash(manifestCompressed, QCryptographicHash::Md5).toHex();
	if (otaUpdateManager.compareMd5s(hash, downloadedManifestMd5))
	{
		qInfo("OTA - Downloaded Maniftest passed MD5sum check");
		removeCurrentManifestFromCache();  // invalidate cache while downloading new stuff
		manifest = manifestCompressed;
		otaUpdateManager.setNewManifest(qUncompress(manifestCompressed));
	} else {
		qInfo("OTA - Downloaded Maniftest failed MD5sum check - try again tomorrow");
	}
	manifestReply->deleteLater();
}

bool OtaPeriodicChecker::compareManifestMd5s()
{
	bool same = true;

	QFile currentManifestMd5File(otaUpdateManager.localCachePath() + ManifestFilename ManifestMd5FilenameExt);
	if (currentManifestMd5File.open(QIODevice::ReadOnly)) {
		QByteArray currentManifestMd5 = currentManifestMd5File.readAll().trimmed();
		currentManifestMd5File.close();
		same = otaUpdateManager.compareMd5s(currentManifestMd5, downloadedManifestMd5);
	} else {
		same = false;
	}

	return same;
}

void OtaPeriodicChecker::writeManifestToCache()
{
	QFile manifestFile(otaUpdateManager.localCachePath() +  ManifestFilename ManifestFilenameExt);
	if (manifestFile.open(QIODevice::WriteOnly)) {
		manifestFile.write(manifest);
		manifestFile.close();
	}

	// md5 must be written last
	QFile manifestMd5File(otaUpdateManager.localCachePath() +  ManifestFilename ManifestMd5FilenameExt);
	if (manifestMd5File.open(QIODevice::WriteOnly)) {
		manifestMd5File.write(downloadedManifestMd5);
		manifestMd5File.close();
	}
}

void OtaPeriodicChecker::removeCurrentManifestFromCache()
{
	QFile::remove(otaUpdateManager.localCachePath() +  ManifestFilename ManifestMd5FilenameExt);
	QFile::remove(otaUpdateManager.localCachePath() +  ManifestFilename ManifestFilenameExt);
}
