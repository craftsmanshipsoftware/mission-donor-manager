// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef UPDATE_MANAGER_H
#define UPDATE_MANAGER_H

#include <QNetworkAccessManager>
#include <QStandardPaths>
#include <QTimer>
#include "periodic-checker.h"
#include "ota-worker.h"
#include "downloader.h"

class OtaUpdateManager : public QObject
{
	Q_OBJECT

public:
	enum State {
		NoUpdate,
		Downloading,
		UpdateReady,
		Updating,
	};
	OtaUpdateManager(const QString &url, const QString &otaID);
	static QString localCachePath() { return QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + '/'; }
	void prepareForNewManifest();
	void startUpdate();
	inline State state() { return currentState; }
	QSharedPointer<QNetworkAccessManager> nam;

signals:
	void upgradeStateChanged(State state);
	void upgradeProgressChanged(int totalSteps, int step, float stepPercent, QString updateText);

private:
	void setNewManifest(QByteArray data);
	void checkForLocalFilesNotInManifest(QJsonObject manifestJson);
	bool compareMd5s(const QByteArray &a, const QByteArray &b);
	void compareComplete(OtaFileListContainer changedFiles);
	void compareLocalCacheComplete(OtaFileListContainer filesNeeded);
	void finishedDownloadActivities();
	void cacheError();
	void disableDownloadResources();
	OtaPeriodicChecker periodicChecker;
	OtaWorker otaWorker;
	OtaDownloader downloader;
	QString baseUrl;
	QByteArray removedFiles;
	State currentState;
	bool update;

	// stuff for performing the upgrade
	void restart();
	QTimer restartDelayTimer;
	int totalSteps = 0;
	int currentStep;

	friend OtaPeriodicChecker;
	friend OtaWorker;
	friend OtaDownloader;
};

#endif
