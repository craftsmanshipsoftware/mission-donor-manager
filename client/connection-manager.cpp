// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QStandardPaths>
#include <QDir>
#include <QJsonDocument>
#include <QJsonObject>
#include <QRandomGenerator>
#if defined(Q_OS_ANDROID) || defined(Q_OS_IOS)
#include <QApplication>
#endif
#include "../common/common.h"
#include "main-screen.h"
#include "connection-manager.h"
#include "qsslutils.h"

//#define SHOW_INCOMING
#ifdef SHOW_INCOMING
#include <QDebug>
#endif

// push notification
//   https://medium.com/@shigmas/push-notifications-in-qt-for-ios-and-android-50c2f9a38444

ConnectionManager::ConnectionManager() : mainWindow(*this)
{
	configPathString = QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation) + '/';
	QDir configPath(configPathString);
	if (!configPath.exists()) {
		configPath.mkdir(configPathString);
	}

	connectTimer.setSingleShot(true);
#if defined(Q_OS_ANDROID) || defined(Q_OS_IOS)
	connect(qApp, &QGuiApplication::applicationStateChanged, this, &ConnectionManager::appStateChanged);
#endif
	connect(&foxmq, &FoxMQ::ready, this, &ConnectionManager::denReady);
	connect(&connectTimer, &QTimer::timeout, this, &ConnectionManager::denEntryFailed);
	connect(&foxmq, &FoxMQ::publish, this, &ConnectionManager::processScream);
	connect(&foxmq, &FoxMQ::postResponse, this, &ConnectionManager::processQueryResponce);
	connect(&foxmq, &FoxMQ::ack, this, &ConnectionManager::processAck);
	connect(&foxmq, &FoxMQ::nack, this, &ConnectionManager::processNack);
	connect(&foxmq, &FoxMQ::closed, this, &ConnectionManager::serverDisconnected);

#ifdef SHOW_INCOMING
	connect(&foxmq, &FoxMQ::errorNoPermission, [=](const QByteArray &key) {
		qDebug() << "No Permission for" << key;
	});
	connect(&foxmq, &FoxMQ::errorUnknownTopic, [=](const QByteArray &key) {
		qDebug() << "Unknown for" << key;
	});
	connect(&foxmq, &FoxMQ::error, [=](const QByteArray &key) {
		qDebug() << "ERROR for" << key;
	});
#endif

	// Login screen signals
	connect(&login, &LoginScreen::databasePasswordSet, this, &ConnectionManager::databasePasswordSet);
	connect(&login, &LoginScreen::databasePasswordEntered, this, &ConnectionManager::databasePasswordEntered);
	connect(&login, &LoginScreen::passwordEntered, this, &ConnectionManager::passwordEntered);
	connect(&login, &LoginScreen::urlEntered, this, &ConnectionManager::urlEntered);
	connect(&login, &LoginScreen::tryConnectionAgain, this, &ConnectionManager::connectToHost);
	connect(&login, &LoginScreen::changeUrlClicked, this, &ConnectionManager::populateUrlFromDisk);
	connect(&login, &LoginScreen::changePassword, this, &ConnectionManager::changePassword);
	connect(&login, &LoginScreen::forgotPassword, this, &ConnectionManager::forgotPassword);
	connect(&login, &LoginScreen::oneTimeCode, this, &ConnectionManager::oneTimeCode);

	// load or generate Ssl certificate and key
	QByteArray privateKeyPEM;
	QByteArray certificatePem;
	if (QFile::exists(configPathString + "cert.pem") && QFile::exists(configPathString + "key.pem")) {
		QFile keyFile(configPathString + "key.pem");
		keyFile.open(QIODevice::ReadOnly);
		privateKeyPEM = keyFile.readAll();
		keyFile.close();
		QFile certificateFile(configPathString + "cert.pem");
		certificateFile.open(QIODevice::ReadOnly);
		certificatePem = certificateFile.readAll();
		certificateFile.close();
	} else {
		// generate key + cert for encryption (not authentication)
		QSslEvpKey priKey = QSslUtils::generateRSAKey(4096);
		int rn = QRandomGenerator::system()->bounded(0, 999'999);  // generate randon serial number for cert
		QSslX509 x509 = QSslUtils::createCA(priKey, "CA", "ON", "Toronto", "Mission Donor Manager Client", "mission-dm", rn, 36500);
		privateKeyPEM = QSslUtils::RSAKeyToPEM(priKey);
		certificatePem = QSslUtils::certificateToPEM(x509);

		QFile keyFile(configPathString + "key.pem");
		keyFile.open(QIODevice::WriteOnly);
		keyFile.write(privateKeyPEM);
		keyFile.close();
		QFile certificateFile(configPathString + "cert.pem");
		certificateFile.open(QIODevice::WriteOnly);
		certificateFile.write(certificatePem);
		certificateFile.close();
	}
	QSslKey qKey(privateKeyPEM, QSsl::Rsa, QSsl::Pem, QSsl::PrivateKey);
	sslConfiguration.setPrivateKey(qKey);
	sslConfiguration.setLocalCertificate(QSslCertificate(certificatePem));
	sslConfiguration.setPeerVerifyMode(QSslSocket::VerifyNone);

	initialShow();
}

void ConnectionManager::initialShow()
{
	// load host file or ask for host info if doesn't exist
	if (QFile::exists(configPathString + "host.json"))
		connectToHost();
	else
		login.showAskForURL("", 11011);
}

ConnectionManager::~ConnectionManager()
{
	disconnect(&foxmq, &FoxMQ::closed, this, &ConnectionManager::serverDisconnected);
}

void ConnectionManager::urlEntered(QString url, int port)
{
	saveHostInfo(url, port);
	connectToHost();
}

void ConnectionManager::connectToHost()
{
	QFile hostfile(configPathString + "host.json");
	hostfile.open(QIODevice::ReadOnly);
	QJsonObject json = QJsonDocument::fromJson(hostfile.readAll()).object();
	QString host = json.value("host").toString();
	int port = json.value("port").toInt();
	login.showConnecting();
	if (foxmq.isConnected())
		foxmq.disconnectFromDen();
	foxmq.connectToDen(host.toUtf8(), port, sslConfiguration);
	connectTimer.start(2000);
}

void ConnectionManager::serverDisconnected()
{
	login.showDisconnected();
	login.show();
	mainWindow.hide();
	receivers.clear();
}

void ConnectionManager::denReady()
{
	foxmq.subscription("server.state", true);
}

void ConnectionManager::denEntryFailed()
{
	login.showNoConnection();
}

void ConnectionManager::saveHostInfo(QString ip, int port)
{
	QJsonObject jObj;
	jObj.insert("host", ip);
	jObj.insert("port", port);
	QFile file(configPathString + "host.json");
	if (file.open(QIODevice::WriteOnly)) {
		QJsonDocument doc(jObj);
		file.write(doc.toJson());
		file.close();
	}
}

void ConnectionManager::populateUrlFromDisk()
{
	QFile hostfile(configPathString + "host.json");
	hostfile.open(QIODevice::ReadOnly);
	QJsonObject json = QJsonDocument::fromJson(hostfile.readAll()).object();
	login.showAskForURL(json.value("host").toString(), json.value("port").toInt());
}

void ConnectionManager::databasePasswordSet(QString pwd)
{
	foxmq.post("server.set-pwd", FoxHead::ContentType::String, pwd.toUtf8());
}

void ConnectionManager::databasePasswordEntered(QString pwd)
{
	foxmq.post("server.pwd", FoxHead::ContentType::String, pwd.toUtf8());
}

void ConnectionManager::passwordEntered(QString email, QString pwd)
{
	QJsonDocument jDoc;
	QJsonObject jObj;
	jObj.insert("email", email);
	jObj.insert("password", pwd);
	jDoc.setObject(jObj);
	foxmq.post("auth.login", FoxHead::ContentType::Json, jDoc.toJson());
}

void ConnectionManager::changePassword(QString email, QString currentPwd, QString newPwd)
{
	QJsonDocument jDoc;
	QJsonObject jObj;
	jObj.insert("email", email);
	jObj.insert("password", currentPwd);
	jObj.insert("new-password", newPwd);
	jDoc.setObject(jObj);
	foxmq.post("auth.change-pwd", FoxHead::ContentType::Json, jDoc.toJson());
}

void ConnectionManager::forgotPassword(QString email)
{
	QJsonDocument jDoc;
	QJsonObject jObj;
	jObj.insert("email", email);
	jDoc.setObject(jObj);
	foxmq.post("auth.reset.request", FoxHead::ContentType::Json, jDoc.toJson());
}

void ConnectionManager::oneTimeCode(QString code)
{
	QJsonDocument jDoc;
	QJsonObject jObj;
	jObj.insert("code", code);
	jDoc.setObject(jObj);
	foxmq.post("auth.reset.code", FoxHead::ContentType::Json, jDoc.toJson());
}

void ConnectionManager::processScream(const QByteArray &topic, ssize_t count, FoxID id, FoxHead::ContentType contentType, const QByteArray &content)
{
#ifdef SHOW_INCOMING
	qDebug() << "processScream" << topic;
#endif
	if (topic == "server.state") {
		connectTimer.stop();
		switch (content.toUInt()) {  // clang-format off
		    case ServerState::NoDBPwd:        login.setDataBasePassword();   break;
		    case ServerState::NeedDecryptPwd: login.enterDataBasePassword(); break;
		    case ServerState::NeedFirstUser:
		    case ServerState::Ready:          login.showAskForPassword();    break;
		}  // clang-format on
	} else {
		for (auto receiver : receivers) {
			if (receiver->shouldReceive(topic))
				receiver->processPublish(topic, count, id, contentType, content);
		}
	}
}

void ConnectionManager::processQueryResponce(const QByteArray &topic, FoxID id, FoxHead::ContentType contentType, const QByteArray &content)
{
#ifdef SHOW_INCOMING
	qDebug() << "processQueryResponce" << topic;
#endif
	if (topic == "auth.login" || topic == "auth.change-pwd") {
		mainWindow.mainScreen->buildFromPermissions(content.toUInt());
		login.hide();
		mainWindow.show();
	} else {
		for (auto receiver : receivers) {
			if (receiver->shouldReceive(topic))
				receiver->processPostResponce(topic, id, contentType, content);
		}
	}
}

void ConnectionManager::processAck(const QByteArray &topic)
{
#ifdef SHOW_INCOMING
	qDebug() << "ACK for" << topic;
#endif
	if (topic == "auth.reset.code") {
		login.showCodeSucceeded();
	} else {
		for (auto receiver : receivers) {
			if (receiver->shouldReceive(topic))
				receiver->processAck(topic);

		}
	}
}

void ConnectionManager::processNack(const QByteArray &topic)
{
#ifdef SHOW_INCOMING
	qDebug() << "nack" << topic;
#endif
	if (topic == "auth.login" || topic == "auth.change-pwd") {
		login.showLoginFailed();
	} else if (topic == "auth.reset.code") {
		login.showCodeFailled();
	} else {
		for (auto receiver : receivers) {
			if (receiver->shouldReceive(topic))
				receiver->processNack(topic);
		}
	}
}

#if defined(Q_OS_ANDROID) || defined(Q_OS_IOS)
void ConnectionManager::appStateChanged(Qt::ApplicationState state)
{
	if (state == Qt::ApplicationSuspended) {  // iOS and Android reject network connections in standby.
		foxmq.disconnectFromDen();
		receivers.clear();
		mainWindow.hide();
		login.showConnecting();
		login.show();
	}
	else if (state == Qt::ApplicationActive) {
		initialShow();
	}
}
#endif
