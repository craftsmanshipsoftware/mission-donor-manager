#!/bin/bash -e

version_string=$(git describe --dirty --tags)

echo "#define VERSION \"$version_string\"" > version.h.next
if ! diff -q version.h.next version.h &> /dev/null; then
	echo "Set version $version_string"
	mv version.h.next version.h
else
	rm version.h.next
fi
