// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef MDM_NAME_LIST_DM_H_
#define MDM_NAME_LIST_DM_H_

#include <QAbstractListModel>

class NameListDataModel : public QAbstractListModel
{
	Q_OBJECT

public:
	int rowCount(const QModelIndex &parent = QModelIndex()) const override ;
	int columnCount(const QModelIndex &parent = QModelIndex()) const override;
	QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
	void clear();
	void append(QJsonObject jDonor);
	void append(int id, const QString &first, const QString &last);
	bool contains(int id);
	uint getID(uint row) { return nameList.at(row).id; }
	inline const QString &getFirstName(uint row) { return nameList.at(row).firstName; }
	inline const QString getFullName(uint row) { return nameList.at(row).firstName + ' ' + nameList.at(row).lastName; }
	inline int size() { return nameList.size(); }
	inline void removeAt(uint row) { nameList.removeAt(row); }
	inline void enabledIdShown(bool show) { showId = show; }

private:
	struct NameListItem {
		int id;
		QString firstName;
		QString lastName;
	};
	QList<NameListItem> nameList;
	bool showId = false;
};

#endif
