#!/bin/bash  -x

mkdir mission.iconset

rsvg-convert -h 1024 mission.svg > mission.iconset/icon_512x512@2x.png
rsvg-convert -h 512 mission.svg > mission.iconset/icon_512x512.png
rsvg-convert -h 512 mission.svg > mission.iconset/icon_256x256@2x.png
rsvg-convert -h 256 mission.svg > mission.iconset/icon_256x256.png
rsvg-convert -h 256 mission.svg > mission.iconset/icon_128x128@2x.png
rsvg-convert -h 128 mission.svg > mission.iconset/icon_128x128.png
rsvg-convert -h 64 mission.svg > mission.iconset/icon_32x32@2x.png
rsvg-convert -h 32 mission.svg > mission.iconset/icon_32x32.png
rsvg-convert -h 32 mission.svg > mission.iconset/icon_16x16@2x.png
rsvg-convert -h 16 mission.svg > mission.iconset/icon_16x16.png

iconutil -c icns mission.iconset
