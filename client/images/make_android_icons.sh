#!/bin/bash

mkdir -p ../android/res/drawable-ldpi
mkdir -p ../android/res/drawable-mdpi
mkdir -p ../android/res/drawable-hdpi
mkdir -p ../android/res/drawable-xhdpi
mkdir -p ../android/res/drawable-xxhdpi
mkdir -p ../android/res/drawable-xxxhdpi

convert -background transparent mission.svg -resize 32 ../android/res/drawable-ldpi/icon.png
convert -background transparent mission.svg -resize 48 ../android/res/drawable-mdpi/icon.png
convert -background transparent mission.svg -resize 72 ../android/res/drawable-hdpi/icon.png
convert -background transparent mission.svg -resize 96 ../android/res/drawable-xhdpi/icon.png
convert -background transparent mission.svg -resize 144 ../android/res/drawable-xxhdpi/icon.png
convert -background transparent mission.svg -resize 192 ../android/res/drawable-xxxhdpi/icon.png
