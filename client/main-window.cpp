// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include "qcolorablesvg.h"
#include "connection-manager.h"
#include "theme.h"
#include "user-input.h"
#include "main-screen.h"
#include "main-window.h"

MainWindow::MainWindow(ConnectionManager &connectionManager) : connectionManager(connectionManager)
{
	mainWidgetStack.setStyleSheet(QString("QStackedWidget {background-color: rgb(%1, %2, %3);}").arg(Theme::MainBackground.red()).arg(Theme::MainBackground.green()).arg(Theme::MainBackground.blue()));
	setCentralWidget(&mainWidgetStack);
	userInput = new UserInput(*this);
	mainWidgetStack.addWidget(userInput);
	mainScreen = new MainScreen(*this);
	mainWidgetStack.addWidget(mainScreen);
	showMainScreen();

	QIcon windowIcon(QColorableSvg::color(QString(":/mission.svg"), 256, 256, Theme::IconColor));
	setWindowIcon(windowIcon);
	setWindowState(Qt::WindowMaximized);
}
