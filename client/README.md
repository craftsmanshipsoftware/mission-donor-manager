The Mission Donor Manager client connects to the daemon using a REST [API](http://www.craftsmanshipsoftware.ca/mission-donor-manager/api/) over the [FoxMQ](https://gitlab.com/craftsmanshipsoftware/foxmq) protocol.  All information shown in the client comes from the daemon.  The client uses model views to display most data.

![ALT](../doc/images/comm-birds-eye-view.svg)

The client’s source code is organized in the following directory structure.  Each subdirectory under the screens directory is a screen in the main top level tab.  All sub widgets associated with a particular  tab are contained in the same subdirectory.

> [ota](client/ota/) - classes for managing over the air upgrades.<br>
> screens<br>
> > [about-screen.cpp](client/screens/about-screen.cpp) - UI to show credits.  Apart of the login screen...not on main tab.<br>
> > [common-widgets](client/screens/common-widgets/) - name says it all.<br>
> > [donations](client/screens/donations/) - Manual donations entry tab.<br>
> > [donor-export](client/screens/donor-export/) - Donor contact info exports tab.<br>
> > [donors](client/screens/donors/) - Create donors, modify contact and communication preferences.<br>
> > [iif-export](client/screens/iif-export/) - Tab for grouping donations and then exporting / downloading the iif to Quick Books.<br>
> > [login.cpp](client/screens/login.cpp) - UI for login.<br>
> > [main-screen.cpp](client/screens/main-screen.cpp) - Holds the main top level tab selector and what ever widget is currently selected.<br>
> > [missions-setup](client/screens/missions-setup/) - Create and modify missions.<br>
> > [mission-view](client/screens/mission-view/) - Mission Donation summaries.<br>
> > [recurring](client/screens/recurring/) - Setup and modify recurring donations, example, from Pre Authorized Debt bank transfer.<br>
> > [setup](client/screens/setup/) - General setup tab, income accounts setup, advantage accounts setup.<br>
> > [tax](client/screens/tax/) - T3010 tax summary, tax receipt generation, tax receipt email sending, signatory management.<br>
> > [thankyou](client/screens/thankyou/) - Create and modify thank you responses.<br>
> > [user-input.cpp](client/screens/user-input.cpp) - Global Discard / Continue / Cancel input<br>
> > [users](client/screens/users/) - Create users (for staff and missionaries), modify permissions and associated missions.<br>

The primary widget for each screen under the main top level tab is also sub-classed from a Receiver class.  The connection manager dispatches incoming publishes and post-responses to the correct screen for processing.

## Building

[Building for Linux](http://www.craftsmanshipsoftware.ca/mission-donor-manager/build-client.html#linux)<br>
[Building for Mac](http://www.craftsmanshipsoftware.ca/mission-donor-manager/build-client.html#mac)<br>
[Building for Windows](http://www.craftsmanshipsoftware.ca/mission-donor-manager/build-client.html#windows)<br>