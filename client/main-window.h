// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef MDM_MAIN_WINDOW_H_
#define MDM_MAIN_WINDOW_H_

#include <QMainWindow>
#include <QStackedWidget>

class ConnectionManager;
class UserInput;
class MainScreen;

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	MainWindow(ConnectionManager &connectionManager);
	QStackedWidget mainWidgetStack;
	inline void showLoginScreen() { mainWidgetStack.setCurrentIndex(0); userInputOpen = false; }
	inline void showMainScreen() { mainWidgetStack.setCurrentIndex(1); userInputOpen = false; }
	inline void showUserInput() { mainWidgetStack.setCurrentIndex(0); userInputOpen = true; }
	inline bool userInputIsOpen() { return userInputOpen; }
	ConnectionManager &connectionManager;
	UserInput *userInput;
	MainScreen *mainScreen;

private:
	bool userInputOpen = false;
};

#endif
