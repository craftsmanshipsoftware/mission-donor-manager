// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QColor>

namespace Theme {
constexpr QColor IconColor = QColor(65, 150, 211);
constexpr QColor MainBackground = QColor(60, 60, 60);
constexpr QColor Text = QColor(255, 255, 255);
constexpr QColor TextDisabled = QColor(160, 160, 160);
constexpr QColor Hightlight = QColor(152, 169, 238);

constexpr QColor FormBackground = QColor(40, 40, 40);
constexpr QColor ButtonBackground = QColor(0, 0, 0);
// tab colours
constexpr QColor TabAreaBackground = FormBackground;
constexpr QColor TabStrip = QColor(255, 255, 255);
constexpr QColor TabSelected = Hightlight;
constexpr QColor TabUnselected = QColor(200, 200, 200);
constexpr QColor TabSelectedBackground = QColor(0, 0, 0);
constexpr QColor TabHover = QColor(80, 80, 80);

constexpr float OuterMargin = 0.025;  // percentage of screen geometry, smaller dimention

#if (defined(Q_OS_ANDROID) || defined(Q_OS_IOS))
constexpr uint H1Size = 22;
constexpr uint H2Size = 16;
#else
constexpr uint H1Size = 18;
constexpr uint H2Size = 14;
#endif

constexpr uint ButtonPadding = 20;     // pixels
constexpr uint ListLineSpacing = 8;    // pixels?
}


