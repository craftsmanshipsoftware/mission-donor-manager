// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QPrinter>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include "../common/receipt-renderer.h"
#include "pdf-writer.h"

void PDFWriter::write(const QString &fileName, const QByteArray &data)
{
	struct SignatorData {
		QImage signature;
		QString name;
	};

	ReceiptRenderer receiptRenderer;
	QPrinter printer;
	QMap<int, SignatorData> signators;
	printer.setOutputFormat(QPrinter::PdfFormat);
	printer.setResolution(300);
	printer.setOutputFileName(fileName);

	QJsonObject json = QJsonDocument::fromJson(data).object();

	QJsonArray jSignators = json.value("signators").toArray();
	for (int i = 0; i < jSignators.size(); i++) {
		QJsonObject jSignator = jSignators.at(i).toObject();
		int id = jSignator.value("id").toInt();
		QByteArray imageData = QByteArray::fromBase64(jSignator.value("signature").toString().toUtf8());
		signators.insert(id, {.signature = QImage::fromData(imageData), .name = jSignator.value("name").toString()});
	}

	receiptRenderer.name = json.value("name").toString();
	receiptRenderer.phone = json.value("phone").toString();
	receiptRenderer.email = json.value("email").toString();
	receiptRenderer.craNumber = json.value("cra-number").toString();
	receiptRenderer.addressLine1 = json.value("address-line-1").toString();
	receiptRenderer.addressLine2 = json.value("address-line-2").toString();
	receiptRenderer.addressLine3 = json.value("address-line-3").toString();
	receiptRenderer.banner = QImage::fromData(QByteArray::fromBase64(json.value("banner").toString().toUtf8()));
	receiptRenderer.issueLocation = json.value("issue-location").toString();

	QJsonArray jArray = json.value("receipts").toArray();
	int pages = jArray.size();

	receiptRenderer.start(&printer);
	for (int i = 0; i < jArray.size(); i++) {
		QJsonObject receipt = jArray.at(i).toObject();

		receiptRenderer.donorName = receipt.value("donor-name").toString();
		receiptRenderer.donorAddressLine1 = receipt.value("donor-address-l1").toString();
		receiptRenderer.donorApt = receipt.value("donor-apt").toString();
		receiptRenderer.donorCity = receipt.value("donor-city").toString();
		receiptRenderer.donorProvince = receipt.value("donor-province").toString();
		receiptRenderer.donorPostalCode = receipt.value("donor-postalcode").toString();
		receiptRenderer.donorCountry = receipt.value("donor-country").toString();
		receiptRenderer.donationNote = receipt.value("donation-note").toString();
		receiptRenderer.donationDate = receipt.value("donation-date").toInt();

		receiptRenderer.type = receipt.value("type").toInt();
		receiptRenderer.receiptId = receipt.value("id").toInt();
		receiptRenderer.dateIssued = QDate::fromJulianDay(receipt.value("date-issued").toInt());
		receiptRenderer.year = receipt.value("year").toInt();
		receiptRenderer.amount = receipt.value("amount").toInt();
		receiptRenderer.advantage = receipt.value("advantage").toInt();
		int signatorId = receipt.value("signator-id").toInt();

		receiptRenderer.signatorName = signators.value(signatorId).name;
		receiptRenderer.signatorSignature = signators.value(signatorId).signature;

		receiptRenderer.renderPage();
		pages--;
		if (pages)
			printer.newPage();
	}
	receiptRenderer.end();
}
