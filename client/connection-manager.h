// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef MDM_CONNECTION_MANAGER_H_
#define MDM_CONNECTION_MANAGER_H_
#include <QObject>
#include <QSslKey>
#include <QSslCertificate>
#include <QTimer>
#include <QJsonObject>
#include "foxmq.h"
#include "main-window.h"
#include "receiver.h"
#include "login.h"

class ConnectionManager : public QObject
{
	Q_OBJECT
public:
	explicit ConnectionManager();
	~ConnectionManager();
	void saveHostInfo(QString ip, int port);
	void registerApiForReceiver(Receiver *receiver) { receivers.append(receiver); }
	FoxMQ foxmq;

private:
	void processScream(const QByteArray &topic, ssize_t count, FoxID id, FoxHead::ContentType contentType, const QByteArray &content);
	void processQueryResponce(const QByteArray &topic, FoxID id, FoxHead::ContentType contentType, const QByteArray &content);
	void processAck(const QByteArray &topic);
	void processNack(const QByteArray &topic);
	void urlEntered(QString url, int port);
	void initialShow();
	void connectToHost();
	void denReady();
	void denEntryFailed();
	void populateUrlFromDisk();
	void databasePasswordSet(QString pwd);
	void databasePasswordEntered(QString pwd);
	void passwordEntered(QString email, QString pwd);
	void changePassword(QString email, QString currentPwd, QString newPwd);
	void forgotPassword(QString email);
	void oneTimeCode(QString code);
	void serverDisconnected();
#if defined(Q_OS_ANDROID) || defined(Q_OS_IOS)
	void appStateChanged(Qt::ApplicationState state);
#endif
	QList<Receiver *> receivers;
	MainWindow mainWindow;
	QString configPathString;
	QSslConfiguration sslConfiguration;
	QTimer connectTimer;
	QJsonObject host;
	LoginScreen login;
};

#endif
