// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QJsonObject>
#include "name-list-dm.h"

int NameListDataModel::rowCount(const QModelIndex &) const
{
	return nameList.size();
}

int NameListDataModel::columnCount(const QModelIndex &) const
{
	return 2;
}


QVariant NameListDataModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	QVariant text;

	if (role != Qt::DisplayRole)
		return QVariant();

	if (orientation == Qt::Horizontal) {
		switch(section) {  // clang format off
			case 0: text = QString(tr("First")); break;
			case 1: text = QString(tr("Last")); break;
		}  // clang format on
	} else {
		text = QVariant(QString());
	}

	return text;
}

QVariant NameListDataModel::data(const QModelIndex &index, int role) const
{
	QVariant item;
	if (role == Qt::DisplayRole) {
		QString text = nameList.at(index.row()).firstName + " " + nameList.at(index.row()).lastName;
		if (showId)
			text+= QString(" - %1").arg(nameList.at(index.row()).id);
		item = text;
	}
	return item;
}

void NameListDataModel::clear()
{
	nameList.clear();
}

void NameListDataModel::append(QJsonObject jDonor)
{
	struct NameListItem dli;
	dli.id = jDonor.value("id").toInt();
	dli.firstName = jDonor.value("forename").toString();
	dli.lastName = jDonor.value("surname").toString();
	nameList.append(dli);
}

void NameListDataModel::append(int id, const QString &first, const QString &last)
{
	struct NameListItem dli;
	dli.id = id;
	dli.firstName = first;
	dli.lastName = last;
	nameList.append(dli);
}

bool NameListDataModel::contains(int id)
{
	bool found = false;
	for (auto item : nameList) {
		if (item.id == id) {
			found = true;
			break;
		}
	}

	return found;
}
