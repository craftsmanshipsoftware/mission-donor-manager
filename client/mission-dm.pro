QT += core gui widgets network
TARGET	= MissionDM
CONFIG+= c++17

SOURCES+= main.cpp

HEADERS+= connection-manager.h
SOURCES+= connection-manager.cpp

HEADERS+= main-window.h
SOURCES+= main-window.cpp

HEADERS+= styler.h
SOURCES+= styler.cpp

HEADERS+= receiver.h
SOURCES+= receiver.cpp

HEADERS+= name-list-dm.h
SOURCES+= name-list-dm.cpp

HEADERS+= pdf-writer.h
SOURCES+= pdf-writer.cpp

HEADERS+= copy-and-paste.h
SOURCES+= copy-and-paste.cpp

HEADERS+= theme.h

include(../common/common.pri)

# FoxMQ
include(../foxmq/client/client.pri)
DEFINES+= QFOXMQ_STATIC

# QtCraftsmanshipWidgets
include(../qtcraftsmanshipwidgets/qtcraftsmanshipwidgets.pri)
DEFINES+= QCRAFTSMANSHIPWIDGETS_STATIC

# QtSslUtils
include(../QtSslUtils/qtsslutils.pri)
DEFINES+= QSSLUTILS_STATIC

!android: include(ota/ota.pri)
include(screens/screens.pri)

RESOURCES+= images/images.qrc

win32 {
	RC_ICONS = images/MissionDM.ico
	RESOURCES+= fonts/fonts.qrc
	RESOURCES+= images/win-icon.qrc
}

macx {
	QMAKE_APPLE_DEVICE_ARCHS = $$MAC_ARCH  # is set to x86_64 or arm64 or both if your Qt build supports
	QMAKE_INFO_PLIST = Info.plist
	ICON = images/mission.icns  # this is built manually in images/make_mac_icons.sh
	RESOURCES+= fonts/fonts.qrc
	INCLUDEPATH+= $$OPENSSL_INCLUDEPATH
	LIBS+= -L$$OPENSSL_LIBS
}

# auto generate a version.h file
linux:!android {
	TO_GENERATE = $$PWD/version.h
	custom_target.output  = $$PWD/version.h
	custom_target.commands = 'cd $$PWD; $$PWD/set_version.sh'
	custom_target.depends = FORCE
	custom_target.input = TO_GENERATE
	QMAKE_EXTRA_COMPILERS += custom_target
}

android {
	greaterThan(QT_MAJOR_VERSION, 5) {
		INCLUDEPATH+= $$ANDROID_SDK_ROOT/android_openssl/ssl_3/include/
		equals(ANDROID_TARGET_ARCH, armeabi-v7a) {
			LIBS+= -L$$ANDROID_SDK_ROOT/android_openssl/ssl_3/armeabi-v7a/
			ANDROID_EXTRA_LIBS+= $$ANDROID_SDK_ROOT/android_openssl/ssl_3/armeabi-v7a/libcrypto_3.so /tk/android-sdk/android_openssl/ssl_3/armeabi-v7a/libssl_3.so
		}
		equals(ANDROID_TARGET_ARCH, arm64-v8a) {
			LIBS+= -L$$ANDROID_SDK_ROOT/android_openssl/ssl_3/arm64-v8a/
			ANDROID_EXTRA_LIBS+= $$ANDROID_SDK_ROOT/android_openssl/ssl_3/arm64-v8a/libcrypto_3.so /tk/android-sdk/android_openssl/ssl_3/arm64-v8a/libssl_3.so
		}
	} else {
		INCLUDEPATH+= $$ANDROID_SDK_ROOT/android_openssl/ssl_1.1/include/
		equals(ANDROID_TARGET_ARCH, armeabi-v7a) {
			LIBS+= -L$$ANDROID_SDK_ROOT/android_openssl/ssl_1.1/armeabi-v7a/
			ANDROID_EXTRA_LIBS+= $$ANDROID_SDK_ROOT/android_openssl/ssl_1.1/armeabi-v7a/libcrypto_1_1.so $$ANDROID_SDK_ROOT/android_openssl/ssl_1.1/armeabi-v7a/libssl_1_1.so
		}
		equals(ANDROID_TARGET_ARCH, arm64-v8a) {
			LIBS+= -L$$ANDROID_SDK_ROOT/android_openssl/ssl_1.1/arm64-v8a/
			ANDROID_EXTRA_LIBS+= $$ANDROID_SDK_ROOT/android_openssl/ssl_1.1/arm64-v8a/libcrypto_1_1.so $$ANDROID_SDK_ROOT/android_openssl/ssl_1.1/arm64-v8a/libssl_1_1.so
		}
	}

	ANDROID_TARGET_SDK_VERSION = 34
	ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
	DISTFILES+= android/AndroidManifest.xml
}
