// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QApplication>
#include <QScroller>
#include <QLabel>
#include <QCheckBox>
#include <QRadioButton>
#include <QListView>
#include <QTableView>
#include <QLineEdit>
#include <QPlainTextEdit>
#include <QPushButton>
#include <QGroupBox>
#include <QComboBox>
#include <QProgressBar>
#include "theme.h"
#include "styler.h"

int fontSize;
int buttonPadding;
void Styler::setup()
{
#ifdef Q_OS_WIN
	constexpr int MinFontSize = 11;
#elif (defined(Q_OS_ANDROID) || defined(Q_OS_IOS))
	constexpr int MinFontSize = 12;
#else
	constexpr int MinFontSize = 10;
#endif

	// check min font size
	fontSize = qApp->font().pointSize();
	QFont f = qApp->font();
	if (fontSize < MinFontSize) {
		fontSize = MinFontSize;
	}
	f.setPointSize(fontSize);
	qApp->setFont(f);

	buttonPadding = Theme::ButtonPadding;
}

void Styler::styleMainWidget(QWidget *widget)
{
	widget->setStyleSheet(QString("QWidget {background-color: rgb(%1, %2, %3);}").arg(Theme::MainBackground.red()).arg(Theme::MainBackground.green()).arg(Theme::MainBackground.blue()));
}

void Styler::styleLabel(QLabel *label, int size)
{
	if (size <= 0)
		size = fontSize;
	label->setStyleSheet(QString("QLabel {border: 0px; color: rgb(%1, %2, %3); font-size: %4pt;}").arg(Theme::Text.red()).arg(Theme::Text.green()).arg(Theme::Text.blue()).arg(size));
}

void Styler::styleButton(QPushButton *button)
{
	button->setStyleSheet(QString("QPushButton { \
	    color: rgb(%1, %2, %3); \
	    background-color: rgb(%4, %5, %6); \
	    border-radius: %13px; min-height: %14px; border-style: outset; padding-left: %13px; padding-right: %13px; } \
	  QPushButton:checked {color: rgb(%7, %8, %9); } \
	  QPushButton:pressed {color: rgb(%7, %8, %9); } \
	  QPushButton:disabled { color: rgb(%10, %11, %12); }")
	    .arg(Theme::Text.red()).arg(Theme::Text.green()).arg(Theme::Text.blue())
	    .arg(Theme::ButtonBackground.red()).arg(Theme::ButtonBackground.green()).arg(Theme::ButtonBackground.blue())
	    .arg(Theme::Hightlight.red()).arg(Theme::Hightlight.green()).arg(Theme::Hightlight.blue())
	    .arg(Theme::TextDisabled.red()).arg(Theme::TextDisabled.green()).arg(Theme::TextDisabled.blue())
	    .arg(buttonPadding).arg(2 * buttonPadding));
};

void Styler::styleCheckBox(QCheckBox *checkBox)
{
	checkBox->setStyleSheet(QString("QCheckBox {color: rgb(%1, %2, %3); background-color: rgb(%4, %5, %6);}")
	                         .arg(Theme::Text.red()).arg(Theme::Text.green()).arg(Theme::Text.blue())
	                         .arg(Theme::MainBackground.red()).arg(Theme::MainBackground.green()).arg(Theme::MainBackground.blue()));
}

void Styler::styleRadioButton(QRadioButton *radioButton)
{
	radioButton->setStyleSheet(QString("QRadioButton { color: rgb(%1, %2, %3); background: rgb(%4, %5, %6);} QRadioButton:disabled {color: rgb(%7, %8, %9); }")
	                           .arg(Theme::Text.red()).arg(Theme::Text.green()).arg(Theme::Text.blue())
	                           .arg(Theme::MainBackground.red()).arg(Theme::MainBackground.green()).arg(Theme::MainBackground.blue())
	                           .arg(Theme::TextDisabled.red()).arg(Theme::TextDisabled.green()).arg(Theme::TextDisabled.blue()));
}

void Styler::styleLineEdit(QLineEdit *lineEdit)
{
	lineEdit->setStyleSheet(QString("QLineEdit { color: rgb(%1, %2, %3); background: rgb(%4, %5, %6);}")
	                         .arg(Theme::Text.red()).arg(Theme::Text.green()).arg(Theme::Text.blue())
	                         .arg(Theme::MainBackground.red()).arg(Theme::MainBackground.green()).arg(Theme::MainBackground.blue()));
}

void Styler::styleSearchLineEdit(QLineEdit *lineEdit)
{
	styleLineEdit(lineEdit);
	lineEdit->addAction(QIcon(":search-icon.svg"), QLineEdit::LeadingPosition);
	lineEdit->setClearButtonEnabled(true);
	lineEdit->setPlaceholderText(QObject::tr("Search..."));
}

void Styler::stylePlainTextEdit(QPlainTextEdit *edit)
{
	edit->setStyleSheet(QString("QPlainTextEdit { color: rgb(%1, %2, %3); background: rgb(%4, %5, %6);}")
	                    .arg(Theme::Text.red()).arg(Theme::Text.green()).arg(Theme::Text.blue())
	                    .arg(Theme::MainBackground.red()).arg(Theme::MainBackground.green()).arg(Theme::MainBackground.blue()));
}

void Styler::styleListView(QListView *listView)
{
	listView->setStyleSheet(QString("QListView { color: rgb(%1, %2, %3); background: rgb(%4, %5, %6);}")
	                        .arg(Theme::Text.red()).arg(Theme::Text.green()).arg(Theme::Text.blue())
	                        .arg(Theme::MainBackground.red()).arg(Theme::MainBackground.green()).arg(Theme::MainBackground.blue()));
}

void Styler::styleTableView(QTableView *tableView)
{
	tableView->setStyleSheet(QString("QTableView { color: rgb(%1, %2, %3); background: rgb(%4, %5, %6); alternate-background-color: rgb(%7, %8, %9);}")
	                         .arg(Theme::Text.red()).arg(Theme::Text.green()).arg(Theme::Text.blue())
	                         .arg(Theme::MainBackground.red()).arg(Theme::MainBackground.green()).arg(Theme::MainBackground.blue())
	                         .arg(Theme::FormBackground.red()).arg(Theme::FormBackground.green()).arg(Theme::FormBackground.blue()));
}

void Styler::styleGroupBox(QGroupBox *groupBox)
{
	groupBox->setStyleSheet(QString("QGroupBox {color: rgb(%1, %2, %3); background-color: rgb(%4, %5, %6); }")
	                        .arg(Theme::Text.red()).arg(Theme::Text.green()).arg(Theme::Text.blue())
	                        .arg(Theme::MainBackground.red()).arg(Theme::MainBackground.green()).arg(Theme::MainBackground.blue()));
}

void Styler::styleComboBox(QComboBox *comboBox)
{
	comboBox->setStyleSheet(QString("QComboBox {color: rgb(%1, %2, %3); background-color: rgb(%4, %5, %6); }")
	                        .arg(Theme::Text.red()).arg(Theme::Text.green()).arg(Theme::Text.blue())
	                        .arg(Theme::MainBackground.red()).arg(Theme::MainBackground.green()).arg(Theme::MainBackground.blue()));
}

void Styler::styleProgressBar(QProgressBar *progressBar)
{
	progressBar->setStyleSheet(QString("QProgressBar {color: rgb(%1, %2, %3); }")
	                        .arg(Theme::Text.red()).arg(Theme::Text.green()).arg(Theme::Text.blue()));
}

void Styler::setRetainSizeWhenHidden(QWidget *widget, bool retainSize)
{
	QSizePolicy sp_retain = widget->sizePolicy();
	sp_retain.setRetainSizeWhenHidden(retainSize);
	widget->setSizePolicy(sp_retain);
}

#if defined(Q_OS_ANDROID) || defined(Q_OS_IOS)
void Styler::styleTouchScrolling(QAbstractItemView *view)
{
	view->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
	view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	QScroller::grabGesture(view, QScroller::LeftMouseButtonGesture);
}
#endif
