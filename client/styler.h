// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef MDM_STYLER_H_
#define MDM_STYLER_H_

class QLabel;
class QPushButton;
class QCheckBox;
class QRadioButton;
class QLineEdit;
class QListView;
class QTableView;
class QPlainTextEdit;
class QGroupBox;
class QComboBox;
class QProgressBar;
class QWidget;
class QAbstractItemView;

class Styler
{
public:
	static void setup();
	static void styleMainWidget(QWidget *widget);
	static void styleLabel(QLabel *label, int size = 0);
	static void styleButton(QPushButton *button);
	static void styleCheckBox(QCheckBox *checkBox);
	static void styleRadioButton(QRadioButton *radioButton);
	static void styleLineEdit(QLineEdit *lineEdit);
	static void styleSearchLineEdit(QLineEdit *lineEdit);
	static void stylePlainTextEdit(QPlainTextEdit *edit);
	static void styleListView(QListView *listView);
	static void styleTableView(QTableView *tableView);
	static void styleGroupBox(QGroupBox *groupBox);
	static void styleComboBox(QComboBox *comboBox);
	static void styleProgressBar(QProgressBar *progressBar);
	static void setRetainSizeWhenHidden(QWidget *widget, bool retainSize);
#if defined(Q_OS_ANDROID) || defined(Q_OS_IOS)
	static void styleTouchScrolling(QAbstractItemView *view);
#endif
};

#endif
