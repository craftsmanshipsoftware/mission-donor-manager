Mission Donor Manager is Donor Management software for Canadian based missionary organizations.     It can also be used by any Canadian non profit for managing donors.  Mission Donor Manager is cloud based with clients for Windows, MacOS, Linux, Android and iOS.

[Official Website](http://www.craftsmanshipsoftware.ca/mission-donor-manager/)

### Features

* Export account data to Quickbooks using Quickbooks iif import
* Paypal transaction history integration for automatically processing donations from your web site made using paypal
* Login for missionaries to view their mission’s financials and donations to their missions
* Global or unique operational deduction percentage per mission
* CRA compatible year end tax receipt generation for donors.  Can be deliver by email or mail depending on donors preference
* CRA compatible inkind donation receipts for donors
* Cash and in kind donation summary by fiscal and calendar year for filing T3010
* Multiple donation entry sources:
    * Automatic for Paypal donations
    * Run monthly recurring Pre Authorized Debit lists
    * Manual entry for cash, cheques and donations from 3rd parties
* Allows donations to be assigned to different Income Accounts for recurring PAD and manual sources.  All Paypal donations use the same Income Account
* Proxy missions can be set up to receive donations on behalf of other organizations
* All data is stored on the cloud server and is encrypted using AES
* Export of donor’s contact info for managing mailchimp mailing lists
* Fine grain permission control for staff users
* Thankyou letters:
    * Automatic thankyou letters for paypal donations
    * Customizable thankyou letters per mission
    * Individual donor setting for email, mail or no thank you letter delivery by donation source (PAD & manual)

![ALT](doc/images/financial_flow.svg)

### Source Code Documentation

[Client](client)<br>
[Daemon](daemon)

### Build Instructions

[Daemon](http://www.craftsmanshipsoftware.ca/mission-donor-manager/server-setup.html)<br>
[Client - Linux](http://www.craftsmanshipsoftware.ca/mission-donor-manager/build-client.html#linux)<br>
[Client - Mac](http://www.craftsmanshipsoftware.ca/mission-donor-manager/build-client.html#mac)<br>
[Client - Windows](http://www.craftsmanshipsoftware.ca/mission-donor-manager/build-client.html#windows)<br>
