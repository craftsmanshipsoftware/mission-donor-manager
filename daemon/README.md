The Mission Donor Manager daemon implements a REST API for the client using the FoxMQ protocol.  The daemon api provides an interface to the SQLite database.  The database is encrypted using [QtCipherSqlitePlugin](https://github.com/devbean/QtCipherSqlitePlugin.git).  A simplified high level view.

![ALT](../doc/images/comm-birds-eye-view.svg)

## Building

[Building and setting up the daemon](http://www.craftsmanshipsoftware.ca/mission-donor-manager/server-setup.html)

## REST API

The API topics are grouped by common function.  [API Documentation](http://www.craftsmanshipsoftware.ca/mission-donor-manager/api/) contains a list of API and an explanation of what each api is used for as well as a cross reference where the api is implemented and used in the client.

> [api/advantage-accounts-api.cpp](daemon/api/advantage-accounts-api.cpp) - Used to add and modify a list of advantage accounts.  Advantage accounts are account numbers that store the advantage amount from a donation with an advantage, eg. Dinner meal value.  The api publishes a list of advantage accounts whenever the list is modified.   Advantage accounts appear in the client in the manual donation screen.<br>
> [api/api.cpp](daemon/api/api.cpp) - Abstract class.  All api are sub-classed from this.  Implements locking for api that require it.<br>
> [api/authentication-api.cpp](daemon/api/authentication-api.cpp) - Checks login credentials.  Sets permitted topic prefixes for user access control.  Implements password resetting and changing.<br>
> [api/campaigns-api.cpp](daemon/api/campaigns-api.cpp) - Used to add and modify a list of campaigns.  Campaigns are a way of tagging donations for tracking fundraising.  Campaigns are used in a manual donation entry.  The api publishes a list of campaigns whenever the list is modified.<br>
> [api/donation-api.cpp](daemon/api/donation-api.cpp) - Used to add and void donations.  add() method is used internally by the PayPal module and the recurring api.  Publishes the donor id and mission id modified by the donation.  There can be multiple modified donors and missions published if more than one donation was made in the PayPal module and the recurring api.<br>
> [api/donor-api.cpp](daemon/api/donor-api.cpp) - Used to add donors, modify donors personal info, keep notes about donors, keep communication preferences, get a quarterly donation summery, get donations and tax receipts.  The api publishes a list of donors whenever the list is modified.<br>
> [api/donor-export-api.cpp](daemon/api/donor-export-api.cpp) - Used to generate a of list of donors and their personal information.  The csv is sent to the client to download as a file.  Filter requirements are send in the request, such as, mission, time and donor geographical location.<br>
> [api/iif-api.cpp](daemon/api/iif-api.cpp) - Used to generate a QuickBook’s iif file to import mission accounts into QuickBooks on a periodic basis (usually monthly).  Api exist to group transactions by time period and income account.  During grouping, donation transactions are marked with an incremental iif export id number.<br>
> [api/income-accounts-api.cpp](daemon/api/income-accounts-api.cpp) - Used to add and modify a list of income accounts.  Income accounts are QuickBooks account numbers that donations are deducted from.  The api publishes a list of income accounts whenever the list is modified.   Income accounts appear in the client in the manual donation screen, recurring donation screen and setup screen.<br>
> [api/mission-api.cpp](daemon/api/mission-api.cpp) - Used to add and modify missions.  There are also api to get mission financials, a list of donors to the mission, donor totals for the mission, recent donors to the mission and a list of missions that are associated with the user making the request.  The api publishes a list of missions whenever the list is modified.<br>
> [api/recurring-api.cpp](daemon/api/recurring-api.cpp) - Used to create lists of recurring donations and also to add and modify donations within each  recurring list.  The api publishes the list of recurring lists whenever the list of recurring lists is modified.   The api also publishes the id of the recurring list when a donation within it is modified.  There is also an api to apply one of the recurring lists for a specific date.<br>
> [api/setup-api.cpp](api/setup-api.cpp) - Used to modify the organizations info and a few global settings.  The api publishes all settings as one topic when they are modified.<br>
> [api/tax-receipt-api.cpp](daemon/api/tax-receipt-api.cpp) - Used to generate tax receipts, send tax receipts by email, create a pdf of snail mail receipts, add and modify signatories and their signatures.  The api publishes a list of signatories whenever the signatory list changes.<br>
> [api/thankyou-api.cpp](daemon/api/thankyou-api.cpp) - Used to modify the global thank you responses as well as mission specific responses.  There are also api used for downloading a pdf for printed snail mail thank you responses. The api also publishes a count of how many thank you responses are waiting to be downloaded.<br>
> [api/users-api.cpp](daemon/api/users-api.cpp) - Used to add users (login accounts), modify user personal info, change user permissions and change the missions that are associated with a user.  The api publishes a list of users whenever the list is modified.<br>

## Non API Modules

> [missiondm.cpp](daemon/missiondm.cpp) - Creates all api blocks.  Initializes the database.  Loads the SSL certificate.  Sets up the FoxMQ den.  Acts as a dispatcher, sending the incoming post to the appropriate api.<br>
> [email-sender.cpp](daemon/email-sender.cpp) - Is a convenient class for sending emails that wraps Poco::Net::SMTPClientSession.  Emails are send in a separate thread and the signal / slot communication between the threads for a queue.<br>
> [paypal.cpp](daemon/paypal.cpp) - Scrapes PayPal T0013 and T0002 transactions from the transaction history using a PayPal REST API app.  Adds the donor if not already in the Mission Donor Manager database.  Checks every 15min.  Asks for the past 7 days worth of transactions.  Transactions are matched to donors using their PayPal merchant ID.  If there is no match, the email address is used to match.  If there still is no match, name and address are compared.  If still no match, a new donor is added.<br>
> [address-matcher.cpp](daemon/address-matcher.cpp) - Used by the paypal module to compare an address in an incoming donation to an address of a donor in the database.  Some addresses, for example with apartment numbers, can very heavily in how users enter them.  The address matcher breaks down the strings into different components and returns if they are a match.<br>
> [activity-journal.cpp](daemon/activity-journal.cpp) - logs financial activities to a file in the same directory as the database on the server.  This is a CRA requirement.

## Database Structure

Financial information is stored in each donor’s account table and each mission’s account table.  (acnt_donor_n and acnt_n respectively, where n equals the id of the donor or mission.)  When a donation is made, exactly one row is added to a donor’s account table and a mission’s account table.  The mission’s account table uses columns donor_id and donor_acnt_idx and the donor’s account table uses column mission_id and acnt_idx as a cross reference to each other.  These store integers.  See [api/donation-api.cpp](daemon/api/donation-api.cpp)

The donor account tables are used to generate tax receipts and summaries for filing a T3010.  The mission account tables are used to export financial data to Quick Books and report on mission financials.

Financial information is only ever added to the tables, never modified.  With the exception of the invalid column, all other columns are populated only once.  The invalid column is a bool that can be toggled to void or unvoid the donation.

Financial amounts are stored as cents as integers.

## Notable Functionality

### Synchronization Between Clients

Client write synchronization is accomplished using the api locking discussed below (two or more clients can’t change something at the same time).  Client read synchronization is defined as when one client makes a change and another client needs to get it’s UI refreshed with the new data.   Client read synchronization is automatic for published data such as the donors list.  The daemon publishes it anytime it changes and subscribing clients will get the new list.  Donor account summary data is not published and must be queried from the daemon for a specific donor id.

Using donor id 5 account summary as an example (donors.ro.accounts), client A and B, where client A is looking at donor 5’s account summary and client B enters a manual donation from donor 5, ideally client A’s UI is refreshed with updated data after the donation is entered.  When a donation is submitted to the daemon, the daemon publishes donations.md.donor-accounts-updated which is a list of donor IDs that have just been updated.  In this case it is just one ID, 5, but can be more than one when PayPal or recurring donations are submitted.  When Client A is showing any users account summary, it subscribes to donations.md.donor-accounts-updated.  If it receives the id of the donor it is currently showing, client A sends a post for donor 5’s account summary which will result in it’s UI getting updated with the adjusted summary info.  When the UI is switched away from the donor’s tab, it de-subscribes to  donations.md.donor-accounts-updated.

Similar mechanism exist for other types of data.  Example, donations.md.mission-accounts-updated is published when one or more mission’s financial info is updated.

### API Locking

When a client needs to alter information, it must get a lock for the api group that the changes will be made in.  (Eg. donors.lock)  Some api groups lock a specific id while others lock the whole group.  For example donors.lock locks modifications for one specific donor while setup.lock locks all setup.  Client A can get a lock on donor 5 and client B can simultaneously get a lock on donor 7.  Client A and client B can not both get a lock on setup, only one at a time.  If the lock succeeds, a FoxMQ ack action is returned.  If the lock fails, a FoxMQ nack action is returned.  If a client disconnects while holding locks, the daemon automatically clears them.

### User Access Control

The Daemon uses the User Access Control built into FoxMQ.  FoxMQ keeps a list of permitted prefixes a client can send to the daemon.  The authentication api sets the list of permitted prefixes when the user logs in based on the values in the database.  If a client sends a topic that is not in the list of permitted prefixes, FoxMQ responds with a FoxMQ error-no-permission action.

Setting a prefix of “donor” will allow the client to send all donor topics.  Setting “donors.ro” will only allow topics starting with donors.ro  .  You can add multiple topics in the same group, for example “donors.ro” and “donors.note-edit”.  This will allow any topic starting with donors.ro and also the donors.note-edit topic.  You could add “don” and this will allow all donors, donation and donor-export topics.

You can also add numbers after the main api.  For example mission.45 is limited to users with mission 45.  The api base class will recognize this and populate it’s variable apiNumerical for use in sub classes.  The mission api takes advantage of this.
