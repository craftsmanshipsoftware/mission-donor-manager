// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef MDM_PAYPAL_TRANSACTIONS_H
#define MDM_PAYPAL_TRANSACTIONS_H

#include <QByteArray>
#include <QDateTime>
#include <QTimer>
#include <QNetworkAccessManager>
#include "address-matcher.h"

class MissionDM;

class PaypalTransactions : public QObject
{
	Q_OBJECT

public:
	PaypalTransactions(MissionDM &mdm);

private:
	void checkToken();
	void tokenRequestFailed();
	void tokenRequestSucceeded();
	void getTransactionList();
	void transactionListRequestSucceeded();
	void transactionListRequestFailed();
	void getCapturedTransactionIDsFromDB();
	int getDonorID(const QJsonObject &jPayerInfo, const QJsonObject &jShippingInfo, int julianDay);
	int getDonorIdByPaypalAccount(const QString &paypalAccountId, int julianDay);
	void  updateSlotLastUsedDate(int donorId, int slot, int julianDay);
	bool donorsPaypalAccountIsBlank(int donorId, int slot);
	int getDonorIdByEmailAddress(const QString &emailAddress);
	int getDonorIdByMaillingAddress(const QJsonObject &jPayerInfo, const QJsonObject &jShippingInfo);
	void updateDonorsPaypalAccount(int donorId, const QString &paypalAccountId, int julianDay);
	void setDonorsPaypalAccount(int donorId, const QString &paypalAccountId, int slot, int julianDay);
	int addNewDonor(const QJsonObject &jPayerInfo, const QJsonObject &jShippingInfo, int julianDay);
	void updateDonorEmail(int donorId, const QString &email);
	QString getMissionIdentifier(const QString &code, const QJsonObject &jCartInfo, const QJsonObject &jTransactionInfo, int &mailFlags);
	void processTransaction(int donorId, const QString &missionIdentifier, const QJsonObject &json, int julianDay, int mailFlags);
	void cullOldNotifications();
	void cullOldTransactionsFromDB();
	MissionDM &mdm;
	struct {
		QByteArray token;
		QDateTime expiry;
	} bearer;
	QNetworkAccessManager nam;
	QTimer transactionCheckTimer;
	QMap<QString, int> capturedTransactionIDs;
	bool addedDonor = false;
	QList<QString> webDonationCodes = {"T0002", "T0013"};
	AddressMatcher matcher;
	QMap<QString, int> badTransactions;
};

#endif
