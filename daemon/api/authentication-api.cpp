// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

// This API Documentation --> http://www.craftsmanshipsoftware.ca/mission-donor-manager/api/index.html#auth

#include <QRandomGenerator>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QSqlQuery>
#include <cryptopp/hex.h>
#include <cryptopp/sha.h>
#include <cryptopp/pwdbased.h>
#include "missiondm.h"
#include "email-sender.h"
#include "permission.h"
#include "authentication-api.h"

AuthenticationApi::AuthenticationApi(MissionDM &api) : SubApi(api)
{
	apiString = "auth";
}

bool AuthenticationApi::incomingPost(const QByteArray &topic, ssize_t, FoxID &asker, FoxHead::ContentType, QByteArray incomingContent)
{
	QByteArray subTopic = stripPrefix(topic);

	if (subTopic == "reset.request") {
		QJsonObject json = QJsonDocument::fromJson(incomingContent).object();
		QString email = json.value("email").toString();
		QSqlQuery query;
		query.prepare("SELECT id FROM users WHERE email=:email");
		query.bindValue(":email", email);
		query.exec();
		if (query.first()) {
			ResetRequest rr;
			int x = QRandomGenerator::system()->bounded(0, 999'999);
			rr.code = QString("%1").arg(x, 6, 10, QChar('0'));
			rr.id = query.value("id").toUInt();
			resetRequests.insert(asker.local(), rr);
			mdm.email.send(mdm.setupApi.donorRelationsEmail.toUtf8(), email.toUtf8(), "Mission DM - Password Reset Code", QString("Enter this one time code in Mission DM to reset your password\n\n%1").arg(rr.code).toUtf8());
			qInfo() << "Sent OTP to user id" << rr.id;
		}
		ackOrNack(asker, true);
	} else if (subTopic == "reset.code") {
		bool ok = false;
		QJsonObject json = QJsonDocument::fromJson(incomingContent).object();
		QString code = json.value("code").toString();
		if (resetRequests.contains(asker.local())) {
			if (!QString::compare(resetRequests.value(asker.local()).code, code)) {
				ok = resetPassword(resetRequests.value(asker.local()).id);
				resetRequests.remove(asker.local());
			}
		}
		ackOrNack(asker, ok);
	} else if (subTopic == "login") {
		QJsonObject json = QJsonDocument::fromJson(incomingContent).object();
		if (mdm.getState() == ServerState::Ready) {
			login(asker, json);
		} else if (mdm.getState() == ServerState::NeedFirstUser) {
			FoxPermittedPrefixes fpp = {"user", "setup"};
			mdm.den.setPermittedTopics(asker, fpp);
			int p = Permission::UserRead | Permission::UserModify | Permission::UserPasswordReset | Permission::Setup;
			mdm.den.answerPost(asker, "auth.login", FoxHead::ContentType::IntAsString, QByteArray::number(p));
		} else {
			ackOrNack(asker, false);
		}
	} else if (subTopic == "change-pwd") {
		QJsonObject json = QJsonDocument::fromJson(incomingContent).object();
		int id = login(asker, json);
		if (id > -1) {
			QByteArray newPassword = json.value("new-password").toString().toUtf8();
			QByteArray salt = generateSalt();
			QByteArray hash = hashPassword(salt, preparePasswordForHashing(newPassword));
			QSqlQuery query;
			query.prepare("UPDATE users SET pwd_salt=:pwd_salt, pwd_hash=:pwd_hash WHERE id=:id");
			query.bindValue(":id", id);
			query.bindValue(":pwd_salt", salt);
			query.bindValue(":pwd_hash", hash);
			query.exec();
			qInfo() << "User id" << id << "has changed their password";
		}
	} else {
		return false;  // make top api send ErrorNoTopic
	}
	return true;
}

int AuthenticationApi::login(FoxID &asker, QJsonObject json)
{
	int id = -1;
	QString email = json.value("email").toString().toLower();
	QByteArray pwd = json.value("password").toString().toUtf8();
	QSqlQuery query;
	query.prepare("SELECT id, pwd_salt, pwd_hash, permissions, is_enabled FROM users WHERE email=:email");
	query.bindValue(":email", email);
	query.exec();
	if (query.first()) {
		int permissions = query.value("permissions").toUInt();
		QByteArray salt = query.value("pwd_salt").toByteArray();
		QByteArray hash = query.value("pwd_hash").toByteArray();
		if (!query.value("is_enabled").toBool())
			qInfo() << "Login attempt from disabled user" << query.value("id").toUInt();
		else if (hash == hashPassword(salt, preparePasswordForHashing(pwd))) {
			id = query.value("id").toUInt();
			mdm.addAuthenticatedUser(asker.local(), id);
			FoxPermittedPrefixes fpp = buildPermittedPrefixes(id, permissions);
			mdm.den.setPermittedTopics(asker, fpp);
			mdm.den.answerPost(asker, "auth.login", FoxHead::ContentType::IntAsString, QByteArray::number(permissions));
			qInfo() << "Login from user id" << id;
			mdm.journal.log(0, "Login from user id " + QString::number(id) + ' ' + mdm.den.getClientsIPAddress(asker).toString());
		} else {
			qInfo() << "Login attempt with wrong password from user id" << query.value("id").toUInt();
		}
	} else {
		qInfo() << "Login attempt from unknown user" << email;
	}
	if (id < 0)
		ackOrNack(asker, false);
	return id;
}

QByteArray AuthenticationApi::generateSalt()
{
	QByteArray seed;
	QRandomGenerator pseudo(QRandomGenerator::securelySeeded());  // seed pseudo with a good random number
	for (int i=0; i<16; i++) {
		uint32_t part = pseudo.generate();
		uint8_t *p = (uint8_t *) &part;
		seed.append(p[0]);
		seed.append(p[1]);
		seed.append(p[2]);
		seed.append(p[3]);
	}
	return seed;
}

QByteArray AuthenticationApi::generateTemporaryPassword()
{
	QByteArray password;
	QRandomGenerator pseudo(QRandomGenerator::securelySeeded());  // seed pseudo with a good random number
	QRandomGenerator switcher(QRandomGenerator::securelySeeded());
	for (int i=0; i<10; i++) {
		if (switcher.bounded(1000) > 500)
			password.append(pseudo.bounded(0x30, 0x39));
		else
			password.append(pseudo.bounded(0x61, 0x7A));
	}
	return password;
}

QByteArray AuthenticationApi::preparePasswordForHashing(const QString &input)
{
	QByteArray output;
	if (input.isEmpty())  // don't loop forever
		return output;
	while (output.size() < 64)
		output.append(input.toUtf8());
	output.truncate(64);
	return output;
}

QByteArray AuthenticationApi::hashPassword(const QByteArray &salt, const QByteArray &pwd)
{
	std::string hexResult;
	CryptoPP::SecByteBlock result(64);
	CryptoPP::PKCS5_PBKDF2_HMAC<CryptoPP::SHA512> pbkdf;
	pbkdf.DeriveKey(result, result.size(), 0x00, (uint8_t *)pwd.data(), pwd.size(), (uint8_t *)salt.data(), salt.size(), 210'000);
	CryptoPP::ArraySource resultEncoder(result, result.size(), true, new CryptoPP::HexEncoder(new CryptoPP::StringSink(hexResult)));
	return QByteArray::fromStdString(hexResult);
}

bool AuthenticationApi::resetPassword(int id)
{
	bool success = false;

	QSqlQuery query;
	query.prepare("SELECT email FROM users WHERE id=:id");
	query.bindValue(":id", id);
	query.exec();
	if (query.first()) {
		QString email = query.value("email").toString();
		query.finish();
		QByteArray pwd = generateTemporaryPassword();
		QByteArray salt = generateSalt();
		QByteArray hash = hashPassword(salt, preparePasswordForHashing(pwd));
		query.clear();
		query.prepare("UPDATE users SET pwd_hash=:pwd_hash, pwd_salt=:pwd_salt WHERE id=:id");
		query.bindValue(":id", id);
		query.bindValue(":pwd_hash", hash);
		query.bindValue(":pwd_salt", salt);
		success = query.exec();
		if (success) {
			mdm.email.send(mdm.setupApi.donorRelationsEmail.toUtf8(), email.toUtf8(), "Mission DM - Password Reset", "This is your temporary password.  Please login to Mission DM and change it.\n\n" + pwd);
			qInfo() << "Sent new password to user id" << id;
		}
	}
	mdm.setOneOrMoreUsersReady();

	return success;
}

FoxPermittedPrefixes AuthenticationApi::buildPermittedPrefixes(int id, int permissionFlags)
{
	FoxPermittedPrefixes fpp;

	Permission p(permissionFlags);

	fpp.insert("auth");

	if (p.hasPermission(Permission::UserRead))
		fpp.insert("users.ro");
	if (p.hasPermission(Permission::UserModify)) {
		fpp.insert("users.add");
		fpp.insert("users.info-edit");
		fpp.insert("users.permissions-edit");
		fpp.insert("users.missions-edit");
		fpp.insert("users.lock");
		fpp.insert("users.unlock");
	}
	if (p.hasPermission(Permission::UserForceLogout))
		fpp.insert("users.force-log-out");

	if (p.hasPermission(Permission::UserPasswordReset))
		fpp.insert("users.reset-password");

	if (p.hasPermission(Permission::DonorModify)) {
		fpp.insert("donors");
	} else if (p.hasPermission(Permission::DonorRead)) {
		fpp.insert("donors.ro");
		fpp.insert("donations.md.donor");
	}

	if (p.hasPermission(Permission::Donations))
		fpp.insert("donations");

	if (p.hasPermission(Permission::RecurringDonations)) {
		fpp.insert("recurring");
		if (!p.hasPermission(Permission::Donations))  // allow users with recurring donations and not manual donations to be able to void donations
			fpp.insert("donations.void");
	}

	if (p.hasPermission(Permission::MissionModify))
		fpp.insert("mission");
	else if (p.hasPermission(Permission::MissionRead | Permission::UserRead | Permission::Donations | Permission::RecurringDonations | Permission::DonorExport | Permission::Thankyou))
		fpp.insert("mission.ro");

	if (p.hasPermission(Permission::IifExport))
		fpp.insert("iif");

	if (p.hasPermission(Permission::DonorExport)) {
		fpp.insert("donor-export");
		fpp.insert("donations.md.income-accounts-updated");
	}

	if (p.hasPermission(Permission::Setup)) {
		fpp.insert("setup");
		fpp.insert("income-accounts");
		fpp.insert("advantage-accounts");
	} else {
		fpp.insert("advantage-accounts.list-enabled");
	}

	if (p.hasPermission(Permission::Campaigns))
		fpp.insert("campaigns");
	else if (p.hasPermission(Permission::Donations))
		fpp.insert("campaigns.list-enabled");

	if (!p.hasPermission(Permission::Setup) && p.hasPermission(Permission::IifExport | Permission::Donations | Permission::RecurringDonations))
		fpp.insert("income-accounts.list-enabled");

	if (p.hasPermission(Permission::MissionViewAll)) {
		fpp.insert("users.ro.missions-updated");
		fpp.insert("mission.my-list");
		fpp.insert("mission.full-list-updated");
		fpp.insert("mission.0");  // wildcard all missions
		fpp.insert("donations.md");
		fpp.insert("mission.donor-info");
	} else if (p.hasPermission(Permission::MissionView)) {
		// add each mission.XXXX that belongs to the user
		QList<int> missionIds = mdm.usersApi.getMissionsList(id);
		for (auto missionId : missionIds)
			fpp.insert(QString("mission.%1").arg(missionId, 4, 10, QChar('0')).toUtf8());
		fpp.insert("users.ro.missions-updated");
		fpp.insert("mission.my-list");
		fpp.insert("donations.md");
		fpp.insert("mission.donor-info");
	}

	if (p.hasPermission(Permission::Tax))
		fpp.insert("tax");

	if (p.hasPermission(Permission::Thankyou))
		fpp.insert("thankyou");

	if (p.hasPermission(Permission::Reports))
		fpp.insert("reports");

	return fpp;
}
