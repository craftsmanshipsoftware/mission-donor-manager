#ifndef COSUSERAPI_H
#define COSUSERAPI_H

#include "api.h"
#include "../QtFoxMQ/den-qt/qfoxmqden.h"

class Permission;

class UserApi : public SubApi
{
public:
	UserApi(ChurchOS &api);
	void loadInfoFromDB();
	bool newRequest(uint32_t user, const QByteArray &topic, ssize_t count, FoxID &id, FoxHead::ContentType contentType, QByteArray incomingContent);
	bool newQuery(uint32_t user, const QByteArray &topic, ssize_t count, FoxID &id, FoxHead::ContentType contentType, QByteArray incomingContent);
	inline uint32_t adminCount() { return _adminCount; }
	QList<const char *> loginPrefixes = {"user.login", "id,"};
	QList<const char *> unprivilegedPrefixes = {

	};

private:
	FoxPermittedPrefixes buildPrefixPermission(Permission p);
	void add(FoxID asker, const QJsonObject &injObj);
	void enable(FoxID asker, const QJsonObject &injObj, bool enabled);
	void login(FoxID &id, const QJsonObject &injObj);
	void setPassword(FoxID asker, int userID, const QJsonObject &injObj);
	void setName(FoxID asker, int userID, const QJsonObject &injObj);
	void getContact(FoxID asker, int userID);
	void setContact(FoxID asker, int userID, const QJsonObject &injObj);
	void getAddress(FoxID asker, int userID);
	void setAddress(FoxID asker, int userID, const QJsonObject &injObj);
	uint highestUserID = 0;
	uint _adminCount = 0;
};

#endif
