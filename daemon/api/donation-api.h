// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef MDM_DONATION_API_H
#define MDM_DONATION_API_H

#include "api.h"

class QSqlQuery;

class DonationApi : public SubApi
{
public:
	DonationApi(MissionDM &api);
	void loadInfoFromDB();
	bool incomingPost(const QByteArray &topic, ssize_t count, FoxID &id, FoxHead::ContentType contentType, QByteArray incomingContent);
	bool add(int source, int type, int thirdParty, int amount, int advantage, int fee, int sa, int aa, int donorId, int missionID, int date, bool anonymous, const QString &inkindDescription, const QString &campaign, int mailFlags, bool disableAdminCut = false);
	void publishModifiedAccounts();
	void publishModifiedIncomeAccounts();
	void markIncomeAccountModified(int incomeAccount);
	void markAllIncomeAccountsModified();

private:
	struct DonationCrossReference {
		int donorId;
		int donationId;
		int missionId;
		int missionAccountIndex;
		int proxyId;
		int proxyIndex;
		int incomeAccount;
		bool missionIndexVoided;
	};
	bool buildCrossReference(int donorId, int donationId, struct DonationCrossReference &dcr);
	bool voidDonation(int donorId, int donationId, bool voided);
	void markDonorModified(int donorId);
	void markMissionModified(int missionId);
	void notifyMissionAssociates(const QSqlQuery &donorQuery, int donorId, int missionId, int amount);
	QVector<bool> modifiedDonors;
	QVector<bool> modifiedMissions;
	QVector<bool> modifiedIncomeAccounts;
};

#endif
