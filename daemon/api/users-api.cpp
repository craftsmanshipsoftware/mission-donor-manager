// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

// This API Documentation --> http://www.craftsmanshipsoftware.ca/mission-donor-manager/api/index.html#users

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QSqlQuery>
#include "missiondm.h"
#include "permission.h"
#include "users-api.h"

#include <QDebug>
#include <QSqlError>

UsersApi::UsersApi(MissionDM &api) : SubApi(api)
{
	apiString = "users";
}

void UsersApi::loadInfoFromDB()
{
	QSqlQuery query;

	if (!mdm.db.tables().contains("users")) {
		qDebug("creating users table");
		query.exec("create table users (id int primary key, forename text, surname text, address text, apt text, city text, province text, country text, postalcode text, email text, cellphone text, homephone text, workphone text, is_enabled bool, missions text, permissions int, pwd_hash blob, pwd_salt blob)");
	}
	buildMissionUserCrossReference();
	publishUsersList();
}

void UsersApi::publishUsersList()
{
	QJsonArray jList;
	QSqlQuery query;
	query.prepare("SELECT id, forename, surname FROM users");
	query.exec();
	if (query.first()) {
		do {
			QJsonObject jUser;
			jUser.insert("id", query.value("id").toInt());
			jUser.insert("forename", query.value("forename").toString());
			jUser.insert("surname", query.value("surname").toString());
			jList.append(jUser);
		} while (query.next());
	}
	QJsonDocument jDoc;
	jDoc.setArray(jList);
	mdm.den.publish("users.ro.list", FoxHead::ContentType::Json, jDoc.toJson(QJsonDocument::Compact));
}

bool UsersApi::incomingPost(const QByteArray &topic, ssize_t, FoxID &asker, FoxHead::ContentType, QByteArray incomingContent)
{
	QByteArray subTopic = stripPrefix(topic);
	QSqlQuery query;

	auto writeUserInfo = [&](int id, QJsonObject &injObj) {
		query.bindValue(":id", id);
		query.bindValue(":forename", injObj.value("forename").toString());
		query.bindValue(":surname", injObj.value("surname").toString());
		query.bindValue(":address", injObj.value("address").toString());
		query.bindValue(":apt", injObj.value("apt").toString());
		query.bindValue(":city", injObj.value("city").toString());
		query.bindValue(":province", injObj.value("province").toString());
		query.bindValue(":country", injObj.value("country").toString());
		query.bindValue(":postalcode", injObj.value("postalcode").toString());
		query.bindValue(":email", injObj.value("email").toString().toLower());
		query.bindValue(":cellphone", injObj.value("cellphone").toString());
		query.bindValue(":homephone", injObj.value("homephone").toString());
		query.bindValue(":workphone", injObj.value("workphone").toString());
		query.bindValue(":is_enabled", injObj.value("is-enabled").toBool());
		ackOrNack(asker, query.exec());
		// fixme if add, return ID, if query fails return nak, if edit succeeds, return ack?
	};

	if (subTopic == "add") {
		QJsonObject injObj = QJsonDocument::fromJson(incomingContent).object();
		query.prepare("INSERT INTO users (id, forename, surname, address, apt, city, province, country, postalcode, email, cellphone, homephone, workphone, is_enabled) VALUES (:id, :forename, :surname, :address, :apt, :city, :province, :country, :postalcode, :email, :cellphone, :homephone, :workphone, :is_enabled)");
		writeUserInfo(mdm.getTableRowCount("users") + 1, injObj);
		publishUsersList();
	} else if (subTopic == "info-edit") {  // fixme - info-update
		QJsonObject injObj = QJsonDocument::fromJson(incomingContent).object();
		uint id = injObj.value("id").toInt();
		if (editLocks.value(id) == asker.local()) {
			if (!disablingLastAdmin(id, injObj)) {
				query.prepare("UPDATE users SET forename=:forename, surname=:surname, address=:address, apt=:apt, city=:city, province=:province, country=:country, postalcode=:postalcode, email=:email, cellphone=:cellphone, homephone=:homephone, workphone=:workphone, is_enabled=:is_enabled WHERE id=:id");
				writeUserInfo(id, injObj);
				publishUsersList();
				mdm.den.publish("users.ro.info-updated", FoxHead::ContentType::IntAsString, QByteArray::number(id));
				mdm.den.removeMessage("users.ro.info-updated");
			} else {
				ackOrNack(asker, false);
			}
		} else {
			ackOrNack(asker, false);
		}
	} else if (subTopic == "permissions-edit") {
		QJsonObject injObj = QJsonDocument::fromJson(incomingContent).object();
		int id = injObj.value("id").toInt();
		if (editLocks.value(id) == asker.local()) {
			if (!disablingLastAdmin(id, injObj)) {
				query.prepare("UPDATE users SET permissions=:permissions WHERE id=:id");
				query.bindValue(":id", id);
				query.bindValue(":permissions", injObj.value("permissions").toInt());
				ackOrNack(asker, query.exec());
				mdm.den.publish("users.ro.permissions-updated", FoxHead::ContentType::IntAsString, QByteArray::number(id));
				mdm.den.removeMessage("users.ro.permissions-updated");
			} else {
				ackOrNack(asker, false);
			}
		} else {
			ackOrNack(asker, false);
		}
	} else if (subTopic == "ro.info") {
		int id = incomingContent.toUInt();
		query.prepare("SELECT * FROM users WHERE id=:id");
		query.bindValue(":id", id);
		query.exec();
		if (query.first()) {
			QJsonDocument outjDoc;
			QJsonObject outjObj;
			outjObj.insert("id", id);
			outjObj.insert("forename", query.value("forename").toString());
			outjObj.insert("surname", query.value("surname").toString());
			outjObj.insert("address", query.value("address").toString());
			outjObj.insert("apt", query.value("apt").toString());
			outjObj.insert("city", query.value("city").toString());
			outjObj.insert("province", query.value("province").toString());
			outjObj.insert("country", query.value("country").toString());
			outjObj.insert("postalcode", query.value("postalcode").toString());
			outjObj.insert("email", query.value("email").toString());
			outjObj.insert("cellphone", query.value("cellphone").toString());
			outjObj.insert("homephone", query.value("homephone").toString());
			outjObj.insert("workphone", query.value("workphone").toString());
			outjObj.insert("is-enabled", query.value("is_enabled").toBool());
			outjDoc.setObject(outjObj);
			mdm.den.answerPost(asker, currentTopic, FoxHead::ContentType::Json, outjDoc.toJson(QJsonDocument::Compact));
		} else {
			ackOrNack(asker, false);
		}
	} else if (subTopic == "ro.permissions") {
		int id = incomingContent.toUInt();
		query.prepare("SELECT permissions FROM users WHERE id=:id");
		query.bindValue(":id", id);
		query.exec();
		int permissions = 0;
		if (query.first())
			permissions = query.value("permissions").toInt();
		QJsonDocument outjDoc;
		QJsonObject outjObj;
		outjObj.insert("permissions", permissions);
		outjDoc.setObject(outjObj);
		mdm.den.answerPost(asker, currentTopic, FoxHead::ContentType::Json, outjDoc.toJson(QJsonDocument::Compact));
	} else if (subTopic == "ro.missions") {
		QList<int> missionList = getMissionsList(incomingContent.toUInt());
		QJsonArray jArray;
		for (int i=0; i<missionList.size(); i++)
			jArray.append(missionList.at(i));
		QJsonDocument outjDoc;
		outjDoc.setArray(jArray);
		mdm.den.answerPost(asker, currentTopic, FoxHead::ContentType::Json, outjDoc.toJson(QJsonDocument::Compact));
	} else if (subTopic == "missions-edit") {
		QJsonObject injObj = QJsonDocument::fromJson(incomingContent).object();
		int id = injObj.value("id").toInt();
		if (editLocks.value(id) == asker.local()) {
			bool ok = updateMissionList(injObj, id);
			ackOrNack(asker, ok);
			if (ok) {
				mdm.den.publish("users.ro.missions-updated", FoxHead::ContentType::IntAsString, QByteArray::number(id));
				mdm.den.removeMessage("users.ro.missions-updated");
			}
		} else {
			ackOrNack(asker, false);
		}
	} else if (subTopic == "reset-password") {
		int id = incomingContent.toUInt();
		ackOrNack(asker, mdm.authenticationApi.resetPassword(id));
	} else if (subTopic == "force-log-out") {
		int id = incomingContent.toUInt();
		ackOrNack(asker, mdm.disconnectUser(id));
	} else if (subTopic == "lock") {
		ackOrNack(asker, lock(incomingContent.toUInt(), asker.local()));
	} else if (subTopic == "unlock") {
		ackOrNack(asker, unlock(incomingContent.toUInt(), asker.local()));
	} else {
		return false;  // make top api send ErrorNoTopic
	}

	return true;
}

QList<int> UsersApi::getMissionsList(int userId, bool includeAll)
{
	QList<int> missionList;
	QSqlQuery query;

	query.prepare("SELECT permissions FROM users WHERE id=:id");
	query.bindValue(":id", userId);
	query.exec();
	query.first();
	Permission p(query.value("permissions").toUInt());
	query.finish();
	query.clear();
	if (includeAll && p.hasPermission(Permission::MissionViewAll)) {
		query.prepare("SELECT id FROM missions");
		query.exec();
		if (query.first()) {
		do {
				missionList.append(query.value("id").toUInt());
			} while (query.next());
		}
	} else {
		query.prepare("SELECT missions FROM users WHERE id=:id");
		query.bindValue(":id", userId);
		query.exec();
		if (query.first()) {
			QString missions = query.value("missions").toString();
			QStringList missionStringList = missions.split(',');
			for (auto  m : missionStringList) {
				if (!m.isEmpty())
					missionList.append(m.toUInt());
			}
		}
	}
	return missionList;
}

bool UsersApi::disablingLastAdmin(int id, QJsonObject &json)
{
	QSqlQuery query;
	int permissions = 0;
	int userModifyCount = 0;
	bool enabled = false;

	// the 3 code blocks below are order for efficiency.

	if (json.contains("is-enabled")) {  // from user-edit change
		if (json.value("is-enabled").toBool())  // check if the request has the user disabled
			return false;
	} else {  // from permissions change
		Permission newP(json.value("permissions").toInt());
		if (newP.hasPermission(Permission::UserModify))
			return false;
	}

	// next check if the user currently has user modify permission
	query.prepare("SELECT permissions, is_enabled FROM users WHERE id=:id");
	query.bindValue(":id", id);
	query.exec();
	if (query.first()) {
		permissions = query.value("permissions").toInt();
		enabled = query.value("is_enabled").toBool();
	}
	Permission p(permissions);
	if (!p.hasPermission(Permission::UserModify) || !enabled)
		return false;

	// if we made it this far, check if there is another user with user modify permissions
	query.finish();
	query.clear();
	query.prepare("SELECT permissions, is_enabled FROM users");
	query.exec();
	if (query.first()) {
		do {
			permissions = query.value("permissions").toInt();
			enabled = query.value("is_enabled").toBool();
			Permission p(permissions);
			if (p.hasPermission(Permission::UserModify) && enabled)
				userModifyCount++;
		} while (query.next());
	}
	if (userModifyCount < 2)
		return true;
	else
		return false;
}

bool UsersApi::updateMissionList(const QJsonObject &json, int id)
{
	QSqlQuery query;
	QString missionString;
	for (auto jMission : json.value("missions").toArray())
		missionString+= QString::number(jMission.toInt()) + ',';
	query.prepare("UPDATE users SET missions=:missions WHERE id=:id");
	query.bindValue(":id", id);
	query.bindValue(":missions", missionString);
	bool ok = query.exec();
	if (ok)
		buildMissionUserCrossReference();
	return ok;
}

void UsersApi::buildMissionUserCrossReference()
{
	missionUserCR = QVector<QList<int>>(mdm.getTableRowCount("missions"));

	QSqlQuery query;
	query.prepare("SELECT id, missions FROM users");
	query.exec();
	int userId;
	if (query.first()) {
		do {
			userId = query.value("id").toInt();
			QStringList missionStringList = query.value("missions").toString().split(',');
			for (auto  m : missionStringList) {
				if (!m.isEmpty())
					missionUserCR[m.toUInt() - 1].append(userId);
			}
		} while (query.next());
	}
}
