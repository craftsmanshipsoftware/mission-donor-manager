// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef MDM_EMAIL_LIST_API_H
#define MDM_EMAIL_LIST_API_H

#include <QSqlQuery>
#include "api.h"

class DonorExportApi : public SubApi
{
public:
	DonorExportApi(MissionDM &api);
	bool incomingPost(const QByteArray &topic, ssize_t count, FoxID &asker, FoxHead::ContentType contentType, QByteArray incomingContent);

private:
	bool regionMatch(int alphaCode, QString province);
	bool filter(QSqlQuery &query);
	void makeHeader(QByteArray &csv);
	QJsonObject addDonorJson(QSqlQuery &query);
	void addDonor(QSqlQuery &query, QByteArray &csv);
	int pastMonths;
	int region1;
	int region2;
	int region3;
	int type;
	bool international;
	bool filterByNewsLetter = false;
	bool filterByToTime = false;
	bool filterByRegion1 = false;
	bool filterByRegion2 = false;
	bool filterByRegion3 = false;
	bool filterByType = false;
	bool filterInactive = false;
	bool exportIdNumber = false;
	bool exportFirstName = false;
	bool exportLastName = false;
	bool exportAddress = false;
	bool exportEmail = false;
	bool exportCellPhone = false;
	bool exportHomePhone = false;
	bool exportWorkPhone = false;
	bool exportPaypalAccounts = false;
};

#endif
