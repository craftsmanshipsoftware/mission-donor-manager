// clang-format off
/**************************************************************************************************
	Copyright (C) 2024  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

// This API Documentation --> http://www.craftsmanshipsoftware.ca/mission-donor-manager/api/index.html#reports

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QSqlQuery>
#include "missiondm.h"
#include "common.h"
#include "reports-api.h"

ReportsApi::ReportsApi(MissionDM &api) : SubApi(api)
{
	apiString = "reports";
}

bool ReportsApi::incomingPost(const QByteArray &topic, ssize_t, FoxID &asker, FoxHead::ContentType, QByteArray incomingContent)
{
	QByteArray subTopic = stripPrefix(topic);
	QSqlQuery query;

	QJsonObject json = QJsonDocument::fromJson(incomingContent).object();
	if (subTopic == "donations") {
		mdm.den.answerPost(asker, topic, FoxHead::ContentType::Blob, buildDonations(json));
	} else if (subTopic == "stats") {
		mdm.den.answerPost(asker, topic, FoxHead::ContentType::Blob, buildStats(json));
	} else if (subTopic == "monthly") {
		QJsonDocument outjDoc;
		outjDoc.setObject(buildMonthlySummary());
		mdm.den.answerPost(asker, currentTopic, FoxHead::ContentType::Json, outjDoc.toJson(QJsonDocument::Compact));
	} else {
		return false;  // make top api send ErrorNoTopic
	}

	return true;
}

QByteArray ReportsApi::buildDonations(const QJsonObject &json)
{
	QList<MissionDM::AccountNameNumber> sourceAccountNameList = mdm.loadAccountNames("source_accounts");
	QList<MissionDM::AccountNameNumber> advantageAccountNameList = mdm.loadAccountNames("advantage_accounts");
	QByteArray fileData;
	int startJulian = json.value("from").toInt();
	int endJulian = json.value("to").toInt();
	bool includeInvalid = json.value("include-invalid").toBool();
	bool includeInkind = json.value("include-inkind").toBool();

	fileData+= "Void\tType\tDonor ID\tFirst\tLast\tDonation ID\tDate\tSource\tAmount\tAdvantage\tFee\tPercent\tGross\tNet\tAdmin Cut\tMission Name\tMission Account\tIncome Account\tIncome Account #\t"
			   "Advantage Account\tAdvantage Account #\tExport ID\tReceipt ID\tAnonymous\tNEFTD\tCampaign\tNote\n";

	int total = mdm.getTableRowCount("donors");
	for (int i=0; i < total; i++) {
		QSqlQuery query;
		query.prepare(QString("SELECT forename, surname FROM donors WHERE id=%1").arg(i + 1));
		query.exec();
		query.first();
		QString firstName = query.value("forename").toString();
		QString lastName = query.value("surname").toString();

		QSqlQuery donationQuery;
		donationQuery.prepare(QString("SELECT * FROM acnt_donor_%1 WHERE date BETWEEN %2 AND %3").arg(i + 1).arg(startJulian).arg(endJulian));
		donationQuery.exec();
		if (!donationQuery.first())
			continue;
		do {
			// check void and inkind
			bool invalid = donationQuery.value("invalid").toBool();
			int type = donationQuery.value("type").toUInt();
			if ((!invalid || includeInvalid) && (type != DonationType::Inkind || includeInkind)) {
				if (invalid)
					fileData+= "Void\t";
				else
					fileData+= "No\t";
				switch (type) {  // clang-format off
					case DonationType::Normal:            fileData+= "Normal\t"; break;
					case DonationType::ThirdParty:        fileData+= "3P\t";     break;
					case DonationType::Inkind:            fileData+= "InKind\t"; break;
					case DonationType::ForeignThirdParty: fileData+= "F3P\t";    break;
				}  // clang-format on
				fileData+= QByteArray::number(i + 1) + '\t';
				fileData+= firstName.toUtf8() + '\t';
				fileData+= lastName.toUtf8() + '\t';
				fileData+= donationQuery.value("id").toString().toUtf8() + '\t';
				fileData+= QDate::fromJulianDay(donationQuery.value("date").toUInt()).toString().toUtf8() + '\t';
				switch (donationQuery.value("source").toUInt()) {  // clang-format off
					case DonationVia::Manual:    fileData+= "Manual\t";    break;
					case DonationVia::Recurring: fileData+= "Recurring\t"; break;
					case DonationVia::Paypal:    fileData+= "PayPal\t";    break;
				}  // clang-format on
				fileData+= convertCentsToString(donationQuery.value("amount").toInt()).toUtf8() + '\t';
				fileData+= convertCentsToString(donationQuery.value("advantage").toInt()).toUtf8() + '\t';
				fileData+= convertCentsToString(donationQuery.value("fee").toInt()).toUtf8() + '\t';
				fileData+= QByteArray::number(donationQuery.value("percent").toFloat() * 100, 'g', 2) + '\t';

				QSqlQuery missionAccountQuery;
				missionAccountQuery.prepare(QString("SELECT * FROM acnt_%1 WHERE id=%2").arg(donationQuery.value("mission_id").toInt()).arg(donationQuery.value("acnt_idx").toInt()));
				missionAccountQuery.exec();
				missionAccountQuery.first();
				fileData+= convertCentsToString(missionAccountQuery.value("gross").toInt()).toUtf8() + '\t';
				fileData+= convertCentsToString(missionAccountQuery.value("amount").toInt()).toUtf8() + '\t';
				fileData+= convertCentsToString(missionAccountQuery.value("cut").toInt()).toUtf8() + '\t';

				QSqlQuery missionQuery;
				missionQuery.prepare(QString("SELECT * FROM missions WHERE id=%1").arg(donationQuery.value("mission_id").toInt()));
				missionQuery.exec();
				missionQuery.first();
				fileData+= missionQuery.value("name").toString().toUtf8() + '\t';
				fileData+= missionQuery.value("account").toString().toUtf8() + '\t';
				int sa = missionAccountQuery.value("sa").toInt() - 1;
				fileData+= sourceAccountNameList.at(sa).name.toUtf8() + '\t';
				fileData+= sourceAccountNameList.at(sa).number.toUtf8() + '\t';
				int aa = missionAccountQuery.value("advtg_acnt").toInt() - 1;
				if (aa >= 0) {
					fileData+= advantageAccountNameList.at(aa).name.toUtf8() + '\t';
					fileData+= advantageAccountNameList.at(aa).number.toUtf8() + '\t';
				} else {
					fileData+= "N.A.\tN.A.\t";
				}
				fileData+= donationQuery.value("export_id").toString().toUtf8() + '\t';
				fileData+= donationQuery.value("rid").toString().toUtf8() + '\t';
				fileData+= donationQuery.value("anonymous").toString().toUtf8() + '\t';
				fileData+= donationQuery.value("neftd").toString().toUtf8() + '\t';
				fileData+= donationQuery.value("campaign").toString().toUtf8() + '\t';
				fileData+= donationQuery.value("inkind").toString().replace('\n', ' ').replace('\t', ' ').toUtf8() + '\t';

				fileData+= '\n';
			}
		} while (donationQuery.next());
	}
	return fileData;
}

QByteArray ReportsApi::buildStats(const QJsonObject &json)
{
	QByteArray fileData;
	int year = json.value("year").toInt();

	auto valueToIndex = [](int value) {
		int index;
		if (value < 10000)
			index = 0;
		else
			index = value / 25000 + 1;
		return index;
	};

	QSqlQuery query;
	int total = mdm.getTableRowCount("donors");
	QVector<int> donorsTypeHisto = QVector<int>(DonorType::Total);
	QVector<int> donationSourceHisto = QVector<int>(DonationVia::Total);
	QVector<QMap<int, bool>> donorAmountHistos = QVector<QMap<int, bool>>(total);
	int highestIndex = 0;
	for (int i=0; i < total; i++) {
		query.prepare("SELECT type FROM donors WHERE id=:id");
		query.bindValue(":id", i + 1);
		query.exec();
		query.first();
		int donorType = query.value("type").toUInt();
		query.finish();
		query.clear();

		query.prepare(QString("SELECT source, amount, type, invalid FROM acnt_donor_%1 WHERE year=%2").arg(i+1).arg(year));
		query.exec();
		QMap<int, bool> donorHisto;
		if (query.first()) {
			bool atLeastOne = false;
			do {
				if (query.value("invalid").toBool())
					continue;
				int type = query.value("type").toUInt();
				if (type == DonationType::Inkind)
					continue;
				int amount = query.value("amount").toUInt();
				int source = query.value("source").toUInt();
				atLeastOne = true;
				donationSourceHisto[source]++;
				int index = valueToIndex(amount);
				if (index > highestIndex)
					highestIndex = index;
				if (!donorHisto.contains(index))
					donorHisto.insert(index, true);
			} while (query.next());
			if (atLeastOne)
				donorsTypeHisto[donorType]++;
		}
		donorAmountHistos[i] = donorHisto;
	}

	QVector<int> amountHisto = QVector<int>(highestIndex + 1);
	for (int i=0; i < total; i++) {
		for (auto da : donorAmountHistos[i].keys()) {
			amountHisto[da]++;
		}
	}

	// clang-format off
	fileData+= "Donor Type Totals:\n\n";
	fileData+= "Individual\t"          + QByteArray::number(donorsTypeHisto.at(DonorType::Individual))         + '\n';
	fileData+= "Business\t"            + QByteArray::number(donorsTypeHisto.at(DonorType::Business))           + '\n';
	fileData+= "Government Agency\t"   + QByteArray::number(donorsTypeHisto.at(DonorType::GovernmentAgency))   + '\n';
	fileData+= "Canadian Charity\t"    + QByteArray::number(donorsTypeHisto.at(DonorType::CanadianCharity))    + '\n';
	fileData+= "Foreign Charity\t"     + QByteArray::number(donorsTypeHisto.at(DonorType::ForeignCharity))     + '\n';
	fileData+= "Non Tax Payer\t"       + QByteArray::number(donorsTypeHisto.at(DonorType::NonTaxPayer))        + '\n';
	fileData+= "Canadian Foundation\t" + QByteArray::number(donorsTypeHisto.at(DonorType::CanadianFoundation)) + '\n';
	fileData+= "\n\n";

	fileData+= "Donations by Source:\n\n";
	fileData+= "Manual Entry\t"    + QByteArray::number(donationSourceHisto.at(DonationVia::Manual))    + '\n';
	fileData+= "Recurring Entry\t" + QByteArray::number(donationSourceHisto.at(DonationVia::Recurring)) + '\n';
	fileData+= "PayPal\t"          + QByteArray::number(donationSourceHisto.at(DonationVia::Paypal))    + '\n';
	fileData+= "\n\n";
	// clang-format on

	fileData+= "Donor by amount counted once:\n\n";
	int from = 0;
	int to = 100;
	for (int i = 0; i < amountHisto.size(); i++) {
		fileData+= + "$" + QByteArray::number(from) + " to < $" + QByteArray::number(to) + '\t' + QByteArray::number(amountHisto.at(i)) + '\n';
		from = to;
		if (i == 0)
			to = 250;
		else
			to+= 250;
	}

	return fileData;
}

QJsonObject ReportsApi::buildMonthlySummary()
{
	QJsonObject json;
	QVector<int> monthTotals = QVector<int>(24);
	QDate current = QDate::currentDate();
	QDate toDate(current.year(), current.month(), 1);
	QDate fromDate = toDate.addMonths(-23);
	toDate = toDate.addMonths(1).addDays(-1);  // get last day of the current month

	int total = mdm.getTableRowCount("donors");
	for (int i=0; i < total; i++) {
		QSqlQuery donationQuery;
		donationQuery.prepare(QString("SELECT * FROM acnt_donor_%1 WHERE date BETWEEN %2 AND %3").arg(i + 1).arg(fromDate.toJulianDay()).arg(toDate.toJulianDay()));
		donationQuery.exec();
		if (!donationQuery.first())
			continue;
		do {
			// check void and inkind
			bool invalid = donationQuery.value("invalid").toBool();
			int type = donationQuery.value("type").toUInt();
			if (!invalid && type != DonationType::Inkind) {
				QDate donationDate = QDate::fromJulianDay(donationQuery.value("date").toUInt());
				int monthIndex = (donationDate.year() - fromDate.year()) * 12 + (donationDate.month() - fromDate.month());
				monthTotals[monthIndex]+= donationQuery.value("amount").toInt();
			}
		} while (donationQuery.next());
	}

	QJsonArray jMonthTotals;
	for (int i = 12; i < 24; i++)
		jMonthTotals.append(monthTotals.at(i));
	json.insert("month-totals", jMonthTotals);

	QJsonArray jMonthDeltas;
	for (int i = 0; i < 12; i++) {
		float percent = ((float)monthTotals.at(i + 12) - monthTotals.at(i)) / monthTotals.at(i) * 100;
		jMonthDeltas.append(percent);
	}
	json.insert("month-deltas", jMonthDeltas);
	json.insert("end-date", toDate.toJulianDay());

	return json;
}
