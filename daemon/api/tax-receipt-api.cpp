// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

// This API Documentation --> http://www.craftsmanshipsoftware.ca/mission-donor-manager/api/index.html#tax

#include <QSqlQuery>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QPrinter>
#include <QFile>
#include <QDir>
#include "email-sender.h"
#include "missiondm.h"
#include "tax-receipt-api.h"

constexpr int NextEmailReceiptDelay = 20'000;  // mS
constexpr const char *SignatureExt = ".signature";

struct SumsByDonorType {
	int individual = 0;
	int individualNeftd = 0;
	int business = 0;
	int businessNeftd = 0;
	int govAgency = 0;
	int canCharity = 0;
	int canFoundation = 0;
	int foreignCharity = 0;
	int nonTaxpayer = 0;
};

TaxReceiptApi::TaxReceiptApi(MissionDM &api, int port) : SubApi(api)
{
	apiString = "tax";

	receiptEmailTempDir = QString("/tmp/mdmd-%1/").arg(port);
	receiptEmailSendTimer.setSingleShot(true);
	connect(&receiptEmailSendTimer, &QTimer::timeout, this, &TaxReceiptApi::sendTimerTimeout);
}

void TaxReceiptApi::loadInfoFromDB()
{
	QSqlQuery query;

	if (!mdm.db.tables().contains("signators")) {
		qDebug("creating signators table");
		query.exec("create table signators (id int primary key, name text, signature_image blob)");
	}
	publishSignators();

	if (!mdm.db.tables().contains("receipts")) {
		qDebug("creating receipts table");
		query.exec("create table receipts (id int primary key, year int, donation_date int, donor_id int, signator_id, type int, date_issued int, amount int, advantage int, did int)");
	}
	publishReceiptSendingState();

	if (!mdm.db.tables().contains("receipt_keys")) {
		qDebug("creating receipt_keys table");
		query.exec("create table receipt_keys (year int primary key, private text, public text)");
	}
}

void TaxReceiptApi::publishSignators()
{
	// first publish id of selected signator
	mdm.den.publish("tax.signator-current", FoxHead::ContentType::IntAsString, QByteArray::number(getCurrentSignatorId()));

	QJsonArray jList;
	QSqlQuery query;
	query.prepare("SELECT id, name FROM signators");
	query.exec();
	if (query.first()) {
		do {
			QJsonObject jSignator;
			jSignator.insert("id", query.value("id").toInt());
			jSignator.insert("name", query.value("name").toString());
			jList.append(jSignator);
		} while (query.next());
	}
	QJsonDocument jDoc;
	jDoc.setArray(jList);
	mdm.den.publish("tax.signators", FoxHead::ContentType::Json, jDoc.toJson(QJsonDocument::Compact));
}

bool TaxReceiptApi::incomingPost(const QByteArray &topic, ssize_t, FoxID &asker, FoxHead::ContentType, QByteArray incomingContent)
{
	QByteArray subTopic = stripPrefix(topic);
	mdm.journal.setActiveUser(mdm.getUserIdOfClient(asker.local()));

	if (subTopic == "summary") {
		QJsonDocument outjDoc;
		outjDoc.setObject(buildSummary(incomingContent));
		mdm.den.answerPost(asker, currentTopic, FoxHead::ContentType::Json, outjDoc.toJson(QJsonDocument::Compact));
	} else 	if (subTopic == "inkind") {
		QJsonDocument outjDoc;
		outjDoc.setObject(buildInKind(incomingContent));
		mdm.den.answerPost(asker, currentTopic, FoxHead::ContentType::Json, outjDoc.toJson(QJsonDocument::Compact));
	} else if (subTopic == "signator-add") {
		bool ok = false;
		if (asker.local() == editLocks.value(0)) {
			ok = addSignator(incomingContent);
			if (ok)
				publishSignators();
		}
		ackOrNack(asker, ok);
	} else if (subTopic == "signature-change") {
		bool ok = false;
		if (asker.local() == editLocks.value(0))
			ok = changeSignature(incomingContent);
		ackOrNack(asker, ok);
	} else if (subTopic == "signator-set") {
		bool ok = false;
		if (asker.local() == editLocks.value(0)) {
			ok = setCurrentSignator(incomingContent.toUInt());
			if (ok)
				publishSignators();
		}
		ackOrNack(asker, ok);
	} else if (subTopic == "generate-receipts") {
		if (asker.local() == editLocks.value(0)) {
			QJsonDocument outjDoc;
			outjDoc.setObject(generateReceipts(incomingContent.toUInt()));
			mdm.den.answerPost(asker, currentTopic, FoxHead::ContentType::Json, outjDoc.toJson(QJsonDocument::Compact));
		} else {
			ackOrNack(asker, false);
		}
	} else if (subTopic == "send-email-receipts") {
		bool ok = true;
		if (emailingTaxReceiptState == EmailingTaxReceiptState::Idle)
			buildEmailReceiptList(incomingContent.toUInt());
		else
			ok = false;
		ackOrNack(asker, ok);
	} else if (subTopic == "snail-mail-receipts") {
		QJsonDocument outjDoc;
		outjDoc.setObject(buildSnailMailReceipts(incomingContent.toUInt()));
		mdm.den.answerPost(asker, currentTopic, FoxHead::ContentType::Json, outjDoc.toJson(QJsonDocument::Compact));
	} else if (subTopic == "preview") {
		QJsonDocument outjDoc;
		outjDoc.setObject(buildPreview());
		mdm.den.answerPost(asker, currentTopic, FoxHead::ContentType::Json, outjDoc.toJson(QJsonDocument::Compact));
	} else if (subTopic == "lock") {
		ackOrNack(asker, lock(0, asker.local()));
	} else if (subTopic == "unlock") {
		ackOrNack(asker, unlock(0, asker.local()));
	} else {
		return false;  // make top api send ErrorNoTopic
	}
	return true;
}

QJsonObject TaxReceiptApi::buildSummary(const QByteArray &request)
{
	struct AgencySum {
		bool isAgency = false;
		int amount = 0;
		QString name;
	};

	struct SumsByDonorType sums;
	QSqlQuery query;
	QJsonObject jsums;

	QJsonObject json = QJsonDocument::fromJson(request).object();
	bool fiscal = json.value("fiscal").toBool();
	int year = json.value("year").toInt();

	// calc start and end julian days for fiscal year or calendar year
	int startJulian;
	int endJulian;
	if (fiscal) {
		QDate fiscalPeriodEnd = QDate::fromJulianDay(mdm.setupApi.fiscalEnd);
		QDate fiscalEndDate(year, fiscalPeriodEnd.month(), fiscalPeriodEnd.day());
		startJulian = fiscalEndDate.addYears(-1).addDays(1).toJulianDay();
		endJulian = fiscalEndDate.toJulianDay();
	} else {
		startJulian = QDate(year, 1, 1).toJulianDay();
		endJulian = QDate(year, 12, 31).toJulianDay();
	}

	int total = mdm.getTableRowCount("donors");
	QVector<AgencySum> donors = QVector<AgencySum>(total);
	for (int i=0; i < total; i++) {
		query.prepare("SELECT forename, type FROM donors WHERE id=:id");
		query.bindValue(":id", i + 1);
		query.exec();
		query.first();
		int donorType = query.value("type").toUInt();
		if (donorType == DonorType::GovernmentAgency || donorType == DonorType::CanadianCharity || donorType == DonorType::ForeignCharity || donorType == DonorType::CanadianFoundation) {
			donors[i].isAgency = true;
			donors[i].name = query.value("forename").toString();
		}
		query.finish();
		query.clear();

		query.prepare(QString("SELECT * FROM acnt_donor_%1 WHERE date BETWEEN %2 AND %3").arg(i + 1).arg(startJulian).arg(endJulian));
		query.exec();
		if (query.first()) {
			do {
				if (query.value("invalid").toBool())
					continue;
				int type = query.value("type").toUInt();
				int amount = query.value("amount").toUInt();
				int advantage = query.value("advantage").toUInt();
				bool neftd = query.value("neftd").toBool();
				auto doSum = [&](struct SumsByDonorType &sums) {
					if (type == DonationType::Normal) {
						switch (donorType) {  // clang-format off
							case DonorType::Individual: {
								if (neftd) {
									sums.individualNeftd+= amount;
								} else {
									sums.individual+= amount - advantage;
									sums.individualNeftd+= advantage;
								}
							} break;
							case DonorType::Business: {
								if (neftd) {
									sums.businessNeftd+= amount;
								} else {
									sums.business+= amount - advantage;
									sums.businessNeftd+= advantage;
								}
							} break;
							case DonorType::GovernmentAgency:   sums.govAgency+= amount;      donors[i].amount+= amount; break;
							case DonorType::CanadianCharity:    sums.canCharity+= amount;     donors[i].amount+= amount; break;
							case DonorType::CanadianFoundation: sums.canFoundation+= amount;  donors[i].amount+= amount; break;
							case DonorType::ForeignCharity:     sums.foreignCharity+= amount; donors[i].amount+= amount; break;
							case DonorType::NonTaxPayer:        sums.nonTaxpayer+= amount;                               break;
						}  // clang-format on
					} else if (type == DonationType::ThirdParty) {
						sums.canCharity+= amount;
						int thirdParty = query.value("thirdparty").toUInt();
						donors[thirdParty - 1].amount+= amount;
					} else if (type == DonationType::ForeignThirdParty) {
						sums.foreignCharity+= amount;
						int thirdParty = query.value("thirdparty").toUInt();
						donors[thirdParty - 1].amount+= amount;  // put the donors amount into the specifc foreign charity's amount for the agency list
					}
				};
				doSum(sums);
			} while (query.next());
		}
		query.finish();
		query.clear();
	}

	auto addSumsToJson = [&](const QString &key, const struct SumsByDonorType &sums, int start, int end) {
		QJsonObject json;
		json.insert("start-julian", start);
		json.insert("end-julian", end);
		json.insert("individual", sums.individual);
		json.insert("individual-neftd", sums.individualNeftd);
		json.insert("business", sums.business);
		json.insert("business-neftd", sums.businessNeftd);
		json.insert("government", sums.govAgency);
		json.insert("canadian-charity", sums.canCharity);
		json.insert("canadian-foundation", sums.canFoundation);
		json.insert("foreign-charity", sums.foreignCharity);
		json.insert("non-taxpayer", sums.nonTaxpayer);
		jsums.insert(key, json);
	};
	addSumsToJson("totals", sums, startJulian, endJulian);

	QJsonArray jAgencies;
	for (int i=0; i < donors.size(); i++) {
		if (donors.at(i).isAgency && donors.at(i).amount) {
			QJsonObject json;
			json.insert("name", donors.at(i).name);
			json.insert("amount", donors.at(i).amount);
			json.insert("id", i + 1);
			jAgencies.append(json);
		}
	}
	jsums.insert("agencies", jAgencies);

	return jsums;
}

bool TaxReceiptApi::addSignator(const QByteArray &request)
{
	QSqlQuery query;
	QJsonObject json = QJsonDocument::fromJson(request).object();
	query.prepare("INSERT INTO signators (id, name) VALUES (:id, :name)");
	int id = mdm.getTableRowCount("signators") + 1;
	query.bindValue(":id", id);
	query.bindValue(":name", json.value("name").toString());
	bool ok = query.exec();
	if (ok)
		mdm.journal.log("TAX - New signator added:" + QString::number(id));
	return ok;
}

bool TaxReceiptApi::changeSignature(const QByteArray &request)
{
	QSqlQuery query;
	QJsonObject json = QJsonDocument::fromJson(request).object();
	int id = json.value("id").toInt();
	query.prepare(QString("UPDATE signators SET signature_image=:signature_image WHERE id=%1").arg(id));
	query.bindValue(":signature_image", QByteArray::fromBase64(json.value("signature-image").toString().toUtf8()));
	bool ok = query.exec();
	if (ok)
		mdm.journal.log("TAX - Signature image updated for signator:" + QString::number(id));
	return ok;
}

int TaxReceiptApi::getCurrentSignatorId()
{
	QSqlQuery query;
	query.prepare("SELECT signator FROM setup WHERE id=0");
	query.exec();
	query.first();
	return query.value("signator").toInt();
}

bool TaxReceiptApi::setCurrentSignator(int id)
{
	QSqlQuery query;
	query.prepare("UPDATE setup SET signator=:signator WHERE id=0");
	query.bindValue(":signator", id);
	bool ok = query.exec();
	if (ok)
		mdm.journal.log("TAX - Changing signator to:" + QString::number(id));
	return ok;
}

// generateReceipts() creates entries "receipts" in the receipts table that are later used to build the pdf tax receipts.  generateReceipts() is divided into two parts,
// the first mostly sums amounts from donations and the second populates the receipts table and updates each donation used with row id of the receipt that the donation
// went towards.  donors (type = QVector<Donor>) is used to keep track of the sum for each donor and also a list of donations to update the row if of the receipt.
// inkind donations are entered in the first summing part of generateReceipts() as inkind get a receipt per donation.
QJsonObject TaxReceiptApi::generateReceipts(int year)
{
	generateSigningKeys(year);

	int totalInkindReceipts = 0;
	int totalCashReceipts = 0;
	int totalCashDonations = 0;
	int signatorId = getCurrentSignatorId();
	int receiptRow = mdm.getTableRowCount("receipts") + 1;
	int donorCount = mdm.getTableRowCount("donors");
	int todaysJulian = QDate::currentDate().toJulianDay();
	QVector<Donor> donors = QVector<Donor>(donorCount);
	mdm.db.transaction();
	for (int i = 0; i<donorCount; i++) {
		QSqlQuery donorInfoQuery;
		donorInfoQuery.prepare(QString("SELECT * FROM donors WHERE id=%1").arg(i + 1));
		donorInfoQuery.exec();
		donorInfoQuery.first();
		int donorType = donorInfoQuery.value("type").toInt();
		if (donorType != DonorType::Individual && donorType != DonorType::Business)
			continue;

		QSqlQuery donorAcountQuery;
		donorAcountQuery.prepare(QString("SELECT * FROM acnt_donor_%1 WHERE year=%2").arg(i + 1).arg(year));
		donorAcountQuery.exec();
		if (donorAcountQuery.first()) {
			do {
				if (donorAcountQuery.value("rid").toInt() <= 0 && !donorAcountQuery.value("invalid").toBool()) {  // check that donation is not already associated with a receipt and is valid
					int donationType = donorAcountQuery.value("type").toInt();
					int did = donorAcountQuery.value("id").toInt();
					int amount = donorAcountQuery.value("amount").toInt();
					int advantage = donorAcountQuery.value("advantage").toInt();
					int donationDate = donorAcountQuery.value("date").toInt();
					bool neftd = donorAcountQuery.value("neftd").toBool();
					if (donationType == DonationType::Inkind && !neftd) {
						writeReceipt(receiptRow, year, donationDate, i + 1, donorAcountQuery.value("id").toInt(), signatorId, DonationType::Inkind, todaysJulian, amount, advantage);
						updateDonationWithReceiptId(i + 1, did, receiptRow);
						receiptRow++;
						totalInkindReceipts++;
					} else if (donationType == DonationType::Normal && advantage) {
						writeReceipt(receiptRow, year, donationDate, i + 1, donorAcountQuery.value("id").toInt(), signatorId, DonationType::Normal, todaysJulian, amount, advantage);
						updateDonationWithReceiptId(i + 1, did, receiptRow);
						receiptRow++;
						totalCashDonations++;
						totalCashReceipts++;
					} else if (donationType == DonationType::Normal && !neftd) {
						donors[i].sum+= amount;
						donors[i].donationDate = donationDate;
						donors[i].didsToUpdate.append(did);
						totalCashDonations++;
					}
				}
			} while (donorAcountQuery.next());
		}
	}

	for (int i = 0; i<donorCount; i++) {
		if (donors.at(i).sum) {
			int donationDate = donors.at(i).donationDate;
			if (donors.at(i).didsToUpdate.size() > 1)  // if there are multiple cash donations, flag multiple by setting donation date to zero.
				donationDate = 0;
			writeReceipt(receiptRow, year, donationDate, i + 1, -1, signatorId, DonationType::Normal, todaysJulian, donors.at(i).sum, 0);
			totalCashReceipts++;
			for (int j = 0; j < donors.at(i).didsToUpdate.size(); j++)
				updateDonationWithReceiptId(i + 1, donors.at(i).didsToUpdate.at(j), receiptRow);
			receiptRow++;
		}
	}

	mdm.db.commit();

	mdm.journal.logReceiptGeneration(year, totalCashDonations, totalCashReceipts, totalInkindReceipts);

	QJsonObject json;
	json.insert("total-cash-donations", totalCashDonations);
	json.insert("total-cash-receipts", totalCashReceipts);
	json.insert("total-inkind-receipts", totalInkindReceipts);
	return json;
}

void TaxReceiptApi::writeReceipt(int row, int year, int donationDate, int donorId, int did, int signatorId, int type, int dateIssued, int amount, int advantage)
{
	QSqlQuery query;
	query.prepare("INSERT INTO receipts (id, year, donation_date, donor_id, did, signator_id, type, date_issued, amount, advantage) VALUES (:id, :year, :donation_date, :donor_id, :did, :signator_id, :type, :date_issued, :amount, :advantage)");
	query.bindValue(":id", row);
	query.bindValue(":year", year);
	query.bindValue(":donation_date", donationDate);
	query.bindValue(":donor_id", donorId);
	query.bindValue(":did", did);
	query.bindValue(":signator_id", signatorId);
	query.bindValue(":type", type);
	query.bindValue(":date_issued", dateIssued);
	query.bindValue(":amount", amount);
	query.bindValue(":advantage", advantage);
	query.exec();
}

void TaxReceiptApi::updateDonationWithReceiptId(int donorId, int did, int receiptId)
{
	QSqlQuery query;
	query.prepare(QString("UPDATE acnt_donor_%1 SET rid=:rid WHERE id=:id").arg(donorId));
	query.bindValue(":id", did);
	query.bindValue(":rid", receiptId);
	query.exec();
}

QJsonObject TaxReceiptApi::buildReceiptCommonData()
{
	QJsonObject json;
	json.insert("name", mdm.setupApi.businessName);
	json.insert("cra-number", mdm.setupApi.craBusinessNumber);
	json.insert("address-line-1", mdm.setupApi.addressLine1);
	json.insert("address-line-2", mdm.setupApi.addressLine2);
	json.insert("address-line-3", mdm.setupApi.addressLine3);
	json.insert("phone", mdm.setupApi.phone);
	json.insert("email", mdm.setupApi.donorRelationsEmail);
	json.insert("issue-location", mdm.setupApi.issueLocation);
	json.insert("banner", QString(mdm.setupApi.banner.toBase64()));
	return json;
}

QJsonArray TaxReceiptApi::buildSignators(const QList<int> &signatorIds)
{
	QJsonArray signators;
	for (int i = 0; i < signatorIds.size(); i++) {
		QJsonObject json;
		QSqlQuery query;
		query.prepare(QString("SELECT * FROM signators WHERE id=%1").arg(signatorIds.at(i)));
		query.exec();
		query.first();
		json.insert("id", signatorIds.at(i));
		json.insert("name", query.value("name").toString());
		json.insert("signature", QString(query.value("signature_image").toByteArray().toBase64()));
		signators.append(json);
	}
	return signators;
}

QJsonObject TaxReceiptApi::buildSnailMailReceipts(int year)
{
	QSqlQuery receiptQuery;
	QJsonArray jArray;
	QList<int> signatorIds;

	QJsonObject json = buildReceiptCommonData();

	receiptQuery.prepare("SELECT * FROM receipts WHERE year=:year");
	receiptQuery.bindValue(":year", year);
	receiptQuery.exec();
	if (receiptQuery.first()) {
		do {
			QSqlQuery donorQuery;
			int donorId = receiptQuery.value("donor_id").toInt();
			donorQuery.prepare(QString("SELECT * FROM donors WHERE id=%1").arg(donorId));
			donorQuery.exec();
			donorQuery.first();
			if (!(donorQuery.value("news").toInt() & Mail::TaxReceiptBySnailMail))
				continue;
			QJsonObject receipt;
			receipt.insert("donor-name", mdm.getAddressSalutation(donorQuery));
			receipt.insert("donor-address-l1", donorQuery.value("address").toString());
			receipt.insert("donor-apt", donorQuery.value("apt").toString());
			receipt.insert("donor-city", donorQuery.value("city").toString());
			receipt.insert("donor-province", donorQuery.value("province").toString());
			receipt.insert("donor-postalcode", donorQuery.value("postalcode").toString());
			receipt.insert("donor-country", donorQuery.value("country").toString());

			receipt.insert("id", receiptQuery.value("id").toInt());
			receipt.insert("year", receiptQuery.value("year").toInt());
			receipt.insert("donation-date", receiptQuery.value("donation_date").toInt());
			receipt.insert("date-issued", receiptQuery.value("date_issued").toInt());
			int type = receiptQuery.value("type").toInt();
			receipt.insert("type", type);
			receipt.insert("amount", receiptQuery.value("amount").toInt());
			receipt.insert("advantage", receiptQuery.value("advantage").toInt());
			int signatorId = receiptQuery.value("signator_id").toInt();
			receipt.insert("signator-id", signatorId);
			if (!signatorIds.contains(signatorId))
				signatorIds.append(signatorId);

			if (type == DonationType::Inkind) {
				QSqlQuery donationNoteQuery;
				donationNoteQuery.prepare(QString("SELECT inkind FROM acnt_donor_%1 WHERE id=%2").arg(donorId).arg(receiptQuery.value("did").toInt()));
				donationNoteQuery.exec();
				donationNoteQuery.first();
				receipt.insert("donation-note", donationNoteQuery.value("inkind").toString());
			}

			jArray.append(receipt);
		} while (receiptQuery.next());
	}
	json.insert("receipts", jArray);
	json.insert("signators", buildSignators(signatorIds));

	mdm.journal.log("TAX - Download snail mail receipts for " + QString::number(year));

	return json;
}

QJsonObject TaxReceiptApi::buildPreview()
{
	QJsonArray jArray;
	QList<int> signatorIds;

	QJsonObject json = buildReceiptCommonData();
	int signatorId = getCurrentSignatorId();
	signatorIds.append(signatorId);

	QJsonObject receipt;
	receipt.insert("donor-name", "John Doe");
	receipt.insert("donor-address-l1", "123 Wayback St");
	receipt.insert("donor-apt", "");
	receipt.insert("donor-city", "Toronto");
	receipt.insert("donor-province", "ON");
	receipt.insert("donor-postalcode", "M7K 4H8");
	receipt.insert("donor-country", "CA");

	receipt.insert("id", 1679);
	receipt.insert("year", QDate::currentDate().year() - 1);
	QDate madeUpDate(QDate::currentDate().year() - 1, 2, 10);
	receipt.insert("donation-date", madeUpDate.toJulianDay());
	receipt.insert("date-issued", QDate::currentDate().toJulianDay());
	receipt.insert("type", DonationType::Normal);
	receipt.insert("amount", 10000);
	receipt.insert("signator-id", signatorId);

	jArray.append(receipt);

	json.insert("receipts", jArray);
	json.insert("signators", buildSignators(signatorIds));

	return json;
}

void TaxReceiptApi::buildEmailReceiptList(int year)
{
	QDir dir;
	dir.mkpath(receiptEmailTempDir);

	emailReceipts.clear();

	QSqlQuery receiptQuery;
	receiptQuery.prepare("SELECT * FROM receipts WHERE year=:year");
	receiptQuery.bindValue(":year", year);
	receiptQuery.exec();

	if (receiptQuery.first()) {
		pdfSigningKey = getPrivateSigningKey(year);
		do {
			QSqlQuery donorQuery;
			donorQuery.prepare(QString("SELECT * FROM donors WHERE id=%1").arg(receiptQuery.value("donor_id").toInt()));
			donorQuery.exec();
			donorQuery.first();
			if (!(donorQuery.value("news").toInt() & Mail::TaxReceiptByEmail)) {
				continue;
			} else {
				emailReceipts.append(receiptQuery.value("id").toInt());
			}
		} while (receiptQuery.next());
		emailReceiptPos = 0;
		receiptRenderer = QSharedPointer<ReceiptRenderer>::create();
		receiptRenderer->name = mdm.setupApi.businessName;
		receiptRenderer->craNumber = mdm.setupApi.craBusinessNumber;
		receiptRenderer->addressLine1 = mdm.setupApi.addressLine1;
		receiptRenderer->addressLine2 = mdm.setupApi.addressLine2;
		receiptRenderer->addressLine3 = mdm.setupApi.addressLine3;
		receiptRenderer->phone = mdm.setupApi.phone;
		receiptRenderer->email = mdm.setupApi.donorRelationsEmail;
		receiptRenderer->issueLocation = mdm.setupApi.issueLocation;
		receiptRenderer->banner = QImage::fromData(mdm.setupApi.banner);
		sendEmailReceipt();
		mdm.journal.log("TAX - Sending Email Receipts for " + QString::number(year));
	}
}

void TaxReceiptApi::sendEmailReceipt()
{
	emailingTaxReceiptState = EmailingTaxReceiptState::Sending;

	QSqlQuery receiptQuery;
	int receiptId = emailReceipts.at(emailReceiptPos);
	receiptQuery.prepare(QString("SELECT * FROM receipts WHERE id=%1").arg(receiptId));
	receiptQuery.exec();
	receiptQuery.first();

	int donorId = receiptQuery.value("donor_id").toInt();
	QSqlQuery donorQuery;
	donorQuery.prepare(QString("SELECT * FROM donors WHERE id=%1").arg(donorId));
	donorQuery.exec();
	donorQuery.first();

	receiptRenderer->donorName = mdm.getAddressSalutation(donorQuery);
	receiptRenderer->donorAddressLine1 = donorQuery.value("address").toString();
	receiptRenderer->donorApt = donorQuery.value("apt").toString();
	receiptRenderer->donorCity = donorQuery.value("city").toString();
	receiptRenderer->donorProvince = donorQuery.value("province").toString();
	receiptRenderer->donorPostalCode = donorQuery.value("postalcode").toString();

	receiptRenderer->receiptId = receiptId;
	receiptRenderer->year =  receiptQuery.value("year").toInt();
	receiptRenderer->donationDate = receiptQuery.value("donation_date").toInt();
	receiptRenderer->dateIssued = QDate::fromJulianDay(receiptQuery.value("date_issued").toInt());
	receiptRenderer->type = receiptQuery.value("type").toInt();
	receiptRenderer->amount = receiptQuery.value("amount").toInt();
	receiptRenderer->advantage = receiptQuery.value("advantage").toInt();

	if (receiptRenderer->type == DonationType::Inkind) {
		QSqlQuery donationNoteQuery;
		donationNoteQuery.prepare(QString("SELECT inkind FROM acnt_donor_%1 WHERE id=%2").arg(donorId).arg(receiptQuery.value("did").toInt()));
		donationNoteQuery.exec();
		donationNoteQuery.first();
		receiptRenderer->donationNote = donationNoteQuery.value("inkind").toString();
	}

	QSqlQuery signatorQuery;
	signatorQuery.prepare(QString("SELECT * FROM signators WHERE id=%1").arg(receiptQuery.value("signator_id").toInt()));
	signatorQuery.exec();
	signatorQuery.first();
	receiptRenderer->signatorName = signatorQuery.value("name").toString();
	receiptRenderer->signatorSignature = QImage::fromData(signatorQuery.value("signature_image").toByteArray());

	QPrinter printer;
	printer.setOutputFormat(QPrinter::PdfFormat);
	printer.setResolution(300);
	receiptEmailTempFileName = receiptEmailTempDir + QString("Receipt %1 for %2.pdf").arg(receiptId).arg(receiptRenderer->donorName);
	printer.setOutputFileName(receiptEmailTempFileName);
	receiptRenderer->start(&printer);
	receiptRenderer->renderPage();
	receiptRenderer->end();

	signPdf(receiptEmailTempFileName);
	mdm.email.send(mdm.setupApi.donorRelationsEmail.toUtf8(), donorQuery.value("email").toString().toUtf8(), "Official Tax Receipt for Donations", "Please find attached your official tax receipt", receiptEmailTempFileName, receiptEmailTempFileName + SignatureExt);

	emailReceiptPos++;
	if (emailReceiptPos == emailReceipts.size())
		emailingTaxReceiptState = EmailingTaxReceiptState::Finished;
	publishReceiptSendingState();
	receiptEmailSendTimer.start(NextEmailReceiptDelay);
}

void TaxReceiptApi::sendTimerTimeout()
{
	QFile::remove(receiptEmailTempFileName);
	if (emailReceiptPos < emailReceipts.size())
		sendEmailReceipt();
	else
		finishEmailingReceipts();
}

void TaxReceiptApi::finishEmailingReceipts()
{
	receiptRenderer.clear();
	emailingTaxReceiptState = EmailingTaxReceiptState::Idle;
	pdfSigningKey.clear();
	publishReceiptSendingState();
}

void TaxReceiptApi::publishReceiptSendingState()
{
	QJsonObject json;
	json.insert("count", emailReceiptPos);
	json.insert("total", emailReceipts.size());
	json.insert("state", emailingTaxReceiptState);
	QJsonDocument jDoc;
	jDoc.setObject(json);
	mdm.den.publish("tax.receipt-sending-state", FoxHead::ContentType::Json, jDoc.toJson(QJsonDocument::Compact));
}

void TaxReceiptApi::generateSigningKeys(int year)
{
	QSqlQuery query;

	query.prepare(QString("SELECT year FROM receipt_keys WHERE year=%1").arg(year));
	query.exec();
	if (!query.first()) {  // no signing keys exist yet for this year, generate
		QSslEvpKey rsa = QSslUtils::generateRSAKey(4096);
		query.prepare("INSERT INTO receipt_keys (year, private, public) VALUES (:year, :private, :public)");
		query.bindValue(":year", year);
		query.bindValue(":private", QSslUtils::RSAKeyToPEM(rsa));
		query.bindValue(":public", QSslUtils::publicKeyToPEM(rsa));
		query.exec();
	}
}

QSslEvpKey TaxReceiptApi::getPrivateSigningKey(int year)
{
	QSqlQuery query;
	QSslEvpKey rsa;

	query.prepare(QString("SELECT private FROM receipt_keys WHERE year=%1").arg(year));
	query.exec();
	if (query.first())
		rsa = QSslUtils::pemToRSAKey(query.value("private").toByteArray());
	return rsa;
}

QSslEvpKey TaxReceiptApi::getPublicSigningKey(int year)
{
	QSqlQuery query;
	QSslEvpKey rsa;

	query.prepare(QString("SELECT public FROM receipt_keys WHERE year=%1").arg(year));
	query.exec();
	if (query.first())
		rsa = QSslUtils::pemToPublicKey(query.value("public").toByteArray());
	return rsa;
}

void TaxReceiptApi::signPdf(const QString &fileName)
{
	QFile pdf(fileName);
	pdf.open(QIODevice::ReadOnly);
	QByteArray signatureData = QSslUtils::hashAndSign3Sha256(pdf.readAll(), pdfSigningKey);
	pdf.close();

	QFile pdfSignatureFile(fileName + SignatureExt);
	pdfSignatureFile.open(QIODevice::WriteOnly);
	pdfSignatureFile.write(signatureData);
	pdfSignatureFile.close();
}

QJsonObject TaxReceiptApi::buildInKind(const QByteArray &request)
{
	QSqlQuery query;
	QJsonObject reply;
	QJsonArray replyList;
	SumsByDonorType sums;

	QJsonObject json = QJsonDocument::fromJson(request).object();
	bool fiscal = json.value("fiscal").toBool();
	int year = json.value("year").toInt();

	// calc start and end julian days for fiscal year or calendar year
	int startJulian;
	int endJulian;
	if (fiscal) {
		QDate fiscalPeriodEnd = QDate::fromJulianDay(mdm.setupApi.fiscalEnd);
		QDate fiscalEndDate(year, fiscalPeriodEnd.month(), fiscalPeriodEnd.day());
		startJulian = fiscalEndDate.addYears(-1).addDays(1).toJulianDay();
		endJulian = fiscalEndDate.toJulianDay();
	} else {
		startJulian = QDate(year, 1, 1).toJulianDay();
		endJulian = QDate(year, 12, 31).toJulianDay();
	}

	int total = mdm.getTableRowCount("donors");
	for (int i=0; i < total; i++) {
		query.prepare("SELECT forename, surname, type FROM donors WHERE id=:id");
		query.bindValue(":id", i + 1);
		query.exec();
		query.first();
		int donorType = query.value("type").toUInt();
		QString firstName = query.value("forename").toString();
		QString lastName = query.value("surname").toString();
		query.finish();
		query.clear();

		query.prepare(QString("SELECT * FROM acnt_donor_%1 WHERE date BETWEEN %2 AND %3").arg(i + 1).arg(startJulian).arg(endJulian));
		query.exec();
		if (query.first()) {
			do {
				if (query.value("invalid").toBool())
					continue;
				int type = query.value("type").toUInt();
				if (type != DonationType::Inkind)
					continue;

				QJsonObject json;
				json.insert("name", firstName + ' ' + lastName);
				json.insert("donor-id", i + 1);
				json.insert("did", query.value("id").toInt());
				json.insert("date", query.value("date").toInt());
				json.insert("rid", query.value("rid").toInt());
				json.insert("note", query.value("inkind").toString());
				int amount = query.value("amount").toUInt();
				int advantage = query.value("advantage").toUInt();
				bool neftd = query.value("neftd").toBool();
				json.insert("amount", amount);

				switch (donorType) {  // clang-format off
				case DonorType::Individual: {
					if (neftd) {
						sums.individualNeftd+= amount;  // I don't think you can make an in kind donation to a non eligible
					} else {
						sums.individual+= amount - advantage;
						sums.individualNeftd+= advantage;
					}
				} break;
				case DonorType::Business: {
					if (neftd) {
						sums.businessNeftd+= amount;
					} else {
						sums.business+= amount - advantage;
						sums.businessNeftd+= advantage;
					}
				} break;
				case DonorType::GovernmentAgency: sums.govAgency+= amount;       break;
				case DonorType::CanadianCharity:  sums.canCharity+= amount;      break;
				case DonorType::CanadianFoundation: sums.canFoundation+= amount; break;
				case DonorType::ForeignCharity:   sums.foreignCharity+= amount;  break;
				case DonorType::NonTaxPayer:      sums.nonTaxpayer+= amount;     break;
				}  // clang-format on
				replyList.append(json);
			} while (query.next());
		}
		query.finish();
		query.clear();
	}
	reply.insert("list", replyList);

	QJsonObject jtotals;
	jtotals.insert("start-julian", startJulian);
	jtotals.insert("end-julian", endJulian);
	jtotals.insert("individual", sums.individual);
	jtotals.insert("individual-neftd", sums.individualNeftd);
	jtotals.insert("business", sums.business);
	jtotals.insert("business-neftd", sums.businessNeftd);
	jtotals.insert("government", sums.govAgency);
	jtotals.insert("canadian-charity", sums.canCharity);
	jtotals.insert("canadian-foundation", sums.canFoundation);
	jtotals.insert("foreign-charity", sums.foreignCharity);
	jtotals.insert("non-taxpayer", sums.nonTaxpayer);
	reply.insert("totals", jtotals);

	return reply;
}
