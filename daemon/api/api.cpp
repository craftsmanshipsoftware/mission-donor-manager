// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include "missiondm.h"
#include "api.h"

bool SubApi::topicBelongsToMe(QByteArray topic)
{
	bool isMyTopic = false;

	currentTopic = topic;
	topic.truncate(apiString.size());
	if (topic == apiString)
		isMyTopic = true;

	return isMyTopic;
}

QByteArray SubApi::stripPrefix(QByteArray topic)
{
	QByteArray subTopic = topic.remove(0, apiString.size() + 1);
	apiNumerical = stripNumerical(subTopic, subTopic);
	return subTopic;
}

int SubApi::stripNumerical(QByteArray &topic, QByteArray &topicWithoutNumerical)
{
	int i=0;
	int numerical = -1;
	while (topic.at(i) >= 0x30 && topic.at(i) <= 0x39 && i < topic.size())
		i++;
	if (i) {
		numerical = topic.left(i).toUInt();
		topicWithoutNumerical = topic.remove(0, i + 1);
	} else {
		topicWithoutNumerical = topic;
	}
	return numerical;
}

void SubApi::ackOrNack(FoxID &asker, bool success)
{
	if (success)
		mdm.den.ack(asker, currentTopic);
	else
		mdm.den.nack(asker, currentTopic);
}

bool SubApi::lock(int objectId, uint64_t askerLocalId)
{
	bool lockSucceeded = true;
	if (editLocks.contains(objectId))
		lockSucceeded = false;
	else
		editLocks.insert(objectId, askerLocalId);
	return lockSucceeded;
}


bool SubApi::unlock(int objectId, uint64_t askerLocalId)
{
	bool unlockSucceeded = false;
	if (askerLocalId == editLocks.value(objectId)) {
		editLocks.remove(objectId);
		unlockSucceeded = true;
	}
	return unlockSucceeded;
}

// remove any locks held by the disconnecting client
void SubApi::foxLeft(FoxID &id)
{
	QMap<int, uint64_t>::iterator it = editLocks.begin();
	while (it != editLocks.end()) {  // go through all locks in case a misbehaving client locked multiple users
		if (it.value() == id.local())
			it = editLocks.erase(it);
		else
			it++;
	}
}
