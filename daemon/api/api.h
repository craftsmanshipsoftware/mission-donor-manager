// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef MDM_API_H
#define MDM_API_H

#include <QByteArray>
#include <QMap>
#include "foxmqden.h"

class MissionDM;

class SubApi
{
public:
	SubApi(MissionDM &mdm) : mdm(mdm) {}
	virtual void loadInfoFromDB() {}
	bool topicBelongsToMe(QByteArray topic);
	QByteArray stripPrefix(QByteArray topic);
	int stripNumerical(QByteArray &topic, QByteArray &topicWithoutNumerical);
	void ackOrNack(FoxID &asker, bool success);
	virtual bool incomingPost(const QByteArray &topic, ssize_t count, FoxID &id, FoxHead::ContentType contentType, QByteArray content) = 0;
	void foxLeft(FoxID &id);

protected:
	bool lock(int objectId, uint64_t askerLocalId);
	bool unlock(int objectId, uint64_t askerLocalId);
	MissionDM &mdm;
	QByteArray apiString;
	QByteArray currentTopic;
	int apiNumerical;
	QMap<int, uint64_t> editLocks;
};

#endif
