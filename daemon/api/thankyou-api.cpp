// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

// This API Documentation --> http://www.craftsmanshipsoftware.ca/mission-donor-manager/api/index.html#thankyou

#include <QSqlQuery>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include "common.h"
#include "missiondm.h"
#include "email-sender.h"
#include "thankyou-api.h"

constexpr int EmailSendInterval = 20'000;

ThankyouApi::ThankyouApi(MissionDM &mdm) : SubApi(mdm)
{
	apiString = "thankyou";

	connect(&emailSendTimer, &QTimer::timeout, this, &ThankyouApi::sendNextEmail);
}

void ThankyouApi::loadInfoFromDB()
{
	QSqlQuery query;

	if (!mdm.db.tables().contains("thankyou")) {
		qDebug("creating thankyou table");
		query.exec("create table thankyou (id int primary key, email text, mail text, email_enabled bool, mail_enabled bool)");
		query.finish();

		//create row 0 (thankyou table only has one row)
		query.clear();
		query.prepare("INSERT INTO thankyou (id) VALUES (0)");
		query.exec();
	}
	publishMailWaitingCount();

	query.prepare("SELECT email_enabled, mail_enabled FROM thankyou WHERE id=0");
	query.exec();
	query.first();
	emailEnabled = query.value("email_enabled").toBool();
	mailEnabled = query.value("mail_enabled").toBool();
}

bool ThankyouApi::incomingPost(const QByteArray &topic, ssize_t, FoxID &asker, FoxHead::ContentType, QByteArray incomingContent)
{
	QByteArray subTopic = stripPrefix(topic);

	if (subTopic == "get-common-templates" || subTopic == "copy-common") {
		QJsonDocument outjDoc;
		outjDoc.setObject(getTemplates());
		mdm.den.answerPost(asker, currentTopic, FoxHead::ContentType::Json, outjDoc.toJson(QJsonDocument::Compact));
	} else if (subTopic == "save-common-templates") {
		if (asker.local() == editLocks.value(0)) {
			bool ok = setTemplates(QJsonDocument::fromJson(incomingContent).object());
			ackOrNack(asker, ok);
			if (ok) {
				mdm.den.publish("thankyou.updated", FoxHead::ContentType::Empty, " ");
				mdm.den.removeMessage("thankyou.updated");
			}
		} else {
			ackOrNack(asker, false);
		}
	} else if (subTopic == "get-mission-templates") {
		QJsonDocument outjDoc;
		outjDoc.setObject(getMissionTemplates(incomingContent.toInt()));
		mdm.den.answerPost(asker, currentTopic, FoxHead::ContentType::Json, outjDoc.toJson(QJsonDocument::Compact));
	} else if (subTopic == "save-mission-templates") {
		if (asker.local() == editLocks.value(0)) {
			QJsonObject json = QJsonDocument::fromJson(incomingContent).object();
			bool ok = setMissionTemplates(json);
			ackOrNack(asker, ok);
			if (ok) {
				mdm.den.publish("mission.ro.updated", FoxHead::ContentType::IntAsString, QByteArray::number(json.value("id").toInt()));
				mdm.den.removeMessage("mission.ro.updated");
			}
		} else {
			ackOrNack(asker, false);
		}
	} else if (subTopic == "get-enabled") {
		QJsonDocument outjDoc;
		outjDoc.setObject(getEnabled());
		mdm.den.answerPost(asker, currentTopic, FoxHead::ContentType::Json, outjDoc.toJson(QJsonDocument::Compact));
	} else if (subTopic == "set-enabled") {
		if (asker.local() == editLocks.value(0)) {
			QJsonObject json = QJsonDocument::fromJson(incomingContent).object();
			bool ok = updateEnabled(json);
			ackOrNack(asker, ok);
			if (ok)
				mdm.den.publishBriefly("thankyou.updated", FoxHead::ContentType::Empty, "");
		} else {
			ackOrNack(asker, false);
		}
	} else if (subTopic == "download") {
		QJsonDocument outjDoc;
		outjDoc.setObject(buildMailListForDownload());
		mdm.den.answerPost(asker, currentTopic, FoxHead::ContentType::Json, outjDoc.toJson(QJsonDocument::Compact));
	} else if (subTopic == "clear") {
		mailList.clear();
		publishMailWaitingCount();
	} else if (subTopic == "lock") {
		ackOrNack(asker, lock(0, asker.local()));
	} else if (subTopic == "unlock") {
		ackOrNack(asker, unlock(0, asker.local()));
	} else {
		return false;  // make top api send ErrorNoTopic
	}
	return true;
}

QJsonObject ThankyouApi::getTemplates()
{
	QJsonObject json;
	QSqlQuery query;
	query.prepare("SELECT * FROM thankyou WHERE id=0");
	query.exec();
	query.first();
	json.insert("email", query.value("email").toString());
	json.insert("mail", query.value("mail").toString());
	return json;
}

bool ThankyouApi::setTemplates(QJsonObject json)
{
	QSqlQuery query;
	query.prepare("UPDATE thankyou SET email=:email, mail=:mail WHERE id=0");
	query.bindValue(":email", json.value("email").toString());
	query.bindValue(":mail", json.value("mail").toString());
	return query.exec();
}

QJsonObject ThankyouApi::getMissionTemplates(int missionId)
{
	QJsonObject json;
	QSqlQuery query;
	query.prepare("SELECT use_custom_thy, custom_mail_thy, custom_email_thy FROM missions WHERE id=:id");
	query.bindValue(":id", missionId);
	query.exec();
	query.first();
	json.insert("email", query.value("custom_email_thy").toString());
	json.insert("mail", query.value("custom_mail_thy").toString());
	json.insert("use-custom", query.value("use_custom_thy").toBool());
	return json;
}

bool ThankyouApi::setMissionTemplates(QJsonObject json)
{
	QSqlQuery query;
	query.prepare("UPDATE missions SET custom_email_thy=:email, custom_mail_thy=:mail, use_custom_thy=:use_custom WHERE id=:id");
	query.bindValue(":id", json.value("id").toInt());
	query.bindValue(":email", json.value("email").toString());
	query.bindValue(":mail", json.value("mail").toString());
	query.bindValue(":use_custom", json.value("use-custom").toBool());
	return query.exec();
}

QJsonObject ThankyouApi::getEnabled()
{
	QJsonObject json;
	QSqlQuery query;
	query.prepare("SELECT email_enabled, mail_enabled FROM thankyou WHERE id=0");
	query.exec();
	query.first();
	json.insert("email-enabled", query.value("email_enabled").toBool());
	json.insert("mail-enabled", query.value("mail_enabled").toBool());
	return json;
}

bool ThankyouApi::updateEnabled(const QJsonObject &json)
{
	QSqlQuery query;
	query.prepare("UPDATE thankyou SET email_enabled=:email_enabled, mail_enabled=:mail_enabled WHERE id=0");
	emailEnabled = json.value("email-enabled").toBool();
	mailEnabled = json.value("mail-enabled").toBool();
	query.bindValue(":email_enabled", emailEnabled);
	query.bindValue(":mail_enabled", mailEnabled);
	return query.exec();
}

void ThankyouApi::doThankyou(const QSqlQuery &donorQuery, int source, int thirdParty, int amount, int missionId, int date, int mailFlags)
{
	int mailOptions = donorQuery.value("news").toInt();
	if (source == DonationVia::Manual && mailOptions & Mail::ThankyouManualByMail) {
		appendMailThankyou(donorQuery, missionId, amount, date);
	} else if (source == DonationVia::Manual && mailOptions & Mail::ThankyouManualByEmail) {
		queueEmailThankyou(donorQuery, missionId, amount, date);
	} else if (thirdParty > 0 && mailOptions & Mail::Thankyou3rdPartyByMail) {
		appendMailThankyou(donorQuery, missionId, amount, date);
	} else if (thirdParty > 0 && mailOptions & Mail::Thankyou3rdPartyByEmail) {
		queueEmailThankyou(donorQuery, missionId, amount, date);
	} else if (source == DonationVia::Recurring && mailOptions & Mail::ThankyouPreAuthByMail) {
		appendMailThankyou(donorQuery, missionId, amount, date);
	} else if (source == DonationVia::Recurring && mailOptions & Mail::ThankyouPreAuthByEmail) {
		queueEmailThankyou(donorQuery, missionId, amount, date);
	} else if (source == DonationVia::Paypal && mailOptions & (Mail::ThankyouPaypalOneTime | Mail::ThankyouPaypalRecurring) & mailFlags) {
		queueEmailThankyou(donorQuery, missionId, amount, date);
	}
}

QString ThankyouApi::getMissionName(int missionId)
{
	QSqlQuery query;
	query.prepare(QString("SELECT name FROM missions WHERE id=%1").arg(missionId));
	query.exec();
	query.first();
	return query.value("name").toString();
}

void ThankyouApi::queueEmailThankyou(const QSqlQuery &donorQuery, int missionId, int amount, int date)
{
	if (!emailEnabled)
		return;

	ThankyouData thankyouData = ThankyouData::create();
	thankyouData->addressSalutation = mdm.getAddressSalutation(donorQuery);
	thankyouData->salutation = mdm.getSalutation(donorQuery);
	thankyouData->email = donorQuery.value("email").toString();
	thankyouData->missionName = getMissionName(missionId);
	thankyouData->missionId = missionId;
	thankyouData->amount = amount;
	thankyouData->date = date;

	if (emailSendTimer.isActive()) {
		emailList.enqueue(thankyouData);
	} else {
		QString body = populateTemplate(getEmailTemplate(thankyouData), thankyouData);
		if (!body.isEmpty())
			mdm.email.send(mdm.setupApi.donorRelationsEmail.toUtf8(), thankyouData->email.toUtf8(), "Gift Confirmation", body.toUtf8());
		emailSendTimer.start(EmailSendInterval);
	}
}

void ThankyouApi::sendNextEmail()
{
	if (!emailList.size()) {
		emailSendTimer.stop();
		return;
	}
	ThankyouData thankyou = emailList.dequeue();
	QString body = populateTemplate(getEmailTemplate(thankyou), thankyou);
	if (!body.isEmpty())
		mdm.email.send(mdm.setupApi.donorRelationsEmail.toUtf8(), thankyou->email.toUtf8(), "Gift Confirmation", body.toUtf8());
}

QString ThankyouApi::getEmailTemplate(const ThankyouData &thankyou)
{
	QString templateString;
	QSqlQuery query;
	query.prepare("SELECT use_custom_thy, custom_email_thy FROM missions WHERE id=:id");
	query.bindValue(":id", thankyou->missionId);
	query.exec();
	query.first();
	if (query.value("use_custom_thy").toBool()) {
		templateString = query.value("custom_email_thy").toString();
	} else {
		query.finish();
		query.clear();
		query.prepare("SELECT email FROM thankyou WHERE id=0");
		query.exec();
		query.first();
		templateString = query.value("email").toString();
	}
	return templateString;
}

QString ThankyouApi::getMailTemplate(const ThankyouData &thankyou)
{
	QString templateString;
	QSqlQuery query;
	query.prepare("SELECT use_custom_thy, custom_mail_thy FROM missions WHERE id=:id");
	query.bindValue(":id", thankyou->missionId);
	query.exec();
	query.first();
	if (query.value("use_custom_thy").toBool()) {
		templateString = query.value("custom_mail_thy").toString();
	} else {
		query.finish();
		query.clear();
		query.prepare("SELECT mail FROM thankyou WHERE id=0");
		query.exec();
		query.first();
		templateString = query.value("mail").toString();
	}
	return templateString;
}

void ThankyouApi::appendMailThankyou(const QSqlQuery &donorQuery, int missionId, int amount, int date)
{
	if (!mailEnabled)
		return;
	ThankyouData thankyouData = ThankyouData::create();
	thankyouData->addressSalutation = mdm.getAddressSalutation(donorQuery);
	thankyouData->salutation = mdm.getSalutation(donorQuery);
	thankyouData->addressLine1 = donorQuery.value("address").toString();
	thankyouData->apt = donorQuery.value("apt").toString();
	thankyouData->city = donorQuery.value("city").toString();
	thankyouData->province = donorQuery.value("province").toString();
	thankyouData->postalCode = donorQuery.value("postalcode").toString();
	thankyouData->country = donorQuery.value("country").toString();
	thankyouData->missionName = getMissionName(missionId);
	thankyouData->missionId = missionId;
	thankyouData->amount = amount;
	thankyouData->date = date;
	mailList.append(thankyouData);
}

QString ThankyouApi::populateTemplate(QString formTemplate, const ThankyouData &data)
{
	int start = 0;
	int stop = 0;
	bool illegalCommand;
	QString var;
	QString replacement;
	start = formTemplate.indexOf("[[", start);
	stop = formTemplate.indexOf("]]", start);
	while (start > -1) {
		illegalCommand = false;
		var = formTemplate.mid(start + 2, stop - (2 + start));
		if (var == "amount")
			replacement = convertCentsToString(data->amount, true);
		else if (var == "date")
			replacement = QDate::fromJulianDay(data->date).toString("MMMM d, yyyy");
		else if (var == "mission")
			replacement = data->missionName;
		else if (var == "salutation")
			replacement = data->salutation;
		else if (var == "address-salutation")
			replacement = data->addressSalutation;
		else
			illegalCommand = true;

		if (!illegalCommand) {
			formTemplate.remove(start, stop - start + 2);
			formTemplate.insert(start, replacement);
		}
		start = formTemplate.indexOf("[[", start + 1);
		stop = formTemplate.indexOf("]]", start);
	}
	return formTemplate;
}

void ThankyouApi::publishMailWaitingCount()
{
	mdm.den.publish("thankyou.mail-waiting-count", FoxHead::ContentType::IntAsString, QByteArray::number(mailList.size()));
}

QJsonObject ThankyouApi::buildMailListForDownload()
{
	QJsonArray jLetters;
	QJsonObject json = mdm.taxApi.buildReceiptCommonData();

	for (int i = 0; i < mailList.size(); i++) {
		ThankyouData thankyou = mailList.at(i);
		QJsonObject jThankyou;
		jThankyou.insert("body", populateTemplate(getMailTemplate(thankyou), thankyou));
		jThankyou.insert("address-l1", thankyou->addressLine1);
		jThankyou.insert("apt", thankyou->apt);
		jThankyou.insert("city", thankyou->city);
		jThankyou.insert("province", thankyou->province);
		jThankyou.insert("postalcode", thankyou->postalCode);
		jThankyou.insert("country", thankyou->country);
		jThankyou.insert("address-salutation", thankyou->addressSalutation);
		jThankyou.insert("mission-name", thankyou->missionName);
		jThankyou.insert("amount", thankyou->amount);
		jThankyou.insert("date", thankyou->date);
		jLetters.append(jThankyou);
	}
	json.insert("letters", jLetters);

	return json;
}
