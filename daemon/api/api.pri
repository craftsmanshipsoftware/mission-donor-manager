HEADERS+= $$PWD/api.h
SOURCES+= $$PWD/api.cpp

HEADERS+= $$PWD/users-api.h
SOURCES+= $$PWD/users-api.cpp

HEADERS+= $$PWD/donor-api.h
SOURCES+= $$PWD/donor-api.cpp

HEADERS+= $$PWD/donation-api.h
SOURCES+= $$PWD/donation-api.cpp

HEADERS+= $$PWD/recurring-api.h
SOURCES+= $$PWD/recurring-api.cpp

HEADERS+= $$PWD/mission-api.h
SOURCES+= $$PWD/mission-api.cpp

HEADERS+= $$PWD/setup-api.h
SOURCES+= $$PWD/setup-api.cpp

HEADERS+= $$PWD/income-accounts-api.h
SOURCES+= $$PWD/income-accounts-api.cpp

HEADERS+= $$PWD/advantage-accounts-api.h
SOURCES+= $$PWD/advantage-accounts-api.cpp

HEADERS+= $$PWD/campaigns-api.h
SOURCES+= $$PWD/campaigns-api.cpp

HEADERS+= $$PWD/iif-api.h
SOURCES+= $$PWD/iif-api.cpp

HEADERS+= $$PWD/donor-export-api.h
SOURCES+= $$PWD/donor-export-api.cpp

HEADERS+= $$PWD/authentication-api.h
SOURCES+= $$PWD/authentication-api.cpp

HEADERS+= $$PWD/tax-receipt-api.h
SOURCES+= $$PWD/tax-receipt-api.cpp

HEADERS+= $$PWD/thankyou-api.h
SOURCES+= $$PWD/thankyou-api.cpp

HEADERS+= $$PWD/reports-api.h
SOURCES+= $$PWD/reports-api.cpp

INCLUDEPATH += $$PWD
