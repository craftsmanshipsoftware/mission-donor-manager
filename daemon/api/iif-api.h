// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef MDM_IIF_API_H
#define MDM_IIF_API_H

#include <QMutex>
#include "api.h"

class IifApi : public SubApi
{
public:
	IifApi(MissionDM &api);
	void loadInfoFromDB();
	bool incomingPost(const QByteArray &topic, ssize_t count, FoxID &asker, FoxHead::ContentType contentType, QByteArray incomingContent);
private:
	struct RowMark {
		int id;
		int donorId;
		int donorAccountIndex;
	};
	struct SourceAccountSum {
		int mission = 0;
		int operations = 0;
	};
	void publishExportIds();
	int mark(int fromJulianDay, int toJulianDay, int sourceAccount);
	QList<RowMark> findTransactionsToMark(const QString &account, int fromJulianDay, int toJulianDay, int sourceAccount, QVector<bool> &modifiedDonors);
	void getAccountNumber(int id, QString &accountNumber, QString &missionName);
	QMap<int, SourceAccountSum> sumTransactionsByExportId(const QString &account, int exportId, QMap<int, QMap<int, int>> &advantageAccountSums);
	void markAccount(const QString &account, QList<RowMark> rowsToMark, int exportId);
	void addTransactionToIif(QByteArray &iifSpl, const QString &account, QString name, int amount);
	int appendDonationToExistingGroup(const QJsonObject &json);
	bool removeDonationFromExistingGroup(const QJsonObject &json);
	QByteArray buildIif(int exportId);
	QJsonObject buildAuditList(int exportId, bool proxies);
	bool processInkind(const int type, const int sa);
	QJsonObject buildUnmarkedList(int sourceAccount);
	void ungroupLastIif();
	QString dateString;
	int proxyDonationCount;
};

#endif
