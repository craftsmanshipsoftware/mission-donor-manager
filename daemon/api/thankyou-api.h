// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef MDM_THANKYOU_API_H
#define MDM_THANKYOU_API_H

#include <QQueue>
#include <QTimer>
#include "api.h"

class QSqlQuery;

class ThankyouApi : public QObject, public SubApi
{
	Q_OBJECT

public:
	ThankyouApi(MissionDM &api);
	void loadInfoFromDB();
	bool incomingPost(const QByteArray &topic, ssize_t count, FoxID &asker, FoxHead::ContentType contentType, QByteArray incomingContent);
	void doThankyou(const QSqlQuery &donorQuery, int source, int thirdParty, int amount, int missionId, int date, int mailFlags);
	void publishMailWaitingCount();

private:
	struct ThankyouDataStruct {
		QString addressSalutation;
		QString salutation;
		QString email;
		QString missionName;
		QString addressLine1;
		QString apt;
		QString city;
		QString province;
		QString postalCode;
		QString country;
		int missionId;
		int amount;
		int date;
	};
	using ThankyouData = QSharedPointer<ThankyouDataStruct>;
	QJsonObject getTemplates();
	bool setTemplates(QJsonObject json);
	QJsonObject getMissionTemplates(int missionId);
	bool setMissionTemplates(QJsonObject json);
	QJsonObject getEnabled();
	bool updateEnabled(const QJsonObject &json);
	QString getMissionName(int missionId);
	void queueEmailThankyou(const QSqlQuery &donorQuery, int missionId, int amount, int date);
	void sendNextEmail();
	void appendMailThankyou(const QSqlQuery &donorQuery, int missionId, int amount, int date);
	QString getEmailTemplate(const ThankyouData &thankyou);
	QString getMailTemplate(const ThankyouData &thankyou);
	QString populateTemplate(QString formTemplate, const ThankyouData &data);
	QJsonObject buildMailListForDownload();
	QList<ThankyouData> mailList;
	QQueue<ThankyouData> emailList;
	QTimer emailSendTimer;
	bool emailEnabled = false;
	bool mailEnabled = false;
};

#endif
