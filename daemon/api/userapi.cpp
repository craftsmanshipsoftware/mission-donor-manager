#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QSqlQuery>
#include "userapi.h"
#include "../churchos.h"
#include "permission.h"

#include <QDebug>
#include <QSqlError>

// generate CSR
// https://security.stackexchange.com/questions/184845/how-to-generate-csrcertificate-signing-request-using-c-and-openssl

// user.login.device-user-name  // logging in client queries the user name associated with the device, backend responds with device's associated user name to display to logging in user.
// user.login.password          // logging in client to send passowrd, backend asks or nacks
//
// user.my.*     gets the user ID from the queriers fox ID - this allows the user to change only their account
// user.admin.*  supplies the user ID in the json quest
//
// user.my.set-password
// user.my.address
// user.my.set-address
// user.my.contact
// user.my.set-contact
// user.my.set-name
//
// admin only
// user.add
// user.set-permissions

// smallgroup.admin.create
// smallgroup.admin.rm
// smallgroup.my.send-prayer-request
// smallgroup.my.rm-prayer-request
// smallgroup.leader.rm-prayer-request
// smallgroup.admin.rm-prayer-request

UserApi::UserApi(ChurchOS &api) : SubApi(api)
{
	apiString = "user";
}

void UserApi::loadInfoFromDB()
{
	QSqlQuery query;

	if (!api.db.tables().contains("users")) {
		qDebug("creating users table");
		query.exec("create table users (id int primary key, permissions int, password text, forename text, surname text, address text, apt text, city text, province text, country text, postalcode text, email text, cellphone text, homephone text, workphone text, visibility int, enabled bool)");
	}

	if (!api.db.tables().contains("devices")) {
		qDebug("creating devices table");
		query.exec("create table devices (id int primary key, user int, enabled bool)");

		// hack this in for testing
		query.clear();
		query.prepare("INSERT INTO devices (id, user, enabled) VALUES (:id, :user, :enabled)");
		query.bindValue(":id", 1);
		query.bindValue(":user", 1);
		query.bindValue(":enabled", true);
		query.exec();
	}
	// fixme find highest device ID

	uint id;
	Permission permissions;
	query.exec("SELECT * FROM users");
	while (query.next()) {
		id = query.value(0).toUInt();
		if (id > highestUserID)
			highestUserID = id;

		permissions = query.value("permissions").toUInt();
		if (permissions.hasPermission(Permission::UserPermissionModify))
			_adminCount++;
	}
}

FoxPermittedPrefixes UserApi::buildPrefixPermission(Permission p)
{
	// fixme load p from db
	FoxPermittedPrefixes prefixPermissions;
	if (p.hasPermission(Permission::UserPermissionModify))
		prefixPermissions.append("user");

	prefixPermissions.append("user.logout");

	return prefixPermissions;
}

bool UserApi::newRequest(uint32_t user, const QByteArray &topic, ssize_t count, FoxID &id, FoxHead::ContentType contentType, QByteArray incomingContent)
{
	return false;
}

bool UserApi::newQuery(uint32_t user, const QByteArray &topic, ssize_t count, FoxID &asker, FoxHead::ContentType contentType, QByteArray incomingContent)
{
	QByteArray subTopic = stripPrefix(topic);
	bool ok;

	QJsonDocument injDoc(QJsonDocument::fromJson(incomingContent));
	QJsonObject injObj = injDoc.object();

	if (subTopic == "add") {
		if (api.getState() == ChurchOS::State::NeedFirstUser) {
			Permission permissions = injObj.value("permissions").toString().toUInt(&ok, 16);
			if (permissions.hasPermission(Permission::UserPermissionModify)) {
				_adminCount++;
				add(asker, injObj);
				FoxPermittedPrefixes newPrefixes = buildPrefixPermission(permissions);
				api.den.setFoxPermittedPrefixes(asker, newPrefixes);
				api.authenticatedUserIDs.insert(asker.lower, highestUserID);
				api.updateState();
			} else {
				api.den.replyError(asker, "user.add"); // fixme, change to custom Action
			}
		} else {
			add(asker, injObj);
		}
	} else if (subTopic == "get-details") {

	} else if (subTopic == "enable") {
		enable(asker, injObj, true);
	} else if (subTopic == "disable") {
		enable(asker, injObj, false);
	} else if (subTopic == "add-spouce") {

	} else if (subTopic == "add-child") {

	} else if (subTopic == "set-permissions") {

	} else if (subTopic == "my.set-password") {
		setPassword(asker, user, injObj);
	} else if (subTopic == "set-password") {
		setPassword(asker, injObj.value("id").toInt(), injObj);
	} else if (subTopic == "my.set-name") {
		setName(asker, user, injObj);
	} else if (subTopic == "set-name") {
		setName(asker, injObj.value("id").toInt(), injObj);
	} else if (subTopic == "my.get-contact") {
		getContact(asker, user);
	} else if (subTopic == "get-contact") {
		getContact(asker, injObj.value("id").toInt());
	} else if (subTopic == "my.set-contact") {
		setContact(asker, user, injObj);
	} else if (subTopic == "set-contact") {
		setContact(asker, injObj.value("id").toInt(), injObj);
	} else if (subTopic == "my.get-address") {
		getAddress(asker, user);
	} else if (subTopic == "get-address") {
		getAddress(asker, injObj.value("id").toInt());
	} else if (subTopic == "my.set-address") {
		setAddress(asker, user, injObj);
	} else if (subTopic == "set-address") {
		setAddress(asker, injObj.value("id").toInt(), injObj);
	} else if (subTopic == "login") {
		login(asker, injObj);
	} else {
		return false;  // make top api send ErrorNoTopic
	}

	return true;
}

void UserApi::login(FoxID &asker, const QJsonObject &injObj)
{
	bool success = false;

	if (api.state == ChurchOS::State::NeedFirstDevice) {
		api.authenticatedUserIDs.insert(asker.local(), highestUserID);
	}

	QSqlQuery query;
	query.prepare("SELECT password, enabled FROM users WHERE id=:id");
	query.bindValue(":id", api.authenticatedUserIDs.value(asker.local()));
	query.exec();
	// fixme hashing
	if (query.next() && injObj.value("password").toString() == query.value("password").toString() && query.value("enabled").toBool()) {
		qDebug("upgrade");
		FoxPermittedPrefixes prefixes;
		prefixes = buildPrefixPermission(Permission(Permission::Admin));
		api.den.setFoxPermittedPrefixes(asker, prefixes);
		success = true;
	}
	ackOrNack(asker, success);
}

/* - Example incoming json
{
  "surname": "Dave",
  "forename": "Smith",
  "password": "123456",
  "permissions": "0xFFFFFFFF"
}
*/
void UserApi::add(FoxID asker, const QJsonObject &injObj)
{
	bool ok;
	highestUserID++;
	Permission permissions = injObj.value("permissions").toString().toUInt(&ok, 16);
	if (permissions.hasPermission(Permission::UserPermissionModify))
		_adminCount++;

	QSqlQuery query;
	query.prepare("INSERT INTO users (id, permissions, password, forename, surname, enabled) VALUES (:id, :permissions, :password, :forename, :surname, :enabled)");
	query.bindValue(":id", highestUserID);
	query.bindValue(":permissions", (uint32_t) permissions);
	// https://cheatsheetseries.owasp.org/cheatsheets/Password_Storage_Cheat_Sheet.html
	query.bindValue(":password", injObj.value("password").toString());
	query.bindValue(":forename", injObj.value("forename").toString());
	query.bindValue(":surname", injObj.value("surname").toString());
	query.bindValue(":enabled", true);
	ackOrNack(asker, query.exec());
	// fixme respond with new user ID instead of nack or ack?
}

void UserApi::enable(FoxID asker, const QJsonObject &injObj, bool enabled)
{
	int userID = 0;

	// fixme check if admin and admin count > 1 if disabling

	userID = injObj.value("id").toInt();
	QSqlQuery query;
	query.prepare("UPDATE users SET enabled=:enabled WHERE id=:id");
	query.bindValue(":enabled", enabled);
	query.bindValue(":id", userID);
	ackOrNack(asker, query.exec());
}

/* Example incoming json.  "id" is only used / valid for admins
{
  "id": 403,
  "password": "123456"
}
*/
void UserApi::setPassword(FoxID asker, int userID, const QJsonObject &injObj)
{
	qDebug() << "setPassword - userID" << userID;
	QString password = injObj.value("password").toString();
	// fixme hash password

	QSqlQuery query;
	query.prepare("UPDATE users SET password=:password WHERE id=:id");
	query.bindValue(":password", password);
	query.bindValue(":id", userID);
	ackOrNack(asker, query.exec());
}

/* - Example incoming json.  "id" is only used / valid for admins
{
  "id": 403,
  "surname": "Dave",
  "forename": "Smith"
}
*/
void UserApi::setName(FoxID asker, int userID, const QJsonObject &injObj)
{
	QSqlQuery query;
	query.prepare("UPDATE users SET forename=:forename, surname=:surname WHERE id=:id");
	query.bindValue(":forename", injObj.value("forename").toString());
	query.bindValue(":surname", injObj.value("surname").toString());
	query.bindValue(":id", userID);
	ackOrNack(asker, query.exec());
}

void UserApi::getContact(FoxID asker, int userID)
{
	QSqlQuery query;
	query.prepare("SELECT email, cellphone, homephone, workphone, visibility FROM users WHERE id=:id");
	query.bindValue(":id", userID);
	query.exec();
	if (query.next()) {
		QJsonDocument outjDoc;
		QJsonObject outjObj;
		outjObj.insert("id", userID);
		outjObj.insert("email", query.value("email").toString());
		outjObj.insert("cellphone", query.value("cellphone").toString());
		outjObj.insert("homephone", query.value("homephone").toString());
		outjObj.insert("workphone", query.value("workphone").toString());
		outjObj.insert("visibility", query.value("visibility").toString());
		outjDoc.setObject(outjObj);
		api.den.answerQuery(asker, currentTopic, FoxHead::ContentType::Json, outjDoc.toJson());
	} else {
		ackOrNack(asker, false);
	}
}

/* - Example incoming json.  "id" is only used / valid for admins
{
  "id": 403,
  "email": "craftsman@craftsmanshipsoftware.ca",
  "cellphone": "905-555-1234",
  "homephone": "905-555-4321",
  "workphone": "905-555-9876",
  "visibility": 16
}
*/
void UserApi::setContact(FoxID asker, int userID, const QJsonObject &injObj)
{
	QSqlQuery query;
	query.prepare("UPDATE users SET email=:email, cellphone=:cellphone, homephone=:homephone, workphone=:workphone, visibility=:visibility WHERE id=:id");
	query.bindValue(":email", injObj.value("email").toString());
	query.bindValue(":cellphone", injObj.value("cellphone").toString());
	query.bindValue(":homephone", injObj.value("homephone").toString());
	query.bindValue(":workphone", injObj.value("workphone").toString());
	query.bindValue(":visibility", injObj.value("visibility").toInt());
	query.bindValue(":id", userID);
	ackOrNack(asker, query.exec());
}

void UserApi::getAddress(FoxID asker, int userID)
{
	QSqlQuery query;
	query.prepare("SELECT address, apt, city, province, country, postalcode FROM users WHERE id=:id");
	query.bindValue(":id", userID);
	query.exec();
	if (query.next()) {
		QJsonDocument outjDoc;
		QJsonObject outjObj;
		outjObj.insert("id", userID);
		outjObj.insert("address", query.value("address").toString());
		outjObj.insert("apt", query.value("apt").toString());
		outjObj.insert("city", query.value("city").toString());
		outjObj.insert("province", query.value("province").toString());
		outjObj.insert("country", query.value("country").toString());
		outjObj.insert("postalcode", query.value("postalcode").toString());
		outjDoc.setObject(outjObj);
		api.den.answerQuery(asker, currentTopic, FoxHead::ContentType::Json, outjDoc.toJson());
	} else {
		ackOrNack(asker, false);
	}
}

void UserApi::setAddress(FoxID asker, int userID, const QJsonObject &injObj)
{
	QSqlQuery query;
	query.prepare("UPDATE users SET address=:address, apt=:apt, city=:city, province=:province, country=:country, postalcode=:postalcode WHERE id=:id");
	query.bindValue(":address", injObj.value("address").toString());
	query.bindValue(":apt", injObj.value("apt").toString());
	query.bindValue(":city", injObj.value("city").toString());
	query.bindValue(":province", injObj.value("province").toString());
	query.bindValue(":country", injObj.value("country").toString());
	query.bindValue(":postalcode", injObj.value("postalcode").toString());
	query.bindValue(":id", userID);
	ackOrNack(asker, query.exec());
}
