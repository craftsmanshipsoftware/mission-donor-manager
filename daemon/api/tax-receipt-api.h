// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef MDM_TAX_RECEIPT_API_H
#define MDM_TAX_RECEIPT_API_H

#include <QTimer>
#include "api.h"
#include "../QtSslUtils/qsslutils.h"
#include "../common/common.h"
#include "../common/receipt-renderer.h"

class TaxReceiptApi : public QObject, public SubApi
{
	Q_OBJECT

public:
	TaxReceiptApi(MissionDM &api, int port);
	void loadInfoFromDB();
	bool incomingPost(const QByteArray &topic, ssize_t count, FoxID &asker, FoxHead::ContentType contentType, QByteArray incomingContent);
	QJsonObject buildReceiptCommonData();
	QJsonArray buildSignators(const QList<int> &signatorIds);
	QSslEvpKey getPrivateSigningKey(int year);

private:
	struct Donor {
		QList<int> didsToUpdate;
		int sum = 0;
		int donationDate = 0;  // store the date of donation if only one cash donation was made in the year.  CRA wants date of donation shown for single donation and year for multi
	};
	QJsonObject buildSummary(const QByteArray &request);
	QJsonObject buildInKind(const QByteArray &request);
	void publishSignators();
	int getCurrentSignatorId();
	bool setCurrentSignator(int id);
	bool addSignator(const QByteArray &request);
	bool changeSignature(const QByteArray &request);
	QJsonObject generateReceipts(int year);
	void writeReceipt(int row, int year, int donationDate, int donorId, int did, int signatorId, int type, int dateIssued, int amount, int advantage);
	void updateDonationWithReceiptId(int donorId, int did, int receiptId);
	QJsonObject buildSnailMailReceipts(int year);
	QJsonObject buildPreview();
	void generateSigningKeys(int year);

	void buildEmailReceiptList(int year);
	void sendEmailReceipt();
	void sendTimerTimeout();
	void finishEmailingReceipts();
	void publishReceiptSendingState();
	QSslEvpKey getPublicSigningKey(int year);
	void signPdf(const QString &fileName);

	QTimer receiptEmailSendTimer;
	QString receiptEmailTempDir;
	QString receiptEmailTempFileName;
	QSharedPointer<ReceiptRenderer> receiptRenderer;
	QVector<int> emailReceipts;
	QSslEvpKey pdfSigningKey;
	int emailReceiptPos = 0;
	int emailingTaxReceiptState = EmailingTaxReceiptState::Idle;
};

#endif
