// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef MDM_USERS_API_H
#define MDM_USERS_API_H

#include <QList>
#include "api.h"

class UsersApi : public SubApi
{
public:
	UsersApi(MissionDM &api);
	void loadInfoFromDB();
	bool incomingPost(const QByteArray &topic, ssize_t count, FoxID &id, FoxHead::ContentType contentType, QByteArray incomingContent);
	QList<int> getMissionsList(int userId, bool includeAll = false);
	inline const QVector<QList<int>> missionUserCrossReference() { return missionUserCR; }

private:
	void publishUsersList();
	bool disablingLastAdmin(int id, QJsonObject &json);
	bool updateMissionList(const QJsonObject &json, int id);
	void buildMissionUserCrossReference();
	QVector<QList<int>> missionUserCR;
};

#endif
