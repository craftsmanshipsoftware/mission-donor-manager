// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

// This API Documentation --> http://www.craftsmanshipsoftware.ca/mission-donor-manager/api/index.html#donor

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QCache>
#include <QSqlQuery>
#include "missiondm.h"
#include "common.h"
#include "donor-api.h"

DonorApi::DonorApi(MissionDM &api) : SubApi(api)
{
	apiString = "donors";
}

void DonorApi::loadInfoFromDB()
{
	QSqlQuery query;

	if (!mdm.db.tables().contains("donors")) {
		qDebug("creating donors table");
		query.exec("create table donors (id int primary key, type int, forename text, surname text, sal text, addr_sal text, other_name text, address text, apt text, city text, province text, country text, postalcode text, email text, cellphone text, homephone text, workphone text, is_3rdparty bool, news int, last int, ppa0 text, ppa1 text, ppa2 text, ppa3 text, ppa4 text, ppa0_last int, ppa1_last int, ppa2_last int, ppa3_last int, ppa4_last int, deceased bool, imported_id text, added int, contact text)");
	}

	if (!mdm.db.tables().contains("donorsNotes")) {
		qDebug("creating donorsNotes table");
		query.exec("create table donorsNotes (id int primary key, log text)");
	}

	publishDonorList();
}

void DonorApi::publishDonorList()
{
	QJsonArray jList;
	QJsonArray j3rdparty;
	QSqlQuery query;
	query.prepare("SELECT id, forename, surname, is_3rdparty FROM donors");
	query.exec();
	if (query.first()) {
		do {
			QJsonObject jDonor;
			jDonor.insert("id", query.value("id").toInt());
			jDonor.insert("forename", query.value("forename").toString());
			jDonor.insert("surname", query.value("surname").toString());
			jList.append(jDonor);
			if (query.value("is_3rdparty").toBool())
				j3rdparty.append(jDonor);
		} while (query.next());
	}
	QJsonDocument jDoc;
	jDoc.setArray(jList);
	mdm.den.publish("donors.ro.list", FoxHead::ContentType::Json, jDoc.toJson(QJsonDocument::Compact));

	QJsonDocument jDoc3rdParties;
	jDoc3rdParties.setArray(j3rdparty);
	mdm.den.publish("donors.ro.3rdparties", FoxHead::ContentType::Json, jDoc3rdParties.toJson(QJsonDocument::Compact));
}

bool DonorApi::incomingPost(const QByteArray &topic, ssize_t, FoxID &asker, FoxHead::ContentType, QByteArray incomingContent)
{
	QByteArray subTopic = stripPrefix(topic);
	QSqlQuery query;

	auto writeDonorInfo = [&](int id, QJsonObject &injObj) {
		query.bindValue(":id", id);
		query.bindValue(":type", injObj.value("type").toInt());
		query.bindValue(":forename", injObj.value("forename").toString());
		query.bindValue(":surname", injObj.value("surname").toString());
		query.bindValue(":sal", injObj.value("salutation").toString());
		query.bindValue(":addr_sal", injObj.value("address-salutation").toString());
		query.bindValue(":other_name", injObj.value("other-name").toString());
		query.bindValue(":address", injObj.value("address").toString());
		query.bindValue(":apt", injObj.value("apt").toString());
		query.bindValue(":city", injObj.value("city").toString());
		query.bindValue(":province", injObj.value("province").toString());
		query.bindValue(":country", injObj.value("country").toString());
		query.bindValue(":postalcode", injObj.value("postalcode").toString());
		query.bindValue(":email", injObj.value("email").toString());
		query.bindValue(":cellphone", injObj.value("cellphone").toString());
		query.bindValue(":homephone", injObj.value("homephone").toString());
		query.bindValue(":workphone", injObj.value("workphone").toString());
		query.bindValue(":is_3rdparty", injObj.value("is-3rdparty").toBool());
		query.bindValue(":ppa0", injObj.value("ppa0").toString());
		query.bindValue(":ppa1", injObj.value("ppa1").toString());
		query.bindValue(":ppa2", injObj.value("ppa2").toString());
		query.bindValue(":ppa3", injObj.value("ppa3").toString());
		query.bindValue(":ppa4", injObj.value("ppa4").toString());
		query.bindValue(":deceased", injObj.value("deceased").toBool());
		query.bindValue(":news", Mail::TaxReceiptByEmail | Mail::ThankyouManualByEmail | Mail::ThankyouPaypalOneTime | Mail::ThankyouManualByEmail);
		query.bindValue(":contact", injObj.value("contact").toString());
//		qDebug() << query.lastError();
		// fixme if add, return ID, if query fails return nak, if edit succeeds, return ack?
	};

	if (subTopic == "add") {
		int newDonorId = getNewDonorId();
		QJsonObject injObj = QJsonDocument::fromJson(incomingContent).object();
		query.prepare("INSERT INTO donors (id, type, forename, surname, sal, addr_sal, other_name, address, apt, city, province, country, postalcode, email, cellphone, homephone, workphone, is_3rdparty, news, ppa0, ppa1, ppa2, ppa3, ppa4, deceased, added, contact) VALUES (:id, :type, :forename, :surname, :sal, :addr_sal, :other_name, :address, :apt, :city, :province, :country, :postalcode, :email, :cellphone, :homephone, :workphone, :is_3rdparty, :news, :ppa0, :ppa1, :ppa2, :ppa3, :ppa4, :deceased, :added, :contact)");
		writeDonorInfo(newDonorId, injObj);
		query.bindValue(":added", QDate::currentDate().toJulianDay());
		bool newDonorOk = query.exec();
		bool noteOk = addNoteEntry(newDonorId);
		bool accountOk = createAccount(newDonorId);
		// fixme - add backout if one query fails
		ackOrNack(asker, newDonorOk && noteOk && accountOk);
		publishDonorList();
	} else if (subTopic == "info-edit") {  // fixme - info-update
		QJsonObject injObj = QJsonDocument::fromJson(incomingContent).object();
		uint id = injObj.value("id").toInt();
		if (editLocks.value(id) == asker.local()) {
			query.prepare("UPDATE donors SET type=:type, forename=:forename, surname=:surname, sal=:sal, addr_sal=:addr_sal, other_name=:other_name, address=:address, apt=:apt, city=:city, province=:province, country=:country, postalcode=:postalcode, email=:email, cellphone=:cellphone, homephone=:homephone, workphone=:workphone, is_3rdparty=:is_3rdparty, ppa0=:ppa0, ppa1=:ppa1, ppa2=:ppa2, ppa3=:ppa3, ppa4=:ppa4, deceased=:deceased, contact=:contact WHERE id=:id");
			writeDonorInfo(id, injObj);
			ackOrNack(asker, query.exec());
			mdm.den.publish("donors.ro.info-updated", FoxHead::ContentType::IntAsString, QByteArray::number(id));
			mdm.den.removeMessage("donors.ro.info-updated");
			publishDonorList();
		} else {
			ackOrNack(asker, false);
		}
	} else if (subTopic == "mail-edit") {  // fixme - mail-update
		QJsonObject injObj = QJsonDocument::fromJson(incomingContent).object();
		uint id = injObj.value("id").toInt();
		if (editLocks.value(id) == asker.local()) {
			query.prepare("UPDATE donors SET news=:news WHERE id=:id");
			query.bindValue(":id", id);
			query.bindValue(":news", injObj.value("news").toInt());
			ackOrNack(asker, query.exec());
			mdm.den.publish("donors.ro.mail-updated", FoxHead::ContentType::IntAsString, QByteArray::number(id));
			mdm.den.removeMessage("donors.ro.mail-updated");
		} else {
			ackOrNack(asker, false);
		}
	} else if (subTopic == "note-edit") {  // fixme - note-update
		QJsonObject injObj = QJsonDocument::fromJson(incomingContent).object();
		uint id = injObj.value("id").toInt();
		if (editLocks.value(id) == asker.local()) {
			query.prepare("UPDATE donorsNotes SET log=:log WHERE id=:id");
			query.bindValue(":id", id);
			query.bindValue(":log", injObj.value("log").toString());
			ackOrNack(asker, query.exec());
			mdm.den.publish("donors.ro.note-updated", FoxHead::ContentType::IntAsString, QByteArray::number(id));
			mdm.den.removeMessage("donors.ro.note-updated");
		} else {
			ackOrNack(asker, false);
		}
	} else if (subTopic == "ro.info") {
		int id = incomingContent.toUInt();
		query.prepare("SELECT * FROM donors WHERE id=:id");
		query.bindValue(":id", id);
		query.exec();
		if (query.first()) {
			QJsonDocument outjDoc;
			QJsonObject outjObj;
			outjObj.insert("id", id);
			outjObj.insert("type", query.value("type").toInt());
			outjObj.insert("forename", query.value("forename").toString());
			outjObj.insert("surname", query.value("surname").toString());
			outjObj.insert("salutation", query.value("sal").toString());
			outjObj.insert("address-salutation", query.value("addr_sal").toString());
			outjObj.insert("other-name", query.value("other_name").toString());
			outjObj.insert("address", query.value("address").toString());
			outjObj.insert("apt", query.value("apt").toString());
			outjObj.insert("city", query.value("city").toString());
			outjObj.insert("province", query.value("province").toString());
			outjObj.insert("country", query.value("country").toString());
			outjObj.insert("postalcode", query.value("postalcode").toString());
			outjObj.insert("email", query.value("email").toString());
			outjObj.insert("cellphone", query.value("cellphone").toString());
			outjObj.insert("homephone", query.value("homephone").toString());
			outjObj.insert("workphone", query.value("workphone").toString());
			outjObj.insert("imported-id", query.value("imported_id").toString());
			outjObj.insert("is-3rdparty", query.value("is_3rdparty").toBool());
			outjObj.insert("ppa0", query.value("ppa0").toString());
			outjObj.insert("ppa1", query.value("ppa1").toString());
			outjObj.insert("ppa2", query.value("ppa2").toString());
			outjObj.insert("ppa3", query.value("ppa3").toString());
			outjObj.insert("ppa4", query.value("ppa4").toString());
			outjObj.insert("ppa0-last", query.value("ppa0_last").toInt());
			outjObj.insert("ppa1-last", query.value("ppa1_last").toInt());
			outjObj.insert("ppa2-last", query.value("ppa2_last").toInt());
			outjObj.insert("ppa3-last", query.value("ppa3_last").toInt());
			outjObj.insert("ppa4-last", query.value("ppa4_last").toInt());
			outjObj.insert("deceased", query.value("deceased").toBool());
			outjObj.insert("added", query.value("added").toInt());
			outjObj.insert("contact", query.value("contact").toString());

			outjDoc.setObject(outjObj);
			mdm.den.answerPost(asker, currentTopic, FoxHead::ContentType::Json, outjDoc.toJson(QJsonDocument::Compact));
		} else {
			ackOrNack(asker, false);
		}
	} else if (subTopic == "ro.mail") {
		int id = incomingContent.toUInt();
		query.prepare("SELECT news FROM donors WHERE id=:id");
		query.bindValue(":id", id);
		query.exec();
		if (query.first()) {
			mdm.den.answerPost(asker, currentTopic, FoxHead::ContentType::IntAsString, QByteArray::number(query.value("news").toInt()));
		} else {
			ackOrNack(asker, false);
		}
	} else if (subTopic == "ro.note") {
		int id = incomingContent.toUInt();
		query.prepare("SELECT log FROM donorsNotes WHERE id=:id");
		query.bindValue(":id", id);
		query.exec();
		if (query.first()) {
			mdm.den.answerPost(asker, currentTopic, FoxHead::ContentType::Blob, query.value("log").toByteArray());
		} else {
			mdm.den.answerPost(asker, currentTopic, FoxHead::ContentType::Blob, "");
		}
	} else if (subTopic == "ro.summary") {
		int id = incomingContent.toUInt();
		QJsonDocument outjDoc;
		outjDoc.setArray(buildSummary(id));
		mdm.den.answerPost(asker, currentTopic, FoxHead::ContentType::Json, outjDoc.toJson(QJsonDocument::Compact));
	} else if (subTopic == "ro.account") {
		QJsonObject injObj = QJsonDocument::fromJson(incomingContent).object();
		QJsonDocument outjDoc;
		outjDoc.setArray(processAccountForPeriod(injObj.value("id").toInt(), injObj.value("financial-period").toInt()));
		mdm.den.answerPost(asker, currentTopic, FoxHead::ContentType::Json, outjDoc.toJson(QJsonDocument::Compact));
	} else if (subTopic == "ro.last-three") {
		QString response = buildLastThreeDonationsString(incomingContent.toUInt());
		mdm.den.answerPost(asker, currentTopic, FoxHead::ContentType::String, response.toUtf8());
	} else if (subTopic == "ro.receipts") {
		QJsonDocument outjDoc;
		outjDoc.setArray(getReceipts(incomingContent.toInt()));
		mdm.den.answerPost(asker, currentTopic, FoxHead::ContentType::Json, outjDoc.toJson(QJsonDocument::Compact));
	} else if (subTopic == "ro.receipt") {
		mdm.journal.setActiveUser(mdm.getUserIdOfClient(asker.local()));
		QJsonDocument outjDoc;
		outjDoc.setObject(buildReceiptInfo(incomingContent.toInt()));
		mdm.den.answerPost(asker, currentTopic, FoxHead::ContentType::Json, outjDoc.toJson(QJsonDocument::Compact));
	} else if (subTopic == "ro.sign-receipt") {
		QJsonObject injObj = QJsonDocument::fromJson(incomingContent).object();
		QByteArray hash = QByteArray::fromBase64(injObj.value("hash").toVariant().toByteArray());
		QSslEvpKey key = mdm.taxApi.getPrivateSigningKey(injObj.value("year").toInt());
		QByteArray signature = QSslUtils::signExisting3Sha256(hash, key);
		QJsonObject outjObj;
		outjObj.insert("signature", QString(signature.toBase64()));
		QJsonDocument outjDoc;
		outjDoc.setObject(outjObj);
		mdm.den.answerPost(asker, currentTopic, FoxHead::ContentType::Json, outjDoc.toJson(QJsonDocument::Compact));
	} else if (subTopic == "lock") {
		ackOrNack(asker, lock(incomingContent.toUInt(), asker.local()));
	} else if (subTopic == "unlock") {
		ackOrNack(asker, unlock(incomingContent.toUInt(), asker.local()));
	} else {
		return false;  // make top api send ErrorNoTopic
	}

	return true;
}

int DonorApi::getNewDonorId()
{
	return mdm.getTableRowCount("donors") + 1;
}

bool DonorApi::createAccount(int donorId)
{
	QSqlQuery query;
	return query.exec(QString("create table acnt_donor_%1 (id int primary key, year int, date int, entered_date int, source int, type int, thirdparty int, amount int, advantage int, fee int, percent real, mission_id int, acnt_idx int, proxy_id int, proxy_idx int, anonymous bool, inkind text, campaign text, invalid bool, export_id int, rid int, neftd bool)").arg(donorId));
}

bool DonorApi::addNoteEntry(int donorId)
{
	QSqlQuery query;
	query.prepare("INSERT INTO donorsNotes (id, log) VALUES (:id, :log)");
	query.bindValue(":id", donorId);
	query.bindValue(":log", "");
	return query.exec();
}

QJsonArray DonorApi::processAccountForPeriod(int id, int financialPeriod)
{
	QCache<int, QString> missionNameCache;
	QJsonArray jList;
	int processingYear = -1;
	int total = 0;
	int receiptableTotal = 0;
	QSqlQuery query;

	JulianDayRange range = convertFinancialSpanToJulianRange(financialPeriod);
	query.prepare(QString("SELECT * FROM acnt_donor_%1 WHERE date BETWEEN %2 AND %3 ORDER BY date").arg(id).arg(range.first).arg(range.second));
	query.exec();
	if (query.first()) {
		do {
			QDate date = QDate::fromJulianDay(query.value("date").toInt());
			int index = query.value("id").toUInt();
			bool invalid = query.value("invalid").toBool();
			bool neftd = query.value("neftd").toBool();
			int type = query.value("type").toUInt();
			int amount = query.value("amount").toUInt();
			int advantage = query.value("advantage").toUInt();
			int fee = query.value("fee").toUInt();
			qreal percentage = query.value("percent").toReal();
			if (date.year() != processingYear) {  // reset totals on year change
				processingYear = date.year();
				receiptableTotal = 0;
				total = 0;
			}
			if (invalid == false) {
				if (type == DonationType::Normal) {
					if (!neftd)
						receiptableTotal+= amount - advantage;
					total+= amount;
				} else if (type == DonationType::ThirdParty) {
					total+= amount;
				}
			}
			QString benefactor;
			int missionID = query.value("mission_id").toUInt();
			if (!missionNameCache.contains(missionID))  // 2 unique of 17, cache = 221-349uS, no cache = 557-617uS
				missionNameCache.insert(missionID, getMissionName(missionID));
			benefactor = *missionNameCache.object(missionID);

			QJsonObject jDonation;
			jDonation.insert("id", index);
			jDonation.insert("amount", amount);
			jDonation.insert("advantage", advantage);
			jDonation.insert("fee", fee);
			jDonation.insert("percentage", percentage);
			jDonation.insert("anonymous", query.value("anonymous").toBool());
			jDonation.insert("balance", total);
			jDonation.insert("benefactor", benefactor);
			jDonation.insert("date", query.value("date").toInt());
			jDonation.insert("date-entered", query.value("entered_date").toInt());
			jDonation.insert("invalid", invalid);
			jDonation.insert("receiptable", receiptableTotal);
			jDonation.insert("type", type);
			jDonation.insert("source", query.value("source").toInt());
			jDonation.insert("export-id", query.value("export_id").toInt());
			jDonation.insert("note", query.value("inkind").toString());
			jList.append(jDonation);
		} while (query.next());
	}
	return jList;
}

QString *DonorApi::getMissionName(int missionID)
{
	QSqlQuery query;
	query.prepare("SELECT name FROM missions WHERE id=:id");
	query.bindValue(":id", missionID);
	query.exec();
	query.first();
	return new QString(query.value("name").toString());
}

QJsonArray DonorApi::buildSummary(int id)
{
	QJsonArray jList;
	QSqlQuery query;

	constexpr int ReportingQuarters = 5;
	constexpr int Quarters = ReportingQuarters + 4;
	int i = 0;
	int total = 0;
	QDate current = QDate::currentDate();
	QDate quartAfterCurrent;

	int q = (current.month() - 1) / 3;  // convert current month to quarter
	q++;    // advance one quarter
	int m = q * 3 + 1;  // convert back to months
	quartAfterCurrent.setDate(current.year() + m / 12, m % 12, 1);
	QDate oldestQuarter = quartAfterCurrent.addMonths(-Quarters * 3);
	QDate nOut = quartAfterCurrent.addMonths(-3);
	QVector<int> pastQuarterTotals(Quarters, 0);

	query.prepare(QString("SELECT * FROM acnt_donor_%1 WHERE date BETWEEN %2 AND %3 ORDER BY date").arg(id).arg(oldestQuarter.toJulianDay()).arg(quartAfterCurrent.addDays(-1).toJulianDay()));
	query.exec();
	if (query.first()) {
		QDate quarterStart = oldestQuarter;
		QDate quarterEnd = quarterStart.addMonths(3).addDays(-1);
		do {
			int date = query.value("date").toUInt();
			bool voided = query.value("invalid").toBool();
			int type = query.value("type").toUInt();
			int amount = query.value("amount").toUInt();

			// check if we changed quarters
			while (quarterEnd.toJulianDay() < date && i < Quarters) {
				quarterStart = quarterStart.addMonths(3);
				quarterEnd = quarterStart.addMonths(3).addDays(-1);
				i++;
				total = 0;
			}

			if (voided == false) {
				if (type != DonationType::Inkind)
					total+= amount;
			}
			pastQuarterTotals[i] = total;
		} while (query.next());
	}

	for (i = ReportingQuarters-1; i > -1; i--) {  // vector 0 = oldest quarter, vector 8 = newest quarter, so go backwards to reverse time and build json newest to oldest
		QJsonObject json;
		json.insert("quarter", QString("Q%1-%2").arg((nOut.month() - 1) / 3 + 1).arg(nOut.year()));
		json.insert("amount", pastQuarterTotals[i+4]);
		json.insert("percent-change", (float)(pastQuarterTotals[i+4] - pastQuarterTotals[i]) / pastQuarterTotals[i]);
		nOut = nOut.addMonths(-3);
		jList.append(json);
	}

	return jList;
}

QString DonorApi::buildLastThreeDonationsString(int id)
{
	QSqlQuery query;
	QString response;
	int donationsCount = mdm.getTableRowCount(QString("acnt_donor_%1").arg(id));
	if (donationsCount) {
		query.prepare(QString("SELECT * FROM acnt_donor_%1 ORDER BY date DESC, id DESC").arg(id));
		query.exec();
		query.first();
		int i = 0;
		do {
			if (!query.value("invalid").toBool()) {
				i++;
				QSqlQuery missionQuery;
				missionQuery.prepare("SELECT * FROM missions WHERE id=:id");
				missionQuery.bindValue(":id", query.value("mission_id").toUInt());
				missionQuery.exec();
				missionQuery.first();
				response+= QDate::fromJulianDay(query.value("date").toUInt()).toString("MMM d yyyy") + " - ";
				response+= missionQuery.value("account").toString() + " - " + missionQuery.value("name").toString() + " - ";
				response+= convertCentsToString(query.value("amount").toUInt(), true) +  '\n';
			}
		} while (query.next() && i < 3);
	}
	return response;
}

QJsonArray DonorApi::getReceipts(int donorId)
{
	QJsonArray jArray;
	QSqlQuery query;
	query.prepare("SELECT * FROM receipts WHERE donor_id=:donor_id");  // fixme order by year
	query.bindValue(":donor_id", donorId);
	query.exec();
	if (query.first()) {
		do {
			QJsonObject json;
			json.insert("id", query.value("id").toInt());
			json.insert("year", query.value("year").toInt());
			json.insert("date-issued", query.value("date_issued").toInt());
			json.insert("type", query.value("type").toInt());
			json.insert("amount", query.value("amount").toInt());
			jArray.append(json);
		} while (query.next());
	}
	return jArray;
}

QJsonObject DonorApi::buildReceiptInfo(int receiptId)
{
	QList<int> signatorIds;
	QSqlQuery receiptQuery;
	QSqlQuery donorQuery;
	QJsonObject json = mdm.taxApi.buildReceiptCommonData();

	QJsonArray jArray;  // a json array is used here because the pdf writer in the cleint excepts it...the writer in the cleint can write multiple receipts
	QJsonObject receipt;

	receiptQuery.prepare("SELECT * FROM receipts WHERE id=:id");
	receiptQuery.bindValue(":id", receiptId);
	receiptQuery.exec();
	receiptQuery.first();
	receipt.insert("id", receiptId);
	receipt.insert("year", receiptQuery.value("year").toInt());
	receipt.insert("donation-date", receiptQuery.value("donation_date").toInt());
	receipt.insert("date-issued", receiptQuery.value("date_issued").toInt());
	int type = receiptQuery.value("type").toInt();
	receipt.insert("type", type);
	receipt.insert("amount", receiptQuery.value("amount").toInt());
	receipt.insert("advantage", receiptQuery.value("advantage").toInt());
	int signatorId = receiptQuery.value("signator_id").toInt();
	receipt.insert("signator-id", signatorId);
	signatorIds.append(signatorId);

	int donorId = receiptQuery.value("donor_id").toInt();
	donorQuery.prepare(QString("SELECT * FROM donors WHERE id=%1").arg(donorId));
	donorQuery.exec();
	donorQuery.first();
	receipt.insert("donor-name", mdm.getAddressSalutation(donorQuery));
	receipt.insert("donor-address-l1", donorQuery.value("address").toString());
	receipt.insert("donor-apt", donorQuery.value("apt").toString());
	receipt.insert("donor-city", donorQuery.value("city").toString());
	receipt.insert("donor-province", donorQuery.value("province").toString());
	receipt.insert("donor-postalcode", donorQuery.value("postalcode").toString());
	receipt.insert("donor-country", donorQuery.value("country").toString());

	if (type == DonationType::Inkind) {
		QSqlQuery donationNoteQuery;
		donationNoteQuery.prepare(QString("SELECT inkind FROM acnt_donor_%1 WHERE id=%2").arg(donorId).arg(receiptQuery.value("did").toInt()));
		donationNoteQuery.exec();
		donationNoteQuery.first();
		receipt.insert("donation-note", donationNoteQuery.value("inkind").toString());
	}

	jArray.append(receipt);
	json.insert("receipts", jArray);
	json.insert("signators", mdm.taxApi.buildSignators(signatorIds));

	mdm.journal.log("DONOR - Downloading receipt:" + QString::number(receiptId) + " for donor:" + QString::number(donorId));

	return json;
}
