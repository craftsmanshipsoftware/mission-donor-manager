// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

// This API Documentation --> http://www.craftsmanshipsoftware.ca/mission-donor-manager/api/index.html#donor-export

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include "missiondm.h"
#include "common.h"
#include "donor-export-api.h"

DonorExportApi::DonorExportApi(MissionDM &api) : SubApi(api)
{
	apiString = "donor-export";
}

bool DonorExportApi::incomingPost(const QByteArray &topic, ssize_t, FoxID &asker, FoxHead::ContentType, QByteArray incomingContent)
{
	QByteArray subTopic = stripPrefix(topic);
	QSqlQuery query;
	int fromJulian = 1;
	int toJulian = 1;
	QByteArray csv;

	if (subTopic == "generate") {
		QJsonObject injObj = QJsonDocument::fromJson(incomingContent).object();

		// get donor filtration criteria
		bool filterByMission = injObj.value("filter-by-mission").toBool();
		int missionId = injObj.value("mission-id").toInt();
		bool filterByUser = injObj.value("filter-by-user").toBool();
		filterByNewsLetter = injObj.value("filter-by-news").toBool();
		international = injObj.value("filter-by-international").toBool();
		filterByRegion1 = injObj.value("filter-by-region1").toBool();
		region1 = injObj.value("region1").toInt();
		filterByRegion2 = injObj.value("filter-by-region2").toBool();
		region2 = injObj.value("region2").toInt();
		filterByRegion3 = injObj.value("filter-by-region3").toBool();
		region3 = injObj.value("region3").toInt();
		filterByType = injObj.value("filter-by-type").toBool();
		filterInactive = injObj.value("filter-inactive").toBool();
		type = injObj.value("type").toInt();

		// get column to export
		exportIdNumber = injObj.value("export-id").toBool();
		exportFirstName = injObj.value("export-firstname").toBool();
		exportLastName = injObj.value("export-lastname").toBool();
		exportAddress = injObj.value("export-address").toBool();
		exportEmail = injObj.value("export-email").toBool();
		exportCellPhone = injObj.value("export-phone-cell").toBool();
		exportHomePhone = injObj.value("export-phone-home").toBool();
		exportWorkPhone = injObj.value("export-phone-work").toBool();
		exportPaypalAccounts = injObj.value("export-paypal").toBool();
		makeHeader(csv);

		if (injObj.value("filter-by-time").toBool() || filterInactive) {
			fromJulian = injObj.value("fromJulian").toInt();
			toJulian = injObj.value("toJulian").toInt();
		} else {
			fromJulian = 1;
			toJulian = QDate::currentDate().toJulianDay();
		}

		QSqlQuery query;
		int donorRowCount = mdm.getTableRowCount("donors");
		QList<int> missionIds;
		QVector<bool> donorsFound = QVector<bool>(donorRowCount);

		if (filterByUser)
			missionIds = mdm.usersApi.getMissionsList(injObj.value("user-id").toInt());
		else
			missionIds.append(missionId);

		if (filterByMission || filterByUser) {
			for (int i=0; i<missionIds.size(); i++) {
				query.clear();
				query.prepare(QString("SELECT donor_id FROM acnt_%1 WHERE date BETWEEN %2 AND %3").arg(missionIds.at(i)).arg(fromJulian).arg(toJulian));
				query.exec();
				if (query.first()) {
					do {
						donorsFound[query.value("donor_id").toInt() - 1] = true;
					} while (query.next());
				}
				query.finish();
				if (filterInactive) {
					query.clear();
					query.prepare(QString("SELECT donor_id FROM acnt_%1 WHERE date BETWEEN %2 AND %3").arg(missionIds.at(i)).arg(toJulian + 1).arg(QDate::currentDate().toJulianDay()));
					query.exec();
					if (query.first()) {
						do {
							donorsFound[query.value("donor_id").toInt() - 1] = false;  // if they made a donation in the toJulian --> present time period, they are not inactive so remove them
						} while (query.next());
					}
					query.finish();
				}
			}
		} else {
			for (int i=0; i<donorRowCount; i++) {
				query.clear();
				query.prepare(QString("SELECT id FROM acnt_donor_%1 WHERE date BETWEEN %2 AND %3").arg(i+1).arg(fromJulian).arg(toJulian));
				query.exec();
				if (query.first())  // if at least one record is found in the date range, the donor gets added.  We don't need to loop through all records found.
					donorsFound[i] = true;
				query.finish();
			}
			if (filterInactive) {
				for (int i=0; i<donorRowCount; i++) {
					query.clear();
					query.prepare(QString("SELECT id FROM acnt_donor_%1 WHERE date BETWEEN %2 AND %3").arg(i+1).arg(toJulian + 1).arg(QDate::currentDate().toJulianDay()));
					query.exec();
					if (query.first())
						donorsFound[i] = false;  // if they made a donation in the toJulian --> present time period, they are not inactive so remove them
					query.finish();
				}
			}
		}

		for (int i=0; i<donorsFound.size(); i++) {
			if (donorsFound.at(i)) {
				query.clear();
				query.prepare("SELECT * FROM donors WHERE id=:id");
				query.bindValue(":id", i + 1);
				query.exec();
				if (query.first()) {
					if (filter(query))
						addDonor(query, csv);
				}
				query.finish();
			}
		}
		mdm.den.answerPost(asker, currentTopic, FoxHead::ContentType::Blob, csv);
	} else if (subTopic == "new-donors") {
		QJsonObject injObj = QJsonDocument::fromJson(incomingContent).object();
		fromJulian = injObj.value("fromJulian").toInt();
		toJulian = injObj.value("toJulian").toInt();
		query.clear();
		query.prepare(QString("SELECT * FROM donors WHERE added BETWEEN %1 AND %2").arg(fromJulian).arg(toJulian));
		query.exec();
		QJsonArray outjArray;
		if (query.first()) {
			do {
				outjArray.append(addDonorJson(query));
			} while (query.next());
		}

		QJsonDocument jdoc;
		jdoc.setArray(outjArray);
		mdm.den.answerPost(asker, currentTopic, FoxHead::ContentType::Json, jdoc.toJson(QJsonDocument::Compact));
	} else {
		return true;
	}

	return true;
}

bool DonorExportApi::filter(QSqlQuery &query)
{
	bool match = false;
	QString province;
	if (international || region1 == ProvinceAlphaCode::All || region2 == ProvinceAlphaCode::All || region3 == ProvinceAlphaCode::All)
		province = query.value("country").toString();
	else
		province = query.value("province").toString();

	if (!(filterByNewsLetter && !(query.value("news").toInt() & Mail::NewsLetter)) && !(international && !(regionMatch(ProvinceAlphaCode::Int, province))) && !(filterByType && !(query.value("type").toInt() == type))
	        && !(filterByRegion1 && !(regionMatch(region1, province) || (filterByRegion2 && regionMatch(region2, province)) || (filterByRegion3 && regionMatch(region3, province))))) {
		match = true;
	}

	return match;
}

bool DonorExportApi::regionMatch(int alphaCode, QString province)
{
	bool match = false;

	switch (alphaCode) {  // clang format off
		case ProvinceAlphaCode::All: QString::compare("CA", province) ? match = false : match  = true; break;
		case ProvinceAlphaCode::NL:  QString::compare("NL", province) ? match = false : match  = true; break;
		case ProvinceAlphaCode::PE:  QString::compare("PE", province) ? match = false : match  = true; break;
		case ProvinceAlphaCode::NS:  QString::compare("NS", province) ? match = false : match  = true; break;
		case ProvinceAlphaCode::NB:  QString::compare("NB", province) ? match = false : match  = true; break;
		case ProvinceAlphaCode::QC:  QString::compare("QC", province) ? match = false : match  = true; break;
		case ProvinceAlphaCode::ON:  QString::compare("ON", province) ? match = false : match  = true; break;
		case ProvinceAlphaCode::MB:  QString::compare("MB", province) ? match = false : match  = true; break;
		case ProvinceAlphaCode::SK:  QString::compare("SK", province) ? match = false : match  = true; break;
		case ProvinceAlphaCode::AB:  QString::compare("AB", province) ? match = false : match  = true; break;
		case ProvinceAlphaCode::BC:  QString::compare("BC", province) ? match = false : match  = true; break;
		case ProvinceAlphaCode::YT:  QString::compare("YT", province) ? match = false : match  = true; break;
		case ProvinceAlphaCode::NT:  QString::compare("NT", province) ? match = false : match  = true; break;
		case ProvinceAlphaCode::NU:  QString::compare("NU", province) ? match = false : match  = true; break;
		case ProvinceAlphaCode::Int: QString::compare("CA", province) ? match = true  : match  = false; break;
	}  // clang format on

	return match;
}

void DonorExportApi::makeHeader(QByteArray &csv)
{
	bool first = true;

	auto addTab = [&]() {
		if (!first)
			csv+= '\t';
		first = false;
	};

	if (exportIdNumber) {
		addTab();
		csv+= "ID";
	}
	if (exportFirstName) {
		addTab();
		csv+= "First Name";
	}
	if (exportLastName) {
		addTab();
		csv+= "Last Name";
	}
	if (exportAddress) {
		addTab();
		csv+= "Address";
	}
	if (exportEmail) {
		addTab();
		csv+= "Email";
	}
	if (exportCellPhone) {
		addTab();
		csv+= "Phone";
	}
	if (exportHomePhone) {
		addTab();
		csv+= "Home Phone";
	}
	if (exportWorkPhone) {
		addTab();
		csv+= "Work Phone";
	}
	if (exportPaypalAccounts) {
		addTab();
		csv+= "Paypal Accounts";
	}
	csv+= '\n';
}

void DonorExportApi::addDonor(QSqlQuery &query, QByteArray &csv)
{
	bool first = true;

	auto addTab = [&]() {
		if (!first)
			csv+= '\t';
		first = false;
	};

	if (exportIdNumber) {
		addTab();
		csv+= query.value("id").toString().toUtf8();
	}
	if (exportFirstName) {
		addTab();
		csv+= query.value("forename").toString().toUtf8();
	}
	if (exportLastName) {
		addTab();
		csv+= query.value("surname").toString().toUtf8();
	}
	if (exportAddress) {
		addTab();
		csv+= query.value("address").toString().toUtf8();
		csv+= ' ' + query.value("apt").toString().toUtf8();
		csv+= ' ' + query.value("city").toString().toUtf8();
		csv+= ' ' + query.value("province").toString().toUtf8();
		csv+= ' ' + query.value("postalcode").toString().toUtf8();
		csv+= ' ' + query.value("country").toString().toUtf8();
	}
	if (exportEmail) {
		addTab();
		csv+= query.value("email").toString().toUtf8();
	}
	if (exportCellPhone) {
		addTab();
		csv+= query.value("cellphone").toString().toUtf8();
	}
	if (exportHomePhone) {
		addTab();
		csv+= query.value("homephone").toString().toUtf8();
	}
	if (exportWorkPhone) {
		addTab();
		csv+= query.value("workphone").toString().toUtf8();
	}
	if (exportPaypalAccounts) {
		addTab();
		csv+= query.value("ppa0").toString().toUtf8() + ' ' + query.value("ppa1").toString().toUtf8() + ' ' + query.value("ppa2").toString().toUtf8() + ' ';
		csv+= query.value("ppa3").toString().toUtf8()  + ' ' + query.value("ppa4").toString().toUtf8();
	}
	csv+= '\n';
}

QJsonObject DonorExportApi::addDonorJson(QSqlQuery &query)
{
	QJsonObject	json;
	QString address;
	json.insert("id", query.value("id").toInt());
	json.insert("first", query.value("forename").toString());
	json.insert("last", query.value("surname").toString());
	address = query.value("address").toString() + ' ' + query.value("apt").toString() + ' ' + query.value("city").toString() + ' ' + query.value("province").toString() + ' ';
	address+= query.value("postalcode").toString() + ' ' + query.value("country").toString();
	json.insert("address", address);
	json.insert("email", query.value("email").toString());
	json.insert("cellphone", query.value("cellphone").toString());
	json.insert("homephone", query.value("homephone").toString());
	json.insert("workphone", query.value("workphone").toString());
	json.insert("paypal", query.value("ppa0").toString() + ' ' + query.value("ppa1").toString() + ' ' + query.value("ppa2").toString());
	return json;
}
