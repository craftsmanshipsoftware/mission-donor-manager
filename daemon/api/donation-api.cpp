// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

// This API Documentation --> http://www.craftsmanshipsoftware.ca/mission-donor-manager/api/index.html#donation

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QSqlQuery>
#include <cmath>
#include "common.h"
#include "missiondm.h"
#include "email-sender.h"
#include "donation-api.h"

DonationApi::DonationApi(MissionDM &api) : SubApi(api)
{
	apiString = "donations";
}

void DonationApi::loadInfoFromDB()
{
	int donors = mdm.getTableRowCount("donors");
	int missions = mdm.getTableRowCount("missions");
	int incomeAccounts = mdm.getTableRowCount("source_accounts");
	if (donors)
		modifiedDonors.resize(donors);
	if (missions)
		modifiedMissions.resize(missions);
	if (incomeAccounts)
		modifiedIncomeAccounts.resize(incomeAccounts);
}

bool DonationApi::incomingPost(const QByteArray &topic, ssize_t, FoxID &asker, FoxHead::ContentType, QByteArray incomingContent)
{
	QByteArray subTopic = stripPrefix(topic);

	mdm.journal.setActiveUser(mdm.getUserIdOfClient(asker.local()));
	if (subTopic == "add") {
		QJsonObject json = QJsonDocument::fromJson(incomingContent).object();
		int amount = json.value("amount-cents").toInt();
		int advantage = json.value("advantage-cents").toInt();
		int fee = json.value("fee-cents").toInt();
		bool anonymous = json.value("anonymous").toBool();
		bool disableAdminCut = json.value("disable-admin-cut").toBool();
		int donorId = json.value("donor-id").toInt();
		int date = json.value("date").toInt();
		int missionID = json.value("mission-id").toInt();
		int type = json.value("type").toInt();
		int thirdParty = json.value("3rd-party-id").toInt();
		int sa = json.value("source-account").toInt();
		int aa = json.value("advantage-account").toInt();
		QString campaign = json.value("campaign").toString();
		QString inkindDescription = json.value("in-kind-description").toString();

		bool ok = add(DonationVia::Manual, type, thirdParty, amount, advantage, fee, sa, aa, donorId, missionID, date, anonymous, inkindDescription, campaign, 0, disableAdminCut);
		ackOrNack(asker, ok);
		if (ok)
			publishModifiedAccounts();
	} else if (subTopic == "void") {
		QJsonObject json = QJsonDocument::fromJson(incomingContent).object();
		bool ok = voidDonation(json.value("donor-id").toInt(), json.value("donation-id").toInt(), json.value("voided").toBool());
		ackOrNack(asker, ok);
		if (ok)
			publishModifiedAccounts();
	} else {
		return false;  // make top api send ErrorNoTopic
	}
	return true;
}

// amount    --> the amount including paypal fees and the advantage.
// advantage --> the amount not eligible for tax deduction
// fee       --> paypal fee.  It is kept in the donor's receipt amount.  It gets removed from the amount recorded in the mission's amount, excluded from split
bool DonationApi::add(int source, int type, int thirdParty, int amount, int advantage, int fee, int sa, int aa, int donorId, int missionID, int date, bool anonymous, const QString &inkindDescription, const QString &campaign, int mailFlags, bool disableAdminCut)
{
	QSqlQuery query;
	int lastDonationDate;

	// the client sends type == DonationType::ThirdParty when it is a Foreign 3rd Party.  It is simpler to correct it here than have more complicated logic in the client.
	if (type == DonationType::ThirdParty) {
		query.prepare(QString("SELECT type FROM donors WHERE id=%1").arg(thirdParty));
		query.exec();
		query.first();
		if (query.value("type") == DonorType::ForeignCharity)
			type = DonationType::ForeignThirdParty;
	}

	// get percentage
	query.prepare("SELECT percentage, proxy, neftd FROM missions WHERE id=:id");
	query.bindValue(":id", missionID);
	query.exec();
	if (!query.first()) {
		qCritical("Donations - Unable to get percentage");
		return false;
	}
	float percentage = query.value("percentage").toFloat();
	if (type == DonationType::Inkind)
		percentage = 0.0f;
	else if (percentage < 0.0f)
		percentage = mdm.setupApi.standardRate;

	if (disableAdminCut)
		percentage = 0.0f;

	// calculate operating cut
	int cut;
	int missionAmount;
	int eligible = amount - advantage;
	if (mdm.setupApi.includePayPalFee) {
		cut = std::round(percentage * eligible);
		missionAmount = eligible - cut;
	} else {
		cut = std::round(percentage * (eligible - fee));
		missionAmount = eligible - cut - fee;
	}

	// If inkind and inkind export to iif is enable, force source account to the dedicated inkind source account
	if (mdm.setupApi.exportGik && type == DonationType::Inkind)
		sa = mdm.setupApi.exportGikAccount;

	// check if mission funds go to another charity via a proxy acount
	int proxy = query.value("proxy").toInt();
	bool usingProxy = false;
	if (proxy > 0)
		usingProxy = true;

	// ignor advantage account value if advantage is zero....client may not send correct advantage account value if there is no advantage in the donation
	if (!advantage)
		aa = 0;

	// load mission's Not Eligible for Tax Deduction flag
	bool neftd = query.value("neftd").toBool();

	// get next donor record ID
	int nextDonorRecordID = mdm.getTableRowCount(QString("acnt_donor_%1").arg(donorId)) + 1;

	// get next mission record ID
	int nextMissionRecordID = mdm.getTableRowCount(QString("acnt_%1").arg(missionID)) + 1;

	// get next proxy mission record ID if mission account uses a proxy account
	int nextProxyId = -1;
	if (usingProxy)
		nextProxyId = mdm.getTableRowCount(QString("acnt_%1").arg(proxy)) + 1;

	// get date of donor's last donation
	QSqlQuery donorQuery;
	donorQuery.prepare("SELECT * FROM donors WHERE id=:id");
	donorQuery.bindValue(":id", donorId);
	donorQuery.exec();
	donorQuery.first();
	lastDonationDate = donorQuery.value("last").toInt();

	// insert record into donor's account table
	query.clear();
	query.prepare(QString("INSERT INTO acnt_donor_%1 (id, year, date, entered_date, source, type, thirdparty, amount, advantage, fee, percent, mission_id, acnt_idx, proxy_id, proxy_idx, anonymous, inkind, campaign, invalid, export_id, neftd) VALUES (:id, :year, :date, :entered_date, :source, :type, :thirdparty, :amount, :advantage, :fee, :percent, :mission_id, :acnt_idx, :proxy_id, :proxy_idx, :anonymous, :inkind, :campaign, :invalid, :export_id, :neftd)").arg(donorId));
	query.bindValue(":id", nextDonorRecordID);
	query.bindValue(":year", QDate::fromJulianDay(date).year());
	query.bindValue(":date", date);
	query.bindValue(":entered_date", QDate::currentDate().toJulianDay());
	query.bindValue(":source", source);
	query.bindValue(":amount", amount);
	query.bindValue(":advantage", advantage);
	query.bindValue(":fee", fee);
	query.bindValue(":percent", percentage);
	query.bindValue(":type", type);
	query.bindValue(":thirdparty", thirdParty);
	query.bindValue(":mission_id", missionID);
	query.bindValue(":acnt_idx", nextMissionRecordID);
	query.bindValue(":proxy_id", proxy);
	query.bindValue(":proxy_idx", nextProxyId);
	query.bindValue(":anonymous", anonymous);
	query.bindValue(":invalid", false);
	query.bindValue(":export_id", -1);
	query.bindValue(":inkind", inkindDescription);
	query.bindValue(":campaign", campaign);
	query.bindValue(":neftd", neftd);
	if (!query.exec()) {
		qCritical() << "Donations - Unable to insert into acnt_donor" << nextDonorRecordID;
		return false;
	}
	query.finish();

	auto deleteRecord = [](const QString &tableName, int id) {
		QSqlQuery query;
		query.prepare(QString("DELETE FROM %1 WHERE id=%2").arg(tableName).arg(id));
		query.exec();
	};

	auto addMissionRecord = [&](int missionID, int nextId, int proxy) {
		QSqlQuery mQuery;
		mQuery.prepare(QString("INSERT INTO acnt_%1 (id, year, date, type, thirdparty, sa, advtg_acnt, amount, advantage, gross, cut, donor_id, donor_acnt_idx, proxy_id, anonymous, invalid, export_id) VALUES (:id, :year, :date, :type, :thirdparty, :sa, :advtg_acnt, :amount, :advantage, :gross, :cut, :donor_id, :donor_acnt_idx, :proxy_id, :anonymous, :invalid, :export_id)").arg(missionID));
		mQuery.bindValue(":id", nextId);
		mQuery.bindValue(":year", QDate::fromJulianDay(date).year());
		mQuery.bindValue(":date", date);
		mQuery.bindValue(":type", type);
		mQuery.bindValue(":thirdparty", thirdParty);
		mQuery.bindValue(":sa", sa);
		mQuery.bindValue(":advtg_acnt", aa);
		mQuery.bindValue(":advantage", advantage);
		mQuery.bindValue(":amount", missionAmount);
		mQuery.bindValue(":gross", eligible);
		mQuery.bindValue(":cut", cut);
		mQuery.bindValue(":donor_id", donorId);
		mQuery.bindValue(":donor_acnt_idx", nextDonorRecordID);
		mQuery.bindValue(":proxy_id", proxy);
		mQuery.bindValue(":anonymous", anonymous);
		mQuery.bindValue(":invalid", false);
		mQuery.bindValue(":export_id", -1);
		return mQuery.exec();
	};

	// insert record into mission's account table
	if (!addMissionRecord(missionID, nextMissionRecordID, proxy)) {
		qCritical() << QString("Donations - Unable to insert into acnt_%1").arg(missionID);
		deleteRecord(QString("acnt_donor_%1").arg(donorId), nextDonorRecordID);
		return false;
	}

	if (usingProxy) {
		if (!addMissionRecord(proxy, nextProxyId, -1)) {
			qCritical() << QString("Donations - Unable to insert into proxy acnt_%1").arg(proxy);
			deleteRecord(QString("acnt_donor_%1").arg(donorId), nextDonorRecordID);
			deleteRecord(QString("acnt_%1").arg(missionID), nextMissionRecordID);
		}
	}

	// record newest donation date in donor table
	if (date > lastDonationDate) {
		lastDonationDate = date;
		query.prepare("UPDATE donors SET last=:last WHERE id=:id");
		query.bindValue(":id", donorId);
		query.bindValue(":last", lastDonationDate);
		query.exec();
		query.finish();
	}

	markDonorModified(donorId);
	markMissionModified(missionID);
	markIncomeAccountModified(sa);
	if (usingProxy)
		markMissionModified(proxy);

	if (type == DonationType::Normal)  // don't send thankyou when it is a 3rd party (they send their own) or a inkind.
		mdm.thankyouApi.doThankyou(donorQuery, source, thirdParty, amount, missionID, date, mailFlags);

	if (amount >= mdm.setupApi.donationNotificationThreshold && type != DonationType::Inkind)
		notifyMissionAssociates(donorQuery, donorId, missionID, amount);

	mdm.journal.logDonation(source, type, thirdParty, amount, advantage, fee, sa, donorId, nextDonorRecordID, missionID, date);

	return true;
}

bool DonationApi::voidDonation(int donorId, int donationId, bool voided)
{
	struct DonationCrossReference dcr;
	bool ok = true;

	auto updateDonorAccountForVoid = [](struct DonationCrossReference &dcr, bool voided) {
		QSqlQuery query;
		query.prepare(QString("UPDATE acnt_donor_%1 SET invalid=:invalid WHERE id=:id").arg(dcr.donorId));
		query.bindValue(":id", dcr.donationId);
		query.bindValue(":invalid", voided);
		return query.exec();
	};

	auto updateMissionAccountForVoid = [](int accountId, int index, bool voided) {
		QSqlQuery query;
		query.prepare(QString("UPDATE acnt_%1 SET invalid=:invalid WHERE id=:id").arg(accountId));
		query.bindValue(":id", index);
		query.bindValue(":invalid", voided);
		return query.exec();
	};

	auto updateReceiptForVoid = [](int rid, int amount) {
		QSqlQuery query;

		// get current amount in receipt
		query.prepare(QString("SELECT amount FROM receipts WHERE id=%1").arg(rid));
		query.exec();
		query.first();
		int total = query.value("amount").toInt();
		total+= amount;

		// set new total
		query.prepare("UPDATE receipts SET amount=:amount WHERE id=:id");
		query.bindValue(":id", rid);
		query.bindValue(":amount", total);
		return query.exec();
	};

	ok = buildCrossReference(donorId, donationId, dcr);

	// first check if invalid will change, else return
	if (!(ok && dcr.missionIndexVoided ^ voided))
		return false;

	// update Donor
	if(ok)
		ok = updateDonorAccountForVoid(dcr, voided);

	// update Mission
	if (ok) {
		ok = updateMissionAccountForVoid(dcr.missionId, dcr.missionAccountIndex, voided);
		if (!ok)
			updateDonorAccountForVoid(dcr, !voided);
	}

	// update Proxy Mission
	if (ok && dcr.proxyId > 0) {
		ok = updateMissionAccountForVoid(dcr.proxyId, dcr.proxyIndex, voided);
		if (!ok) {
			updateDonorAccountForVoid(dcr, !voided);
			updateMissionAccountForVoid(dcr.missionId, dcr.missionAccountIndex, !voided);
		}
	}

	// update Receipt
	if (ok) {
		QSqlQuery query;
		query.prepare(QString("SELECT amount, rid FROM acnt_donor_%1 WHERE id=%2").arg(donorId).arg(donationId));
		query.exec();
		query.first();
		int rid = query.value("rid").toInt();
		if (rid > 0) {                          // only update receipt if donation has a receipt assocaited with it
			int amount = query.value("amount").toInt();
			if (voided)
				amount*= -1;
			ok = updateReceiptForVoid(rid, amount);
			if (!ok) {
				updateDonorAccountForVoid(dcr, !voided);
				updateMissionAccountForVoid(dcr.missionId, dcr.missionAccountIndex, !voided);
				if (dcr.proxyId > 0)
					updateMissionAccountForVoid(dcr.proxyId, dcr.proxyIndex, !voided);
			}
		}
	}

	if (ok) {
		markDonorModified(dcr.donorId);
		markMissionModified(dcr.missionId);
		markIncomeAccountModified(dcr.incomeAccount);
		if (dcr.proxyId > 0)
			markMissionModified(dcr.proxyId);
	}

	mdm.journal.logVoiding(voided, donorId, dcr.donationId);

	return ok;
}

bool DonationApi::buildCrossReference(int donorId, int donationId, struct DonationCrossReference &dcr)
{
	QSqlQuery query;
	bool ok = true;

	dcr.donorId = donorId;
	dcr.donationId = donationId;
	query.prepare(QString("SELECT mission_id, acnt_idx, proxy_id, proxy_idx, invalid FROM acnt_donor_%1 WHERE id=:id").arg(donorId));
	query.bindValue(":id", donationId);
	query.exec();
	if (query.first()) {
		dcr.missionId = query.value("mission_id").toUInt();
		dcr.missionAccountIndex = query.value("acnt_idx").toUInt();
		dcr.proxyId = query.value("proxy_id").toUInt();
		dcr.proxyIndex = query.value("proxy_idx").toUInt();
		dcr.missionIndexVoided = query.value("invalid").toBool();
		query.finish();
		query.clear();
		query.prepare(QString("SELECT sa FROM acnt_%1 WHERE id=%2").arg(dcr.missionId).arg(dcr.missionAccountIndex));
		query.exec();
		query.first();
		dcr.incomeAccount = query.value("sa").toUInt();
	} else {
		ok = false;
	}

	return ok;
}

void DonationApi::markDonorModified(int donorId)
{
	if (modifiedDonors.size() < donorId)
		modifiedDonors.resize(donorId);
	modifiedDonors[donorId - 1] = true;
}

void DonationApi::markMissionModified(int missionId)
{
	if (modifiedMissions.size() < missionId)
		modifiedMissions.resize(missionId);
	modifiedMissions[missionId - 1] = true;
}

void DonationApi::markIncomeAccountModified(int incomeAccount)
{
	if (modifiedIncomeAccounts.size() < incomeAccount)
		modifiedIncomeAccounts.resize(incomeAccount);
	modifiedIncomeAccounts[incomeAccount - 1] = true;
}

void DonationApi::markAllIncomeAccountsModified()
{
	int incomeAccounts = mdm.getTableRowCount("source_accounts");
	if (modifiedIncomeAccounts.size() < incomeAccounts)
		modifiedIncomeAccounts.resize(incomeAccounts);
	for (int i = 0; i < modifiedIncomeAccounts.size(); i++)
		modifiedIncomeAccounts[i] = true;
}

// this is called externally by recurring donations and paypal after they have finished calling add() multiple times.  It is also called internally after add() or voidDonation().
// It's purpose is to inform clients which donors and missions financials have been updated.  If a client is currently viewing one of those donor's or mission's financials, it can query a refresh
void DonationApi::publishModifiedAccounts()
{
	QJsonArray jDonorsArray;
	for (int i=0; i< modifiedDonors.size(); i++) {
		if (modifiedDonors.at(i)) {
			jDonorsArray.append(i+1);
			modifiedDonors[i] = false;  // clear flag after adding to publish
		}
	}

	QJsonArray jMissionsArray;
	for (int i=0; i< modifiedMissions.size(); i++) {
		if (modifiedMissions.at(i)) {
			jMissionsArray.append(i+1);
			modifiedMissions[i] = false;  // clear flag after adding to publish
		}
	}

	if (jDonorsArray.size()) {
		QJsonDocument jDonorsDoc;
		jDonorsDoc.setArray(jDonorsArray);
		mdm.den.publish("donations.md.donor-accounts-updated", FoxHead::ContentType::Json, jDonorsDoc.toJson(QJsonDocument::Compact));
		mdm.den.removeMessage("donations.md.donor-accounts-updated");
	}

	if (jMissionsArray.size()) {
		QJsonDocument jMissionsDoc;
		jMissionsDoc.setArray(jMissionsArray);
		mdm.den.publish("donations.md.mission-accounts-updated", FoxHead::ContentType::Json, jMissionsDoc.toJson(QJsonDocument::Compact));
		mdm.den.removeMessage("donations.md.mission-accounts-updated");
	}

	publishModifiedIncomeAccounts();
	mdm.thankyouApi.publishMailWaitingCount();
}

void DonationApi::publishModifiedIncomeAccounts()
{
	QJsonArray jIncomeAccountsArray;
	for (int i=0; i< modifiedIncomeAccounts.size(); i++) {
		if (modifiedIncomeAccounts.at(i)) {
			jIncomeAccountsArray.append(i+1);
			modifiedIncomeAccounts[i] = false;  // clear flag after adding to publish
		}
	}

	if (jIncomeAccountsArray.size()) {
		QJsonDocument jIncomeAccountsDoc;
		jIncomeAccountsDoc.setArray(jIncomeAccountsArray);
		mdm.den.publish("donations.md.income-accounts-updated", FoxHead::ContentType::Json, jIncomeAccountsDoc.toJson(QJsonDocument::Compact));
		mdm.den.removeMessage("donations.md.income-accounts-updated");
	}
}

void DonationApi::notifyMissionAssociates(const QSqlQuery &donorQuery, int donorId, int missionId, int amount)
{
	if (mdm.usersApi.missionUserCrossReference().size() > (missionId - 1)) {  // missionUserCR does not get updated until a user is associated with a Mission.  Donations can happen after a mission is added and before a user is associated with it, so don't crash!
		QList<int> userList = mdm.usersApi.missionUserCrossReference().at(missionId - 1);
		QString donorString = mdm.getSalutation(donorQuery) + " (id: " + QString::number((donorId)) + ")";
		for (int i = 0; i < userList.size(); i++ ) {
			QSqlQuery missionQuery;
			missionQuery.prepare("SELECT name, account FROM missions WHERE id=:id");
			missionQuery.bindValue(":id", missionId);
			missionQuery.exec();
			missionQuery.first();
			QString missionString = missionQuery.value("name").toString() + " (" + missionQuery.value("account").toString() + ")";

			QSqlQuery userQuery;
			missionQuery.prepare("SELECT email FROM users WHERE id=:id");
			missionQuery.bindValue(":id", userList.at(i));
			missionQuery.exec();
			missionQuery.first();

			QString body = '\n' + donorString + " has made a significant donation of " + convertCentsToString(amount, true)+ " to " + missionString + '\n';
			body+= "\nContact info\n\n";
			body+= donorQuery.value("email").toString() + "\n\n";
			body+= donorQuery.value("address").toString() + " " + donorQuery.value("apt").toString() + '\n';
			body+= donorQuery.value("city").toString() + ", " + donorQuery.value("province").toString() + ' ' + donorQuery.value("postalcode").toString() + '\n';
			body+= donorQuery.value("country").toString();
			mdm.email.send(mdm.setupApi.donorRelationsEmail.toUtf8(), missionQuery.value("email").toString().toUtf8(), "Mission DM - Significant Donation", body.toUtf8());
		}
	}
}
