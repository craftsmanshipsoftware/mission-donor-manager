// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

// This API Documentation --> http://www.craftsmanshipsoftware.ca/mission-donor-manager/api/index.html#income-accounts

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QSqlQuery>
#include "missiondm.h"
#include "common.h"
#include "income-accounts-api.h"

IncomeAccountsApi::IncomeAccountsApi(MissionDM &api) : SubApi(api)
{
	apiString = "income-accounts";
}

void IncomeAccountsApi::loadInfoFromDB()
{
	QSqlQuery query;

	if (!mdm.db.tables().contains("source_accounts")) {
		qDebug("creating source accounts table");
		query.exec("create table source_accounts (id int primary key, name text, number text, enabled bool)");
		query.finish();
	}
	publishSourceAccounts();
}

bool IncomeAccountsApi::incomingPost(const QByteArray &topic, ssize_t, FoxID &asker, FoxHead::ContentType, QByteArray incomingContent)
{
	QByteArray subTopic = stripPrefix(topic);
	QSqlQuery query;

	if (subTopic == "add") {
		QJsonObject json = QJsonDocument::fromJson(incomingContent).object();
		query.prepare("INSERT INTO source_accounts (id, name, number, enabled) VALUES (:id, :name, :number, :enabled)");
		query.bindValue(":id", mdm.getTableRowCount("source_accounts") + 1);
		query.bindValue(":name", json.value("name").toString());
		query.bindValue(":number", json.value("number").toString());
		query.bindValue(":enabled", json.value("enabled").toBool());
		bool ok = query.exec();
		ackOrNack(asker, ok);
		if (ok)
			publishSourceAccounts();
	} else if (subTopic == "update") {
		if (asker.local() == editLocks.value(0)) {
			QJsonObject json = QJsonDocument::fromJson(incomingContent).object();
			query.prepare("UPDATE source_accounts SET name=:name, number=:number, enabled=:enabled WHERE id=:id");
			int id = json.value("id").toInt();
			bool enabled = json.value("enabled").toBool();
			query.bindValue(":id", id);
			query.bindValue(":name", json.value("name").toString());
			query.bindValue(":number", json.value("number").toString());
			query.bindValue(":enabled", enabled);
			bool ok = query.exec();
			ackOrNack(asker, ok);
			if (ok) {
				checkIfPaypalSourceAccountGotDisabled(id, enabled);
				publishSourceAccounts();
			}
		} else {
			ackOrNack(asker, false);
		}
	} else if (subTopic == "lock") {
		ackOrNack(asker, lock(0, asker.local()));
	} else if (subTopic == "unlock") {
		ackOrNack(asker, unlock(0, asker.local()));
	} else {
		return false;  // make top api send ErrorNoTopic
	}

	return true;
}

void IncomeAccountsApi::checkIfPaypalSourceAccountGotDisabled(int id, bool enabled)
{
	if (mdm.setupApi.paypalSourceAccount == id && !enabled) {
		QSqlQuery query;
		query.prepare("UPDATE setup SET paypal_sa=:paypal_sa WHERE id=0");
		query.bindValue(":paypal_sa", -1);
		query.exec();
	}
}

void IncomeAccountsApi::publishSourceAccounts()
{
	QSqlQuery query;
	QJsonArray accountsAll;
	QJsonArray accountsEnabled;

	query.prepare("SELECT * FROM source_accounts");
	query.exec();
	if (query.first()) {
		do {
			QJsonObject account;
			int id = query.value("id").toInt();
			QString name = query.value("name").toString();
			QString number = query.value("number").toString();
			bool enabled = query.value("enabled").toBool();

			account.insert("id", id);
			account.insert("name", name);
			account.insert("number", number);
			account.insert("enabled", enabled);
			accountsAll.append(account);
			if (enabled)
				accountsEnabled.append(account);

		} while (query.next());
	}

	QJsonDocument jDocAll;
	jDocAll.setArray(accountsAll);
	mdm.den.publish("income-accounts.list-all", FoxHead::ContentType::Json, jDocAll.toJson(QJsonDocument::Compact));

	QJsonDocument jDocEnabled;
	jDocEnabled.setArray(accountsEnabled);
	mdm.den.publish("income-accounts.list-enabled", FoxHead::ContentType::Json, jDocEnabled.toJson(QJsonDocument::Compact));
}
