// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef MDM_DONOR_API_H
#define MDM_DONOR_API_H

#include "api.h"

class QSqlQuery;

class DonorApi : public SubApi
{
public:
	DonorApi(MissionDM &api);
	void loadInfoFromDB();
	bool incomingPost(const QByteArray &topic, ssize_t count, FoxID &asker, FoxHead::ContentType contentType, QByteArray incomingContent);
	int getNewDonorId();
	bool addNoteEntry(int donorId);
	bool createAccount(int donorId);
	void publishDonorList();

private:
	QString buildLastThreeDonationsString(int id);
	QJsonArray processAccountForPeriod(int id, int financialPeriod);
	QString *getMissionName(int missionID);
	QJsonArray buildSummary(int id);
	QJsonArray getReceipts(int donorId);
	QJsonObject buildReceiptInfo(int receiptId);
};

#endif
