// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

// This API Documentation --> http://www.craftsmanshipsoftware.ca/mission-donor-manager/api/index.html#setup

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QSqlQuery>
#include "missiondm.h"
#include "common.h"
#include "setup-api.h"

SetupApi::SetupApi(MissionDM &api) : SubApi(api)
{
	apiString = "setup";
}

void SetupApi::loadInfoFromDB()
{
	QSqlQuery query;

	if (!mdm.db.tables().contains("setup")) {
		qDebug("creating setup table");
		query.exec("create table setup (id int primary key, paypal_client_id text, paypal_secret_key text, paypal_sa int, include_fee, outgoing_email_server text, outgoing_email_server_port int, donor_rel_email text, standard_rate real, operation_account text, business_number text, business_name text, address_l1 text, address_l2 text, address_l3 text, phone text, fiscal_end int, signator int, issue_location text, banner blob, dn_threshold int DEFAULT 100000 NOT NULL, export_gik bool, export_gik_account int)");
		query.finish();

		//create row 0 (setup table only has one row)
		query.clear();
		query.prepare("INSERT INTO setup (id) VALUES (0)");
		query.exec();
	} else {
		populateFromDB();
		QJsonObject json;
		populateReceiptFromDB(json);
		loadBannerFromDB();
	}
}

QJsonObject SetupApi::populateFromDB()
{
	QSqlQuery query;
	QJsonObject json;

	query.prepare("SELECT * FROM setup WHERE id=0");
	query.exec();
	query.first();

	paypalClientId = query.value("paypal_client_id").toString();
	json.insert("paypal-client-id", paypalClientId);

	paypalSecret = query.value("paypal_secret_key").toString();
	json.insert("paypal-secret-key", paypalSecret);

	paypalSourceAccount = query.value("paypal_sa").toInt();
	json.insert("paypal-sa", paypalSourceAccount);

	includePayPalFee =  query.value("include_fee").toBool();
	json.insert("include-fee", includePayPalFee);

	outgoingMailServer = query.value("outgoing_email_server").toString();
	json.insert("outgoing-email-server", outgoingMailServer);

	outgoingMailPort = query.value("outgoing_email_server_port").toInt();
	json.insert("outgoing-email-server-port", outgoingMailPort);

	donorRelationsEmail = query.value("donor_rel_email").toString();
	json.insert("donor-rel-email", donorRelationsEmail);

	standardRate = query.value("standard_rate").toReal();
	json.insert("standard-rate", standardRate);

	operationsAccountNumber = query.value("operation_account").toString();
	json.insert("operation-account", operationsAccountNumber);

	fiscalEnd = query.value("fiscal_end").toInt();
	if (!fiscalEnd)  // show today's date if not yet set
		fiscalEnd = QDate::currentDate().toJulianDay();
	json.insert("fiscal-end", fiscalEnd);

	donationNotificationThreshold = query.value("dn_threshold").toInt();
	json.insert("dn-threshold", donationNotificationThreshold);

	exportGik =  query.value("export_gik").toBool();
	json.insert("export-gik", exportGik);

	exportGikAccount = query.value("export_gik_account").toInt();
	json.insert("export-gik-account", exportGikAccount);

	return json;
}

void SetupApi::populateReceiptFromDB(QJsonObject &json)
{
	QSqlQuery query;

	query.prepare("SELECT * FROM setup WHERE id=0");
	query.exec();
	query.first();

	craBusinessNumber = query.value("business_number").toString();
	json.insert("cra-business-number", craBusinessNumber);

	businessName = query.value("business_name").toString();
	json.insert("business-name", businessName);

	addressLine1 = query.value("address_l1").toString();
	json.insert("address-l1", addressLine1);

	addressLine2 = query.value("address_l2").toString();
	json.insert("address-l2", addressLine2);

	addressLine3 = query.value("address_l3").toString();
	json.insert("address-l3", addressLine3);

	phone = query.value("phone").toString();
	json.insert("phone", phone);

	issueLocation = query.value("issue_location").toString();
	json.insert("issue-location", issueLocation);
}

void SetupApi::loadBannerFromDB()
{
	QSqlQuery query;
	query.prepare("SELECT banner FROM setup WHERE id=0");
	query.exec();
	query.first();
	banner = query.value("banner").toByteArray();
}

bool SetupApi::incomingPost(const QByteArray &topic, ssize_t, FoxID &asker, FoxHead::ContentType, QByteArray incomingContent)
{
	QByteArray subTopic = stripPrefix(topic);
	QSqlQuery query;

	if (subTopic == "get") {
		QJsonDocument jDoc;
		QJsonObject json = populateFromDB();
		populateReceiptFromDB(json);
		jDoc.setObject(json);
		mdm.den.answerPost(asker, currentTopic, FoxHead::ContentType::Json, jDoc.toJson(QJsonDocument::Compact));
	} else if (subTopic == "set") {
		if (asker.local() == editLocks.value(0)) {
			QJsonObject json = QJsonDocument::fromJson(incomingContent).object();
			query.prepare("UPDATE setup SET paypal_client_id=:paypal_client_id, paypal_secret_key=:paypal_secret_key, paypal_sa=:paypal_sa, include_fee=:include_fee, outgoing_email_server=:outgoing_email_server, outgoing_email_server_port=:outgoing_email_server_port, donor_rel_email=:donor_rel_email, standard_rate=:standard_rate, operation_account=:operation_account, business_number=:business_number, business_name=:business_name, address_l1=:address_l1, address_l2=:address_l2, address_l3=:address_l3, phone=:phone, issue_location=:issue_location, fiscal_end=:fiscal_end, dn_threshold=:dn_threshold, export_gik_account=:export_gik_account, export_gik=:export_gik WHERE id=0");

			query.bindValue(":paypal_client_id", json.value("paypal-client-id").toString());
			query.bindValue(":paypal_secret_key", json.value("paypal-secret-key").toString());
			query.bindValue(":paypal_sa", json.value("paypal-sa").toInt());
			query.bindValue(":include_fee", json.value("include-fee").toBool());
			query.bindValue(":outgoing_email_server", json.value("outgoing-email-server").toString());
			query.bindValue(":outgoing_email_server_port", json.value("outgoing-email-server-port").toInt());
			query.bindValue(":donor_rel_email", json.value("donor-rel-email").toString());
			query.bindValue(":standard_rate", json.value("standard-rate").toDouble());
			query.bindValue(":operation_account", json.value("operation-account").toString());
			query.bindValue(":business_number", json.value("cra-business-number").toString());
			query.bindValue(":business_name", json.value("business-name").toString());
			query.bindValue(":address_l1", json.value("address-l1").toString());
			query.bindValue(":address_l2", json.value("address-l2").toString());
			query.bindValue(":address_l3", json.value("address-l3").toString());
			query.bindValue(":phone", json.value("phone").toString());
			query.bindValue(":issue_location", json.value("issue-location").toString());
			query.bindValue(":fiscal_end", json.value("fiscal-end").toInt());
			query.bindValue(":dn_threshold", json.value("dn-threshold").toInt());
			query.bindValue(":export_gik", json.value("export-gik").toBool());
			query.bindValue(":export_gik_account", json.value("export-gik-account").toInt());

			bool ok = query.exec();
			ackOrNack(asker, ok);
			if (ok) {
				setupLock.lock();
				populateFromDB();
				setupLock.unlock();
				mdm.den.publish("setup.updated", FoxHead::ContentType::Empty, "updated");  // fixme - foxmq bug - doesn't publish if blank
				mdm.den.removeMessage("setup.updated");
			}
		} else {
			ackOrNack(asker, false);
		}
	} else if (subTopic == "set-banner") {
		bool ok = false;
		if (asker.local() == editLocks.value(0)) {
			query.prepare("UPDATE setup SET banner=:banner WHERE id=0");
			query.bindValue(":banner", incomingContent);
			ok = query.exec();
			if (ok) {
				setupLock.lock();
				banner = incomingContent;
				setupLock.unlock();
			}
		}
		ackOrNack(asker, ok);
	} else if (subTopic == "lock") {
		ackOrNack(asker, lock(0, asker.local()));
	} else if (subTopic == "unlock") {
		ackOrNack(asker, unlock(0, asker.local()));
	} else {
		return false;  // make top api send ErrorNoTopic
	}

	return true;
}
