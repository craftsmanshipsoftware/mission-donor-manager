// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

// This API Documentation --> http://www.craftsmanshipsoftware.ca/mission-donor-manager/api/index.html#iif

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QSqlQuery>
#include "missiondm.h"
#include "common.h"
#include "iif-api.h"

IifApi::IifApi(MissionDM &api) : SubApi(api)
{
	apiString = "iif";
}

void IifApi::loadInfoFromDB()
{
	QSqlQuery query;

	if (!mdm.db.tables().contains("iif")) {
		qDebug("creating iif export id table");
		query.exec("create table iif (id int primary key, end int, sa int)");
		mdm.db.commit();
	}
	publishExportIds();
}

void IifApi::publishExportIds()
{
	QJsonArray jList;
	QSqlQuery query;

	query.prepare("SELECT * FROM iif");
	query.exec();
	if (query.first()) {
		do {
			QJsonObject jExportId;
			jExportId.insert("id", query.value("id").toInt());
			int end = query.value("end").toInt();
			jExportId.insert("end", end);
			int sa = query.value("sa").toInt();
			if (sa > 0)
				jExportId.insert("sa", mdm.getIncomeAccount(sa));
			else
				jExportId.insert("sa", "All");
			jList.append(jExportId);
		} while (query.next());
	}
	QJsonDocument jDoc;
	jDoc.setArray(jList);
	mdm.den.publish("iif.exports", FoxHead::ContentType::Json, jDoc.toJson(QJsonDocument::Compact));
}

bool IifApi::incomingPost(const QByteArray &topic, ssize_t, FoxID &asker, FoxHead::ContentType, QByteArray incomingContent)
{
	QByteArray subTopic = stripPrefix(topic);
	QSqlQuery query;

	if (subTopic == "mark") {
		int newIifId = -1;
		if (asker.local() == editLocks.value(0)) {
			mdm.journal.setActiveUser(mdm.getUserIdOfClient(asker.local()));
			QJsonObject json = QJsonDocument::fromJson(incomingContent).object();
			int sourceAccount = json.value("source-account").toInt();
			int startDay = json.value("start-day").toInt();
			int julianDay = json.value("end-day").toInt();
			if (julianDay < 0) {  // use end of last month if negative
				QDate date =  QDate::currentDate().addMonths(-1);
				QDate endOfLastMonth(date.year(), date.month(), date.daysInMonth());
				julianDay = endOfLastMonth.toJulianDay();
			}
			int transactionsMarked = mark(startDay, julianDay, sourceAccount);
			mdm.den.answerPost(asker, topic, FoxHead::ContentType::IntAsString, QByteArray::number(transactionsMarked));
			if (transactionsMarked > 0) {
				publishExportIds();
				if (sourceAccount > 0)
					mdm.donationsApi.markIncomeAccountModified(sourceAccount);
				else
					mdm.donationsApi.markAllIncomeAccountsModified();
				mdm.donationsApi.publishModifiedIncomeAccounts();
				newIifId = mdm.getTableRowCount("iif");
			}
			mdm.journal.logIifGrouping(newIifId, sourceAccount, julianDay, transactionsMarked);
		} else {
			ackOrNack(asker, false);
		}
	} else if (subTopic == "mark-append") {
		if (!editLocks.size()) {
			mdm.journal.setActiveUser(mdm.getUserIdOfClient(asker.local()));
			int error = appendDonationToExistingGroup(QJsonDocument::fromJson(incomingContent).object());
			mdm.den.answerPost(asker, currentTopic, FoxHead::ContentType::IntAsString, QByteArray::number(error));
			if (!error)
				mdm.donationsApi.publishModifiedIncomeAccounts();
		} else {
			ackOrNack(asker, false);
		}
	} else if (subTopic == "unmark-donation") {
		if (!editLocks.size()) {
			mdm.journal.setActiveUser(mdm.getUserIdOfClient(asker.local()));
			bool error = removeDonationFromExistingGroup(QJsonDocument::fromJson(incomingContent).object());
			mdm.den.answerPost(asker, currentTopic, FoxHead::ContentType::IntAsString, QByteArray::number(error));
			if (!error)
				mdm.donationsApi.publishModifiedIncomeAccounts();
		} else {
			ackOrNack(asker, false);
		}
	} else if (subTopic == "ungroup-last") {
		if (asker.local() == editLocks.value(0)) {
			mdm.journal.setActiveUser(mdm.getUserIdOfClient(asker.local()));
			ungroupLastIif();
		} else {
			ackOrNack(asker, false);
		}
	} else if (subTopic == "download") {
		mdm.den.answerPost(asker, currentTopic, FoxHead::ContentType::Blob, buildIif(incomingContent.toUInt()));
	} else if (subTopic == "audit") {
		QJsonDocument jDoc;
		jDoc.setObject(buildAuditList(incomingContent.toUInt(), false));
		mdm.den.answerPost(asker, currentTopic, FoxHead::ContentType::Json, jDoc.toJson(QJsonDocument::Compact));
	} else if (subTopic == "proxy") {
		QJsonDocument jDoc;
		jDoc.setObject(buildAuditList(incomingContent.toUInt(), true));
		mdm.den.answerPost(asker, currentTopic, FoxHead::ContentType::Json, jDoc.toJson(QJsonDocument::Compact));
	} else if (subTopic == "unmarked") {
		QJsonDocument jDoc;
		jDoc.setObject(buildUnmarkedList(incomingContent.toUInt()));
		mdm.den.answerPost(asker, currentTopic, FoxHead::ContentType::Json, jDoc.toJson(QJsonDocument::Compact));
	} else if (subTopic == "lock") {
		ackOrNack(asker, lock(0, asker.local()));
	} else if (subTopic == "unlock") {
		ackOrNack(asker, unlock(0, asker.local()));
	} else {
		return false;  // make top api send ErrorNoTopic
	}

	return true;
}

int IifApi::mark(int fromJulianDay, int toJulianDay, int sourceAccount)
{
	int totalRowsmarked = 0;
	int exportId = mdm.getTableRowCount("iif") + 1;
	QVector<bool> modifiedDonors = QVector<bool>(mdm.getTableRowCount("donors"));

	// Mark Mission's transactions
	int total = mdm.getTableRowCount("missions");

	mdm.db.transaction();
	proxyDonationCount = 0;
	for (int i=1; i < (total + 1); i++) {
		QList<RowMark> rowsToMark;
		QString account = QString("acnt_%1").arg(i);
		rowsToMark = findTransactionsToMark(account, fromJulianDay, toJulianDay, sourceAccount, modifiedDonors);
		if (rowsToMark.size())
			markAccount(account, rowsToMark, exportId);
		totalRowsmarked+= rowsToMark.size();
	}

	// log export in iff table if anything was marked
	if (totalRowsmarked) {
		QSqlQuery query;
		query.prepare("INSERT INTO iif (id, end, sa) VALUES (:id, :end, :sa)");
		query.bindValue(":id", exportId);
		query.bindValue(":end", toJulianDay);
		query.bindValue(":sa", sourceAccount);
		query.exec();
	}
	mdm.db.commit();

	// refresh the donor's account view
	QJsonArray jDonorsArray;
	for (int i = 0; i < modifiedDonors.size(); i++) {
		if (modifiedDonors.at(i))
			jDonorsArray.append(i + 1);
	}
	if (jDonorsArray.size()) {
		QJsonDocument jDonorsDoc;
		jDonorsDoc.setArray(jDonorsArray);
		mdm.den.publishBriefly("donations.md.donor-accounts-updated", FoxHead::ContentType::Json, jDonorsDoc.toJson(QJsonDocument::Compact));
	}

	return totalRowsmarked - proxyDonationCount;
}

QList<IifApi::RowMark> IifApi::findTransactionsToMark(const QString &account, int fromJulianDay, int toJulianDay, int sourceAccount, QVector<bool> &modifiedDonors)
{
	QList<RowMark> rowsToMark;
	QSqlQuery query;

	if (sourceAccount > 0)  // positive means limit selection to a specific source account
		query.prepare(QString("SELECT id, donor_id, donor_acnt_idx, proxy_id, type FROM %1 WHERE export_id=-1 AND date BETWEEN %2 AND %3 AND sa=%4").arg(account).arg(fromJulianDay).arg(toJulianDay).arg(sourceAccount));
	else                     // negative means select all / any source accounts
		query.prepare(QString("SELECT id, donor_id, donor_acnt_idx, proxy_id, type FROM %1 WHERE export_id=-1 AND date BETWEEN %2 AND %3 ").arg(account).arg(fromJulianDay).arg(toJulianDay));
	query.exec();
	if (query.first()) {
		do {
			if (processInkind(query.value("type").toUInt(), sourceAccount)) {  // only process inkind if export inkind is enabled and SA=GIK, allways export normal
				RowMark rm;
				rm.id = query.value("id").toUInt();
				rm.donorId = query.value("donor_id").toUInt();
				modifiedDonors[rm.donorId - 1] = true;
				rm.donorAccountIndex = query.value("donor_acnt_idx").toUInt();
				if (query.value("proxy_id").toInt() > 0)
					proxyDonationCount++;
				rowsToMark.append(rm);
			}
		} while (query.next());
	}

	return rowsToMark;
}

void IifApi::markAccount(const QString &account, QList<RowMark> rowsToMark, int exportId)
{
	QSqlQuery query;
	for (int i=0; i<rowsToMark.size(); i++) {
		// mark row in mission acount
		query.clear();
		query.prepare(QString("UPDATE %1 SET export_id=:export_id WHERE id=:id").arg(account));
		query.bindValue(":id", rowsToMark.at(i).id);
		query.bindValue(":export_id", exportId);
		query.exec();
		query.finish();

		// mark row in donor account
		query.clear();
		query.prepare(QString("UPDATE acnt_donor_%1 SET export_id=:export_id WHERE id=:id").arg(rowsToMark.at(i).donorId));
		query.bindValue(":id", rowsToMark.at(i).donorAccountIndex);
		query.bindValue(":export_id", exportId);
		query.exec();
		query.finish();
	}
}

int IifApi::appendDonationToExistingGroup(const QJsonObject &json)
{
	int error = 0;
	int donorId = json.value("donor-id").toInt();
	int donationId = json.value("donation-id").toInt();
	int iifId =  json.value("iif").toInt();
	int dbIif = 0;
	int missionId;
	int missionDonationRow;
	int proxyId = -1;
	int proxyIdx = -1;
	int donationIncomeAccount;
	QSqlQuery query;

	// first check if given iif exists
	if (iifId > mdm.getTableRowCount("iif") || iifId == 0) {
		error = IifAppendError::InvalidIif;
	} else {
		// find mission account and row associated with this donation
		query.prepare(QString("SELECT mission_id, acnt_idx, proxy_id, proxy_idx, export_id FROM acnt_donor_%1 WHERE id=%2").arg(donorId).arg(donationId));
		query.exec();
		if (query.first()) {
			missionId = query.value("mission_id").toInt();
			missionDonationRow = query.value("acnt_idx").toInt();
			proxyId = query.value("proxy_id").toInt();
			proxyIdx = query.value("proxy_idx").toInt();
			dbIif = query.value("export_id").toInt();
		} else {
			error = IifAppendError::DonationNotFound;  // this should only happen if there is a bug in the client.
		}
		query.finish();
		query.clear();
	}

	// check if donation is already exported
	if (!error && dbIif > 0)
		error = IifAppendError::DonationAlreadyGrouped;  // this should only happen if there is a bug in the client.

	// check if donation income account == iif income account
	if (!error) {
		query.prepare(QString("SELECT sa FROM acnt_%1 WHERE id=%2").arg(missionId).arg(missionDonationRow));
		query.exec();
		query.first();
		donationIncomeAccount = query.value("sa").toInt();
		query.finish();
		query.clear();
		query.prepare(QString("SELECT sa FROM iif WHERE id=%1").arg(iifId));
		query.exec();
		query.first();
		if (donationIncomeAccount != query.value("sa").toInt())
			error = IifAppendError::IncomeAccountMissmatch;
	}

	if (!error) {
		mdm.donationsApi.markIncomeAccountModified(donationIncomeAccount);
		mdm.db.transaction();

		// mark row in mission acount
		query.prepare(QString("UPDATE acnt_%1 SET export_id=%2 WHERE id=%3").arg(missionId).arg(iifId).arg(missionDonationRow));
		query.exec();
		query.finish();

		// mark row in donor account
		query.clear();
		query.prepare(QString("UPDATE acnt_donor_%1 SET export_id=%2 WHERE id=%3").arg(donorId).arg(iifId).arg(donationId));
		query.exec();
		query.finish();

		if (proxyId > 0) {
			query.prepare(QString("UPDATE acnt_%1 SET export_id=%2 WHERE id=%3").arg(proxyId).arg(iifId).arg(proxyIdx));
			query.exec();
			query.finish();
		}
		mdm.db.commit();

		mdm.journal.logIifAppend(iifId, donorId, donationId);

		// refresh the donor's account view
		QJsonDocument jDonorsDoc;
		QJsonArray jDonorsArray;
		jDonorsArray.append(donorId);
		jDonorsDoc.setArray(jDonorsArray);
		mdm.den.publish("donations.md.donor-accounts-updated", FoxHead::ContentType::Json, jDonorsDoc.toJson(QJsonDocument::Compact));
		mdm.den.removeMessage("donations.md.donor-accounts-updated");
	}
	return error;
}

bool IifApi::removeDonationFromExistingGroup(const QJsonObject &json)
{
	int donorId = json.value("donor-id").toInt();
	int donationId = json.value("donation-id").toInt();
	int proxyId = -1;
	int proxyIdx = -1;
	int missionId;
	int missionDonationRow;
	int donationIncomeAccount;
	int dbIif;
	bool error = false;
	QSqlQuery query;

	// find mission account and row associated with this donation
	query.prepare(QString("SELECT mission_id, acnt_idx, proxy_id, proxy_idx, export_id FROM acnt_donor_%1 WHERE id=%2").arg(donorId).arg(donationId));
	query.exec();
	if (query.first()) {
		missionId = query.value("mission_id").toInt();
		missionDonationRow = query.value("acnt_idx").toInt();
		proxyId = query.value("proxy_id").toInt();
		proxyIdx = query.value("proxy_idx").toInt();
		dbIif = query.value("export_id").toInt();
	} else {
		error = true;  // this should only happen if there is a bug in the client.
	}
	query.finish();
	query.clear();

	if (!error) {
		query.prepare(QString("SELECT sa FROM acnt_%1 WHERE id=%2").arg(missionId).arg(missionDonationRow));
		query.exec();
		query.first();
		donationIncomeAccount = query.value("sa").toInt();
		query.finish();
		query.clear();
		mdm.donationsApi.markIncomeAccountModified(donationIncomeAccount);
		mdm.db.transaction();

		// mark row in mission acount
		query.prepare(QString("UPDATE acnt_%1 SET export_id=-1 WHERE id=%2").arg(missionId).arg(missionDonationRow));
		query.exec();
		query.finish();

		// mark row in donor account
		query.clear();
		query.prepare(QString("UPDATE acnt_donor_%1 SET export_id=-1 WHERE id=%2").arg(donorId).arg(donationId));
		query.exec();
		query.finish();

		if (proxyId > 0) {
			query.prepare(QString("UPDATE acnt_%1 SET export_id=-1 WHERE id=%2").arg(proxyId).arg(proxyIdx));
			query.exec();
			query.finish();
		}
		mdm.db.commit();

		mdm.journal.logIifRemoveDonation(dbIif, donorId, donationId);

		// refresh the donor's account view
		QJsonDocument jDonorsDoc;
		QJsonArray jDonorsArray;
		jDonorsArray.append(donorId);
		jDonorsDoc.setArray(jDonorsArray);
		mdm.den.publishBriefly("donations.md.donor-accounts-updated", FoxHead::ContentType::Json, jDonorsDoc.toJson(QJsonDocument::Compact));
	}
	return error;
}

// An iif export can have one or multiple source accounts.  iif exports with multiple source accounts have multiple lines starting with TRNS.
// Each TRNS line can have one or multiple SPL lines.  SPL = where the money goes taken from the source account listed on the TRNS.
// buildIif() looks for transactions in each mission account as well as the operational account with group IDs matching what was passed to it.
// buildIif() uses sumTransactionsByExportId() on each account.  sumTransactionsByExportId returns QMap<int, int> which is a list of source account ID and their sums.
// the iif output SPL "text" for each source account is stored seperately in QMap iifSplBySourceAccount as SPL is added to the iif output at the end of buildIif()
// buildIif() uses addTransactionToIif() for each source account returned by sumTransactionsByExportId() for each mission account (and operation account).
// addTransactionToIif() adds the SPL records / lines which are stored in iifSplBySourceAccount.
QByteArray IifApi::buildIif(int exportId)
{
	QByteArray iif;
	QMap<int, int> sourceAccountTotals;
	QMap<int, int> operationsAccountSums;
	QMap<int, QByteArray> iifSplBySourceAccount;
	QList<MissionDM::AccountNameNumber> sourceAccountNameList;
	QList<MissionDM::AccountNameNumber> advantageAccountNameList;
	QMap<int, QMap<int, int>> advantageAccountSums;   // = QMap<source account id, QMap<advantage account id, total>>

	QSqlQuery iifDateQuery;
	iifDateQuery.prepare("SELECT end FROM iif WHERE id=:id");
	iifDateQuery.bindValue(":id", exportId);
	iifDateQuery.exec();
	iifDateQuery.first();
	dateString = QDate::fromJulianDay(iifDateQuery.value("end").toUInt()).toString("MM/dd/yy");
	iifDateQuery.finish();

	// load all source accounts from db
	sourceAccountNameList = mdm.loadAccountNames("source_accounts");

	// load all advantage accounts from db
	advantageAccountNameList = mdm.loadAccountNames("advantage_accounts");

	// Check for transactions in missions
	int total = mdm.getTableRowCount("missions");
	for (int i=1; i < (total + 1); i++) {
		QString table = QString("acnt_%1").arg(i);
		QMap<int, SourceAccountSum> accountSums = sumTransactionsByExportId(table, exportId, advantageAccountSums);
		for (auto sa : accountSums.keys()) {
			int sum = accountSums[sa].mission;
			operationsAccountSums[sa]+= accountSums[sa].operations;
			QString accountNumber;
			QString missionName;
			getAccountNumber(i, accountNumber, missionName);
			addTransactionToIif(iifSplBySourceAccount[sa], accountNumber, missionName, sum);
			sourceAccountTotals[sa]+= sum;
		}
	}

	// Check for transactions in operations account
	for (auto sa : operationsAccountSums.keys()) {
		int sum = operationsAccountSums.value(sa);
		addTransactionToIif(iifSplBySourceAccount[sa], mdm.setupApi.operationsAccountNumber, "OPPERATIONS", sum);
		sourceAccountTotals[sa]+= sum;
	}

	// add any Advantage Accounts for each Source Account
	for (auto sa : advantageAccountSums.keys()) {
		for (auto aa : advantageAccountSums.value(sa).keys()) {
			int sum = advantageAccountSums.value(sa).value(aa);
			addTransactionToIif(iifSplBySourceAccount[sa], advantageAccountNameList.value(aa - 1).number, advantageAccountNameList.value(aa - 1).name, sum);
			sourceAccountTotals[sa]+= sum;
		}
	}

	// build header (may not be needed, but just to be consistant with example files)
	//	iif+= "!TRNS\tTRNSID\tTRNSTYPE\tDATE\tACCNT\tNAME\tCLASS\tAMOUNT\tDOCNUM\tCLEAR\n";
	//	iif+= "!SPL\tSPLID\tTRNSTYPE\tDATE\tACCNT\tNAME\tCLASS\tAMOUNT\tDOCNUM\tCLEAR\n";
	//	iif+= "!ENDTRNS\t\t\t\t\t\t\t\t\t\n";

	iif+= "!TRNS\tTRNSID\tTRNSTYPE\tDATE\tACCNT\tCLASS\tAMOUNT\n";
	iif+= "!SPL\tSPLID\tTRNSTYPE\tDATE\tACCNT\tCLASS\tAMOUNT\n";
	iif+= "!ENDTRNS\t\t\t\t\t\t\n";

	bool first = true;
	for (auto sa : iifSplBySourceAccount.keys()) {
		if (first)
			first = false;
		else
			iif+= "\n\n";
		float amountInDollars = sourceAccountTotals[sa];
		amountInDollars/= 100;

		iif+= "TRNS\t\tGENERAL JOURNAL\t" + dateString.toUtf8() + '\t' + sourceAccountNameList.value(sa - 1).number.toUtf8() +
		        '\t' + sourceAccountNameList.value(sa - 1).name.toUtf8() + '\t' + QString("%1\n").arg(amountInDollars, 0, 'f', 2).toUtf8();
		iif+= iifSplBySourceAccount.value(sa);
		iif+= "ENDTRNS\t\t";
	}

	return iif;
}

void IifApi::getAccountNumber(int id, QString &accountNumber, QString &missionName) {
	QSqlQuery query;
	query.prepare("SELECT account, name FROM missions WHERE id=:id");
	query.bindValue(":id", id);
	query.exec();
	query.first();
	accountNumber = query.value("account").toString();
	missionName = query.value("name").toString();
	query.finish();
};

QMap<int, IifApi::SourceAccountSum> IifApi::sumTransactionsByExportId(const QString &account, int exportId, QMap<int, QMap<int, int>> &advantageAccountSums)
{
	QMap<int, SourceAccountSum> accountSums;
	QSqlQuery query;

	query.prepare(QString("SELECT type, amount, advantage, cut, sa, proxy_id, invalid, advtg_acnt FROM %1 WHERE export_id=%2").arg(account).arg(exportId));
	query.exec();
	if (query.first()) {
		do {
			int type = query.value("type").toInt();
			int sa = query.value("sa").toUInt();
			if (!query.value("invalid").toBool() && processInkind(type, sa) && query.value("proxy_id").toInt() <= 0) {
				accountSums[sa].mission+= query.value("amount").toUInt();
				accountSums[sa].operations+= query.value("cut").toUInt();

				// look if there was an advantage recorded
				int aa = query.value("advtg_acnt").toUInt();
				if (aa > 0)
					advantageAccountSums[sa][aa]+= query.value("advantage").toUInt();
			}
		} while (query.next());
	}

    return accountSums;
}

void IifApi::addTransactionToIif(QByteArray &iifSpl, const QString &account, QString name, int amount)
{
	float amountInDollars = amount;
	amountInDollars/= -100;
	name.replace(':', ' ');
	iifSpl+= "SPL\t\tGENERAL JOURNAL\t" + dateString.toUtf8() + '\t' + account.toUtf8() + QString("\t%1\t%2\n").arg(name).arg(amountInDollars, 0, 'f', 2).toUtf8();
}

bool IifApi::processInkind(const int type, const int sa)
{
	return !(type == DonationType::Inkind && (!mdm.setupApi.exportGik || sa != mdm.setupApi.exportGikAccount));
}

QJsonObject IifApi::buildAuditList(int exportId, bool proxies)
{
	QSqlQuery query;
	QList<MissionDM::AccountNameNumber> sourceAccountNameList;
	QJsonObject jRoot;
	QJsonArray jArray;
	int totalFees = 0;
	int totalGross = 0;

	// load all source account names from db ...there typically isn't that many
	sourceAccountNameList = mdm.loadAccountNames("source_accounts");

	// load all advantage accounts from db
	QList<MissionDM::AccountNameNumber> advantageAccountNameList = mdm.loadAccountNames("advantage_accounts");

	// Check for transactions in missions
	int total = mdm.getTableRowCount("missions");
	for (int i=0; i < total; i++) {
		query.clear();
		query.prepare(QString("SELECT * FROM acnt_%1 WHERE export_id=%2").arg(i + 1).arg(exportId));
		query.exec();
		if (query.first()) {
			// get mission name and account
			QString accountNumber;
			QString missionName;
			int sa;
			int aa;
			int donorId;
			int donorAcountIdx;
			getAccountNumber(i + 1, accountNumber, missionName);
			do {
				int type = query.value("type").toInt();
				sa = query.value("sa").toUInt() - 1;
				if (!query.value("invalid").toBool() && processInkind(type, sa + 1) && ((!proxies && query.value("proxy_id").toInt() <= 0) || (proxies && query.value("proxy_id").toInt() > 0))) {
					aa = query.value("advtg_acnt").toUInt() - 1;
					donorId = query.value("donor_id").toUInt();
					donorAcountIdx = query.value("donor_acnt_idx").toInt();
					QJsonObject transaction;
					transaction.insert("mission-name", missionName);
					transaction.insert("mission-account", accountNumber);
					transaction.insert("source-account-name", sourceAccountNameList.at(sa).name);
					transaction.insert("source-account-number", sourceAccountNameList.at(sa).number);
					transaction.insert("amount-net", query.value("amount").toInt());
					int gross = query.value("gross").toInt();
					totalGross+= gross;
					transaction.insert("amount-gross", gross);
					transaction.insert("donor-id", donorId);
					transaction.insert("donor-acnt-idx", donorAcountIdx);
					transaction.insert("date", query.value("date").toInt());
					transaction.insert("type", query.value("type").toInt());
					transaction.insert("anonymous", query.value("anonymous").toBool());
					transaction.insert("amount-operations", query.value("cut").toInt());
					transaction.insert("advantage", query.value("advantage").toInt());
					QString advantageAccountName;
					QString advantageAccountNumber;
					if (aa > -1) {
						advantageAccountName = advantageAccountNameList.at(aa).name;
						advantageAccountNumber = advantageAccountNameList.at(aa).number;
					}
					transaction.insert("advantage-account-name", advantageAccountName);
					transaction.insert("advantage-account-number", advantageAccountNumber);


					QSqlQuery donationQuery;
					donationQuery.prepare(QString("SELECT forename, surname FROM donors WHERE id=%1").arg(donorId));
					donationQuery.exec();
					donationQuery.first();
					transaction.insert("first-name", donationQuery.value("forename").toString());
					transaction.insert("last-name", donationQuery.value("surname").toString());
					donationQuery.finish();

					donationQuery.clear();
					donationQuery.prepare(QString("SELECT fee, source, percent, inkind, campaign, neftd FROM acnt_donor_%1 WHERE id=%2").arg(donorId).arg(donorAcountIdx));
					donationQuery.exec();
					donationQuery.first();
					int fee = donationQuery.value("fee").toInt();
					if (!proxies)  // don't make sense to show total fees for proxies
						totalFees+= fee;
					transaction.insert("fee", fee);
					transaction.insert("source", donationQuery.value("source").toInt());
					transaction.insert("percent", donationQuery.value("percent").toReal());
					transaction.insert("note", donationQuery.value("inkind").toString());
					transaction.insert("campaign", donationQuery.value("campaign").toString());
					transaction.insert("neftd", donationQuery.value("neftd").toBool());

					jArray.append(transaction);
				}
			} while (query.next());
		}
		query.finish();
	}
	jRoot.insert("transactions", jArray);
	jRoot.insert("total-fees", totalFees);
	jRoot.insert("total-gross", totalGross);

	return jRoot;
}

QJsonObject IifApi::buildUnmarkedList(int sourceAccount)
{
	QList<RowMark> rowsToMark;
	QSqlQuery query;
	QJsonObject json;
	QJsonArray jDonationList;
	int grossTotal = 0;
	int netTotal = 0;

	auto makeDescription = [](int donorId, int missionId) {
		QString description;
		QSqlQuery query;
		query.prepare(QString("SELECT forename, surname FROM donors WHERE id=%1").arg(donorId));
		query.exec();
		query.first();
		description = query.value("forename").toString() + ' ' + query.value("surname").toString();
		query.finish();
		query.clear();
		query.prepare(QString("SELECT name FROM missions WHERE id=%1").arg(missionId));
		query.exec();
		query.first();
		description+= " - " + query.value("name").toString();
		return description;
	};

	auto getGross = [](int donorId, int donationId) {
		QSqlQuery query;
		query.prepare(QString("SELECT amount FROM acnt_donor_%1 WHERE id=%2").arg(donorId).arg(donationId));
		query.exec();
		query.first();
		return query.value("amount").toUInt();
	};

	int total = mdm.getTableRowCount("missions");
	for (int i=1; i < (total + 1); i++) {
		QString account = QString("acnt_%1").arg(i);
		query.prepare(QString("SELECT type, date, amount, donor_id, donor_acnt_idx, proxy_id, invalid FROM %1 WHERE export_id=-1 AND sa=%2").arg(account).arg(sourceAccount));
		query.exec();
		if (query.first()) {
			do {
				QJsonObject jDonation;
				if (processInkind(query.value("type").toUInt(), sourceAccount) && !query.value("invalid").toBool() && query.value("proxy_id").toInt() <= 0) {
					int date = query.value("date").toUInt();
					int amount = query.value("amount").toUInt();
					int donorId = query.value("donor_id").toUInt();
					int gross = getGross(query.value("donor_id").toUInt(), query.value("donor_acnt_idx").toUInt());
					grossTotal+= gross;
					netTotal+= amount;
					jDonation.insert("date", date);
					jDonation.insert("gross", gross);
					jDonation.insert("description", makeDescription(donorId, i));
					jDonationList.append(jDonation);
				}
			} while (query.next());
		}
	}

	json.insert("donations", jDonationList);
	json.insert("gross-total", grossTotal);
	json.insert("net-total", netTotal);
	return json;
}

void IifApi::ungroupLastIif()
{
	QJsonArray jDonorsArray;
	int iif = mdm.getTableRowCount("iif");
	int total = mdm.getTableRowCount("donors");
	mdm.db.transaction();
	for (int i=0; i < total; i++) {
		QSqlQuery donationQuery;
		donationQuery.prepare(QString("SELECT export_id FROM acnt_donor_%1 WHERE export_id=%2").arg(i + 1).arg(iif));
		donationQuery.exec();
		if (donationQuery.first())
			jDonorsArray.append(i + 1);
		donationQuery.finish();
		donationQuery.clear();
		donationQuery.prepare(QString("UPDATE acnt_donor_%1 SET export_id=-1 WHERE export_id=%2").arg(i + 1).arg(iif));
		donationQuery.exec();
	}

	total = mdm.getTableRowCount("missions");
	for (int i=0; i < total; i++) {
		QSqlQuery query;
		query.prepare(QString("SELECT sa FROM acnt_%1 WHERE export_id=%2").arg(i + 1).arg(iif));
		query.exec();
		if (query.first()) {
			do {
				mdm.donationsApi.markIncomeAccountModified(query.value("sa").toInt());
			} while (query.next());
		}
		query.finish();
		query.clear();

		query.prepare(QString("UPDATE acnt_%1 SET export_id=-1 WHERE export_id=%2").arg(i + 1).arg(iif));
		query.exec();
	}

	QSqlQuery donationQuery;
	donationQuery.prepare(QString("DELETE FROM iif WHERE id=%1").arg(iif));
	donationQuery.exec();
	mdm.db.commit();

	if (jDonorsArray.size()) {
		// refresh the donor's account view
		QJsonDocument jDonorsDoc;
		jDonorsDoc.setArray(jDonorsArray);
		mdm.den.publishBriefly("donations.md.donor-accounts-updated", FoxHead::ContentType::Json, jDonorsDoc.toJson(QJsonDocument::Compact));

		// refresh iif list and income account transactions view
		publishExportIds();
		mdm.donationsApi.publishModifiedIncomeAccounts();
	}
	mdm.journal.logIifUngroup(iif);
}
