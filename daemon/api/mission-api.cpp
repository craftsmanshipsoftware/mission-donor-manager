// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

// This API Documentation --> http://www.craftsmanshipsoftware.ca/mission-donor-manager/api/index.html#mission

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QSqlQuery>
#include "common.h"
#include "missiondm.h"
#include "mission-api.h"

MissionApi::MissionApi(MissionDM &api, UsersApi &usersApi) : SubApi(api), usersApi(usersApi)
{
	apiString = "mission";
}

void MissionApi::loadInfoFromDB()
{
	QSqlQuery query;

	if (!mdm.db.tables().contains("missions")) {
		qDebug("creating missions table");
		query.exec("create table missions (id int primary key, name text, account text, percentage real, button_id text, proxy int, is_proxy bool, neftd bool, use_custom_thy bool, custom_mail_thy text, custom_email_thy text)");
	}
	publishMissionsList();
}

void MissionApi::publishMissionsList()
{
	QJsonArray jList;
	QJsonArray jListProxy;
	QSqlQuery query;
	query.prepare("SELECT * FROM missions");
	query.exec();
	if (query.first()) {
		do {
			QJsonObject jMission;
			jMission.insert("id", query.value("id").toInt());
			jMission.insert("forename", query.value("name").toString());
			jMission.insert("surname", "- " + query.value("account").toString());
			if (query.value("is_proxy").toBool())
				jListProxy.append(jMission);
			jList.append(jMission);
		} while (query.next());
	}
	QJsonDocument jDoc;
	jDoc.setArray(jList);
	mdm.den.publish("mission.ro.list", FoxHead::ContentType::Json, jDoc.toJson(QJsonDocument::Compact));
	jDoc.setArray(jListProxy);
	mdm.den.publish("mission.ro.list-proxy", FoxHead::ContentType::Json, jDoc.toJson(QJsonDocument::Compact));
	mdm.den.publishBriefly("mission.full-list-updated", FoxHead::Empty, "");
}

bool MissionApi::incomingPost(const QByteArray &topic, ssize_t, FoxID &asker, FoxHead::ContentType, QByteArray incomingContent)
{
	QByteArray subTopic = stripPrefix(topic);
	QSqlQuery query;
	bool ok = false;
	if (subTopic == "add") {
		QJsonObject injObj = QJsonDocument::fromJson(incomingContent).object();
		if (missionInfoIsUnique(injObj, -1)) {
			int id = mdm.getTableRowCount("missions") + 1;
			query.prepare("INSERT INTO missions (id, name, percentage, button_id, account, proxy, is_proxy, neftd) VALUES (:id, :name, :percentage, :button_id, :account, :proxy, :is_proxy, :neftd)");
			query.bindValue(":id", id);
			query.bindValue(":name", injObj.value("name").toString());
			query.bindValue(":percentage", injObj.value("percent").toDouble());
			query.bindValue(":button_id", injObj.value("button-id").toString());
			query.bindValue(":account", injObj.value("account").toString());
			bool isProxy = injObj.value("is-proxy").toBool();
			int proxy = injObj.value("proxy").toInt();
			if (isProxy)
				proxy = -1;
			query.bindValue(":is_proxy", isProxy);
			query.bindValue(":proxy", proxy);
			query.bindValue(":neftd", injObj.value("neftd").toBool());
			query.finish();
			ok = query.exec();
			query.exec(QString("create table acnt_%1 (id int primary key, year int, date int, type int, thirdparty int, sa int, advtg_acnt int, amount int, advantage int, gross int, cut int, donor_id int, donor_acnt_idx int, proxy_id int, anonymous bool, invalid bool, export_id int)").arg(id));
		}
		ackOrNack(asker, ok);
		if (ok)
			publishMissionsList();
	} else if (subTopic == "update") {
		QJsonObject injObj = QJsonDocument::fromJson(incomingContent).object();
		uint id = injObj.value("id").toInt();
		if (editLocks.value(id) == asker.local() && missionInfoIsUnique(injObj, id)) {
			query.prepare("UPDATE missions SET name=:name, percentage=:percentage, button_id=:button_id, account=:account, proxy=:proxy, is_proxy=:is_proxy, neftd=:neftd WHERE id=:id");
			query.bindValue(":id", id);
			query.bindValue(":percentage", injObj.value("percent").toDouble());
			query.bindValue(":button_id", injObj.value("button-id").toString());
			query.bindValue(":account", injObj.value("account").toString());
			query.bindValue(":name", injObj.value("name").toString());
			bool isProxy = injObj.value("is-proxy").toBool();
			int proxy = injObj.value("proxy").toInt();
			if (isProxy)
				proxy = -1;
			query.bindValue(":is_proxy", isProxy);
			query.bindValue(":proxy", proxy);
			query.bindValue(":neftd", injObj.value("neftd").toBool());
			bool ok = query.exec();
			ackOrNack(asker, ok);
			if (ok) {
				publishMissionsList();
				mdm.den.publish("mission.ro.updated", FoxHead::ContentType::IntAsString, QByteArray::number(id));
				mdm.den.removeMessage("mission.ro.updated");
			}
		} else {
			ackOrNack(asker, false);
		}
	} else if (subTopic == "ro.account") {
		int id = incomingContent.toUInt();
		query.prepare("SELECT * FROM missions WHERE id=:id");
		query.bindValue(":id", id);
		query.exec();
		if (query.first()) {
			QJsonDocument outjDoc;
			QJsonObject outjObj;
			outjObj.insert("id", id);
			outjObj.insert("account", query.value("account").toString());
			outjObj.insert("percent", query.value("percentage").toDouble());
			outjObj.insert("button-id", query.value("button_id").toString());
			outjObj.insert("name", query.value("name").toString());
			outjObj.insert("proxy", query.value("proxy").toInt());
			outjObj.insert("is-proxy", query.value("is_proxy").toBool());
			outjObj.insert("neftd", query.value("neftd").toBool());
			outjDoc.setObject(outjObj);
			mdm.den.answerPost(asker, currentTopic, FoxHead::ContentType::Json, outjDoc.toJson(QJsonDocument::Compact));
		} else {
			ackOrNack(asker, false);
		}
	} else if (subTopic == "my-list") {
		int myId = mdm.getUserIdOfClient(asker.local());
		QList<int> missionIds = mdm.usersApi.getMissionsList(myId, true);
		QJsonArray jArray;
		for (auto missionId : missionIds) {
			QJsonObject jObject;
			jObject.insert("id", missionId);
			query.clear();
			query.prepare("SELECT name FROM missions WHERE id=:id");
			query.bindValue(":id", missionId);
			query.exec();
			query.first();
			jObject.insert("forename", query.value("name").toString());
			query.finish();
			jArray.append(jObject);
		}
		QJsonDocument jDoc;
		jDoc.setArray(jArray);
		mdm.den.answerPost(asker, currentTopic, FoxHead::ContentType::Json, jDoc.toJson(QJsonDocument::Compact));
	} else if (subTopic == "financials") {
		int ytd = 0;
		QJsonArray jArray;
		QDate monthStart;
		monthStart.setDate(QDate::currentDate().year(), QDate::currentDate().month(), 1);
		int startYear = monthStart.year();
		for (int m=0; m<NumberOfMonthsInMissionView; m++) {
			QDate monthEnd = monthStart.addMonths(1).addDays(-1);
			int total = 0;
			query.prepare(QString("SELECT * FROM acnt_%1 WHERE date BETWEEN %2 AND %3 ORDER BY date").arg(apiNumerical).arg(monthStart.toJulianDay()).arg(monthEnd.toJulianDay()));
			query.exec();
			if (query.first()) {
				do {
					int type = query.value("type").toUInt();
					bool invalid = query.value("invalid").toBool();
					if (type != DonationType::Inkind && !invalid)
						total+= query.value("amount").toUInt();
				} while(query.next());
				if (monthStart.year() == startYear)  // stop adding up YTD after going into last year.
					ytd+= total;
			}
			QJsonObject jMonth;
			jMonth.insert("amount", total);
			jMonth.insert("date", monthStart.toJulianDay());
			jArray.append(jMonth);
			monthStart = monthStart.addMonths(-1);
		}
		QJsonObject jYtd;  // hack on YTD at the end
		jYtd.insert("amount", ytd);
		jYtd.insert("date", 0);
		jArray.append(jYtd);
		QJsonDocument jDoc;
		jDoc.setArray(jArray);
		mdm.den.answerPost(asker, currentTopic, FoxHead::ContentType::Json, jDoc.toJson(QJsonDocument::Compact));
	} else if (subTopic == "donors-by-sum") {
		QJsonDocument jDoc;
		JulianDayRange range = convertFinancialSpanToJulianRange(incomingContent.toUInt());
		jDoc.setArray(findDonors(apiNumerical, range, true));
		mdm.den.answerPost(asker, currentTopic, FoxHead::ContentType::Json, jDoc.toJson(QJsonDocument::Compact));
	} else if (subTopic == "donors-by-latest") {
		QJsonDocument jDoc;
		JulianDayRange range = convertFinancialSpanToJulianRange(incomingContent.toUInt());
		jDoc.setArray(findDonors(apiNumerical, range, false));
		mdm.den.answerPost(asker, currentTopic, FoxHead::ContentType::Json, jDoc.toJson(QJsonDocument::Compact));
	} else if (subTopic == "donations-list") {
		QJsonDocument jDoc;
		JulianDayRange range = convertFinancialSpanToJulianRange(incomingContent.toUInt());
		jDoc.setArray(findDonations(apiNumerical, range));
		mdm.den.answerPost(asker, currentTopic, FoxHead::ContentType::Json, jDoc.toJson(QJsonDocument::Compact));
	} else if (subTopic == "donor-info") {
		QSqlQuery donorNameQuery;
		donorNameQuery.prepare("SELECT forename, surname, email, address, apt, city, province, country, postalcode FROM donors WHERE id=:id");
		donorNameQuery.bindValue(":id", incomingContent.toUInt());
		donorNameQuery.exec();
		QJsonObject json;
		if (donorNameQuery.first()) {
			json.insert("email", donorNameQuery.value("email").toString());
			QString address = donorNameQuery.value("forename").toString() + ' ' + donorNameQuery.value("surname").toString();
			address+= '\n' + donorNameQuery.value("address").toString();
			QString apt = donorNameQuery.value("apt").toString();
			if (!apt.isEmpty())
				address+= '\n' + apt;
			address+= '\n' + donorNameQuery.value("city").toString() + ", " + donorNameQuery.value("province").toString() + ", "  + donorNameQuery.value("postalcode").toString();
			address+= '\n' + donorNameQuery.value("country").toString();
			json.insert("address", address);
		}
		QJsonDocument jDoc;
		jDoc.setObject(json);
		mdm.den.answerPost(asker, currentTopic, FoxHead::ContentType::Json, jDoc.toJson(QJsonDocument::Compact));
	} else if (subTopic == "lock") {
		ackOrNack(asker, lock(incomingContent.toUInt(), asker.local()));
	} else if (subTopic == "unlock") {
		ackOrNack(asker, unlock(incomingContent.toUInt(), asker.local()));
	} else {
		return true;
	}

	return true;
}

QJsonArray MissionApi::findDonors(int missionId, JulianDayRange range, bool accumulate)
{
	struct Total {
		int cash = 0;
		int inkind = 0;
	};

	struct Last {
		int julianDay;
		int did;
	};

	QMap<int, struct Total> donorAmountMap;
	QMap<int, struct Last> donorDateMap;
	QSqlQuery query;
	QJsonArray jArray;
	struct Total anonymousTotals;

	auto addAmount = [&](int donorId, int amount, int type, bool anonymous) {
		if (type == DonationType::Inkind) {
			if (anonymous) {
				anonymousTotals.inkind+= amount;
			} else {
				donorAmountMap[donorId].inkind+= amount;
			}
		} else {
			if (anonymous) {
				anonymousTotals.cash+= amount;
			} else {
				donorAmountMap[donorId].cash+= amount;
			}
		}
	};

	query.prepare(QString("SELECT * FROM acnt_%1 WHERE date BETWEEN %2 AND %3 ORDER BY date DESC").arg(missionId).arg(range.first).arg(range.second));
	query.exec();
	if (query.first()) {
		do {
			int donorId = query.value("donor_id").toUInt();
			int type = query.value("type").toUInt();
			int amount = query.value("gross").toUInt();  // gross is before percentage is taken out
			bool anonymous = query.value("anonymous").toBool();
			if (!query.value("invalid").toBool()) {
				if (accumulate)
					addAmount(donorId, amount, type, anonymous);
				else if (!donorDateMap.contains(donorId) && !anonymous)
					donorDateMap.insert(donorId, {.julianDay = query.value("date").toInt(), .did = query.value("donor_acnt_idx").toInt() });
			}
		} while(query.next());
	}

	if (accumulate) {
		for (auto key : donorAmountMap.keys()) {
			QJsonObject json;
			QString firstname;
			QString lastname;
			lookupDonorName(key, firstname, lastname);
			json.insert("name", firstname);
			json.insert("lastname", lastname);
			json.insert("amount", donorAmountMap.value(key).cash);
			json.insert("inkind", donorAmountMap.value(key).inkind);
			json.insert("id", key);
			jArray.append(json);
		}
		QJsonObject json;
		if (anonymousTotals.cash || anonymousTotals.inkind) {
			json.insert("name", "Anonymous");
			json.insert("amount", anonymousTotals.cash);
			json.insert("inkind", anonymousTotals.inkind);
			jArray.append(json);
		}
	} else {
		for (auto key : donorDateMap.keys()) {
			QJsonObject json;
			json.insert("date", donorDateMap.value(key).julianDay);
			QString firstname;
			QString lastname;
			lookupDonorName(key, firstname, lastname);
			json.insert("name", firstname);
			json.insert("lastname", lastname);
			json.insert("id", key);
			json.insert("note", lookupDonationNote(key, donorDateMap.value(key).did));
			jArray.append(json);
		}
	}
	return jArray;
}

void  MissionApi::lookupDonorName(int donorId, QString &firstname, QString &lastname)
{
	QSqlQuery donorNameQuery;
	donorNameQuery.prepare("SELECT forename, surname FROM donors WHERE id=:id");
	donorNameQuery.bindValue(":id", donorId);
	donorNameQuery.exec();
	if (donorNameQuery.first()) {
		firstname = donorNameQuery.value("forename").toString();
		lastname = donorNameQuery.value("surname").toString();
	}
};

QString MissionApi::lookupDonationNote(int donorId, int did)
{
	QSqlQuery query;
	QString note;
	query.prepare(QString("SELECT inkind FROM acnt_donor_%1 WHERE id=:id").arg(donorId));
	query.bindValue(":id", did);
	query.exec();
	if (query.first()) {
		note = query.value("inkind").toString();
	}
	return note;
};

QJsonArray MissionApi::findDonations(int missionId, JulianDayRange range)
{
	QSqlQuery query;
	QJsonArray jArray;

	query.prepare(QString("SELECT * FROM acnt_%1 WHERE date BETWEEN %2 AND %3 ORDER BY date DESC").arg(missionId).arg(range.first).arg(range.second));
	query.exec();
	if (query.first()) {
		do {
			int donorId = query.value("donor_id").toUInt();
			int type = query.value("type").toUInt();
			int amount = query.value("gross").toUInt();  // gross is before percentage is taken out
			if (!query.value("invalid").toBool()) {
				QJsonObject json;
				QString firstname;
				QString lastname;
				bool anonymous = query.value("anonymous").toBool();
				if (anonymous) {
					firstname = "Anonymous";
					donorId = 0;
				} else {
					lookupDonorName(donorId, firstname, lastname);
				}
				json.insert("name", firstname);
				json.insert("lastname", lastname);
				json.insert("amount", amount);
				if (type == DonationType::Inkind)
					json.insert("inkind", true);
				else
					json.insert("inkind", false);
				json.insert("date", query.value("date").toInt());
				json.insert("id", donorId);
				json.insert("note", lookupDonationNote(donorId, query.value("donor_acnt_idx").toInt()));
				jArray.append(json);
			}
		} while(query.next());
	}
	return jArray;
}

bool MissionApi::missionInfoIsUnique(const QJsonObject &json, int id)
{
	bool unique = true;
	QString name = json.value("name").toString().toLower();
	QString account = json.value("account").toString().toLower();
	QString buttonId = json.value("button-id").toString().toLower();

	// mission can not have the same account number as the operations admin account
	if (account == mdm.setupApi.operationsAccountNumber.toLower())
		unique = false;

	auto checkAccounts = [](const QString &account, const QString &tableName)
	{
		QSqlQuery query;
		bool found = false;
		query.prepare(QString("SELECT number FROM %1").arg(tableName));
		query.exec();
		if (query.first()) {
			do {
				if (query.value("number").toString().toLower() == account) {
					found = true;
					break;
				}
			} while (query.next());
		}
		return found;
	};

	if (unique && checkAccounts(account, "source_accounts"))  // mission can not have the same account number as any Source/Income account
		unique = false;

	if (unique && checkAccounts(account, "advantage_accounts"))  // mission can not have the same account number as any advantage account
		unique = false;

	// mission can not have the same name or button ID as any other mission
	QSqlQuery query;
	query.prepare("SELECT id, name, button_id FROM missions");
	query.exec();
	if (query.first()) {
		do {
			if (query.value("id").toInt() != id && (query.value("name").toString().toLower() == name || (!buttonId.isEmpty() && query.value("button_id").toString().toLower() == buttonId))) {
				unique = false;
				break;
			}
		} while (query.next());
	}

	return unique;
}
