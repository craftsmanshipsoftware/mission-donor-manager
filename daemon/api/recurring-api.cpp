// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

// This API Documentation --> http://www.craftsmanshipsoftware.ca/mission-donor-manager/api/index.html#recurring

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QSqlQuery>
#include "common.h"
#include "missiondm.h"
#include "recurring-api.h"

// Recurring api has geometry of list * list = (recurring list) * (individual donation list)....so each recurring list is a list of donations.
// Examples, Paypal recurring is one list of donations and monthly bank transfers are a seperate list of donations.
// Recurring lists are "run", usually monthly.
//
// recurring.list.add         q/r --> used to create a new recurring list
// recurring.list.update      q/r --> used to rename an existing recurring list
// recurring.donation.add     q/r --> used to add a new recurring donation transfer to a recurring list
// recurring.donation.update  q/r --> used to make changes to an existing recurring donation transfer in a recurring list
// recurring.ro.list          pub --> is the published recurring lists
// recurring.ro.donation-list q/r --> returns a json array of donations and their details within a specific requested recurring list.
// recurring.ro.list-updated  pub --> is published when a list changed from recurring.donation.add or recurring.donation.update.  Publishes the id of the list that changed.

RecurringApi::RecurringApi(MissionDM &api) : SubApi(api)
{
	apiString = "recurring";
}

void RecurringApi::loadInfoFromDB()
{
	QSqlQuery query;

	if (!mdm.db.tables().contains("recurring")) {
		qDebug("creating recurring table");
		query.exec("create table recurring (id integer primary key, name text)");
	}
	publishRecurringList();
}

void RecurringApi::publishRecurringList()
{
	QJsonArray jList;
	QSqlQuery query;
	query.prepare("SELECT * FROM recurring");
	query.exec();
	if (query.first()) {
		do {
			QJsonObject jListItem;
			jListItem.insert("id", query.value("id").toInt());
			jListItem.insert("forename", query.value("name").toString());
			jList.append(jListItem);
		} while (query.next());
	}
	QJsonDocument jDoc;
	jDoc.setArray(jList);
	mdm.den.publish("recurring.ro.list", FoxHead::ContentType::Json, jDoc.toJson(QJsonDocument::Compact));
}

bool RecurringApi::incomingPost(const QByteArray &topic, ssize_t, FoxID &asker, FoxHead::ContentType, QByteArray incomingContent)
{
	QByteArray subTopic = stripPrefix(topic);
	QSqlQuery query;

	auto writeDonation = [&](int id, QJsonObject &injObj) {
		if (id)
			query.bindValue(":id", id);
		query.bindValue(":sa", injObj.value("sa").toInt());
		query.bindValue(":amount", injObj.value("amount").toInt());
		query.bindValue(":donor_id", injObj.value("donor-id").toInt());
		query.bindValue(":mission_id", injObj.value("mission-id").toInt());
		query.bindValue(":inactive", injObj.value("inactive").toInt());
		query.bindValue(":anonymus", injObj.value("anonymus").toBool());
		query.bindValue(":enabled", injObj.value("enabled").toBool());
		bool ok = query.exec();
		ackOrNack(asker, ok);
		return ok;
	};

	if (subTopic == "list.add") {
		QJsonObject injObj = QJsonDocument::fromJson(incomingContent).object();
		query.prepare("INSERT INTO recurring (id, name) VALUES (NULL, :name)");
		query.bindValue(":name", injObj.value("name").toString());
		bool ok = query.exec();
		int id = query.lastInsertId().toInt();
		ackOrNack(asker, ok);
		if (ok)
			query.exec(QString("create table recurring_%1 (id integer primary key, sa int, amount int, donor_id int, mission_id int, anonymus bool, enabled bool, inactive int)").arg(id));
		publishRecurringList();
	} else if (subTopic == "list.update") {
		QJsonObject injObj = QJsonDocument::fromJson(incomingContent).object();
		uint id = injObj.value("id").toInt();
		if (asker.local() == editLocks.value(id)) {
			query.prepare("UPDATE recurring SET name=:name WHERE id=:id");
			query.bindValue(":id", id);
			query.bindValue(":name", injObj.value("name").toString());
			ackOrNack(asker, query.exec());
			publishRecurringList();
		} else {
			ackOrNack(asker, false);
		}
	} else if (subTopic == "ro.donation-list") {
		QJsonDocument jDoc;
		jDoc.setObject(getDonationList(incomingContent.toUInt()));
		mdm.den.answerPost(asker, currentTopic, FoxHead::ContentType::Json, jDoc.toJson(QJsonDocument::Compact));
	} else if (subTopic == "donation.add") {
		QJsonObject injObj = QJsonDocument::fromJson(incomingContent).object();
		uint listId = injObj.value("list-id").toInt();
		query.prepare(QString("INSERT INTO recurring_%1 (id, sa, amount, donor_id, mission_id, anonymus, enabled, inactive) VALUES (NULL, :sa, :amount, :donor_id, :mission_id, :anonymus, :enabled, :inactive)").arg(listId));
		if (writeDonation(0, injObj)) {
			mdm.den.publish("recurring.ro.list-updated", FoxHead::ContentType::IntAsString, QByteArray::number(listId));
			mdm.den.removeMessage("recurring.ro.list-updated");
		}
	} else if (subTopic == "donation.update") {
		QJsonObject injObj = QJsonDocument::fromJson(incomingContent).object();
		uint listId = injObj.value("list-id").toInt();
		if (asker.local() == editLocks.value(listId)) {
			query.prepare(QString("UPDATE recurring_%1 SET sa=:sa, amount=:amount, donor_id=:donor_id, mission_id=:mission_id, anonymus=:anonymus, enabled=:enabled, inactive=:inactive WHERE id=:id").arg(listId));
			if (writeDonation(injObj.value("id").toInt(), injObj)) {
				mdm.den.publish("recurring.ro.list-updated", FoxHead::ContentType::IntAsString, QByteArray::number(listId));
				mdm.den.removeMessage("recurring.ro.list-updated");
			}
		}
	} else if (subTopic == "donation.delete") {
		QJsonObject injObj = QJsonDocument::fromJson(incomingContent).object();
		uint listId = injObj.value("list-id").toInt();
		uint did = injObj.value("did").toInt();
		if (asker.local() == editLocks.value(listId)) {
			query.prepare(QString("DELETE FROM recurring_%1 WHERE id=%2").arg(listId).arg(did));
			if (query.exec()) {
				mdm.den.publish("recurring.ro.list-updated", FoxHead::ContentType::IntAsString, QByteArray::number(listId));
				mdm.den.removeMessage("recurring.ro.list-updated");
			}
		}
	} else if (subTopic == "run") {
		QJsonObject injObj = QJsonDocument::fromJson(incomingContent).object();
		int listId = injObj.value("id").toInt();
		int julianDay = injObj.value("julian-day").toInt();

		if (asker.local() == editLocks.value(listId)) {
			mdm.journal.setActiveUser(mdm.getUserIdOfClient(asker.local()));
			QSqlQuery query;
			query.setForwardOnly(true);
			query.prepare(QString("SELECT * FROM recurring_%1").arg(listId));
			query.exec();
			bool ok = query.first();
			if (ok) {
				mdm.db.transaction();
				do {
					int id = query.value("id").toInt();
					int sa = query.value("sa").toInt();
					int amount = query.value("amount").toInt();
					int donorId =  query.value("donor_id").toInt();
					int missionId =  query.value("mission_id").toInt();
					bool anonymus = query.value("anonymus").toBool();
					bool enabled = query.value("enabled").toBool();
					int inactive =  query.value("inactive").toInt();
					if (enabled)
						mdm.donationsApi.add(DonationVia::Recurring, DonationType::Normal, -1, amount, 0, 0, sa, 0, donorId, missionId, julianDay, anonymus, "", "", 0);
					if (inactive > 0) {
						QSqlQuery inactiveUpdateQuery;
						if (inactive == 1) {  // need to set enabled true so it applies next post
							inactive--;
							inactiveUpdateQuery.prepare(QString("UPDATE recurring_%1 SET inactive=%2, enabled=1 WHERE id=%3").arg(listId).arg(inactive).arg(id));
						} else {
							inactive--;
							inactiveUpdateQuery.prepare(QString("UPDATE recurring_%1 SET inactive=%2 WHERE id=%3").arg(listId).arg(inactive).arg(id));
						}
						inactiveUpdateQuery.exec();
					}
				} while(query.next());
				ok = mdm.db.commit();
			}
			ackOrNack(asker, ok);
			editLocks.remove(listId);
			if (ok)
				mdm.donationsApi.publishModifiedAccounts();
		} else {
			ackOrNack(asker, false);
		}
	} else if (subTopic == "lock") {
		ackOrNack(asker, lock(incomingContent.toUInt(), asker.local()));
	} else if (subTopic == "unlock") {
		ackOrNack(asker, unlock(incomingContent.toUInt(), asker.local()));
	} else {
		return true;
	}

	return true;
}

QJsonObject RecurringApi::getDonationList(int listId)
{
	QJsonObject json;
	QJsonArray jArray;
	QSqlQuery query;
	int total = 0;
	query.setForwardOnly(true);
	query.prepare(QString("SELECT * FROM recurring_%1").arg(listId));
	query.exec();
	if (query.first()) {
		do {
			QJsonObject jTransaction;
			jTransaction.insert("id", query.value("id").toInt());
			jTransaction.insert("sa", query.value("sa").toInt());
			int amount = query.value("amount").toInt();
			jTransaction.insert("amount", amount);
			jTransaction.insert("donor-id", query.value("donor_id").toInt());
			jTransaction.insert("mission-id", query.value("mission_id").toInt());
			jTransaction.insert("inactive", query.value("inactive").toInt());
			jTransaction.insert("anonymus", query.value("anonymus").toBool());
			bool enabled = query.value("enabled").toBool();
			jTransaction.insert("enabled", enabled);
			if (enabled)
				total+= amount;
			jArray.append(jTransaction);
		} while(query.next());
	}
	json.insert("list", jArray);
	json.insert("total", total);
	return json;
}

