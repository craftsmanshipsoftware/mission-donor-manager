// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef MISSIONDM_H
#define MISSIONDM_H

#include <QSqlDatabase>
#include "foxmqden.h"
#include "email-sender.h"
#include "activity-journal.h"
#include "users-api.h"
#include "donor-api.h"
#include "donation-api.h"
#include "recurring-api.h"
#include "mission-api.h"
#include "setup-api.h"
#include "income-accounts-api.h"
#include "advantage-accounts-api.h"
#include "campaigns-api.h"
#include "iif-api.h"
#include "donor-export-api.h"
#include "authentication-api.h"
#include "tax-receipt-api.h"
#include "thankyou-api.h"
#include "reports-api.h"

class UserApi;
class DeviceInfo;
class PaypalTransactions;

class MissionDM : public QObject
{
	Q_OBJECT

public:
	struct AccountNameNumber {
		QString name;
		QString number;
	};
	explicit MissionDM(size_t port, size_t webPort, const QString &dbDir);
	void publishState();
	inline int getState() const { return state; }
	void addAuthenticatedUser(u_int64_t asker, int id) { authenticatedUserIDs.insert(asker, id); }
	int getUserIdOfClient(u_int64_t asker) { return authenticatedUserIDs.value(asker); }
	void setOneOrMoreUsersReady();
	int getTableRowCount(const QString &tableName);
	QString getAddressSalutation(const QSqlQuery &query);
	QString getSalutation(const QSqlQuery &query);
	QString getIncomeAccount(int index);
	QList<MissionDM::AccountNameNumber> loadAccountNames(const QString &table);
	bool disconnectUser(uint32_t id);
	FoxmqDen den;
	QSqlDatabase db;

	AuthenticationApi authenticationApi;
	DonorApi donorApi;
	DonationApi donationsApi;
	SetupApi setupApi;
	UsersApi usersApi;
	TaxReceiptApi taxApi;
	ThankyouApi thankyouApi;

	Email email;
	ActivityJournal journal;

public slots:
	void newRequest(const QByteArray &topic, ssize_t count, FoxID id, FoxHead::ContentType contentType, QByteArray content);
	void incomingPost(const QByteArray &topic, ssize_t count, FoxID id, FoxHead::ContentType contentType, QByteArray content);

private:
	void openDB(const QString &password, bool setPwd);
	void setupCrypto();
	void clientLeft(const FoxID &id);
	QString configPathString;
	QSslConfiguration sslConfiguration;
	QMap<uint64_t, uint32_t> authenticatedUserIDs;

	MissionApi missionApi;
	IifApi iifApi;
	IncomeAccountsApi incomeAccountsApi;
	AdvantageAccountsApi advantageAccountsApi;
	CampaignsApi campaignsApi;
	RecurringApi recurringApi;
	DonorExportApi emailListApi;
	ReportsApi reportsApi;
	std::list<SubApi *> apis = {&usersApi, &donorApi, &donationsApi, &missionApi, &setupApi, &incomeAccountsApi, &advantageAccountsApi, &campaignsApi, &iifApi, &recurringApi, &emailListApi, &authenticationApi, &taxApi, &thankyouApi, &reportsApi};
	PaypalTransactions *paypal;
	int state;

private slots:
	void newClient(FoxID id, bool authenticated);
};

#endif
