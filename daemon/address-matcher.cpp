// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QVector>
#include <QRegularExpression>
#include "../qthypocoristicenglish/qhypocoristic-english.h"
#include "address-matcher.h"

bool AddressMatcher::match()
{
	bool match;

	if (a.last != b.last)
		return false;

	if (a.line1Condensed == b.line1Condensed
		&& a.line2Condensed == b.line2Condensed
		&& a.city == b.city
		&& a.province == b.province
		&& a.countryCode == b.countryCode
		&& a.postalCode == b.postalCode) {
		match = true;
	} else {
		prepAddressForComplexCompare(b);
		if (a.postalCode == b.postalCode && complexAddressCompare())
			match = true;
		else
			match = false;
	}

	// if everything matches so far, try and match the first name (most complicated).  Try exact match first
	QString aCondensedFirst = condenseString(a.first);
	QString bCondensedFirst = condenseString(b.first);
	if (match && aCondensedFirst != bCondensedFirst) {
		// If exact match fails, try if one is a part of the other "Jon" in "Jon and Rachel"
		if (!(aCondensedFirst.contains(bCondensedFirst) || bCondensedFirst.contains(aCondensedFirst))) {
			// If part-of-other fails, try hypocoristic match ("Jim" and "James")
			bool hypocoristicMatch = false;
			QStringList aNames = a.first.simplified().toLower().split(' ');
			QStringList bNames = b.first.simplified().toLower().split(' ');
			[&] {
				for (auto aName : aNames) {
					aName = aName.toLower();
					for (auto bName : bNames) {
						if (QHypocoristicEnglish::check(aName, bName)) {
							hypocoristicMatch = true;
							return;
						}
					}
				}
			}();
			// if we got this far without a match then return no match
			if (!hypocoristicMatch) {
				match = false;
			}
		}
	}

	return match;
}

void AddressMatcher::condense(struct AddrInfo &ai)
{
	ai.last = condenseString(ai.last);
	ai.line1Condensed = condenseString(ai.line1);
	ai.line2Condensed = condenseString(ai.line2);
	ai.city = condenseString(ai.city);
	ai.province = condenseString(ai.province);
	ai.countryCode = condenseString(ai.countryCode);
	ai.postalCode = condenseString(ai.postalCode);
}


QString AddressMatcher::condenseString(QString string)
{
	string = string.simplified().toLower();
	string.replace(" ", "");
	string.replace("-", "");
	return string;
}

void AddressMatcher::prepAddressForComplexCompare(struct AddrInfo &ai)
{
	auto seperate = [](QStringList inputList, QChar c) {
		QStringList output;
		for (auto item : inputList) {
			QStringList newParts = item.split(c, Qt::SkipEmptyParts);
			if (newParts.size())
				output+= newParts;
			else if (item != c)
				output+= item;
		}
		return output;
	};

	QString combined = ai.line1.toLower() + " " + ai.line2.toLower();
	QStringList parts = combined.split(QRegularExpression("(?<=\\d)(?=\\D)|(?=\\d)(?<=\\D)"));  //  first split places which are between digit (\d) and non-digit (\D).
	parts = seperate(parts, ' ');
	parts = seperate(parts, '-');
	parts = seperate(parts, ',');
	parts = seperate(parts, '.');
	ai.forComplexCompare = parts;
}

bool AddressMatcher::complexAddressCompare()
{
	int aNumbersTotal = 0;
	int bNumbersTotal = 0;
	int numbersTotal = 0;
	int numbersMatched = 0;
	int anyMatched = 0;

	// mark which parts of A are numbers
	QVector<bool> aNumbers(a.forComplexCompare.size());
	for (int i=0; i<a.forComplexCompare.size(); i++) {
		bool ok;
		a.forComplexCompare.at(i).toInt(&ok);
		aNumbers[i] = ok;
		if (ok)
			aNumbersTotal++;
	}

	// mark which parts of B are numbers
	QVector<bool> bNumbers(b.forComplexCompare.size());
	for (int i=0; i<b.forComplexCompare.size(); i++) {
		bool ok;
		b.forComplexCompare.at(i).toInt(&ok);
		bNumbers[i] = ok;
		if (ok)
			bNumbersTotal++;
	}

	numbersTotal = qMin(aNumbersTotal, bNumbersTotal);

	QVector<bool> bMatched(b.forComplexCompare.size());

	for (auto aPart : a.forComplexCompare) {
		int i=0;
		for (auto bPart : b.forComplexCompare) {
			if (!bMatched.at(i) && aPart == bPart) {
				bMatched[i] = true;
				anyMatched++;
				if (bNumbers.at(i))
					numbersMatched++;
				break;
			}
			i++;
		}
	}

	// check if all number parts matched AND at least one non number part matched
	if (numbersMatched == numbersTotal && anyMatched > numbersMatched)
		return true;
	else
		return false;
}
