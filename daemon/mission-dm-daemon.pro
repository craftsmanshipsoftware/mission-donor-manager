QT+= core network sql
TARGET= mission-dm-daemon
HEADERS+= missiondm.h   paypal.h   address-matcher.h   email-sender.h   activity-journal.h
SOURCES+= missiondm.cpp paypal.cpp address-matcher.cpp email-sender.cpp activity-journal.cpp main.cpp
# INCLUDEPATH=+ ../ does work?
INCLUDEPATH=+ ../QtFoxMQ/common/
CONFIG+= c++17

#internal components
include(../common/common.pri)
include(api/api.pri)

# QtFoxMQ
DEFINES+= QFOXMQ_STATIC
include(../foxmq/den/den.pri)

# QtSslUtils
include(../QtSslUtils/qtsslutils.pri)
DEFINES+= QSSLUTILS_STATIC

# QHypocoristicEnglish
include(../qthypocoristicenglish/qt-hypocoristic-english.pri)
DEFINES+= QHYPOCORISTIC_ENGLISH_STATIC

LIBS+= -lPocoNet
LIBS+= -lcrypto++

# auto generate a version.h file
linux {
	TO_GENERATE = $$PWD/version.h
	custom_target.output  = $$PWD/version.h
	custom_target.commands = 'cd $$PWD; $$PWD/set_version.sh'
	custom_target.depends = FORCE
	custom_target.input = TO_GENERATE
	QMAKE_EXTRA_COMPILERS += custom_target
}
