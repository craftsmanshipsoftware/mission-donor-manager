// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QGuiApplication>
#include <QCommandLineParser>
#include <syslog.h>
#include "missiondm.h"
#include "email-sender.h"
#include "version.h"

int port = 0;

void systemLogOutput(QtMsgType, const QMessageLogContext &, const QString &msg)
{
	syslog(LOG_INFO, QString::number(port).toUtf8() + " %s\n", msg.toUtf8().constData());
}

int main(int argc, char *argv[])
{
	QGuiApplication app(argc, argv);

	app.setApplicationName("mission-dm-daemon");
	app.setApplicationVersion(VERSION);

	int webPort = 0;
	QString customDbDir;
	QCommandLineParser parser;

	QCommandLineOption helpOption = parser.addHelpOption();
	QCommandLineOption versionOption = parser.addVersionOption();

	QCommandLineOption portOption({"p", "port"}, "Ssl Socket Server Port", "1000 to 65535");
	parser.addOption(portOption);

	QCommandLineOption webPortOption({"w", "web-socket-port"}, "Web Socket Server Port", "1000 to 65535");
	parser.addOption(webPortOption);

	QCommandLineOption dbDirOption({"d", "db-dir"}, "Optional custom DB directory, custom DB dir", "path");
	parser.addOption(dbDirOption);

	if (!parser.parse(app.arguments())) {
		qInfo() << QObject::tr("Parameters Incorrect");
		return 1;
	}

	if (parser.isSet(helpOption)) {
		parser.showHelp();
		return 0;
	}

	if (parser.isSet(versionOption)) {
		parser.showVersion();
		return 0;
	}

	if (parser.isSet(portOption)) {
		bool ok;
		int passedPort = parser.value(portOption).toUInt(&ok);
		if (ok && passedPort <= 65535) {
			port = passedPort;
		} else {
			qCritical("Bad Port Number");
			exit(1);
		}
	} else {
		qCritical("No Ssl port specified");
		exit(1);
	}

	if (parser.isSet(webPortOption)) {
		bool ok;
		int passedPort = parser.value(webPortOption).toUInt(&ok);
		if (ok && passedPort <= 65535) {
			webPort = passedPort;
		} else {
			qCritical("Bad Web Socket Port Number");
			exit(1);
		}
	}

	if (parser.isSet(portOption) && parser.isSet(webPortOption) && port == webPort) {
		qCritical("Ssl server and web socket server can not be on the same port");
		exit(1);
	}

	if (parser.isSet(dbDirOption))
		customDbDir = parser.value(dbDirOption);

	qInstallMessageHandler(systemLogOutput);
	MissionDM missionDM(port, webPort, customDbDir);

	return app.exec();
}
