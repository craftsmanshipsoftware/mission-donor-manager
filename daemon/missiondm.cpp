// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QCoreApplication>
#include <QSysInfo>
#include <QSqlQuery>
#include <QSslKey>
#include <QFile>
#include <QDir>
#include <QStandardPaths>
#include <QCoreApplication>
//#include "../QtFoxMQ/common/foxmq-id.h"
#include "qsslutils.h"
#include "paypal.h"
#include "missiondm.h"

//#define SHOW_INCOMING

#define DB_FILE_NAME "mission-dm.sqlite3"

MissionDM::MissionDM(size_t port, size_t webPort, const QString &dbDir) : authenticationApi(*this), donorApi(*this), donationsApi(*this), setupApi(*this), usersApi(*this), taxApi(*this, port), thankyouApi(*this),
	email(setupApi), journal(*this), missionApi(*this, usersApi), iifApi(*this), incomeAccountsApi(*this), advantageAccountsApi(*this), campaignsApi(*this), recurringApi(*this), emailListApi(*this),
	reportsApi(*this)
{
	if (dbDir.isEmpty())
		configPathString = QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation) + '/' + QString::number(port) + '/';
	else
		configPathString = dbDir;  // use custom DB path
	QDir configPath(configPathString);
	if (!configPath.exists()) {
		if (!configPath.mkpath(configPathString)) {
			qInfo() << "Unable to create dir" << configPathString;
			exit(1);
		}
	}

	journal.openPath(configPathString);
	journal.log(0, "Starting version " + qApp->applicationVersion());

	if (QFile::exists(configPathString + DB_FILE_NAME)) {
		state = ServerState::NeedDecryptPwd;
		qInfo("State - Needs DB password");
	} else {
		state = ServerState::NoDBPwd;
		qInfo("State - new DB");
	}

	db = QSqlDatabase::addDatabase("SQLITECIPHER");
	db.setDatabaseName(configPathString + DB_FILE_NAME);

	setupCrypto();

	connect(&den, &FoxmqDen::contentChange, this, &MissionDM::newRequest);
	connect(&den, &FoxmqDen::incomingPost, this, &MissionDM::incomingPost);
	connect(&den, &FoxmqDen::newClient, this, &MissionDM::newClient);
	connect(&den, &FoxmqDen::clientDisconnected, this, &MissionDM::clientLeft);

	if (den.startSSLSockets(port, sslConfiguration)) {
		qInfo() << "Started Ssl server on" << port;
	} else {
		qFatal("Api - Can not start Ssl server");
		QCoreApplication::quit();
	}

	if (webPort) {
		if (den.startWebSockets("MDM", webPort, sslConfiguration)) {
			qInfo() << "Started Web Socket server on" << webPort;
		} else {
			qFatal("Api - Can not start Web Socket server");
			QCoreApplication::quit();
		}
	}
	publishState();
}

void MissionDM::openDB(const QString &password, bool setPwd = false)
{
	qInfo("Attempt to open DB");
	db.setPassword(password);
	if (setPwd)
		db.setConnectOptions("QSQLITE_CREATE_KEY");
	if (!db.open()) {
		qWarning("Could not open Database!");
	} else {
		for (auto api : apis)
			api->loadInfoFromDB();
		paypal =  new PaypalTransactions(*this);

		if (getTableRowCount("users") < 1) {
			state = ServerState::NeedFirstUser;
			qInfo("State - Needs first user");
		} else {
			state = ServerState::Ready;
			qInfo("State - Ready");
		}
	}
	publishState();
}

void MissionDM::setupCrypto()
{
	// generate key and certificate in one go with the below command in the config/port dir.
	// this is for encryption only ....not authentication
	//    openssl req -x509 -nodes -days 36500 -newkey rsa:4096 -keyout key.pem -out cert.pem

	QFile keyFile(configPathString + "key.pem");
	keyFile.open(QIODevice::ReadOnly);
	QByteArray privateKeyPEM = keyFile.readAll();
	keyFile.close();
	QFile certificateFile(configPathString + "cert.pem");
	certificateFile.open(QIODevice::ReadOnly);
	QByteArray certificatePem = certificateFile.readAll();
	certificateFile.close();
	QSslKey qKey(privateKeyPEM, QSsl::Rsa, QSsl::Pem, QSsl::PrivateKey);
	sslConfiguration.setPrivateKey(qKey);
	sslConfiguration.setLocalCertificate(QSslCertificate(certificatePem));
}

void MissionDM::publishState()
{
	den.publish("server.state", FoxHead::ContentType::IntAsString, QByteArray::number((int)state));
}

void MissionDM::newClient(FoxID id, bool)
{
	qInfo() << "Client connected" << den.getClientsIPAddress(id);
	FoxPermittedPrefixes p;
	p.insert("server");
	p.insert("auth");
	den.setPermittedTopics(id, p);
}

void MissionDM::newRequest(const QByteArray &topic, ssize_t, FoxID asker, FoxHead::ContentType, QByteArray)
{
#ifdef SHOW_INCOMING
	qDebug() << "newRequestChange" << topic;
#endif
	den.sendUnknownTopic(asker, topic);
}

void MissionDM::incomingPost(const QByteArray &topic, ssize_t count, FoxID asker, FoxHead::ContentType contentType, QByteArray content)
{
#ifdef SHOW_INCOMING
	qDebug() << "newAskForResponse" << topic;
#endif
	bool subApiResponded = false;

	if (state == ServerState::NoDBPwd && topic == "server.set-pwd") {
		subApiResponded = true;
		openDB(content, true);
	} else if (state == ServerState::NeedDecryptPwd && topic == "server.pwd") {
		subApiResponded = true;
		openDB(content);
	} else {
		for (auto api : apis) {
			if (api->topicBelongsToMe(topic)) {
				subApiResponded = api->incomingPost(topic, count, asker, contentType, content);
				break;
			}
		}
	}

	if (!subApiResponded)
		den.sendUnknownTopic(asker, topic);
}

void MissionDM::clientLeft(const FoxID &id)
{
	FoxID removed = id;
	int userId = authenticatedUserIDs.value(removed.local());
	QString msg;
	if (!userId) {
		qInfo() << "Client left";
	} else {
		qInfo() << "Client" << userId << "left";
		journal.log(0, "Client " + QString::number(userId) + " left");
	}
	authenticatedUserIDs.remove(removed.local());

	for (auto api : apis)
		api->foxLeft(removed);

	authenticationApi.removeAnyClientPasswordResetRequest(removed);
}

void MissionDM::setOneOrMoreUsersReady()
{
	if (state == ServerState::NeedFirstUser) {
		state = ServerState::Ready;
		publishState();
	}
}

int MissionDM::getTableRowCount(const QString &tableName)
{
	QSqlQuery query;
	int count = 0;

	query.prepare(QString("SELECT COUNT(1) FROM %1").arg(tableName));
	query.exec();
	if (query.first())
		count = query.value(0).toInt();

	return count;
}

QString MissionDM::getAddressSalutation(const QSqlQuery &query)
{
	QString addressSalutation = query.value("addr_sal").toString();
	if (addressSalutation.isEmpty())
		addressSalutation = query.value("forename").toString() + ' ' + query.value("surname").toString();
	return addressSalutation;
}

QString MissionDM::getSalutation(const QSqlQuery &query)
{
	QString addressSalutation = query.value("sal").toString();
	if (addressSalutation.isEmpty())
		addressSalutation = query.value("forename").toString() + ' ' + query.value("surname").toString();
	return addressSalutation;
}

QString MissionDM::getIncomeAccount(int index)
{
	QSqlQuery query;
	query.exec(QString("SELECT number FROM source_accounts WHERE ID=%1").arg(index));
	query.first();
	return query.value("number").toString();
}

// used by others to build a list of Income account or Advantage account names and account numbers. Acceptable tables names
//  "source_accounts"
//  "advantage_accounts"
QList<MissionDM::AccountNameNumber> MissionDM::loadAccountNames(const QString &table)
{
	QList<AccountNameNumber> accountNameList;
	QSqlQuery query;
	query.prepare(QString("SELECT * FROM %1").arg(table));
	query.exec();
	if (query.first()) {
		do {
			struct AccountNameNumber sa;
			sa.name = query.value("name").toString();
			sa.number = query.value("number").toString();
			accountNameList.append(sa);
		} while (query.next());
	}
	return accountNameList;
}

bool MissionDM::disconnectUser(uint32_t id)
{
	bool atLeastOneFound = false;
	for (auto client : authenticatedUserIDs.keys()) {
		if (id == authenticatedUserIDs.value(client)) {
			den.disconnectClient(client);
			atLeastOneFound = true;
		}
	}
	return atLeastOneFound;
}
