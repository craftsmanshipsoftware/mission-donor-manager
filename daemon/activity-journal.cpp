// clang-format off
/**************************************************************************************************
	Copyright (C) 2024  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QSqlQuery>
#include <QDate>
#include <QDebug>
#include "common.h"
#include "missiondm.h"
#include "activity-journal.h"

ActivityJournal::ActivityJournal(MissionDM &mdm) : mdm(mdm)
{

}

void ActivityJournal::openPath(const QString &path)
{
	journalPath = path;
	checkIfYearChanged();
}

void ActivityJournal::checkIfYearChanged()
{
	int year = QDate::currentDate().year();
	if (year != currentYear) {
		journalFile.close();
		journalFile.setFileName(journalPath + "journal-" + QString::number(year) + ".txt");
		if (!journalFile.open(QIODevice::WriteOnly | QIODevice::Append))
			qWarning() << "unable to open journal at" << journalPath;
	}
	currentYear = year;
}

void ActivityJournal::log(int id, const QString &string)
{
	checkIfYearChanged();
	if (journalFile.isOpen()) {
		journalFile.write(QDateTime::currentDateTime().toString("yyyy-MM-ddThh:mm:ss, ").toUtf8() + ' ');
		if (id == 0)
			journalFile.write("System");
		else
			journalFile.write("User " + QByteArray::number(id));

		journalFile.write(" - " + string.toUtf8() + '\n');
		journalFile.flush();
	}
}

void ActivityJournal::logDonation(int source, int type, int thirdParty, int amount, int advantage, int fee, int sa, int donorId, int did, int missionID, int date)
{
	QSqlQuery query;
	QString msg;
	msg+="DONATION, ";
	switch (type) {
		case DonationType::Normal :    msg+= "Normal, ";     break;
		case DonationType::Inkind:     msg+= "Inkind, ";     break;
		case DonationType::ThirdParty: msg+= "Thirdparty, "; break;
	}
	switch (source) {
		case DonationVia::Manual:    msg+= "src=Manual, "; break;
		case DonationVia::Recurring: msg+= "src=Recr, ";   break;
		case DonationVia::Paypal:    msg+= "src=PayPal, "; break;
	}
	msg+= "3P=" + QString::number(thirdParty) + ", ";
	msg+= "A=$" + convertCentsToString(amount) + ", ";
	msg+= "Av=$" + convertCentsToString(advantage) + ", ";
	msg+= "F=$" + convertCentsToString(fee) + ", ";

	msg+= "IA=" + mdm.getIncomeAccount(sa) + ", ";
	msg+= "donorID=" + QString::number(donorId) + ", ";
	msg+= "DID=" + QString::number(did) + ", ";

	query.clear();
	query.exec(QString("SELECT account FROM missions WHERE ID=%1").arg(missionID));
	query.first();
	msg+= "missionID=" + query.value("account").toString() + ", ";

	msg+= QDate::fromJulianDay(date).toString("yyyy-MM-dd");

	log(userId, msg);
}

void ActivityJournal::logVoiding(bool voided, int donorId, int did)
{
	QString msg;
	if (voided)
		msg+="VOID, ";
	else
		msg+="UNVOID, ";
	msg+= "donorID=" + QString::number(donorId) + ", ";
	msg+= "DID=" + QString::number(did) + ", ";

	log(userId, msg);
}

void ActivityJournal::logIifGrouping(int iifId, int sa, int julianDay, int transactionsMarked)
{
	QString msg;
	msg+="IIF-GROUPING, ";
	msg+="IIF=" + QString::number(iifId) + ", ";
	if (sa < 1)
		msg+= "IA=ALL, ";
	else
		msg+= "IA=" + mdm.getIncomeAccount(sa) + ", ";
	msg+= "End=" + QDate::fromJulianDay(julianDay).toString("yyyy-MM-dd") + ", ";
	msg+= "Transactions=" + QString::number(transactionsMarked);

	log(userId, msg);
}

void ActivityJournal::logIifAppend(int iifId, int donorId, int donationId)
{
	QString msg;
	msg+="IIF-APPEND, ";
	msg+= "IIF=" + QString::number(iifId) + ", ";
	msg+= "Donor=" + QString::number(donorId) + ", ";
	msg+= "Donation Idx=" + QString::number(donationId);
	log(userId, msg);
}

void ActivityJournal::logIifRemoveDonation(int iifId, int donorId, int donationId)
{
	QString msg;
	msg+="IIF-REMOVE-DONATION, ";
	msg+= "IIF=" + QString::number(iifId) + ", ";
	msg+= "Donor=" + QString::number(donorId) + ", ";
	msg+= "Donation Idx=" + QString::number(donationId);
	log(userId, msg);
}

void ActivityJournal::logIifUngroup(int iifId)
{
	QString msg;
	msg+="IIF-UNGROUP, ";
	msg+= "IIF=" + QString::number(iifId);
	log(userId, msg);
}

void ActivityJournal::logReceiptGeneration(int year, int totalCashDonations, int totalCashReceipts, int totalInkindReceipts)
{
	QString msg;
	msg+="TAX - Receipts generation summary " + QString::number(year) + ", ";
	msg+= "T.Cash Donations=" + QString::number(totalCashDonations) + ", ";
	msg+= "T.Cash Receipts=" + QString::number(totalCashReceipts) + ", ";
	msg+= "T.Inkind Receipts=" + QString::number(totalInkindReceipts);
	log(userId, msg);
}
