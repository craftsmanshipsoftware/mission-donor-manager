// clang-format off
/**************************************************************************************************
	Copyright (C) 2023-2025  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrlQuery>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QSqlQuery>
#include "common.h"
#include "missiondm.h"
#include "email-sender.h"
#include "paypal.h"

//#define BaseURL "https://api-m.sandbox.paypal.com/"
#define BaseURL "https://api.paypal.com/"

//#define LogJsonFieldIfNoIdentifierFound
//#define LogTransactions

PaypalTransactions::PaypalTransactions(MissionDM &mdm) : mdm(mdm)
{
	connect(&transactionCheckTimer, &QTimer::timeout, this, &PaypalTransactions::checkToken);

	if (!mdm.db.tables().contains("paypal_captured_transaction_ids")) {
		QSqlQuery query;
		qInfo("creating paypal captured transaction ids table");
		query.exec("create table paypal_captured_transaction_ids (id string primary key, day int)");
		mdm.db.commit();
	}

	transactionCheckTimer.start(15*60*1000);
	checkToken();
}

void PaypalTransactions::checkToken()
{
	if (mdm.setupApi.paypalClientId.isEmpty() || mdm.setupApi.paypalSourceAccount < 1)  // paypal not enabled, do nothing
		return;

	if (bearer.token.isEmpty() || bearer.expiry < QDateTime::currentDateTime()) {
		QNetworkRequest tokenRequest;
		QUrlQuery data;
		tokenRequest.setUrl(QUrl(BaseURL"v1/oauth2/token"));
		tokenRequest.setRawHeader("authorization", "Basic " + QByteArray(mdm.setupApi.paypalClientId.toUtf8() + ':' + mdm.setupApi.paypalSecret.toUtf8()).toBase64());
		tokenRequest.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
		data.addQueryItem("grant_type", "client_credentials");
		QNetworkReply *tokenReply = nam.post(tokenRequest, data.toString(QUrl::FullyEncoded).toUtf8());
		connect(tokenReply, &QNetworkReply::finished, this, &PaypalTransactions::tokenRequestSucceeded);
		connect(tokenReply, &QNetworkReply::errorOccurred, this, &PaypalTransactions::tokenRequestFailed);
	} else {
		getTransactionList();
	}
}

void PaypalTransactions::tokenRequestFailed()
{
	qInfo() << "Paypal token request failed" << sender();
	sender()->deleteLater();
}

void PaypalTransactions::tokenRequestSucceeded()
{
	QNetworkReply *tokenReply = qobject_cast<QNetworkReply *>(sender());
	QJsonObject json = QJsonDocument::fromJson(tokenReply->readAll()).object();
	bearer.token = json.value("access_token").toString().toUtf8();
	QDateTime expiryDate = QDateTime::currentDateTime();
	bearer.expiry = expiryDate.addSecs(json.value("expires_in").toInt() - 3600);
	tokenReply->deleteLater();
	getTransactionList();
}

void PaypalTransactions::getTransactionList()
{
	QNetworkRequest request;
	QDateTime to = QDateTime::currentDateTime();
	QDateTime from = to.addDays(-7);  // grab the past week
	QString fromParameter = QString("start_date=%1-%2-%3T%4:%5:%6-0000")
	        .arg(from.date().year()).arg(from.date().month()).arg(from.date().day())
	        .arg(from.time().hour()).arg(from.time().minute()).arg(from.time().second());
	QString toParameter = QString("end_date=%1-%2-%3T%4:%5:%6-0000")
	        .arg(to.date().year()).arg(to.date().month()).arg(to.date().day())
	        .arg(to.time().hour()).arg(to.time().minute()).arg(to.time().second());;
	request.setUrl(QUrl(BaseURL"v1/reporting/transactions?fields=all&" + fromParameter + '&' + toParameter));
	request.setRawHeader("Authorization", "Bearer " + bearer.token);
	request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
	QNetworkReply *transactionReply = nam.get(request);
	connect(transactionReply, &QNetworkReply::finished, this, &PaypalTransactions::transactionListRequestSucceeded);
	connect(transactionReply, &QNetworkReply::errorOccurred, this, &PaypalTransactions::transactionListRequestFailed);
	getCapturedTransactionIDsFromDB();
	cullOldNotifications();
	cullOldTransactionsFromDB();
}

void PaypalTransactions::getCapturedTransactionIDsFromDB()
{
	QSqlQuery query;
	capturedTransactionIDs.clear();
	// transactions older than 3 months are removed else where, so we can load them all.
	query.prepare(QString("SELECT id FROM paypal_captured_transaction_ids"));
	query.exec();
	if (query.first()) {
		do {
			capturedTransactionIDs.insert(query.value("id").toString(), 0);
		} while (query.next());
	}
}

void PaypalTransactions::transactionListRequestFailed()
{
	sender()->deleteLater();
}

void PaypalTransactions::transactionListRequestSucceeded()
{
	QNetworkReply *transactionReply = qobject_cast<QNetworkReply *>(sender());
	QJsonObject json = QJsonDocument::fromJson(transactionReply->readAll()).object();
	transactionReply->deleteLater();
	QJsonArray transactions = json.value("transaction_details").toArray();
	mdm.db.transaction();
    for (auto item : transactions) {
		QJsonObject jItem = item.toObject();
		QJsonObject jTransactionInfo = jItem.value("transaction_info").toObject();
		QString transactionId = jTransactionInfo.value("transaction_id").toString();
		QString code = jTransactionInfo.value("transaction_event_code").toString();
		QString dateString = jTransactionInfo.value("transaction_initiation_date").toString();
		QDateTime ddt = QDateTime::fromString(dateString, "yyyy-MM-ddThh:mm:sstt");
		int julianDay =  ddt.toLocalTime().date().toJulianDay();
		if (webDonationCodes.contains(code) && !capturedTransactionIDs.contains(transactionId)) {
			int donorID = getDonorID(jItem.value("payer_info").toObject(), jItem.value("shipping_info").toObject(), julianDay);
			int mailFlags = 0;
			QString identifier = getMissionIdentifier(code, jItem.value("cart_info").toObject(), jTransactionInfo, mailFlags);
			if (!identifier.isEmpty()) {
				processTransaction(donorID, identifier, jTransactionInfo, julianDay, mailFlags);
			} else {
				if (!badTransactions.contains(transactionId)) {
					QString body = "Paypal mission identifier not found in transaction " + transactionId;
					mdm.email.send(mdm.setupApi.donorRelationsEmail.toUtf8(), mdm.setupApi.donorRelationsEmail.toUtf8(), "Mission DM - Paypal mission identifier not found in transaction", body.toUtf8());
					badTransactions.insert(transactionId, julianDay);
					qInfo() << body;
				}
			}
		}
	}
	mdm.db.commit();

	if (addedDonor)
		mdm.donorApi.publishDonorList();
	addedDonor = false;
	mdm.donationsApi.publishModifiedAccounts();
}

int PaypalTransactions::getDonorID(const QJsonObject &jPayerInfo, const QJsonObject &jShippingInfo, int julianDay)
{
	int donorId = 0;
	bool updatePaypalAccountId = false;
	bool updateEmail = false;
	QString email = jPayerInfo.value("email_address").toString().toLower();

	QString paypalAccountID = jPayerInfo.value("account_id").toString();
	if (!paypalAccountID.isEmpty())
		donorId = getDonorIdByPaypalAccount(paypalAccountID, julianDay);

	if (!donorId) {
		donorId = getDonorIdByEmailAddress(email);
		updatePaypalAccountId = true;
	}

	if (!donorId) {
		donorId = getDonorIdByMaillingAddress(jPayerInfo, jShippingInfo);
		updatePaypalAccountId = true;
		updateEmail = true;
	}

	// mail in donor made a first time Paypal or Credit Card donation
	// or donor used a Credit Card instead of paypal acount or vise-versa
	if (donorId) {
		if (updatePaypalAccountId)
			updateDonorsPaypalAccount(donorId, paypalAccountID, julianDay);
		if (updateEmail) {
			updateDonorEmail(donorId, email);
			QString body = "Paypal processed a donation from donor ID:" + QString::number(donorId) +" with a different email (" + email +  ") than on file";
			mdm.email.send(mdm.setupApi.donorRelationsEmail.toUtf8(), mdm.setupApi.donorRelationsEmail.toUtf8(), "Mission DM - donor used alternate email in Paypal", body.toUtf8());
			qInfo() << "Paypal processed a donation from donor ID:" + QString::number(donorId) +" with a different email than on file";;
		}
	}

	if (!donorId)
		donorId = addNewDonor(jPayerInfo, jShippingInfo, julianDay);

	return donorId;
}

int PaypalTransactions::getDonorIdByPaypalAccount(const QString &paypalAccountId, int julianDay)
{
	int donorId = 0;
	QSqlQuery query;

	query.prepare("SELECT id, ppa0, ppa1, ppa2, ppa3, ppa4 FROM donors WHERE ppa0=:ppa OR ppa1=:ppa OR ppa2=:ppa OR ppa3=:ppa OR ppa4=:ppa");
	query.bindValue(":ppa", paypalAccountId);
	query.exec();
	if (query.first()) {
		donorId = query.value("id").toInt();
		if (paypalAccountId == query.value("ppa0").toString())
			updateSlotLastUsedDate(donorId, 0, julianDay);
		else if(paypalAccountId == query.value("ppa1").toString())
			updateSlotLastUsedDate(donorId, 1, julianDay);
		else if(paypalAccountId == query.value("ppa2").toString())
			updateSlotLastUsedDate(donorId, 2, julianDay);
		else if(paypalAccountId == query.value("ppa3").toString())
			updateSlotLastUsedDate(donorId, 3, julianDay);
		else
			updateSlotLastUsedDate(donorId, 4, julianDay);
	}

	return donorId;
}

void  PaypalTransactions::updateSlotLastUsedDate(int donorId, int slot, int julianDay)
{
	QSqlQuery query;
	query.prepare(QString("UPDATE donors SET ppa%1_last=:last WHERE id=:id").arg(slot));
	query.bindValue(":id", donorId);
	query.bindValue(":last", julianDay);
	query.exec();
}

bool PaypalTransactions::donorsPaypalAccountIsBlank(int donorId, int slot)
{
	QSqlQuery query;
	bool blank = false;

	query.prepare(QString("SELECT ppa%1 FROM donors WHERE id=:id").arg(slot));
	query.bindValue(":id", donorId);
	query.exec();
	if (query.first()) {
		if (query.value(QString("ppa%1").arg(slot)).toString().isEmpty())
			blank = true;
	}

	return blank;
}

int PaypalTransactions::getDonorIdByEmailAddress(const QString &emailAddress)
{
	int donorId = 0;
	QSqlQuery query;

	query.prepare("SELECT id FROM donors WHERE email=:email");
	query.bindValue(":email", emailAddress);
	query.exec();
	if (query.first())
		donorId = query.value("id").toInt();

	return donorId;
}

int PaypalTransactions::getDonorIdByMaillingAddress(const QJsonObject &jPayerInfo, const QJsonObject &jShippingInfo)
{
	int donorId = 0;
	QSqlQuery query;

	QJsonObject jAddress = jShippingInfo.value("address").toObject();
	QJsonObject jPayerName = jPayerInfo.value("payer_name").toObject();

	if (jPayerName.contains("given_name") || jPayerName.contains("surname")) {  // Individual
		matcher.a.first = jPayerName.value("given_name").toString();
		matcher.a.last = jPayerName.value("surname").toString();
	} else {  // business
		matcher.a.first = jPayerName.value("alternate_full_name").toString();
	}

	matcher.a.line1 = jAddress.value("line1").toString();
	matcher.a.line2 = jAddress.value("line2").toString();
	matcher.a.city = jAddress.value("city").toString();
	matcher.a.province = jAddress.value("state").toString();
	matcher.a.countryCode = jAddress.value("country_code").toString();
	matcher.a.postalCode = jAddress.value("postal_code").toString();
	matcher.condense(matcher.a);
	matcher.prepAddressForComplexCompare(matcher.a);

	query.prepare("SELECT id, forename, surname, address, apt, city, province, country, postalcode FROM donors");
	query.exec();
	if (!query.first())
		return donorId;
	do {
		matcher.b.first = query.value("forename").toString();
		matcher.b.last = query.value("surname").toString();
		matcher.b.line1 = query.value("address").toString();
		matcher.b.line2 = query.value("apt").toString();
		matcher.b.city = query.value("city").toString();
		matcher.b.province = query.value("province").toString();
		matcher.b.countryCode = query.value("country").toString();;
		matcher.b.postalCode = query.value("postalcode").toString();;
		matcher.condense(matcher.b);

		if (matcher.match()) {
			donorId = query.value("id").toInt();
			break;
		}
	} while (query.next());

	return donorId;
}

void PaypalTransactions::updateDonorsPaypalAccount(int donorId, const QString &paypalAccountId, int julianDay)
{
	QSqlQuery query;

	if (donorsPaypalAccountIsBlank(donorId, 0)) {
		setDonorsPaypalAccount(donorId, paypalAccountId, 0, julianDay);
	} else if (donorsPaypalAccountIsBlank(donorId, 1)) {
		setDonorsPaypalAccount(donorId, paypalAccountId, 1, julianDay);
	} else if (donorsPaypalAccountIsBlank(donorId, 2)) {
		setDonorsPaypalAccount(donorId, paypalAccountId, 2, julianDay);
	} else if (donorsPaypalAccountIsBlank(donorId, 3)) {
		setDonorsPaypalAccount(donorId, paypalAccountId, 3, julianDay);
	} else if (donorsPaypalAccountIsBlank(donorId, 4)) {
		setDonorsPaypalAccount(donorId, paypalAccountId, 4, julianDay);
	} else {  // all are used, replace the oldest
		int slot = 0;
		query.prepare("SELECT ppa0_last, ppa1_last, ppa2_last, ppa3_last, ppa4_last FROM donors WHERE id=:id");
		query.bindValue(":id", donorId);
		query.exec();
		if (query.first()) {
			int slot0Date = query.value("ppa0_last").toInt();
			int slot1Date = query.value("ppa1_last").toInt();
			int slot2Date = query.value("ppa2_last").toInt();
			int slot3Date = query.value("ppa3_last").toInt();
			int slot4Date = query.value("ppa4_last").toInt();
			int min = qMin(slot0Date, qMin(slot1Date, qMin(slot2Date, qMin(slot3Date, slot4Date))));
			if (min == slot0Date)
				slot = 0;
			else if (min == slot1Date)
				slot = 1;
			else if (min == slot2Date)
				slot = 2;
			else if (min == slot3Date)
				slot = 3;
			else
				slot = 4;
		}
		setDonorsPaypalAccount(donorId, paypalAccountId, slot, julianDay);
	}
}

void PaypalTransactions::setDonorsPaypalAccount(int donorId, const QString &paypalAccountId, int slot, int julianDay)
{
	QSqlQuery query;
	query.prepare(QString("UPDATE donors SET ppa%1=:ppa, ppa%1_last=:last WHERE id=:id").arg(slot));
	query.bindValue(":id", donorId);
	query.bindValue(":ppa", paypalAccountId);
	query.bindValue(":last", julianDay);
	query.exec();

}

int PaypalTransactions::addNewDonor(const QJsonObject &jPayerInfo, const QJsonObject &jShippingInfo, int julianDay)
{
	QSqlQuery query;
	int donorId;
	int donorType;

	QJsonObject jAddress = jShippingInfo.value("address").toObject();
	QJsonObject jPayerName = jPayerInfo.value("payer_name").toObject();

	donorId = mdm.donorApi.getNewDonorId();
	query.prepare("INSERT INTO donors (id, type, forename, surname, address, apt, city, province, country, postalcode, email, ppa0, ppa0_last, news, added) VALUES (:id, :type, :forename, :surname, :address, :apt, :city, :province, :country, :postalcode, :email, :ppa0, :ppa0_last, :news, :added)");
	query.bindValue(":id", donorId);
	query.bindValue(":address", jAddress.value("line1").toString());
	query.bindValue(":apt", jAddress.value("line2").toString());
	query.bindValue(":city", jAddress.value("city").toString());
	query.bindValue(":province", jAddress.value("state").toString());
	query.bindValue(":country", jPayerInfo.value("country_code").toString());
	query.bindValue(":postalcode", jAddress.value("postal_code").toString());
	query.bindValue(":email", jPayerInfo.value("email_address").toString().toLower());
	query.bindValue(":ppa0", jPayerInfo.value("account_id").toString());
	query.bindValue(":ppa0_last", julianDay);
	QString log;
	if (jPayerName.contains("given_name") || jPayerName.contains("surname")) {  // Individual
		donorType = DonorType::Individual;
		QString first = jPayerName.value("given_name").toString();
		QString last = jPayerName.value("surname").toString();
		query.bindValue(":forename", first);
		query.bindValue(":surname", last);
		log = "Paypal module added a new Donor, ID:" + QString::number(donorId);
		QString body = "Paypal module added a new Donor, " +  first + ' ' + last + ", ID:" + QString::number(donorId);
		mdm.email.send(mdm.setupApi.donorRelationsEmail.toUtf8(), mdm.setupApi.donorRelationsEmail.toUtf8(), "Mission DM - New donor added in Paypal", body.toUtf8());
	} else {
		donorType = DonorType::Business;
		query.bindValue(":forename", jPayerName.value("alternate_full_name").toString());
		log = "Paypal module added a new business, ID:" + QString::number(donorId) + "  Please check if this is a business or charity";
		mdm.email.send(mdm.setupApi.donorRelationsEmail.toUtf8(), mdm.setupApi.donorRelationsEmail.toUtf8(), "Mission DM - New business donor added in Paypal", log.toUtf8());
	}
	qInfo() << log;
	if (jPayerInfo.value("country_code").toString() != "CA")
		donorType = DonorType::NonTaxPayer;
	query.bindValue(":type", donorType);
	query.bindValue(":news", Mail::ThankyouPaypalOneTime | Mail::ThankyouPaypalRecurring | Mail::TaxReceiptByEmail);
	query.bindValue(":added", julianDay);
	query.exec();

	mdm.donorApi.addNoteEntry(donorId);
	mdm.donorApi.createAccount(donorId);
	// fixme check return values, set donorId = 0 if failure?
	addedDonor = true;

	return donorId;
}

void PaypalTransactions::updateDonorEmail(int donorId, const QString &email)
{
	auto getEmail = [](int donorId, const QString &column) {
		QString email;
		QSqlQuery query;
		query.prepare(QString("SELECT %1 FROM donors WHERE id=%2").arg(column).arg(donorId));
		query.exec();
		query.first();
		email = query.value(column).toString();
		return email;
	};

	// only add primary if it is blank
	QString existingPrimaryEmail = getEmail(donorId, "email");
	if (existingPrimaryEmail.isEmpty()) {
		QSqlQuery query;
		query.prepare("UPDATE donors SET email=:email WHERE id=:id");
		query.bindValue(":id", donorId);
		query.bindValue(":email", email);
		query.exec();
		mdm.journal.log(QString("Paypal module added primary email for donor %1").arg(donorId));
	}
}

// Could be
//   a donation button id or              transaction_info->custom_field
//   SOAP item_number from a T0013 or     transaction_info->cart_info-->item_details[0]-->item_code
//   SOAP item_name from a T0002          transaction_info->transaction_subject - parse for [....]
QString PaypalTransactions::getMissionIdentifier(const QString &code, const QJsonObject &jCartInfo, const QJsonObject &jTransactionInfo, int &mailFlags)
{
	QString missionAccount;

	if (jTransactionInfo.contains("custom_field")) {	// if using donation button
		mailFlags = Mail::ThankyouPaypalOneTime;
		missionAccount = jTransactionInfo.value("custom_field").toString();
	} else {
		if (code == "T0002") { // subscription - look in subject for identifier
			mailFlags = Mail::ThankyouPaypalRecurring;
			QString subject = jTransactionInfo.value("transaction_subject").toString();
			int start = subject.indexOf('[');
			int stop = subject.indexOf(']', start);
			if (start > 0 && stop > 0)
				missionAccount = subject.mid(start + 1, stop - start - 1);
#ifdef LogJsonFieldIfNoIdentifierFound
			if (missionAccount.isEmpty())
				qInfo() << "PayPal T0002 - subject = " << subject;
#endif
		} else {  // one time = T0013 - look in cart_info-->item_details[0]-->item_code
			mailFlags = Mail::ThankyouPaypalOneTime;
			QJsonArray jItemDetails = jCartInfo.value("item_details").toArray();
			missionAccount = jItemDetails.at(0).toObject().value("item_code").toString();
#ifdef LogJsonFieldIfNoIdentifierFound
			if (missionAccount.isEmpty())
				qInfo() << "PayPal T0013 - item_details = " << jItemDetails;
#endif
		}
	}

	return missionAccount;
}

void PaypalTransactions::processTransaction(int donorId, const QString &missionIdentifier, const QJsonObject &json, int julianDay, int mailFlags)
{
	QSqlQuery query;
	int fee;
	int amount;
	int missionID;

	QString id = json.value("transaction_id").toString();

	// find a mission with the matching paypal button ID
	query.prepare("SELECT id FROM missions WHERE button_id=:button_id");
	query.bindValue(":button_id", missionIdentifier);
	query.exec();
	if (!query.first()) {
		QString transactionId = json.value("transaction_id").toString();
		if (!badTransactions.contains(transactionId)) {
			QString body = "Paypal - Unable to find mission with identifier " + missionIdentifier + " for transaction " + transactionId;
			mdm.email.send(mdm.setupApi.donorRelationsEmail.toUtf8(), mdm.setupApi.donorRelationsEmail.toUtf8(), "Mission DM - Paypal - Unable to find mission", body.toUtf8());
			badTransactions.insert(transactionId, julianDay);
			qInfo() << body;
		}
		return;
	} else {
		missionID = query.value("id").toInt();
	}
	query.finish();

	// get amount
	QJsonObject jAmount = json.value("transaction_amount").toObject();
	QString amountString = jAmount.value("value").toString();
	amountString.remove('.');  // remove decimal point, amounts are stored in cents
	amount = amountString.toInt();

	// get fee
	QJsonObject jFee = json.value("fee_amount").toObject();
	QString feeString  = jFee.value("value").toString();
	feeString.remove('.');  // remove decimal point, amount are stored in cents
	feeString.remove('-');  // remove negative symbol
	fee = feeString.toInt();

	// check for note
	QString note = json.value("transaction_note").toString();
	if (!note.isEmpty()) {
		QSqlQuery missionNameQuery;
		missionNameQuery.prepare(QString("SELECT name FROM missions WHERE id=%1").arg(missionID));
		missionNameQuery.exec();
		missionNameQuery.first();
		QString missionName = missionNameQuery.value("name").toString();
		QString body = QString("Paypal Web donor with id:%1 left a note donating to mission %2\n\n%3").arg(donorId).arg(missionName).arg(note);
		mdm.email.send(mdm.setupApi.donorRelationsEmail.toUtf8(), mdm.setupApi.donorRelationsEmail.toUtf8(), "Mission DM - Paypal donor left a note", body.toUtf8());
	}

	mdm.journal.setActiveUser(0);  // 0 = system / no user
	if (mdm.donationsApi.add(DonationVia::Paypal, DonationType::Normal, -1, amount, 0, fee, mdm.setupApi.paypalSourceAccount, 0, donorId, missionID, julianDay, false, note, "", mailFlags)) {
		query.prepare("INSERT INTO paypal_captured_transaction_ids (id, day) VALUES (:id, :day)");
		query.bindValue(":id", id);
		// store date transaction appeared in transaction history, not necessarily the initial date of the transaction.  Transactions can be delayed from a bad CC
		query.bindValue(":day", QDate::currentDate().toJulianDay());
		query.exec();
#ifdef LogTransactions
		qInfo() << "PayPal Donation: Donor =" << donorId << "Mission =" << missionID << "transaction =" << id;
#endif
	}
}

void PaypalTransactions::cullOldNotifications()
{
	int oldestJuilianDay = QDate::currentDate().addMonths(-2).toJulianDay();
	QMap<QString, int>::iterator it;
	for (it = badTransactions.begin(); it != badTransactions.end();) {
		if (it.value() < oldestJuilianDay)
			it = badTransactions.erase(it);
		else
			it++;
	}
}

void PaypalTransactions::cullOldTransactionsFromDB()
{
	QDate threeMonthsOld = QDate::currentDate().addMonths(-3);
	mdm.db.transaction();
	QSqlQuery rmQuery;
	rmQuery.exec(QString("DELETE FROM paypal_captured_transaction_ids WHERE day BETWEEN 0 and %1").arg(threeMonthsOld.toJulianDay()));
	mdm.db.commit();
}
