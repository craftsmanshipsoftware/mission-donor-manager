// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include "setup-api.h"
#include "email-sender.h"
#include <Poco/Net/SMTPClientSession.h>
#include <Poco/Net/MailMessage.h>
#include <Poco/Net/MailRecipient.h>
#include <Poco/Net/FilePartSource.h>
#include <Poco/Net/StringPartSource.h>
#include <QFileInfo>

Email::Email(SetupApi &setup) : setup(setup)
{
	moveToThread(&senderThread);
	senderThread.start();
	connect(this, &Email::doSend, this, &Email::internalSend);
}

void Email::send(const QByteArray &from, const QByteArray &to, const QByteArray &subject, const QByteArray &body, const QString &attachmentFN, const QString &attachment2FN)
{
	if (!setup.outgoingMailServer.isEmpty() && !to.isEmpty())
		emit doSend(from, to, subject, body, attachmentFN, attachment2FN);
}

void Email::internalSend(const QByteArray from, const QByteArray to, const QByteArray subject, const QByteArray body, const QString attachmentFN, const QString attachment2FN)
{
	Poco::Net::MailMessage msg;
	msg.addRecipient(Poco::Net::MailRecipient(Poco::Net::MailRecipient::PRIMARY_RECIPIENT, to.constData(), ""));
	msg.setSender(from.constData());
	msg.setSubject(subject.constData());
	msg.addContent(new Poco::Net::StringPartSource(body.constData()));
	QFileInfo attachement(attachmentFN);
	if (!attachement.absoluteFilePath().isEmpty())
		msg.addAttachment(attachement.fileName().toStdString(), new Poco::Net::FilePartSource(attachement.absoluteFilePath().toStdString()));

	QFileInfo attachement2(attachment2FN);
	if (!attachement2.absoluteFilePath().isEmpty())
		msg.addAttachment(attachement2.fileName().toStdString(), new Poco::Net::FilePartSource(attachement2.absoluteFilePath().toStdString()));

	setup.setupLock.lock();
	Poco::Net::SMTPClientSession smtp(setup.outgoingMailServer.toUtf8().constData(), setup.outgoingMailPort);
	setup.setupLock.unlock();
	smtp.login();
	smtp.sendMessage(msg);
	smtp.close();
}
