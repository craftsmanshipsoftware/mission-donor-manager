// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef MDM_EMAIL_SENDER_H
#define MDM_EMAIL_SENDER_H

#include <QThread>

class SetupApi;

class Email : public QObject
{
	Q_OBJECT
public:
	Email(SetupApi &setup);
	void send(const QByteArray &from, const QByteArray &to, const QByteArray &subject, const QByteArray &body, const QString &attachmentFN = "", const QString &attachment2FN = "");

private:
	void internalSend(const QByteArray from, const QByteArray to, const QByteArray subject, const QByteArray body, const QString attachmentFN, const QString attachment2FN);
	SetupApi &setup;
	QThread senderThread;

signals:
	void doSend(const QByteArray from, const QByteArray to, const QByteArray subject, const QByteArray body, const QString attachmentFN, const QString attachment2FN);
};

#endif


