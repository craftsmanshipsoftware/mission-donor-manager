// clang-format off
/**************************************************************************************************
	Copyright (C) 2024  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef MDM_ACTIVITY_JOURNAL_H
#define MDM_ACTIVITY_JOURNAL_H

#include <QObject>
#include <QFile>

class MissionDM;

class ActivityJournal
{
public:
	ActivityJournal(MissionDM &mdm);
	void openPath(const QString &path);
	inline void setActiveUser(size_t id) { userId = id; }
	void log(int id, const QString &string);
	inline void log(const QString &string) { log(userId, string); }
	void logDonation(int source, int type, int thirdParty, int amount, int advantage, int fee, int sa, int donorId, int did, int missionID, int date);
	void logVoiding(bool voided, int donorId, int did);
	void logIifGrouping(int iifId, int sa, int julianDay, int transactionsMarked);
	void logIifAppend(int iifId, int donorId, int donationId);
	void logIifRemoveDonation(int iifId, int donorId, int donationId);
	void logIifUngroup(int iifId);
	void logReceiptGeneration(int year, int totalCashDonations, int totalCashReceipts, int totalInkindReceipts);

private:
	void checkIfYearChanged();
	MissionDM &mdm;
	QString journalPath;
	QFile journalFile;
	int currentYear = 0;
	size_t userId = 0;
};

#endif


