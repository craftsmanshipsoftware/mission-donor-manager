// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef MDM_COMMON_H
#define MDM_COMMON_H

#include <QDoubleValidator>
#include "stdint.h"

constexpr int NumberOfMonthsInMissionView = 14;
constexpr int MaxFirstNameDisplayLengthInMissionView = 20;

enum ServerState {
	NoDBPwd,
	NeedDecryptPwd,
	NeedFirstUser,
	Ready,
};

namespace DonorType {
enum {
	Individual,
	Business,
	GovernmentAgency,
	CanadianCharity,
	ForeignCharity,
	NonTaxPayer,
	CanadianFoundation,
	Total
};
}

namespace DonationVia {
enum {
	Manual,
	Recurring,
	Paypal,
	Total
};
}

enum DonationType {
	Normal,
	ThirdParty,
	Inkind,
	ForeignThirdParty,
};

enum FinancialSpan {
	All,
	Previous30Days,
	Previous90Days,
	Previous120Days,
	Previous365Days,
	YTD,
	PreviousYear,
	Previous3Years,
};

enum IifAppendError {
	InvalidIif = 1,
	DonationNotFound,
	IncomeAccountMissmatch,
	DonationAlreadyGrouped,
};

namespace ProvinceAlphaCode
{
enum {
	All,
	NL,
	PE,
	NS,
	NB,
	QC,
	ON,
	MB,
	SK,
	AB,
	BC,
	YT,
	NT,
	NU,
	Int,
};
}

// clang-format off
enum Mail {
	NewsLetter                = 0x001,
	TaxReceiptByEmail         = 0x004,
	TaxReceiptBySnailMail     = 0x008,
	ThankyouPreAuthByMail     = 0x010,
	ThankyouPreAuthByEmail    = 0x020,
	ThankyouManualByMail      = 0x040,
	ThankyouManualByEmail     = 0x080,
	Thankyou3rdPartyByMail    = 0x100,
	Thankyou3rdPartyByEmail   = 0x200,
	ThankyouPaypalOneTime     = 0x400,
	ThankyouPaypalRecurring   = 0x800,
};
// clang-format on

enum EmailingTaxReceiptState {
	Idle,
	Sending,
	Finished,
};

int convertStringToCents(QString amount);
QString convertCentsToString(double cents, bool includeDollarSign = false);

class AmountValidator : public QDoubleValidator
{
public:
	AmountValidator() : QDoubleValidator(0, 1000000, 2) {}
};

using JulianDayRange = QPair<int, int>;  // .first = from Julian day .second = to Julian day

JulianDayRange convertFinancialSpanToJulianRange(int financialPeriod);

#endif
