// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef MDM_RECEIPT_RENDERER_H_
#define MDM_RECEIPT_RENDERER_H_

#include <QFont>
#include <QImage>
#include <QDate>
#include <QPainter>
#include <QFontDatabase>

class QPaintDevice;

class ReceiptRenderer
{
public:
	void start(QPaintDevice *device);
	void renderPage();
	void end();

	// receipt info
	QString donorName;
	QString donorAddressLine1;
	QString donorApt;
	QString donorCity;
	QString donorProvince;
	QString donorCountry;
	QString donorPostalCode;
	QString name;
	QString issueLocation;
	QString addressLine1;
	QString addressLine2;
	QString addressLine3;
	QString phone;
	QString email;
	QString craNumber;
	QString signatorName;
	QString donationNote;
	QImage signatorSignature;
	QImage banner;
	QDate dateIssued;
	int year;
	int donationDate;
	int receiptId;
	int amount;
	int advantage;
	int type;

private:
	QPainter painter;
	QFontDatabase fdb;
};

#endif
