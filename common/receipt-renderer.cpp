// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QPainter>
#include <QFontDatabase>
#include "common.h"
#include "receipt-renderer.h"

// available Roboto styles on Debian
//   "Medium", "Thin Italic", "Light", "Black", "Bold", "Thin", "Light Italic", "Regular", "Italic", "Bold Italic", "Black Italic",
//   "Medium Italic", "Bold Condensed Italic", "Condensed", "Bold Condensed", "Condensed Italic")

constexpr QColor backgroundBox = QColor(235, 235, 235);

constexpr QSize pageSize = QSize(2400, 3425);
constexpr int FirstReceiptYPos = 0;
constexpr int SecondReceiptYPos = 1130;

constexpr int NameYPos = 60;
constexpr int AddressYPos = 140;
constexpr int craYPos = 340;
constexpr int OfficialBannerMsgYPos = 360;
constexpr int DetailsYPos = 456;
constexpr int MailingYPos = 850;
constexpr int SignatureYPos = 825;
constexpr int SignatorNameYPos = 1000;
constexpr int ReceivedYPos = 637;
constexpr int InKindNoteYPos = 662;
constexpr int FooterYPos = 1028;
constexpr int DividerYPos = 1100;

constexpr int IndentLeft = 32;
constexpr int IndentRight = 1188;
constexpr int IndentAddress = 78;
constexpr int IndentSignator = 1500;
constexpr int StopLeft = 1125;

void ReceiptRenderer::start(QPaintDevice *device)
{
	painter.begin(device);
#if defined(Q_OS_WIN) || defined(Q_OS_MAC)
	fdb.addApplicationFont(":Roboto-Bold.ttf");
	fdb.addApplicationFont(":Roboto-Regular.ttf");
#endif
}

void ReceiptRenderer::renderPage()
{
	painter.drawImage(0, 0 + FirstReceiptYPos, banner);
	if (type != DonationType::Inkind)
		painter.drawImage(0, 0 + SecondReceiptYPos, banner);

	painter.drawImage(IndentSignator, SignatureYPos + FirstReceiptYPos, signatorSignature);
	if (type != DonationType::Inkind)
		painter.drawImage(IndentSignator, SignatureYPos + SecondReceiptYPos, signatorSignature);

	auto drawOfficialBannerMag = [&](int offset) {
		painter.fillRect(0, offset + OfficialBannerMsgYPos, pageSize.width(), 50, backgroundBox);
		painter.setFont(fdb.font("RobotoBold", "", 9));
		painter.drawText(16, offset + OfficialBannerMsgYPos + 41, "OFFICIAL DONATION RECEIPT FOR INCOME TAX PURPOSES");
	};
	drawOfficialBannerMag(FirstReceiptYPos);
	if (type != DonationType::Inkind)
		drawOfficialBannerMag(SecondReceiptYPos);

	auto drawFooter = [&](int offset) {
		painter.fillRect(0, offset + FooterYPos, pageSize.width(), 64, backgroundBox);
		painter.setFont(fdb.font("RobotoRegular", "", 6));
		painter.drawText(16, offset + FooterYPos + 42,
		                 "For information on all registered charities within Canada under the Income Tax Act, please visit: Canada Revenue Agency www.canada.ca/charities-giving");
	};
	drawFooter(FirstReceiptYPos);
	if (type != DonationType::Inkind)
		drawFooter(SecondReceiptYPos);

	auto drawName = [&](int offset) {
		painter.setFont(fdb.font("RobotoRegular", "", 18));
		painter.drawText(IndentRight, offset + NameYPos, name);
	};
	drawName(FirstReceiptYPos);
	if (type != DonationType::Inkind)
		drawName(SecondReceiptYPos);

	auto drawCRANumber = [&](int offset) {
		painter.setFont(fdb.font("RobotoRegular", "", 9));
		QString cra = "Charitable Registraction Number: " + craNumber;
		painter.drawText(IndentRight, offset + craYPos, cra);
	};
	drawCRANumber(FirstReceiptYPos);
	if (type != DonationType::Inkind)
		drawCRANumber(SecondReceiptYPos);

	auto drawContact = [&](int offset) {
		constexpr int advance = 44;
		int advancePos = 0;
		painter.setFont(fdb.font("RobotoRegular", "", 9));
		if (!addressLine1.isEmpty()) {
			painter.drawText(IndentRight, offset + AddressYPos, addressLine1);
			advancePos += advance;
		}
		if (!addressLine2.isEmpty()) {
			painter.drawText(IndentRight, offset + AddressYPos + advancePos, addressLine2);
			advancePos += advance;
		}
		if (!addressLine3.isEmpty()) {
			painter.drawText(IndentRight, offset + AddressYPos + advancePos, addressLine3);
			advancePos += advance;
		}
		if (!phone.isEmpty() || !email.isEmpty()) {
			QString str = phone;
			if (!phone.isEmpty() && !email.isEmpty())
				str += " / ";
			str += email;
			painter.drawText(IndentRight, offset + AddressYPos + advancePos, str);
		}
	};
	drawContact(FirstReceiptYPos);
	if (type != DonationType::Inkind)
		drawContact(SecondReceiptYPos);

	auto drawSignatorName = [&](int offset) {
		painter.setFont(fdb.font("RobotoRegular", "", 7));
		painter.drawText(IndentSignator, offset + SignatorNameYPos, signatorName);
	};
	drawSignatorName(FirstReceiptYPos);
	if (type != DonationType::Inkind)
		drawSignatorName(SecondReceiptYPos);

	auto drawMailing = [&](int offset) {
		int lineOffset = 0;
		painter.setFont(fdb.font("RobotoRegular", "", 9));
		painter.drawText(IndentAddress, offset + MailingYPos + lineOffset, donorName);
		lineOffset += 50;
		painter.drawText(IndentAddress, offset + MailingYPos + lineOffset, donorAddressLine1 + " " + donorApt);
		lineOffset += 50;
		painter.drawText(IndentAddress, offset + MailingYPos + lineOffset, donorCity + ' ' + donorProvince + ' ' + donorPostalCode);
	};
	drawMailing(FirstReceiptYPos);
	if (type != DonationType::Inkind)
		drawMailing(SecondReceiptYPos);

	auto drawDetails = [&](int offset) {
		// labels, left side
		painter.setFont(fdb.font("RobotoBold", "", 9));
		painter.drawText(IndentLeft, offset + DetailsYPos, "Receipt #");
		painter.drawText(IndentLeft, offset + DetailsYPos + 56, "Issue Date:");
		if (donationDate != 0)
			painter.drawText(IndentLeft, offset + DetailsYPos + 112, "Donation Date");
		else
			painter.drawText(IndentLeft, offset + DetailsYPos + 112, "Donation Year");
		painter.drawText(IndentLeft, offset + DetailsYPos + 168, "Location Receipt Issued:");
		painter.drawText(2 * IndentAddress, offset + DetailsYPos + 224, "Received From:");
		// labels, right side
		if (!advantage) {
			painter.drawText(IndentRight, offset + DetailsYPos, "Amount:");
		} else {
			painter.drawText(IndentRight, offset + DetailsYPos, "Total Gift Received:");
			painter.drawText(IndentRight, offset + DetailsYPos + 56, "Advantage:");
			painter.drawText(IndentRight, offset + DetailsYPos + 112, "Amount of Gift Eligible for Tax Purposes:");
		}


		painter.setFont(fdb.font("RobotoRegular", "", 9));
		// donor info, left side
		QString receiptNumber = QString::number(receiptId);
		painter.drawText(StopLeft - painter.fontMetrics().horizontalAdvance(receiptNumber), offset + DetailsYPos, receiptNumber);
		QString dateIssuedString = dateIssued.toString("MMMM d, yyyy");
		painter.drawText(StopLeft - painter.fontMetrics().horizontalAdvance(dateIssuedString), offset + DetailsYPos + 56, dateIssuedString);
		QString yearString;
		if (donationDate != 0)
			yearString = QDate::fromJulianDay(donationDate).toString("MMMM d, yyyy");
		else
			yearString = QString::number(year);
		painter.drawText(StopLeft - painter.fontMetrics().horizontalAdvance(yearString), offset + DetailsYPos + 112, yearString);
		painter.drawText(StopLeft - painter.fontMetrics().horizontalAdvance(issueLocation), offset + DetailsYPos + 168, issueLocation);
		// donor info, right side
		QString amountString = convertCentsToString(amount, true);
		painter.drawText(pageSize.width() - painter.fontMetrics().horizontalAdvance(amountString), offset + DetailsYPos, amountString);
		if (advantage) {
			QString advatnageString = convertCentsToString(advantage, true);
			painter.drawText(pageSize.width() - painter.fontMetrics().horizontalAdvance(amountString), offset + DetailsYPos + 56, advatnageString);
			QString eligibleString = convertCentsToString(amount - advantage, true);
			painter.drawText(pageSize.width() - painter.fontMetrics().horizontalAdvance(amountString), offset + DetailsYPos + 112, eligibleString);
		}
	};
	drawDetails(FirstReceiptYPos);
	if (type != DonationType::Inkind)
		drawDetails(SecondReceiptYPos);

	auto drawLines = [&](int offset) {
		// left side
		painter.drawLine(0, DetailsYPos + 15 + offset, StopLeft, DetailsYPos + 15 + offset);
		painter.drawLine(0, DetailsYPos + 71 + offset, StopLeft, DetailsYPos + 71 + offset);
		painter.drawLine(0, DetailsYPos + 127 + offset, StopLeft, DetailsYPos + 127 + offset);
		painter.drawLine(0, DetailsYPos + 183 + offset, StopLeft, DetailsYPos + 183 + offset);
		// right side
		painter.drawLine(IndentRight, DetailsYPos + 15 + offset, pageSize.width(), DetailsYPos + 15 + offset);
		if (advantage) {
			painter.drawLine(IndentRight, DetailsYPos + 71 + offset, pageSize.width(), DetailsYPos + 71 + offset);
			painter.drawLine(IndentRight, DetailsYPos + 127 + offset, pageSize.width(), DetailsYPos + 127 + offset);
		}
	};

	painter.save();
	QPen pen = painter.pen();
	pen.setColor(QColor(127, 127, 127));
	painter.setPen(pen);
	drawLines(FirstReceiptYPos);
	if (type != DonationType::Inkind)
		drawLines(SecondReceiptYPos);
	painter.drawLine(0, DividerYPos, pageSize.width(), DividerYPos);
	painter.restore();

	if (type == DonationType::Inkind) {
		painter.setFont(fdb.font("RobotoBold", "", 8));
		painter.drawText(IndentRight, ReceivedYPos, "Property received by charity:");
		painter.setFont(fdb.font("RobotoRegular", "", 8));
		QRect rect = QRect(IndentRight, InKindNoteYPos, pageSize.width() - IndentRight, 300);
		painter.drawText(rect, Qt::AlignLeft | Qt::AlignTop | Qt::TextWordWrap, donationNote);
	}
}

void ReceiptRenderer::end()
{
	painter.end();
}
