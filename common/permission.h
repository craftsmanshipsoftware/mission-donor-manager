// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#ifndef MDM_PERMISSION_H
#define MDM_PERMISSION_H

#include "stdint.h"

class Permission
{
public:
	enum {
		// clang-format off
		None                       = 0x0000'0000,
		UserRead                   = 0x0000'0001,
		UserModify                 = 0x0000'0002,
		UserPasswordReset          = 0x0000'0004,
		DonorRead                  = 0x0000'0008,
		DonorModify                = 0x0000'0010,
		MissionRead                = 0x0000'0020,
		MissionModify              = 0x0000'0040,
		Donations                  = 0x0000'0080,
		RecurringDonations         = 0x0000'0100,
		IifExport                  = 0x0000'0200,
		DonorExport                = 0x0000'0400,
		Setup                      = 0x0000'0800,
		MissionView                = 0x0000'1000,
		MissionViewAll             = 0x0000'2000,
		Tax                        = 0x0000'4000,
		Thankyou                   = 0x0000'8000,
		Campaigns                  = 0x0001'0000,
		Reports                    = 0x0002'0000,
		UserForceLogout            = 0x0004'0000,
		Admin                      = 0xFFFF'FFFF,
		// clang-format on
	};
	Permission(uint32_t p = 0) { value = p; }
	inline bool hasPermission(uint32_t check) { return value & check; }
	operator unsigned int() { return value; }

private:
	uint32_t value;
};

#endif
