// clang-format off
/**************************************************************************************************
	Copyright (C) 2023  Craftsmanship Software Inc.
	This file is part of Mission Donor Manager

	Mission Donor Manager is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3
	of the License, or (at your option) any later version.

	Mission Donor Manager is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Mission Donor Manager; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**************************************************************************************************/
// clang-format on

#include <QString>
#include <QDate>
#include "common.h"

int convertStringToCents(QString amount)
{
	if (amount.contains('.')) {  // convert to cents
		QChar zeros[2] = { '0', '0' };
		amount.append(zeros, (amount.indexOf('.') + 3) - amount.length());
		amount.remove('.');
	} else {
		amount.append("00");
	}

	return amount.toUInt();
}

QString convertCentsToString(double cents, bool includeDollarSign)
{
	const char *base;
	if (includeDollarSign)
		base = "$%L1";
	else
		base = "%L1";
	return QString(base).arg(cents / 100, 0, 'f', 2);
}

JulianDayRange convertFinancialSpanToJulianRange(int financialPeriod)
{
	JulianDayRange range;
	if (financialPeriod == FinancialSpan::Previous30Days) {
		range.first = QDate::currentDate().addDays(-30).toJulianDay();
		range.second = QDate::currentDate().toJulianDay();
	} else if (financialPeriod == FinancialSpan::Previous90Days) {
		range.first = QDate::currentDate().addDays(-90).toJulianDay();
		range.second = QDate::currentDate().toJulianDay();
	} else if (financialPeriod == FinancialSpan::Previous120Days) {
		range.first = QDate::currentDate().addDays(-120).toJulianDay();
		range.second = QDate::currentDate().toJulianDay();
	} else if (financialPeriod == FinancialSpan::Previous365Days) {
		range.first = QDate::currentDate().addDays(-365).toJulianDay();
		range.second = QDate::currentDate().toJulianDay();
	} else if (financialPeriod == FinancialSpan::YTD) {
		QDate fromDate(QDate::currentDate().year(), 1, 1);  // Jan 1st of current year
		range.first = fromDate.toJulianDay();
		range.second = QDate::currentDate().toJulianDay();
	} else if (financialPeriod == FinancialSpan::PreviousYear) {
		QDate date(QDate::currentDate().year() - 1, 1, 1);  // Jan 1st of previous year
		range.first = date.toJulianDay();
		date.setDate(QDate::currentDate().year() - 1, 12, 31);  // Dec 31st of previous year
		range.second = date.toJulianDay();
	} else if (financialPeriod == FinancialSpan::Previous3Years) {
		QDate date(QDate::currentDate().year() - 3, 1, 1);  // Jan 1st of previous year
		range.first = date.toJulianDay();
		range.second = QDate::currentDate().toJulianDay();
	} else if (financialPeriod == FinancialSpan::All) {
		range.first = 1;
		range.second = QDate::currentDate().toJulianDay();
	}
	return range;
}
